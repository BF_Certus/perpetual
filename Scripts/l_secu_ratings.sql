CREATE TABLE DV.l_SECU_ratings (
  hk_l_SECU_ratings binary(20) NOT NULL 
, hk_h_client_master binary(20) NOT NULL, hk_h_rating_agency binary(20) NOT NULL 
, hk_h_reporting_period binary(20) NOT NULL, hk_h_security binary(20) NOT NULL 
, hk_h_transaction binary(20) NOT NULL, dss_record_source varchar(255) 
, dss_load_datetime datetime2, dss_create_time datetime2);
CREATE UNIQUE NONCLUSTERED INDEX l_SECU_ratings_idx_0 ON DV.l_SECU_ratings (hk_l_SECU_ratings) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_SECU_ratings_idx_1 ON DV.l_SECU_ratings (hk_h_client_master) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_SECU_ratings_idx_2 ON DV.l_SECU_ratings (hk_h_rating_agency) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_SECU_ratings_idx_3 ON DV.l_SECU_ratings (hk_h_reporting_period) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_SECU_ratings_idx_4 ON DV.l_SECU_ratings (hk_h_security) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_SECU_ratings_idx_5 ON DV.l_SECU_ratings (hk_h_transaction) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_SECU_ra_6a025008_idx_1 ON DV.l_SECU_ratings (hk_h_client_master) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_SECU_ra_6a025008_idx_2 ON DV.l_SECU_ratings (hk_h_rating_agency) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_SECU_ra_6a025008_idx_3 ON DV.l_SECU_ratings (hk_h_reporting_period) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_SECU_ra_6a025008_idx_4 ON DV.l_SECU_ratings (hk_h_security) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_SECU_ra_6a025008_idx_5 ON DV.l_SECU_ratings (hk_h_transaction) WITH (SORT_IN_TEMPDB = OFF);
ALTER TABLE DV.l_SECU_ratings ADD CONSTRAINT l_SECU_ra_6a025008_idx_PK PRIMARY KEY NONCLUSTERED (hk_l_SECU_ratings) WITH (SORT_IN_TEMPDB = OFF);
CREATE UNIQUE NONCLUSTERED INDEX l_SECU_ratings_idx_A ON DV.l_SECU_ratings (hk_h_client_master,hk_h_rating_agency,hk_h_reporting_period,hk_h_security,hk_h_transaction) WITH (SORT_IN_TEMPDB = OFF);
