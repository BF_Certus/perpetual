/****** Object:  View [INGEST].[TRAN_LMI]    Script Date: 4/07/2018 11:01:25 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--Drop view [INGEST].[TRAN_LMI]
--go
Create View [INGEST].[TRAN_LMI] as 
 
  WITH Nbrs ( Number ) AS (
    SELECT 1 UNION ALL
    SELECT 1 + Number FROM Nbrs WHERE Number < 99
)
SELECT h_cm.ClientID, h_fm.FeedID, h_rp.Reporting_Period, h_t.Master_Trust_Name_or_SPV, h_t.Series_Trust_Name_or_Series, h_t.Transaction_ID,
DelimCount as LMI_Count, N.Number as LMI_Position,
Case when DelimCount = 0 then LMI_Provider_Name else SubString(  LMI_Provider_Name , dbo.FN_CERTUS_INSTR(  LMI_Provider_Name ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(LMI_Provider_Name,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  LMI_Provider_Name ,';',1,N.Number-1)-1) end as LMI_Provider_Name,
Case when DelimCount = 0 then LMI_Provider_ABN else SubString(  LMI_Provider_ABN , dbo.FN_CERTUS_INSTR(  LMI_Provider_ABN ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(LMI_Provider_ABN,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  LMI_Provider_ABN ,';',1,N.Number-1)-1) end as LMI_Provider_ABN,
Case when DelimCount = 0 then Loans_Insured else SubString(  Loans_Insured , dbo.FN_CERTUS_INSTR(  Loans_Insured ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Loans_Insured,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Loans_Insured ,';',1,N.Number-1)-1) end as Loans_Insured,
Case when DelimCount = 0 then Timely_Payment_Claims_Outstanding else SubString(  Timely_Payment_Claims_Outstanding , dbo.FN_CERTUS_INSTR(  Timely_Payment_Claims_Outstanding ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Timely_Payment_Claims_Outstanding,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Timely_Payment_Claims_Outstanding ,';',1,N.Number-1)-1) end as Timely_Payment_Claims_Outstanding,
Case when DelimCount = 0 then LMI_Provider_Jurisdiction else SubString(  LMI_Provider_Jurisdiction , dbo.FN_CERTUS_INSTR(  LMI_Provider_Jurisdiction ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(LMI_Provider_Jurisdiction,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  LMI_Provider_Jurisdiction ,';',1,N.Number-1)-1) end as LMI_Provider_Jurisdiction,
Case when DelimCount = 0 then LMI_Provider_ID else SubString(  LMI_Provider_ID , dbo.FN_CERTUS_INSTR(  LMI_Provider_ID ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(LMI_Provider_ID,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  LMI_Provider_ID ,';',1,N.Number-1)-1) end as LMI_Provider_ID,
Case when DelimCount = 0 then LMI_Provider_ID_Descriptor else SubString(  LMI_Provider_ID_Descriptor , dbo.FN_CERTUS_INSTR(  LMI_Provider_ID_Descriptor ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(LMI_Provider_ID_Descriptor,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  LMI_Provider_ID_Descriptor ,';',1,N.Number-1)-1) end as LMI_Provider_ID_Descriptor,
Case when DelimCount = 0 then Loan_Currency_Insurance else SubString(  Loan_Currency_Insurance , dbo.FN_CERTUS_INSTR(  Loan_Currency_Insurance ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Loan_Currency_Insurance,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Loan_Currency_Insurance ,';',1,N.Number-1)-1) end as Loan_Currency_Insurance
from 
	DV.S_TRAN_v00 s
	inner join 
	DV.L_TRAN_v00 l on s.hk_l_TRAN_v00=l.hk_l_TRAN_v00
	inner join
	DV.h_client_master h_cm on h_cm.hk_h_client_master=l.hk_h_client_master
	inner join
	DV.h_feed_master h_fm on h_fm.hk_h_feed_master=l.hk_h_feed_master
	inner join
	DV.h_reporting_period h_rp on h_rp.hk_h_reporting_period=l.hk_h_reporting_period
	inner join
	DV.h_transaction h_t on h_t.hk_h_transaction=l.hk_h_transaction
	inner join
	(select TRAN_v00_DelimiterCount.hk_l_TRAN_v00, case 
		when Delim_LMI_Provider_Name=Delim_LMI_Provider_ABN
		and Delim_LMI_Provider_Name=Delim_Loans_Insured
		and Delim_LMI_Provider_Name=Delim_Timely_Payment_Claims_Outstanding
		and Delim_LMI_Provider_Name=Delim_LMI_Provider_Jurisdiction
		and Delim_LMI_Provider_Name=Delim_LMI_Provider_ID
		and Delim_LMI_Provider_Name=Delim_LMI_Provider_ID_Descriptor
		and Delim_LMI_Provider_Name=Delim_Loan_Currency_Insurance
		then Delim_LMI_Provider_Name+1 end as DelimCount from INGEST.TRAN_v00_DelimiterCount) delim on delim.hk_l_TRAN_v00=l.hk_l_TRAN_v00
	inner join
	Nbrs N ON (N.Number <= DelimCount) or (DelimCount = 0 and N.Number =1)
GO


