/****** Object:  View [INGEST].[RMBS_v00_DataConversion]    Script Date: 22/06/2018 4:51:16 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create view [INGEST].[RMBS_v00_DataConversion] as 
Select hk_l_RMBS_V00

,COALESCE(Try_Convert([date], Collateral_Date,105), Try_Convert([date], Collateral_Date, 121)) as Collateral_Date 









, Try_Convert([decimal](24,9), Replace(Most_Recent_Approval_Amount,',','')) as Most_Recent_Approval_Amount 
, Try_Convert([decimal](24,9), Replace(Current_Balance,',','')) as Current_Balance 
, Try_Convert([decimal](24,9), Replace(Scheduled_Balance,',','')) as Scheduled_Balance 
, Try_Convert(bit,  Case when Replace(Loan_Guarantee_Flag,',','') = 'True' THEN 1 WHEN Replace(Loan_Guarantee_Flag,',','') = 'False' THEN 0 ELSE Replace(Loan_Guarantee_Flag,',','') END ) as Loan_Guarantee_Flag 
, Try_Convert(bit,  Case when Replace(Redraw_Feature_Flag,',','') = 'True' THEN 1 WHEN Replace(Redraw_Feature_Flag,',','') = 'False' THEN 0 ELSE Replace(Redraw_Feature_Flag,',','') END ) as Redraw_Feature_Flag 
, Try_Convert([decimal](24,9), Replace(Available_Redraw,',','')) as Available_Redraw 
, Try_Convert(bit,  Case when Replace(Offset_Account_Flag,',','') = 'True' THEN 1 WHEN Replace(Offset_Account_Flag,',','') = 'False' THEN 0 ELSE Replace(Offset_Account_Flag,',','') END ) as Offset_Account_Flag 
, Try_Convert([decimal](24,9), Replace(Offset_Account_Balance,',','')) as Offset_Account_Balance 

, Try_Convert(bit,  Case when Replace(Conforming_Mortgage,',','') = 'True' THEN 1 WHEN Replace(Conforming_Mortgage,',','') = 'False' THEN 0 ELSE Replace(Conforming_Mortgage,',','') END ) as Conforming_Mortgage 

,COALESCE(Try_Convert([date], Origination_Date_Settlement_Date,105), Try_Convert([date], Origination_Date_Settlement_Date, 121)) as Origination_Date_Settlement_Date 


, Try_Convert([decimal](24,9), Replace(Loan_Term,',','')) as Loan_Term 
, Try_Convert([decimal](24,9), Replace(Remaining_Term,',','')) as Remaining_Term 
, Try_Convert([decimal](24,9), Replace(Seasoning,',','')) as Seasoning 
,COALESCE(Try_Convert([date], Maturity_Date,105), Try_Convert([date], Maturity_Date, 121)) as Maturity_Date 
,COALESCE(Try_Convert([date], Loan_Securitised_Date,105), Try_Convert([date], Loan_Securitised_Date, 121)) as Loan_Securitised_Date 
 , Case when Try_Convert([decimal](24,9), Replace(Original_LTV,',','')) >1 THEN Try_Convert([decimal](24,9), Replace(Original_LTV,',',''))/100 ELSE Try_Convert([decimal](24,9), Replace(Original_LTV,',','')) END as Original_LTV 
 , Case when Try_Convert([decimal](24,9), Replace(Current_LTV,',','')) >1 THEN Try_Convert([decimal](24,9), Replace(Current_LTV,',',''))/100 ELSE Try_Convert([decimal](24,9), Replace(Current_LTV,',','')) END as Current_LTV 
 , Case when Try_Convert([decimal](24,9), Replace(Scheduled_LTV,',','')) >1 THEN Try_Convert([decimal](24,9), Replace(Scheduled_LTV,',',''))/100 ELSE Try_Convert([decimal](24,9), Replace(Scheduled_LTV,',','')) END as Scheduled_LTV 


, Try_Convert([decimal](24,9), Replace(Scheduled_Minimum_Payment,',','')) as Scheduled_Minimum_Payment 


 , Case when Try_Convert([decimal](24,9), Replace(LMI_Attachment_Point,',','')) >1 THEN Try_Convert([decimal](24,9), Replace(LMI_Attachment_Point,',',''))/100 ELSE Try_Convert([decimal](24,9), Replace(LMI_Attachment_Point,',','')) END as LMI_Attachment_Point 

, Try_Convert([decimal](24,9), Replace(Variable_Loan_Amount,',','')) as Variable_Loan_Amount 

 , Case when Try_Convert([decimal](24,9), Replace(Current_Interest_Rate,',','')) >1 THEN Try_Convert([decimal](24,9), Replace(Current_Interest_Rate,',',''))/100 ELSE Try_Convert([decimal](24,9), Replace(Current_Interest_Rate,',','')) END as Current_Interest_Rate 
 , Case when Try_Convert([decimal](24,9), Replace(Reference_Index_Value,',','')) >1 THEN Try_Convert([decimal](24,9), Replace(Reference_Index_Value,',',''))/100 ELSE Try_Convert([decimal](24,9), Replace(Reference_Index_Value,',','')) END as Reference_Index_Value 
 , Case when Try_Convert([decimal](24,9), Replace(Interest_Margin,',','')) >1 THEN Try_Convert([decimal](24,9), Replace(Interest_Margin,',',''))/100 ELSE Try_Convert([decimal](24,9), Replace(Interest_Margin,',','')) END as Interest_Margin 

, Try_Convert(bit,  Case when Replace(Restructuring_Arrangement_Flag,',','') = 'True' THEN 1 WHEN Replace(Restructuring_Arrangement_Flag,',','') = 'False' THEN 0 ELSE Replace(Restructuring_Arrangement_Flag,',','') END ) as Restructuring_Arrangement_Flag 
,COALESCE(Try_Convert([date], Interest_Only_Expiry_Date,105), Try_Convert([date], Interest_Only_Expiry_Date, 121)) as Interest_Only_Expiry_Date 
,COALESCE(Try_Convert([date], Current_Rate_Expiry_Date,105), Try_Convert([date], Current_Rate_Expiry_Date, 121)) as Current_Rate_Expiry_Date 

, Try_Convert([decimal](24,9), Replace(Default_Balance,',','')) as Default_Balance 
, Try_Convert([decimal](24,9), Replace(Foreclosure_Proceeds,',','')) as Foreclosure_Proceeds 
,COALESCE(Try_Convert([date], Date_of_Foreclosure,105), Try_Convert([date], Date_of_Foreclosure, 121)) as Date_of_Foreclosure 
, Try_Convert([decimal](24,9), Replace(Loss_on_Sale,',','')) as Loss_on_Sale 
, Try_Convert([int], Replace(Days_In_Arrears,',','')) as Days_In_Arrears 
, Try_Convert([decimal](24,9), Replace(Amount_in_Arrears,',','')) as Amount_in_Arrears 
, Try_Convert([int], Replace(Days_Since_Last_Scheduled_Payment,',','')) as Days_Since_Last_Scheduled_Payment 
, Try_Convert([decimal](24,9), Replace(Cumulative_Missed_Payments,',','')) as Cumulative_Missed_Payments 
, Try_Convert([decimal](24,9), Replace(Claim_Submitted_to_LMI,',','')) as Claim_Submitted_to_LMI 
, Try_Convert([decimal](24,9), Replace(Claim_Paid_by_LMI,',','')) as Claim_Paid_by_LMI 
, Try_Convert([decimal](24,9), Replace(Claim_Denied_by_LMI,',','')) as Claim_Denied_by_LMI 
, Try_Convert([decimal](24,9), Replace(Claim_Pending_with_LMI,',','')) as Claim_Pending_with_LMI 


, Try_Convert([decimal](24,9), Replace(Original_Property_Value,',','')) as Original_Property_Value 
, Try_Convert([decimal](24,9), Replace(Most_Recent_Property_Value,',','')) as Most_Recent_Property_Value 


,COALESCE(Try_Convert([date], Most_Recent_Property_Valuation_Date,105), Try_Convert([date], Most_Recent_Property_Valuation_Date, 121)) as Most_Recent_Property_Valuation_Date 




, Try_Convert(bit,  Case when Replace(First_Home_Buyer_Flag,',','') = 'True' THEN 1 WHEN Replace(First_Home_Buyer_Flag,',','') = 'False' THEN 0 ELSE Replace(First_Home_Buyer_Flag,',','') END ) as First_Home_Buyer_Flag 



, Try_Convert([int], Replace(Employment_Type_Secondary_Borrower,',','')) as Employment_Type_Secondary_Borrower 
, Try_Convert([int], Replace(Borrower_type_Primary_Borrower,',','')) as Borrower_type_Primary_Borrower 
, Try_Convert([int], Replace(Borrower_type_Secondary_Borrower,',','')) as Borrower_type_Secondary_Borrower 

, Try_Convert([int], Replace(Legal_Entity_Type_Secondary_Borrower,',','')) as Legal_Entity_Type_Secondary_Borrower 
, Try_Convert(bit,  Case when Replace(Bankruptcy_Flag,',','') = 'True' THEN 1 WHEN Replace(Bankruptcy_Flag,',','') = 'False' THEN 0 ELSE Replace(Bankruptcy_Flag,',','') END ) as Bankruptcy_Flag 
,COALESCE(Try_Convert([date], Last_Credit_Discharge_Date,105), Try_Convert([date], Last_Credit_Discharge_Date, 121)) as Last_Credit_Discharge_Date 
, Try_Convert([int], Replace(Number_of_Debtors,',','')) as Number_of_Debtors 


, Try_Convert([int], Replace(Credit_Score_Secondary_Borrower,',','')) as Credit_Score_Secondary_Borrower 

, Try_Convert([decimal](24,9), Replace(Debt_Serviceability_Metric_Score,',','')) as Debt_Serviceability_Metric_Score 
, Try_Convert([decimal](24,9), Replace(Income,',','')) as Income 

, Try_Convert([decimal](24,9), Replace(Secondary_Income,',','')) as Secondary_Income 
, Try_Convert([int], Replace(Income_Verification_for_Secondary_Income,',','')) as Income_Verification_for_Secondary_Income 
, Try_Convert([int], Replace(ABS_Statistical_Area,',','')) as ABS_Statistical_Area 











 , Case when Try_Convert([decimal](24,9), Replace(Interest_Rate,',','')) >1 THEN Try_Convert([decimal](24,9), Replace(Interest_Rate,',',''))/100 ELSE Try_Convert([decimal](24,9), Replace(Interest_Rate,',','')) END as Interest_Rate 
,COALESCE(Try_Convert([date], Origination_Date,105), Try_Convert([date], Origination_Date, 121)) as Origination_Date 
FROM DV.s_rmbs_v00
		where s_rmbs_v00.dss_load_datetime = (Select max(dss_load_datetime) from dv.s_rmbs_v00 z where z.hk_l_rmbs_v00 = s_rmbs_v00.hk_l_rmbs_v00)
;
GO


