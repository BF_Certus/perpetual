CREATE TABLE DV.bvs_TRAN_servicer (
  hk_l_TRAN_servicer binary(20) NOT NULL, dss_load_datetime datetime 
, dss_change_hash binary(20) NOT NULL, dss_record_source varchar(255) 
, dss_start_date datetime, dss_version int, dss_create_time datetime 
, feedid varchar(100), servicer_count int, servicer_position int 
, servicer_abn varchar(5000), servicer_jurisdiction varchar(5000) 
, servicing_fee_currency varchar(5000), servicing_fee varchar(5000) 
, servicing_role varchar(5000), servicer_id varchar(5000) 
, servicer_id_descriptor varchar(5000));