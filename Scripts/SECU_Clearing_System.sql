/****** Object:  View [INGEST].[SECU_Clearing_System]    Script Date: 2/07/2018 3:13:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--Drop view [INGEST].[SECU_Clearing_System]
Create View [INGEST].[SECU_Clearing_System] as 
 
  WITH Nbrs ( Number ) AS (
    SELECT 1 UNION ALL
    SELECT 1 + Number FROM Nbrs WHERE Number < 99
)
SELECT 
ClientID,

   T.Reporting_Period,
T.Report_Date,
T.Transaction_Id,
T.Master_Trust_Name_Or_Spv,
T.Series_Trust_Name_Or_Series,
T.Tranche_Name,
Case when N.Number = 0 then  Clearing_System_Name   when dbo.FN_CERTUS_INSTR(  Clearing_System_Name ,';',1,N.Number) = 0  then right(  Clearing_System_Name , Len(  Clearing_System_Name )-dbo.FN_CERTUS_INSTR(  Clearing_System_Name ,';',1,N.Number-1)) else SubString(  Clearing_System_Name , dbo.FN_CERTUS_INSTR(  Clearing_System_Name ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR(  Clearing_System_Name ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Clearing_System_Name ,';',1,N.Number-1)-1)end as   Clearing_System_Name
,Feed_Name
FROM
   (  Select dbo.FN_CERTUS_DELIM_COUNT(
		Clearing_System_Name,';') as DelimCount, 
		Clearing_System_Name, Reporting_Period,
Report_Date,
ClientID,
Transaction_Id,
Master_Trust_Name_Or_Spv,
Series_Trust_Name_Or_Series,
Tranche_Name,
Feed_Name
FROM INGEST.SECU_v00) T
   JOIN
   Nbrs N ON (N.Number <= T.DelimCount) or (T.DelimCount = 0 and N.Number =1)
GO


