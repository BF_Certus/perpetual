CREATE TABLE DV.bvs_TRAN_account (
  hk_l_TRAN_account binary(20) NOT NULL, dss_load_datetime datetime 
, dss_change_hash binary(20) NOT NULL, dss_record_source varchar(255) 
, dss_start_date datetime, dss_version int, dss_create_time datetime 
, feedid varchar(100), account_count int, account_position int 
, ending_balance varchar(5000), target_balance varchar(5000) 
, account_provider_abn varchar(5000), account_provider_id varchar(5000) 
, account_provider_id_descriptor varchar(5000), account_currency varchar(5000) 
, account_fee varchar(5000), beginning_balance varchar(5000) 
, account_type varchar(5000), account_name varchar(5000));