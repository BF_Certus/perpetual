/****** Object:  View [INGEST].[RMBS_COL_v00]    Script Date: 2/07/2018 3:30:19 PM ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

Create View [INGEST].[RMBS_COL_v00] as 
SELECT ClientID, S.Feed_Name, Convert([nvarchar](100),PRC000) as Reporting_Period
,Convert([nvarchar](255),PRC001) as Collateral_Date
,Convert([nvarchar](255),PRC002) as Transaction_Id
,Convert([nvarchar](255), PRC003) as Master_Trust_Name_Or_Spv
,Convert([nvarchar](255), PRC004) as Series_Trust_Name_Or_Series
,Convert([nvarchar](255), PRC005) as Group_Loan_Id
,Convert([nvarchar](255), PRC006) as Loan_Id
,Convert([nvarchar](255), PRC007) as Collateral_Id
,Convert([nvarchar](255),PRC008) as Security_Property_Postcode
,Convert([nvarchar](255), PRC009) as Security_Property_Country
,Convert([nvarchar](255),PRC010) as Original_Security_Property_Value
,Convert([nvarchar](255),PRC011) as Most_Recent_Security_Property_Value
,Convert([nvarchar](255), PRC012) as Original_Property_Valuation_Type
,Convert([nvarchar](255), PRC013) as Most_Recent_Property_Valuation_Type
,Convert([nvarchar](255),PRC014) as Most_Recent_Security_Property_Valuation_Date
,Convert([nvarchar](255), PRC015) as Security_Property_Purpose
,Convert([nvarchar](255), PRC016) as Security_Property_Type
,Convert([nvarchar](255), PRC017) as Lien
,Convert([nvarchar](255),PRC018) as Abs_Statistical_Area
,Convert([nvarchar](255), PRC019) as Main_Security_Methodology
,Convert([nvarchar](255),PRC020) as Main_Security_Flag
,Convert([nvarchar](255), PRC021) as Property_Valuation_Currency FROM INGEST.Ext_Stage_RMBS_COL S inner join  
  Ingest.Feed_Master FM on S.FeedID = FM.FeedID;
GO


