CREATE TABLE DV.bvs_rmbs_trn_paying_agent (
  hk_l_rmbs_trn_paying_agent binary(20) NOT NULL, dss_load_datetime datetime 
, dss_change_hash binary(20) NOT NULL, dss_record_source varchar(255) 
, dss_start_date datetime, dss_version int, dss_create_time datetime 
, paying_agent_count int, paying_agent_position int 
, paying_agent_abn varchar(5000), paying_agent_jurisdiction varchar(5000) 
, paying_agent_id varchar(5000), paying_agent_id_descriptor varchar(5000) 
, paying_agent_fee_currency varchar(5000), paying_agent_fee varchar(5000));