truncate table LOAD.load_TRAN_account;
INSERT INTO LOAD.load_TRAN_account
(clientid, reporting_period, master_trust_name_or_spv, series_trust_name_or_series,
transaction_id, account_provider_name, account_provider_jurisdiction, feedid,
account_count, account_position, ending_balance, target_balance, account_provider_abn,
account_provider_id, account_provider_id_descriptor, account_currency,
account_fee, beginning_balance, account_type, account_name, dss_record_source,
dss_load_datetime)
SELECT
TRAN_Account.ClientID,TRAN_Account.Reporting_Period,TRAN_Account.Master_Trust_Name_or_SPV,
TRAN_Account.Series_Trust_Name_or_Series,TRAN_Account.Transaction_ID,
TRAN_Account.Account_Provider_Name,TRAN_Account.Account_Provider_Jurisdiction,
TRAN_Account.FeedID,TRAN_Account.Account_Count,TRAN_Account.Account_Position,
TRAN_Account.Ending_Balance,TRAN_Account.Target_Balance,TRAN_Account.Account_Provider_ABN,
TRAN_Account.Account_Provider_ID,TRAN_Account.Account_Provider_ID_Descriptor,
TRAN_Account.Account_Currency,TRAN_Account.Account_Fee,TRAN_Account.Beginning_Balance,
TRAN_Account.Account_Type,TRAN_Account.Account_Name,NULL,NULL
From INGEST.TRAN_Account TRAN_Account
;

;
UPDATE LOAD.load_TRAN_account SET dss_record_source = 'Perpetual.INGEST.TRAN_Account',dss_load_datetime = GetDate() ;

