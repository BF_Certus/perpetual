CREATE TABLE DV.h_asset_class_master (
  hk_h_asset_class_master binary(20) NOT NULL, dss_record_source varchar(255) 
, dss_load_datetime datetime2, dss_create_time datetime2 
, asset_class_id varchar(100) NOT NULL);
CREATE UNIQUE NONCLUSTERED INDEX h_asset_class_master_idx_0 ON DV.h_asset_class_master (hk_h_asset_class_master) WITH (SORT_IN_TEMPDB = OFF);
CREATE UNIQUE NONCLUSTERED INDEX h_asset_class_master_idx_A ON DV.h_asset_class_master (asset_class_id) WITH (SORT_IN_TEMPDB = OFF);
ALTER TABLE DV.h_asset_class_master ADD CONSTRAINT h_asset_class_c6a9892f_idx_PK PRIMARY KEY NONCLUSTERED (hk_h_asset_class_master) WITH (SORT_IN_TEMPDB = OFF);
CREATE UNIQUE NONCLUSTERED INDEX h_asset_class_c6a9892f_idx_A ON DV.h_asset_class_master (asset_class_id) WITH (SORT_IN_TEMPDB = OFF);
