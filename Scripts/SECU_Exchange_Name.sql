/****** Object:  View [INGEST].[SECU_Exchange_Name]    Script Date: 2/07/2018 3:13:44 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




--Drop view [INGEST].[SECU_Exchange_Name]
Create View [INGEST].[SECU_Exchange_Name] as 
 
  WITH Nbrs ( Number ) AS (
    SELECT 1 UNION ALL
    SELECT 1 + Number FROM Nbrs WHERE Number < 99
)
SELECT 
ClientID,
   T.Reporting_Period,
T.Report_Date,

T.Transaction_Id,
T.Master_Trust_Name_Or_Spv,
T.Series_Trust_Name_Or_Series,
T.Tranche_Name,
Case when N.Number = 0 then  Exchange_Name   when dbo.FN_CERTUS_INSTR(  Exchange_Name ,';',1,N.Number) = 0  then right(  Exchange_Name , Len(  Exchange_Name )-dbo.FN_CERTUS_INSTR(  Exchange_Name ,';',1,N.Number-1)) else SubString(  Exchange_Name , dbo.FN_CERTUS_INSTR(  Exchange_Name ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR(  Exchange_Name ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Exchange_Name ,';',1,N.Number-1)-1)end as   Exchange_Name,
Feed_Name
FROM
   (  Select dbo.FN_CERTUS_DELIM_COUNT(
		Exchange_Name,';') as DelimCount, 
		Exchange_Name, Reporting_Period,
Report_Date,
ClientID,
Transaction_Id,
Master_Trust_Name_Or_Spv,
Series_Trust_Name_Or_Series,
Tranche_Name,
Feed_Name
FROM INGEST.SECU_v00) T
   JOIN
   Nbrs N ON (N.Number <= T.DelimCount) or (T.DelimCount = 0 and N.Number =1)
GO


