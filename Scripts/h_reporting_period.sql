CREATE TABLE DV.h_reporting_period (
  hk_h_reporting_period binary(20) NOT NULL, dss_record_source varchar(255) 
, dss_load_datetime datetime2, dss_create_time datetime2 
, clientid varchar(100) NOT NULL, reporting_period varchar(100));
CREATE UNIQUE NONCLUSTERED INDEX h_reporting_period_idx_A ON DV.h_reporting_period (clientid,reporting_period) WITH (SORT_IN_TEMPDB = OFF);
CREATE UNIQUE NONCLUSTERED INDEX h_reporting_p_eadb3be9_idx_A ON DV.h_reporting_period (clientid,reporting_period) WITH (SORT_IN_TEMPDB = OFF);
CREATE UNIQUE NONCLUSTERED INDEX h_reporting_period_idx_0 ON DV.h_reporting_period (hk_h_reporting_period) WITH (SORT_IN_TEMPDB = OFF);
