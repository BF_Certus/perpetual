CREATE TABLE DV.h_trigger (
  hk_h_trigger binary(20) NOT NULL, dss_record_source varchar(255) 
, dss_load_datetime datetime2, dss_create_time datetime2 
, trigger_name varchar(100));
CREATE UNIQUE NONCLUSTERED INDEX h_trigger_idx_0 ON DV.h_trigger (hk_h_trigger) WITH (SORT_IN_TEMPDB = OFF);
CREATE UNIQUE NONCLUSTERED INDEX h_trigger_idx_A ON DV.h_trigger (trigger_name) WITH (SORT_IN_TEMPDB = OFF);
