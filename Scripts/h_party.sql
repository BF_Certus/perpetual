CREATE TABLE DV.h_party (
  hk_h_party binary(20) NOT NULL, dss_record_source varchar(255) 
, dss_load_datetime datetime2, dss_create_time datetime2 
, clientid varchar(100) NOT NULL, party_name varchar(100));
CREATE UNIQUE NONCLUSTERED INDEX h_party_25fc904f_idx_A ON DV.h_party (clientid,party_name) WITH (SORT_IN_TEMPDB = OFF);
CREATE UNIQUE NONCLUSTERED INDEX h_party_idx_0 ON DV.h_party (hk_h_party) WITH (SORT_IN_TEMPDB = OFF);
CREATE UNIQUE NONCLUSTERED INDEX h_party_idx_A ON DV.h_party (clientid,party_name) WITH (SORT_IN_TEMPDB = OFF);
ALTER TABLE DV.h_party ADD CONSTRAINT h_party_25fc904f_idx_PK PRIMARY KEY NONCLUSTERED (hk_h_party) WITH (SORT_IN_TEMPDB = OFF);
