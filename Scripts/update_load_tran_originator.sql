truncate table LOAD.load_TRAN_originator;
INSERT INTO LOAD.load_TRAN_originator
(clientid, feedid, reporting_period, master_trust_name_or_spv, series_trust_name_or_series,
transaction_id, originator_name, originator_count, originator_position,
originator_abn, originator_jurisdiction, originator_id, originator_id_descriptor,
loans_originated_settlement_date, loans_originated, dss_record_source, dss_load_datetime)
SELECT
TRAN_Originator.ClientID,TRAN_Originator.FeedID,TRAN_Originator.Reporting_Period,
TRAN_Originator.Master_Trust_Name_or_SPV,TRAN_Originator.Series_Trust_Name_or_Series,
TRAN_Originator.Transaction_ID,TRAN_Originator.Originator_Name,
TRAN_Originator.Originator_Count,TRAN_Originator.Originator_Position,
TRAN_Originator.Originator_ABN,TRAN_Originator.Originator_Jurisdiction,
TRAN_Originator.Originator_ID,TRAN_Originator.Originator_ID_Descriptor,
TRAN_Originator.Loans_Originated_Settlement_Date,TRAN_Originator.Loans_Originated,
NULL,NULL
From INGEST.TRAN_Originator TRAN_Originator
;

;
UPDATE LOAD.load_TRAN_originator SET dss_record_source = 'Perpetual.INGEST.TRAN_Originator',dss_load_datetime = GetDate() ;

