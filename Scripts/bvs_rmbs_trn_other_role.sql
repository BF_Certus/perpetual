CREATE TABLE DV.bvs_rmbs_trn_other_role (
  hk_l_rmbs_trn_other_role binary(20) NOT NULL, dss_load_datetime datetime 
, dss_change_hash binary(20) NOT NULL, dss_record_source varchar(255) 
, dss_start_date datetime, dss_version int, dss_create_time datetime 
, feedid varchar(100), other_role_count int, other_role_abn varchar(5000) 
, other_role_jurisdiction varchar(5000), other_role_id varchar(5000) 
, other_role_id_descriptor varchar(5000), other_role_fee_currency varchar(5000) 
, other_role_fee varchar(5000));