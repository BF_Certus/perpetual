truncate table LOAD.load_security;
INSERT INTO LOAD.load_security
(clientid, transaction_id, master_trust_name_or_spv, series_trust_name_or_series,
tranche_name, feedname, dss_record_source, dss_load_datetime)
SELECT
Security.ClientID,Security.Transaction_ID,Security.Master_Trust_Name_or_SPV,
Security.Series_Trust_Name_or_Series,Security.Tranche_Name,Security.FeedName,
FeedName,GETDATE()
From Ingest.SECU_v00 Security;