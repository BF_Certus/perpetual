/****** Object:  View [INGEST].[TRAN_v00_Validations]    Script Date: 22/06/2018 4:51:31 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create view [INGEST].[TRAN_v00_Validations] as 

Select
hk_l_TRAN_v00,
	[Collateral_Date_VLD]							= CASE Collateral_Date WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Most_Recent_Approval_Amount_VLD]				= CASE Most_Recent_Approval_Amount WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Current_Balance_VLD]							= CASE Current_Balance WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Scheduled_Balance_VLD]							= CASE Scheduled_Balance WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Available_Redraw_VLD]							= CASE Available_Redraw WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Offset_Account_Balancet_VLD]					= CASE Offset_Account_Balance WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Loan_Term_VLD]									= 0,
			[Remaining_Term_VLD]							= 0,
			[Seasoning_VLD]									= 0,
			[Maturity_Date_VLD]								= CASE Maturity_Date WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Loan_Securitised_Date_VLD]						= CASE Loan_Securitised_Date WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Original_LTV_VLD]								= CASE Original_LTV WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Current_LTV_VLD]								= CASE Current_LTV WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Scheduled_LTV_VLD]								= CASE Scheduled_LTV WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Scheduled_Minimum_Payment_VLD]					= CASE Scheduled_Minimum_Payment WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[LMI_Attachment_Point_VLD]						= CASE LMI_Attachment_Point WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Variable_Loan_Amount_VLD]						= CASE Variable_Loan_Amount WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Current_Interest_Rate_VLD]						= CASE Current_Interest_Rate WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Interest_Only_Expiry_Date_VLD]					= CASE Interest_Only_Expiry_Date WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Current_Rate_Expiry_Date_VLD]					= CASE Current_Rate_Expiry_Date WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Default_Balance_VLD]							= CASE Default_Balance WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Foreclosure_Proceeds_VLD]						= CASE Foreclosure_Proceeds WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Loss_on_Sale_VLD]								= CASE Loss_on_Sale WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Days_In_Arrears_VLD]							= CASE Days_In_Arrears WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Amount_in_Arrears_VLD]							= CASE Amount_in_Arrears WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Claim_Submitted_to_LMI_VLD]					= CASE Claim_Submitted_to_LMI WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Claim_Paid_by_LMI_VLD]							= CASE Claim_Paid_by_LMI WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Claim_Denied_by_LMI_VLD]						= CASE Claim_Denied_by_LMI WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Claim_Pending_with_LMI_VLD]					= CASE Claim_Pending_with_LMI WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Original_Property_Value_VLD]					= 0,
			[Most_Recent_Property_Value_VLD]				= CASE Most_Recent_Property_Value WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Loan_Guarantee_Flag_VLD]						= CASE Loan_Guarantee_flag WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Redraw_Feature_Flag_VLD]						= CASE Redraw_Feature_flag WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Offset_Account_Flag_VLD]						= CASE Offset_Account_flag WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Conforming_Mortgage_VLD]						= CASE Conforming_Mortgage WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Origination_Date_Settlement_Date_VLD]			= CASE Origination_Date_Settlement_Date WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Loan_Origination_Channel_Flag_VLD]				= CASE Loan_Origination_Channel_flag WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Reference_Index_Value_VLD]						= CASE Reference_Index_Value WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Interest_Margin_VLD]							= CASE Interest_Margin WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Restructuring_Arrangement_Flag_VLD]			= CASE Restructuring_Arrangement_flag WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Date_of_Foreclosure_VLD]						= CASE Date_of_Foreclosure WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Days_Since_Last_Scheduled_Payment_VLD]			= CASE Days_Since_Last_Scheduled_Payment WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Cumulative_Missed_Payments_VLD]				= CASE Cumulative_Missed_Payments WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Most_Recent_Property_Valuation_Date_VLD]		= CASE Most_Recent_Property_Valuation_Date WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[First_Home_Buyer_Flag_VLD]						= CASE First_Home_Buyer_Flag WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Employment_Type_Secondary_Borrower_VLD]		= CASE Employment_Type_Secondary_Borrower WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Borrower_type_Primary_Borrower_VLD]			= CASE Borrower_type_Primary_Borrower WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Borrower_type_Secondary_Borrower_VLD]			= CASE Borrower_type_Secondary_Borrower WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Legal_Entity_Type_Secondary_Borrower_VLD]		= CASE Legal_Entity_Type_Secondary_Borrower WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Bankruptcy_Flag_VLD]							= CASE Bankruptcy_Flag WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Last_Credit_Discharge_Date_VLD]				= CASE Last_Credit_Discharge_Date WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Number_of_Debtors_VLD]							= CASE Number_of_Debtors WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Credit_Score_Secondary_Borrower_VLD]			= CASE Credit_Score_Secondary_Borrower WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Debt_Serviceability_Metric_Score_VLD]			= CASE Debt_Serviceability_Metric_Score WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Income_VLD]									= CASE Income WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Secondary_Income_VLD]							= CASE Secondary_Income WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Income_Verification_for_Secondary_Income_VLD]	= CASE Income_Verification_for_Secondary_Income WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[ABS_Statistical_Area_VLD]						= CASE ABS_Statistical_Area WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END ,
			[Interest_Rate_VLD]								= CASE Interest_Rate WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END,
			[Property_Postcode_VLD]							= CASE Property_Postcode WHEN 'ND1' THEN 2 WHEN 'ND2' THEN 3 WHEN 'ND3' THEN 4 WHEN 'ND4' THEN 5 WHEN 'ND5' THEN 6 WHEN 'ND6' THEN 7 ELSE 1 END
From dv.s_TRAN_v00 
		where s_TRAN_v00.dss_load_datetime = (Select max(dss_load_datetime) from dv.s_TRAN_v00 z where z.hk_l_TRAN_v00 = s_TRAN_v00.hk_l_TRAN_v00)
GO


