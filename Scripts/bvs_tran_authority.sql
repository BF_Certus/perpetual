CREATE TABLE DV.bvs_TRAN_authority (
  hk_l_TRAN_authority binary(20) NOT NULL, dss_load_datetime datetime 
, dss_change_hash binary(20) NOT NULL, dss_record_source varchar(255) 
, dss_start_date datetime, dss_version int, dss_create_time datetime 
, authority_count int, authority_position int, authority_type varchar(5000) 
, authority_holder_abn varchar(5000) 
, authority_holder_jurisdiction varchar(5000), authority_holder_id varchar(5000) 
, authority_holder_id_descriptor varchar(5000))
;