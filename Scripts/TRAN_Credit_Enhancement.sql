/****** Object:  View [INGEST].[TRAN_Credit_Enhancement]    Script Date: 4/07/2018 11:01:25 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--Drop view [INGEST].[TRAN_Credit_Enhancement]
--go
Create View [INGEST].[TRAN_Credit_Enhancement] as 
 
  WITH Nbrs ( Number ) AS (
    SELECT 1 UNION ALL
    SELECT 1 + Number FROM Nbrs WHERE Number < 99
)
SELECT h_cm.ClientID, h_fm.FeedID, h_rp.Reporting_Period, h_t.Master_Trust_Name_or_SPV, h_t.Series_Trust_Name_or_Series, h_t.Transaction_ID,
DelimCount as Credit_Enhancement_Count, N.Number as Credit_Enhancement_Position,
Case when DelimCount = 0 then Credit_Enhancement_Name else SubString(  Credit_Enhancement_Name , dbo.FN_CERTUS_INSTR(  Credit_Enhancement_Name ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Credit_Enhancement_Name,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Credit_Enhancement_Name ,';',1,N.Number-1)-1) end as Credit_Enhancement_Name,
Case when DelimCount = 0 then Credit_Enhancement_Provider_Name else SubString(  Credit_Enhancement_Provider_Name , dbo.FN_CERTUS_INSTR(  Credit_Enhancement_Provider_Name ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Credit_Enhancement_Provider_Name,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Credit_Enhancement_Provider_Name ,';',1,N.Number-1)-1) end as Credit_Enhancement_Provider_Name,
Case when DelimCount = 0 then Credit_Enhancement_Provider_ABN else SubString(  Credit_Enhancement_Provider_ABN , dbo.FN_CERTUS_INSTR(  Credit_Enhancement_Provider_ABN ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Credit_Enhancement_Provider_ABN,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Credit_Enhancement_Provider_ABN ,';',1,N.Number-1)-1) end as Credit_Enhancement_Provider_ABN,
Case when DelimCount = 0 then Credit_Enhancement_Provider_Jurisdiction else SubString(  Credit_Enhancement_Provider_Jurisdiction , dbo.FN_CERTUS_INSTR(  Credit_Enhancement_Provider_Jurisdiction ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Credit_Enhancement_Provider_Jurisdiction,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Credit_Enhancement_Provider_Jurisdiction ,';',1,N.Number-1)-1) end as Credit_Enhancement_Provider_Jurisdiction,
Case when DelimCount = 0 then Credit_Enhancement_Provider_ID else SubString(  Credit_Enhancement_Provider_ID , dbo.FN_CERTUS_INSTR(  Credit_Enhancement_Provider_ID ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Credit_Enhancement_Provider_ID,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Credit_Enhancement_Provider_ID ,';',1,N.Number-1)-1) end as Credit_Enhancement_Provider_ID,
Case when DelimCount = 0 then Credit_Enhancement_Provider_ID_Descriptor else SubString(  Credit_Enhancement_Provider_ID_Descriptor , dbo.FN_CERTUS_INSTR(  Credit_Enhancement_Provider_ID_Descriptor ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Credit_Enhancement_Provider_ID_Descriptor,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Credit_Enhancement_Provider_ID_Descriptor ,';',1,N.Number-1)-1) end as Credit_Enhancement_Provider_ID_Descriptor,
Case when DelimCount = 0 then Credit_Enhancement_Type else SubString(  Credit_Enhancement_Type , dbo.FN_CERTUS_INSTR(  Credit_Enhancement_Type ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Credit_Enhancement_Type,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Credit_Enhancement_Type ,';',1,N.Number-1)-1) end as Credit_Enhancement_Type,
Case when DelimCount = 0 then Credit_Enhancement_Currency else SubString(  Credit_Enhancement_Currency , dbo.FN_CERTUS_INSTR(  Credit_Enhancement_Currency ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Credit_Enhancement_Currency,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Credit_Enhancement_Currency ,';',1,N.Number-1)-1) end as Credit_Enhancement_Currency,
Case when DelimCount = 0 then Credit_Enhancement_Fee else SubString(  Credit_Enhancement_Fee , dbo.FN_CERTUS_INSTR(  Credit_Enhancement_Fee ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Credit_Enhancement_Fee,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Credit_Enhancement_Fee ,';',1,N.Number-1)-1) end as Credit_Enhancement_Fee,
Case when DelimCount = 0 then Initial_Credit_Enhancement_Amount else SubString(  Initial_Credit_Enhancement_Amount , dbo.FN_CERTUS_INSTR(  Initial_Credit_Enhancement_Amount ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Initial_Credit_Enhancement_Amount,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Initial_Credit_Enhancement_Amount ,';',1,N.Number-1)-1) end as Initial_Credit_Enhancement_Amount,
Case when DelimCount = 0 then Beginning_Amount_Available_Credit else SubString(  Beginning_Amount_Available_Credit , dbo.FN_CERTUS_INSTR(  Beginning_Amount_Available_Credit ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Beginning_Amount_Available_Credit,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Beginning_Amount_Available_Credit ,';',1,N.Number-1)-1) end as Beginning_Amount_Available_Credit,
Case when DelimCount = 0 then Restorations_Credit else SubString(  Restorations_Credit , dbo.FN_CERTUS_INSTR(  Restorations_Credit ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Restorations_Credit,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Restorations_Credit ,';',1,N.Number-1)-1) end as Restorations_Credit,
Case when DelimCount = 0 then Draws_Credit else SubString(  Draws_Credit , dbo.FN_CERTUS_INSTR(  Draws_Credit ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Draws_Credit,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Draws_Credit ,';',1,N.Number-1)-1) end as Draws_Credit,
Case when DelimCount = 0 then Reduction_Credit else SubString(  Reduction_Credit , dbo.FN_CERTUS_INSTR(  Reduction_Credit ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Reduction_Credit,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Reduction_Credit ,';',1,N.Number-1)-1) end as Reduction_Credit,
Case when DelimCount = 0 then Ending_Amount_Available_Credit else SubString(  Ending_Amount_Available_Credit , dbo.FN_CERTUS_INSTR(  Ending_Amount_Available_Credit ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Ending_Amount_Available_Credit,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Ending_Amount_Available_Credit ,';',1,N.Number-1)-1) end as Ending_Amount_Available_Credit,
Case when DelimCount = 0 then Beginning_Amount_Drawn_Credit else SubString(  Beginning_Amount_Drawn_Credit , dbo.FN_CERTUS_INSTR(  Beginning_Amount_Drawn_Credit ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Beginning_Amount_Drawn_Credit,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Beginning_Amount_Drawn_Credit ,';',1,N.Number-1)-1) end as Beginning_Amount_Drawn_Credit,
Case when DelimCount = 0 then Ending_Amount_Drawn_Credit else SubString(  Ending_Amount_Drawn_Credit , dbo.FN_CERTUS_INSTR(  Ending_Amount_Drawn_Credit ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Ending_Amount_Drawn_Credit,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Ending_Amount_Drawn_Credit ,';',1,N.Number-1)-1) end as Ending_Amount_Drawn_Credit
from 
	DV.S_TRAN_v00 s
	inner join 
	DV.L_TRAN_v00 l on s.hk_l_TRAN_v00=l.hk_l_TRAN_v00
	inner join
	DV.h_client_master h_cm on h_cm.hk_h_client_master=l.hk_h_client_master
	inner join
	DV.h_feed_master h_fm on h_fm.hk_h_feed_master=l.hk_h_feed_master
	inner join
	DV.h_reporting_period h_rp on h_rp.hk_h_reporting_period=l.hk_h_reporting_period
	inner join
	DV.h_transaction h_t on h_t.hk_h_transaction=l.hk_h_transaction
	inner join
	(select TRAN_v00_DelimiterCount.hk_l_TRAN_v00, case
		when Delim_Credit_Enhancement_Name=Delim_Credit_Enhancement_Provider_Name
		and Delim_Credit_Enhancement_Name=Delim_Credit_Enhancement_Provider_ABN
		and Delim_Credit_Enhancement_Name=Delim_Credit_Enhancement_Provider_Jurisdiction
		and Delim_Credit_Enhancement_Name=Delim_Credit_Enhancement_Provider_ID
		and Delim_Credit_Enhancement_Name=Delim_Credit_Enhancement_Provider_ID_Descriptor
		and Delim_Credit_Enhancement_Name=Delim_Credit_Enhancement_Type
		and Delim_Credit_Enhancement_Name=Delim_Credit_Enhancement_Currency
		and Delim_Credit_Enhancement_Name=Delim_Credit_Enhancement_Fee
		and Delim_Credit_Enhancement_Name=Delim_Initial_Credit_Enhancement_Amount
		and Delim_Credit_Enhancement_Name=Delim_Beginning_Amount_Available_Credit
		and Delim_Credit_Enhancement_Name=Delim_Restorations_Credit
		and Delim_Credit_Enhancement_Name=Delim_Draws_Credit
		and Delim_Credit_Enhancement_Name=Delim_Reduction_Credit
		and Delim_Credit_Enhancement_Name=Delim_Ending_Amount_Available_Credit
		and Delim_Credit_Enhancement_Name=Delim_Beginning_Amount_Drawn_Credit
		and Delim_Credit_Enhancement_Name=Delim_Ending_Amount_Drawn_Credit
		then Delim_Credit_Enhancement_Name+1 end as DelimCount from INGEST.TRAN_v00_DelimiterCount) delim on delim.hk_l_TRAN_v00=l.hk_l_TRAN_v00
	inner join
	Nbrs N ON (N.Number <= DelimCount) or (DelimCount = 0 and N.Number =1)

GO


