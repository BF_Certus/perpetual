truncate table LOAD.load_TRAN_trigger;
INSERT INTO LOAD.load_TRAN_trigger
(clientid, reporting_period, master_trust_name_or_spv, series_trust_name_or_series,
transaction_id, trigger_position, trigger_name, feedid, trigger_count,
trigger_description, trigger_state, dss_record_source, dss_load_datetime)
SELECT
TRAN_Trigger.ClientID,TRAN_Trigger.Reporting_Period,TRAN_Trigger.Master_Trust_Name_or_SPV,
TRAN_Trigger.Series_Trust_Name_or_Series,TRAN_Trigger.Transaction_ID,
TRAN_Trigger.Trigger_Position,TRAN_Trigger.Trigger_Name,TRAN_Trigger.FeedID,
TRAN_Trigger.Trigger_Count,TRAN_Trigger.Trigger_Description,TRAN_Trigger.Trigger_State,
NULL,NULL
From INGEST.TRAN_Trigger TRAN_Trigger
;
UPDATE LOAD.load_TRAN_trigger SET dss_record_source = 'Perpetual.INGEST.TRAN_Trigger',dss_load_datetime = GetDate() ;