/****** Object:  View [INGEST].[SECU_v00_DataConversion]    Script Date: 6/07/2018 2:14:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create View [INGEST].[SECU_v00_DataConversion] as 
Select hk_l_SECU_v00

,coalesce(Try_Convert([date], Report_Date,105), Try_Convert([date], Report_Date, 121)) as Report_Date 







,coalesce(Try_Convert([date], Issue_Date,105), Try_Convert([date], Issue_Date, 121)) as Issue_Date 
,coalesce(Try_Convert([date], Accrual_Begin_Date,105), Try_Convert([date], Accrual_Begin_Date, 121)) as Accrual_Begin_Date 
,coalesce(Try_Convert([date], Accrual_End_Date,105), Try_Convert([date], Accrual_End_Date, 121)) as Accrual_End_Date 
, Try_Convert([int], Replace(Accrual_Period,',','')) as Accrual_Period 





, Try_Convert([decimal](24,9), Replace(Original_Subordination,',','')) as Original_Subordination 
, Try_Convert([decimal](24,9), Replace(Current_Subordination,',','')) as Current_Subordination 
, Try_Convert([decimal](18,2), Replace(Current_Principal_Face_Amount,',','')) as Current_Principal_Face_Amount 

, Try_Convert([bit], Replace(Coupon_Payment_Reference,',','')) as Coupon_Payment_Reference 
,coalesce(Try_Convert([date], Distribution_Date,105), Try_Convert([date], Distribution_Date, 121)) as Distribution_Date 
,coalesce(Try_Convert([date], Record_Date,105), Try_Convert([date], Record_Date, 121)) as Record_Date 
,coalesce(Try_Convert([date], Determination_Date,105), Try_Convert([date], Determination_Date, 121)) as Determination_Date 
, Try_Convert([decimal](24,9), Replace(Beginning_Stated_Factor,',','')) as Beginning_Stated_Factor 
, Try_Convert([decimal](24,9), Replace(Ending_Stated_Factor,',','')) as Ending_Stated_Factor 
, Try_Convert([decimal](18,2), Replace(Original_Principal_Face_Amount,',','')) as Original_Principal_Face_Amount 
, Try_Convert([decimal](18,2), Replace(Beginning_Invested_Amount,',','')) as Beginning_Invested_Amount 
, Try_Convert([decimal](18,2), Replace(Ending_Invested_Amount,',','')) as Ending_Invested_Amount 
, Try_Convert([decimal](18,2), Replace(Beginning_Stated_Amount,',','')) as Beginning_Stated_Amount 
, Try_Convert([decimal](18,2), Replace(Ending_Stated_Amount,',','')) as Ending_Stated_Amount 
, Try_Convert([decimal](24,9), Replace(Beginning_Invested_Factor,',','')) as Beginning_Invested_Factor 
, Try_Convert([decimal](24,9), Replace(Ending_Invested_Factor,',','')) as Ending_Invested_Factor 

, Try_Convert([decimal](24,9), Replace(Coupon_Reference_Index_Value,',','')) as Coupon_Reference_Index_Value 
, Try_Convert([decimal](24,9), Replace(Coupon_Margin,',','')) as Coupon_Margin 
, Try_Convert([decimal](24,9), Replace(Step_Up_Margin,',','')) as Step_Up_Margin 
,coalesce(Try_Convert([date], Step_Up_Date,105), Try_Convert([date], Step_Up_Date, 121)) as Step_Up_Date 
, Try_Convert([decimal](18,2), Replace(Scheduled_Coupon,',','')) as Scheduled_Coupon 
, Try_Convert([decimal](24,9), Replace(Scheduled_Coupon_Factor,',','')) as Scheduled_Coupon_Factor 
, Try_Convert([decimal](24,9), Replace(Current_Coupon_Rate,',','')) as Current_Coupon_Rate 
, Try_Convert([decimal](18,2), Replace(Coupon_Distribution,',','')) as Coupon_Distribution 
, Try_Convert([decimal](24,9), Replace(Coupon_Distribution_Factor,',','')) as Coupon_Distribution_Factor 
, Try_Convert([decimal](18,2), Replace(Scheduled_Principal,',','')) as Scheduled_Principal 
, Try_Convert([decimal](24,9), Replace(Scheduled_Principal_Factor,',','')) as Scheduled_Principal_Factor 
, Try_Convert([decimal](18,2), Replace(Principal_Distribution,',','')) as Principal_Distribution 
, Try_Convert([decimal](24,9), Replace(Principal_Distribution_Factor,',','')) as Principal_Distribution_Factor 
, Try_Convert([decimal](24,9), Replace(Coupon_Shortfall_Factor,',','')) as Coupon_Shortfall_Factor 
, Try_Convert([decimal](24,9), Replace(Ending_Cumulative_Coupon_Shortfall_Factor,',','')) as Ending_Cumulative_Coupon_Shortfall_Factor 
, Try_Convert([decimal](18,2), Replace(Coupon_Shortfall,',','')) as Coupon_Shortfall 
, Try_Convert([decimal](18,2), Replace(Ending_Cumulative_Coupon_Shortfall,',','')) as Ending_Cumulative_Coupon_Shortfall 
, Try_Convert([decimal](18,2), Replace(Principal_Shortfall,',','')) as Principal_Shortfall 
, Try_Convert([decimal](18,2), Replace(Ending_Cumulative_Principal_Shortfall,',','')) as Ending_Cumulative_Principal_Shortfall 
,coalesce(Try_Convert([date], Legal_Maturity_Date,105), Try_Convert([date], Legal_Maturity_Date, 121)) as Legal_Maturity_Date 
, Try_Convert([decimal](24,9), Replace(Original_Estimated_Weighted_Average_Life,',','')) as Original_Estimated_Weighted_Average_Life 
, Try_Convert([decimal](24,9), Replace(Current_Weighted_Average_Life,',','')) as Current_Weighted_Average_Life 
,coalesce(Try_Convert([date], Expected_Final_Maturity_Date,105), Try_Convert([date], Expected_Final_Maturity_Date, 121)) as Expected_Final_Maturity_Date 
, Try_Convert([decimal](24,9), Replace(Redemption_Price,',','')) as Redemption_Price 
, Try_Convert([bigint], Replace(ABN,',','')) as ABN 




,coalesce(Try_Convert([date], Soft_Bullet_Date,105), Try_Convert([date], Soft_Bullet_Date, 121)) as Soft_Bullet_Date 

, Try_Convert([decimal](24,9), Replace(Step_Up_Rate,',','')) as Step_Up_Rate 
, Try_Convert([decimal](18,2), Replace(Beginning_Cumulative_Chargeoffs,',','')) as Beginning_Cumulative_Chargeoffs 
, Try_Convert([decimal](18,2), Replace(Beginning_Cumulative_Coupon_Shortfall,',','')) as Beginning_Cumulative_Coupon_Shortfall 
, Try_Convert([decimal](18,2), Replace(Beginning_Cumulative_Principal_Shortfall,',','')) as Beginning_Cumulative_Principal_Shortfall 
, Try_Convert([decimal](18,2), Replace(Coupon_Shortfall_Makeup,',','')) as Coupon_Shortfall_Makeup 
, Try_Convert([decimal](18,2), Replace(Principal_Shortfall_Makeup,',','')) as Principal_Shortfall_Makeup 
, Try_Convert([decimal](18,2), Replace(Allocated_Chargeoffs,',','')) as Allocated_Chargeoffs 
, Try_Convert([decimal](18,2), Replace(Chargeoff_Restorations,',','')) as Chargeoff_Restorations 
, Try_Convert([decimal](18,2), Replace(Ending_Cumulative_Chargeoffs,',','')) as Ending_Cumulative_Chargeoffs 
, Try_Convert([decimal](24,9), Replace(Beginning_Cumulative_Chargeoff_Factor,',','')) as Beginning_Cumulative_Chargeoff_Factor 
, Try_Convert([decimal](24,9), Replace(Beginning_Cumulative_Coupon_Shortfall_Factor,',','')) as Beginning_Cumulative_Coupon_Shortfall_Factor 
, Try_Convert([decimal](24,9), Replace(Beginning_Cumulative_Principal_Shortfall_Factor,',','')) as Beginning_Cumulative_Principal_Shortfall_Factor 
, Try_Convert([decimal](24,9), Replace(Coupon_Shortfall_Makeup_Factor,',','')) as Coupon_Shortfall_Makeup_Factor 
, Try_Convert([decimal](24,9), Replace(Principal_Shortfall_Makeup_Factor,',','')) as Principal_Shortfall_Makeup_Factor 
, Try_Convert([decimal](24,9), Replace(Principal_Shortfall_Factor,',','')) as Principal_Shortfall_Factor 
, Try_Convert([decimal](24,9), Replace(Allocated_Chargeoff_Factor,',','')) as Allocated_Chargeoff_Factor 
, Try_Convert([decimal](24,9), Replace(Chargeoff_Restoration_Factor,',','')) as Chargeoff_Restoration_Factor 
, Try_Convert([decimal](24,9), Replace(Ending_Cumulative_Chargeoff_Factor,',','')) as Ending_Cumulative_Chargeoff_Factor 
, Try_Convert([decimal](24,9), Replace(Ending_Cumulative_Principal_Shortfall_Factor,',','')) as Ending_Cumulative_Principal_Shortfall_Factor 
, Try_Convert([decimal](24,9), Replace(Coupon_Rate,',','')) as Coupon_Rate 


FROM DV.s_SECU_v00
		where s_SECU_v00.dss_load_datetime = (Select max(dss_load_datetime) from dv.s_SECU_v00 z where z.hk_l_SECU_v00 = s_SECU_v00.hk_l_SECU_v00)
;
GO


