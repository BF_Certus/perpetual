CREATE TABLE LOAD.load_SECU_ratings (
  clientid int, reporting_period nvarchar(100), transaction_id nvarchar(255) 
, master_trust_name_or_spv nvarchar(255) 
, series_trust_name_or_series nvarchar(255), tranche_name nvarchar(255) 
, name nvarchar(255), report_date nvarchar(255), original_rating nvarchar(255) 
, current_rating nvarchar(255), abn nvarchar(255), jurisdiction nvarchar(255) 
, id nvarchar(255), id_descriptor nvarchar(255), dss_record_source varchar(255) 
, dss_load_datetime datetime2);