CREATE TABLE STAGE.stage_SECU_ratings (
  hk_l_SECU_ratings binary(20) NOT NULL 
, hk_h_rating_agency binary(20) NOT NULL, hk_h_security binary(20) NOT NULL 
, hk_h_transaction binary(20) NOT NULL 
, hk_h_reporting_period binary(20) NOT NULL 
, hk_h_client_master binary(20) NOT NULL, clientid int, name nvarchar(255) 
, reporting_period nvarchar(100), transaction_id nvarchar(255) 
, master_trust_name_or_spv nvarchar(255) 
, series_trust_name_or_series nvarchar(255), tranche_name nvarchar(255) 
, report_date nvarchar(255), original_rating nvarchar(255) 
, current_rating nvarchar(255), abn nvarchar(255), jurisdiction nvarchar(255) 
, id nvarchar(255), id_descriptor nvarchar(255) 
, dss_change_hash_stage_SECU_ratings binary(20) NOT NULL 
, dss_record_source varchar(255), dss_load_datetime datetime2 
, dss_create_time datetime2);