select
--		[FeedID],
[Feed_Name], 
[PS000], [PS001], [PS002], [PS003], [PS004], [PS005], [PS006], [PS007], [PS008], [PS009], [PS010], [PS011], [PS012], [PS013], [PS014], [PS015], 
[PS016], [PS017], [PS018], [PS019], [PS020], [PS021], [PS022], [PS023], [PS024], [PS025], [PS026], [PS027], [PS028], [PS029], [PS030], [PS031], 
[PS032], [PS033], [PS034], [PS035], [PS036], [PS037], [PS038], [PS039], [PS040], [PS041], [PS042], [PS043], [PS044], [PS045], [PS046], [PS047], 
[PS048], [PS049], [PS050], [PS051], [PS052], [PS053], [PS054], [PS055], [PS056], [PS057], [PS058], [PS059], [PS060], [PS061], [PS062], [PS063], 
[PS064], [PS065], [PS066], [PS067], [PS068], [PS069], [PS070], [PS071], [PS072], [PS073], [PS074], [PS075], [PS076], [PS077], [PS078], [PS079], 
[PS080], [PS081], [PS082], [PS083], [PS084], [PS085], [PS086] 
from INGEST.Ext_Stage_SECU 
except
select l_SECU_v00.dss_record_source, h_reporting_period.reporting_period, Report_Date, h_transaction.transaction_id, h_security.Master_Trust_Name_Or_Spv, 
h_security.Series_Trust_Name_Or_Series, Tranche_Name, Isin, Austraclear_Series_Id, Clearing_System_Name, Issue_Date, Accrual_Begin_Date, Accrual_End_Date, 
Accrual_Period, Tranche_Currency, Exchange_Name, Name,Original_Rating, Current_Rating, Original_Subordination, Current_Subordination, 
Current_Principal_Face_Amount, Offer_Type, Coupon_Payment_Reference, Distribution_Date, Record_Date, Determination_Date, Beginning_Stated_Factor, 
Ending_Stated_Factor, Original_Principal_Face_Amount, Beginning_Invested_Amount, Ending_Invested_Amount, Beginning_Stated_Amount, Ending_Stated_Amount, 
Beginning_Invested_Factor, Ending_Invested_Factor, Coupon_Reference_Index, Coupon_Reference_Index_Value, Coupon_Margin, Step_Up_Margin, Step_Up_Date, 
Scheduled_Coupon, Scheduled_Coupon_Factor, Current_Coupon_Rate, Coupon_Distribution, Coupon_Distribution_Factor, Scheduled_Principal, 
Scheduled_Principal_Factor, Principal_Distribution, Principal_Distribution_Factor, Coupon_Shortfall_Factor, Ending_Cumulative_Coupon_Shortfall_Factor, 
Coupon_Shortfall, Ending_Cumulative_Coupon_Shortfall, Principal_Shortfall, Ending_Cumulative_Principal_Shortfall, Legal_Maturity_Date, 
Original_Estimated_Weighted_Average_Life, Current_Weighted_Average_Life, Expected_Final_Maturity_Date, Redemption_Price, ABN, Jurisdiction, 
Id, Id_Descriptor, Governing_Law, Soft_Bullet_Date, Coupon_Basis, Step_Up_Rate, Beginning_Cumulative_Chargeoffs, Beginning_Cumulative_Coupon_Shortfall, 
Beginning_Cumulative_Principal_Shortfall, Coupon_Shortfall_Makeup, Principal_Shortfall_Makeup, Allocated_Chargeoffs, Chargeoff_Restorations, 
Ending_Cumulative_Chargeoffs, Beginning_Cumulative_Chargeoff_Factor, Beginning_Cumulative_Coupon_Shortfall_Factor, Beginning_Cumulative_Principal_Shortfall_Factor, 
Coupon_Shortfall_Makeup_Factor, Principal_Shortfall_Makeup_Factor, Principal_Shortfall_Factor, Allocated_Chargeoff_Factor, Chargeoff_Restoration_Factor, 
Ending_Cumulative_Chargeoff_Factor, Ending_Cumulative_Principal_Shortfall_Factor, Coupon_Rate 
from DV.s_SECU_v00 
inner join DV.l_SECU_v00 on s_SECU_v00.[hk_l_SECU_v00]=l_SECU_v00.[hk_l_SECU_v00] 
inner join DV.h_reporting_period on h_reporting_period.hk_h_reporting_period=l_SECU_v00.hk_h_reporting_period 
inner join DV.h_transaction on h_transaction.hk_h_transaction=l_SECU_v00.hk_h_transaction 
inner join DV.h_security on h_security.hk_h_security=l_SECU_v00.hk_h_security 
inner join DV.h_isin on h_isin.hk_h_isin=l_SECU_v00.hk_h_isin;


select l_SECU_v00.dss_record_source, h_reporting_period.reporting_period, Report_Date, h_transaction.transaction_id, h_security.Master_Trust_Name_Or_Spv, 
h_security.Series_Trust_Name_Or_Series, Tranche_Name, Isin, Austraclear_Series_Id, Clearing_System_Name, Issue_Date, Accrual_Begin_Date, Accrual_End_Date, 
Accrual_Period, Tranche_Currency, Exchange_Name, Name,Original_Rating, Current_Rating, Original_Subordination, Current_Subordination, 
Current_Principal_Face_Amount, Offer_Type, Coupon_Payment_Reference, Distribution_Date, Record_Date, Determination_Date, Beginning_Stated_Factor, 
Ending_Stated_Factor, Original_Principal_Face_Amount, Beginning_Invested_Amount, Ending_Invested_Amount, Beginning_Stated_Amount, Ending_Stated_Amount, 
Beginning_Invested_Factor, Ending_Invested_Factor, Coupon_Reference_Index, Coupon_Reference_Index_Value, Coupon_Margin, Step_Up_Margin, Step_Up_Date, 
Scheduled_Coupon, Scheduled_Coupon_Factor, Current_Coupon_Rate, Coupon_Distribution, Coupon_Distribution_Factor, Scheduled_Principal, 
Scheduled_Principal_Factor, Principal_Distribution, Principal_Distribution_Factor, Coupon_Shortfall_Factor, Ending_Cumulative_Coupon_Shortfall_Factor, 
Coupon_Shortfall, Ending_Cumulative_Coupon_Shortfall, Principal_Shortfall, Ending_Cumulative_Principal_Shortfall, Legal_Maturity_Date, 
Original_Estimated_Weighted_Average_Life, Current_Weighted_Average_Life, Expected_Final_Maturity_Date, Redemption_Price, ABN, Jurisdiction, 
Id, Id_Descriptor, Governing_Law, Soft_Bullet_Date, Coupon_Basis, Step_Up_Rate, Beginning_Cumulative_Chargeoffs, Beginning_Cumulative_Coupon_Shortfall, 
Beginning_Cumulative_Principal_Shortfall, Coupon_Shortfall_Makeup, Principal_Shortfall_Makeup, Allocated_Chargeoffs, Chargeoff_Restorations, 
Ending_Cumulative_Chargeoffs, Beginning_Cumulative_Chargeoff_Factor, Beginning_Cumulative_Coupon_Shortfall_Factor, Beginning_Cumulative_Principal_Shortfall_Factor, 
Coupon_Shortfall_Makeup_Factor, Principal_Shortfall_Makeup_Factor, Principal_Shortfall_Factor, Allocated_Chargeoff_Factor, Chargeoff_Restoration_Factor, 
Ending_Cumulative_Chargeoff_Factor, Ending_Cumulative_Principal_Shortfall_Factor, Coupon_Rate 
from DV.s_SECU_v00 
inner join DV.l_SECU_v00 on s_SECU_v00.[hk_l_SECU_v00]=l_SECU_v00.[hk_l_SECU_v00] 
inner join DV.h_reporting_period on h_reporting_period.hk_h_reporting_period=l_SECU_v00.hk_h_reporting_period 
inner join DV.h_transaction on h_transaction.hk_h_transaction=l_SECU_v00.hk_h_transaction 
inner join DV.h_security on h_security.hk_h_security=l_SECU_v00.hk_h_security 
inner join DV.h_isin on h_isin.hk_h_isin=l_SECU_v00.hk_h_isin

except
select
--		[FeedID],
[Feed_Name], 
[PS000], [PS001], [PS002], [PS003], [PS004], [PS005], [PS006], [PS007], [PS008], [PS009], [PS010], [PS011], [PS012], [PS013], [PS014], [PS015], 
[PS016], [PS017], [PS018], [PS019], [PS020], [PS021], [PS022], [PS023], [PS024], [PS025], [PS026], [PS027], [PS028], [PS029], [PS030], [PS031], 
[PS032], [PS033], [PS034], [PS035], [PS036], [PS037], [PS038], [PS039], [PS040], [PS041], [PS042], [PS043], [PS044], [PS045], [PS046], [PS047], 
[PS048], [PS049], [PS050], [PS051], [PS052], [PS053], [PS054], [PS055], [PS056], [PS057], [PS058], [PS059], [PS060], [PS061], [PS062], [PS063], 
[PS064], [PS065], [PS066], [PS067], [PS068], [PS069], [PS070], [PS071], [PS072], [PS073], [PS074], [PS075], [PS076], [PS077], [PS078], [PS079], 
[PS080], [PS081], [PS082], [PS083], [PS084], [PS085], [PS086] 
from INGEST.Ext_Stage_SECU;

SELECT
*
FROM [INGEST].[Ext_Stage_TRAN]
except
select h_feed_master.[FeedID], s_TRAN_rts.[Feed_Name], h_reporting_period.reporting_period, CONVERT(varchar, Report_Date, 105) as Report_Date, h_transaction.transaction_id, 
h_transaction.Master_Trust_Name_Or_Spv, h_transaction.Series_Trust_Name_Or_Series, [Data_Location], [Responsible_Party_Name], [Responsible_Party_ABN], 
[Issuing_Trustee_Name], [Issuing_Trustee_ABN], [Indenture_or_Security_Trustee_Name], [Indenture_or_Security_Trustee_ABN], [Sponsor_Name], [Sponsor_ABN], 
[Arranger_Name], [Arranger_ABN], [Underwriter_Name], [Underwriter_ABN], [Manager_Name], [Manager_ABN], [Servicer_Name], [Servicer_ABN], 
[Liquidity_Provider_Name], [Liquidity_Provider_ABN], [LMI_Provider_Name], [LMI_Provider_ABN], [Paying_Agent_Name], [Paying_Agent_ABN], 
[Swap_or_Hedge_Provider_Name], [Swap_or_Hedge_Provider_ABN], [Swap_or_Hedge_Type], [Governing_Law], [Trigger_Name], [Revolving_Period_Close_Date], 
[Trigger_Description], [Pre_Funding_Close_Date], [Asset_Substitution_Close_Date], [Collection_Period_Frequency], [Remittance_Frequency], 
[Senior_Fees_and_Expenses], [Credit_Enhancement_Name], [Account_Provider_Name], [Ending_Balance], [Target_Balance], [Initial_Amount_Available], 
[Beginning_Amount_Available_Liquidity], [Ending_Amount_Available_Liquidity], [Notional_Principal_Amount], [Collection_Period_Begin_Date], 
[Collection_Period_End_Date], [Loans_Insured], [Timely_Payment_Claims_Outstanding], [Responsible_Party_Jurisdiction], [Responsible_Party_ID], 
[Responsible_Party_ID_Descriptor], [Issuing_Trustee_Jurisdiction], [Issuing_Trustee_ID], [Issuing_Trustee_ID_Descriptor], [Issuing_Trustee_Fee_Currency], 
[Issuing_Trustee_Fee], [Indenture_or_Security_Trustee_Jurisdiction], [Indenture_or_Security_Trustee_ID], [Indenture_or_Security_Trustee_ID_Descriptor], 
[Indenture_or_Security_Trustee_Fee_Currency], [Indenture_or_Security_Trustee_Fee], [Sponsor_Jurisdiction], [Sponsor_ID], [Sponsor_ID_Descriptor], 
[Arranger_Jurisdiction], [Arranger_ID], [Arranger_ID_Descriptor], [Underwriter_Jurisdiction], [Underwriter_ID], [Underwriter_ID_Descriptor], 
[Manager_Jurisdiction], [Manager_ID], [Manager_ID_Descriptor], [Manager_Fee_Currency], [Manager_Fee], [Paying_Agent_Jurisdiction], [Paying_Agent_ID], 
[Paying_Agent_ID_Descriptor], [Paying_Agent_Fee_Currency], [Paying_Agent_Fee], [Originator_Name], [Originator_ABN], [Originator_Jurisdiction], 
[Originator_ID], [Originator_ID_Descriptor], [Loans_Originated_Settlement_Date], [Loans_Originated], [Seller_Name], [Seller_ABN], [Seller_Jurisdiction], 
[Seller_ID], [Seller_ID_Descriptor], [Loans_Sold_Settlement_Date], [Loans_Sold], [Servicer_Jurisdiction], [Servicing_Fee_Currency], [Servicing_Fee], 
[Liquidity_Provider_Jurisdiction], [Liquidity_Provider_ID], [Liquidity_Provider_ID_Descriptor], [Liquidity_Facility_Name], [Liquidity_Facility_Type], 
[Reimbursements], [Draws_Liquidity], [Reduction_Liquidity], [Beginning_Amount_Drawn_Liquidity], [Ending_Amount_Drawn_Liquidity], [Facility_Currency], 
[Facility_Fee], [Credit_Enhancement_Provider_Name], [Credit_Enhancement_Provider_ABN], [Credit_Enhancement_Provider_Jurisdiction], 
[Credit_Enhancement_Provider_ID], [Credit_Enhancement_Provider_ID_Descriptor], [Credit_Enhancement_Type], [Credit_Enhancement_Currency], 
[Credit_Enhancement_Fee], [Initial_Credit_Enhancement_Amount], [Beginning_Amount_Available_Credit], [Restorations_Credit], [Draws_Credit], 
[Reduction_Credit], [Ending_Amount_Available_Credit], [Beginning_Amount_Drawn_Credit], [Ending_Amount_Drawn_Credit], [LMI_Provider_Jurisdiction], 
[LMI_Provider_ID], [LMI_Provider_ID_Descriptor], [Loan_Currency_Insurance], [Loans_Insured_Settlement_Date], [Loans_Insured_TPC_Settlement_Date], 
[Loans_Insured_TPC], [LMI_Claims_Outstanding], [Swap_or_Hedge_Provider_Jurisdiction], [Swap_or_Hedge_Provider_ID], [Swap_or_Hedge_Provider_ID_Descriptor], 
[Swap_or_Hedge_Name], [Pay_Leg_Currency], [Pay_Leg_Reference_Index], [Pay_Leg_Reference_Index_Value], [Pay_Leg_Margin_or_Rate], [Pay_Leg_Amount], 
[Receive_Leg_Currency], [Receive_Leg_Reference_Index], [Receive_Leg_Reference_Index_Value], [Receive_Leg_Margin_or_Rate], [Receive_Leg_Amount], 
[Exchange_Rate], [Swap_or_Hedge_Fee_Currency], [Swap_or_Hedge_Fee], [Trigger_State], [Account_Provider_ABN], [Account_Provider_Jurisdiction], 
[Account_Provider_ID], [Account_Provider_ID_Descriptor], [Account_Currency], [Account_Fee], [Beginning_Balance], [Signature_Authority_Name], 
[Signature_Authority_ABN], [Signature_Authority_Jurisdiction], [Signature_Authority_ID], [Signature_Authority_ID_Descriptor], [Authority_Type], 
[Authority_Holder_Name], [Authority_Holder_ABN], [Authority_Holder_Jurisdiction], [Authority_Holder_ID], [Authority_Holder_ID_Descriptor], 
[Eligible_Loan_Criteria], [Next_Report_Date], [Trust_Settlement_Date], [Servicing_Role], [Account_Type], [Role_Name], [Account_Name], 
[Account_Signature_Authority_Name], [Servicer_ID], [Servicer_ID_Descriptor], [Other_Role_Name], [Other_Role_ABN], [Other_Role_Jurisdiction], 
[Other_Role_ID], [Other_Role_ID_Descriptor], [Other_Role_Fee_Currency], [Other_Role_Fee], [Arranger_Fee_Currency], [Arranger_Fee], 
[Underwriter_Fee_Currency], [Underwriter_Fee], [Responsible_Party_Fee_Currency], [Responsible_Party_Fee]
from DV.s_TRAN_v00 
inner join DV.l_TRAN_v00 on s_TRAN_v00.[hk_l_TRAN_v00]=l_TRAN_v00.[hk_l_TRAN_v00] 
inner join DV.s_TRAN_rts on s_TRAN_rts.[hk_l_TRAN_v00]=l_TRAN_v00.[hk_l_TRAN_v00] and s_TRAN_rts.[feed_name] = (select distinct [feed_name] from [INGEST].[Ext_Stage_TRAN])
inner join DV.h_feed_master on h_feed_master.hk_h_feed_master=l_TRAN_v00.hk_h_feed_master
inner join DV.h_reporting_period on h_reporting_period.hk_h_reporting_period=l_TRAN_v00.hk_h_reporting_period 
inner join DV.h_transaction on h_transaction.hk_h_transaction=l_TRAN_v00.hk_h_transaction;

select h_feed_master.[FeedID], s_TRAN_rts.[Feed_Name], h_reporting_period.reporting_period, CONVERT(varchar, Report_Date, 105) as Report_Date, h_transaction.transaction_id, 
h_transaction.Master_Trust_Name_Or_Spv, h_transaction.Series_Trust_Name_Or_Series, [Data_Location], [Responsible_Party_Name], [Responsible_Party_ABN], 
[Issuing_Trustee_Name], [Issuing_Trustee_ABN], [Indenture_or_Security_Trustee_Name], [Indenture_or_Security_Trustee_ABN], [Sponsor_Name], [Sponsor_ABN], 
[Arranger_Name], [Arranger_ABN], [Underwriter_Name], [Underwriter_ABN], [Manager_Name], [Manager_ABN], [Servicer_Name], [Servicer_ABN], 
[Liquidity_Provider_Name], [Liquidity_Provider_ABN], [LMI_Provider_Name], [LMI_Provider_ABN], [Paying_Agent_Name], [Paying_Agent_ABN], 
[Swap_or_Hedge_Provider_Name], [Swap_or_Hedge_Provider_ABN], [Swap_or_Hedge_Type], [Governing_Law], [Trigger_Name], [Revolving_Period_Close_Date], 
[Trigger_Description], [Pre_Funding_Close_Date], [Asset_Substitution_Close_Date], [Collection_Period_Frequency], [Remittance_Frequency], 
[Senior_Fees_and_Expenses], [Credit_Enhancement_Name], [Account_Provider_Name], [Ending_Balance], [Target_Balance], [Initial_Amount_Available], 
[Beginning_Amount_Available_Liquidity], [Ending_Amount_Available_Liquidity], [Notional_Principal_Amount], [Collection_Period_Begin_Date], 
[Collection_Period_End_Date], [Loans_Insured], [Timely_Payment_Claims_Outstanding], [Responsible_Party_Jurisdiction], [Responsible_Party_ID], 
[Responsible_Party_ID_Descriptor], [Issuing_Trustee_Jurisdiction], [Issuing_Trustee_ID], [Issuing_Trustee_ID_Descriptor], [Issuing_Trustee_Fee_Currency], 
[Issuing_Trustee_Fee], [Indenture_or_Security_Trustee_Jurisdiction], [Indenture_or_Security_Trustee_ID], [Indenture_or_Security_Trustee_ID_Descriptor], 
[Indenture_or_Security_Trustee_Fee_Currency], [Indenture_or_Security_Trustee_Fee], [Sponsor_Jurisdiction], [Sponsor_ID], [Sponsor_ID_Descriptor], 
[Arranger_Jurisdiction], [Arranger_ID], [Arranger_ID_Descriptor], [Underwriter_Jurisdiction], [Underwriter_ID], [Underwriter_ID_Descriptor], 
[Manager_Jurisdiction], [Manager_ID], [Manager_ID_Descriptor], [Manager_Fee_Currency], [Manager_Fee], [Paying_Agent_Jurisdiction], [Paying_Agent_ID], 
[Paying_Agent_ID_Descriptor], [Paying_Agent_Fee_Currency], [Paying_Agent_Fee], [Originator_Name], [Originator_ABN], [Originator_Jurisdiction], 
[Originator_ID], [Originator_ID_Descriptor], [Loans_Originated_Settlement_Date], [Loans_Originated], [Seller_Name], [Seller_ABN], [Seller_Jurisdiction], 
[Seller_ID], [Seller_ID_Descriptor], [Loans_Sold_Settlement_Date], [Loans_Sold], [Servicer_Jurisdiction], [Servicing_Fee_Currency], [Servicing_Fee], 
[Liquidity_Provider_Jurisdiction], [Liquidity_Provider_ID], [Liquidity_Provider_ID_Descriptor], [Liquidity_Facility_Name], [Liquidity_Facility_Type], 
[Reimbursements], [Draws_Liquidity], [Reduction_Liquidity], [Beginning_Amount_Drawn_Liquidity], [Ending_Amount_Drawn_Liquidity], [Facility_Currency], 
[Facility_Fee], [Credit_Enhancement_Provider_Name], [Credit_Enhancement_Provider_ABN], [Credit_Enhancement_Provider_Jurisdiction], 
[Credit_Enhancement_Provider_ID], [Credit_Enhancement_Provider_ID_Descriptor], [Credit_Enhancement_Type], [Credit_Enhancement_Currency], 
[Credit_Enhancement_Fee], [Initial_Credit_Enhancement_Amount], [Beginning_Amount_Available_Credit], [Restorations_Credit], [Draws_Credit], 
[Reduction_Credit], [Ending_Amount_Available_Credit], [Beginning_Amount_Drawn_Credit], [Ending_Amount_Drawn_Credit], [LMI_Provider_Jurisdiction], 
[LMI_Provider_ID], [LMI_Provider_ID_Descriptor], [Loan_Currency_Insurance], [Loans_Insured_Settlement_Date], [Loans_Insured_TPC_Settlement_Date], 
[Loans_Insured_TPC], [LMI_Claims_Outstanding], [Swap_or_Hedge_Provider_Jurisdiction], [Swap_or_Hedge_Provider_ID], [Swap_or_Hedge_Provider_ID_Descriptor], 
[Swap_or_Hedge_Name], [Pay_Leg_Currency], [Pay_Leg_Reference_Index], [Pay_Leg_Reference_Index_Value], [Pay_Leg_Margin_or_Rate], [Pay_Leg_Amount], 
[Receive_Leg_Currency], [Receive_Leg_Reference_Index], [Receive_Leg_Reference_Index_Value], [Receive_Leg_Margin_or_Rate], [Receive_Leg_Amount], 
[Exchange_Rate], [Swap_or_Hedge_Fee_Currency], [Swap_or_Hedge_Fee], [Trigger_State], [Account_Provider_ABN], [Account_Provider_Jurisdiction], 
[Account_Provider_ID], [Account_Provider_ID_Descriptor], [Account_Currency], [Account_Fee], [Beginning_Balance], [Signature_Authority_Name], 
[Signature_Authority_ABN], [Signature_Authority_Jurisdiction], [Signature_Authority_ID], [Signature_Authority_ID_Descriptor], [Authority_Type], 
[Authority_Holder_Name], [Authority_Holder_ABN], [Authority_Holder_Jurisdiction], [Authority_Holder_ID], [Authority_Holder_ID_Descriptor], 
[Eligible_Loan_Criteria], [Next_Report_Date], [Trust_Settlement_Date], [Servicing_Role], [Account_Type], [Role_Name], [Account_Name], 
[Account_Signature_Authority_Name], [Servicer_ID], [Servicer_ID_Descriptor], [Other_Role_Name], [Other_Role_ABN], [Other_Role_Jurisdiction], 
[Other_Role_ID], [Other_Role_ID_Descriptor], [Other_Role_Fee_Currency], [Other_Role_Fee], [Arranger_Fee_Currency], [Arranger_Fee], 
[Underwriter_Fee_Currency], [Underwriter_Fee], [Responsible_Party_Fee_Currency], [Responsible_Party_Fee]
from DV.s_TRAN_v00 
inner join DV.l_TRAN_v00 on s_TRAN_v00.[hk_l_TRAN_v00]=l_TRAN_v00.[hk_l_TRAN_v00] 
inner join DV.s_TRAN_rts on s_TRAN_rts.[hk_l_TRAN_v00]=l_TRAN_v00.[hk_l_TRAN_v00] and s_TRAN_rts.[feed_name] = (select distinct [feed_name] from [INGEST].[Ext_Stage_TRAN])
inner join DV.h_feed_master on h_feed_master.hk_h_feed_master=l_TRAN_v00.hk_h_feed_master
inner join DV.h_reporting_period on h_reporting_period.hk_h_reporting_period=l_TRAN_v00.hk_h_reporting_period 
inner join DV.h_transaction on h_transaction.hk_h_transaction=l_TRAN_v00.hk_h_transaction
except
SELECT
*
FROM [INGEST].[Ext_Stage_TRAN];