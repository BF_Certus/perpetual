
IF OBJECT_ID ('DV.load_audit_sp', 'U') IS NOT NULL  
   DROP TABLE DV.load_audit_sp;  
go  

create table DV.load_audit_sp 
(
	[Audit_id] bigint not null identity(1,1),
	[Procedue_Name] [varchar](128) NOT NULL,
	[BatchId]	[int] NULL,
	[Started] [datetime2] NOT NULL,
	[Finished] [datetime2] NULL,
	[Run_Sequence] [int] NULL,
	[Status] [varchar](256) NULL,
	[Message] [varchar](4000) NULL,
	[Rows_Affected] [int] NULL,
	CONSTRAINT load_audit_sp_PK primary key clustered (Audit_id)
)
go


--select * from INGEST.dv_load_audit_sp;

