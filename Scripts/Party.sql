/****** Object:  View [INGEST].[Party]    Script Date: 18/06/2018 12:17:56 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--Select * FROM INGEST.Party
--Drop view INGEST.Party;
Create View  [INGEST].[Party] as 

Select ClientID, Party_Name, Feed_Name
From (
Select  ClientID, Servicer_Name as Party_Name, Feed_Name
FROM Ingest.RMBS_v00 

Union Select ClientID,  Originator_Name as Party_Name, Feed_Name
FROM Ingest.RMBS_v00 

Union Select  ClientID, Seller_Name as Party_Name, Feed_Name
FROM Ingest.RMBS_v00 

--LMI Provider Name is not included here as it 
--Union Select LMI_Provider_Name as Party_Name, Feed_Name  --Select * 
--FROM Ingest.stage_RMBS_v00

) A 
Group by ClientID, Party_Name, Feed_Name
GO