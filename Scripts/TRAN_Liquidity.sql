/****** Object:  View [INGEST].[TRAN_Liquidity]    Script Date: 4/07/2018 11:01:25 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--Drop view [INGEST].[TRAN_Liquidity]
--go
Create View [INGEST].[TRAN_Liquidity] as 
 
  WITH Nbrs ( Number ) AS (
    SELECT 1 UNION ALL
    SELECT 1 + Number FROM Nbrs WHERE Number < 99
)
SELECT h_cm.ClientID, h_fm.FeedID, h_rp.Reporting_Period, h_t.Master_Trust_Name_or_SPV, h_t.Series_Trust_Name_or_Series, h_t.Transaction_ID,
DelimCount as Liquidity_Count, N.Number as Liquidity_Position,
Case when DelimCount = 0 then Liquidity_Provider_Name else SubString(  Liquidity_Provider_Name , dbo.FN_CERTUS_INSTR(  Liquidity_Provider_Name ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Liquidity_Provider_Name,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Liquidity_Provider_Name ,';',1,N.Number-1)-1) end as Liquidity_Provider_Name,
Case when DelimCount = 0 then Liquidity_Provider_ABN else SubString(  Liquidity_Provider_ABN , dbo.FN_CERTUS_INSTR(  Liquidity_Provider_ABN ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Liquidity_Provider_ABN,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Liquidity_Provider_ABN ,';',1,N.Number-1)-1) end as Liquidity_Provider_ABN,
Case when DelimCount = 0 then Initial_Amount_Available else SubString(  Initial_Amount_Available , dbo.FN_CERTUS_INSTR(  Initial_Amount_Available ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Initial_Amount_Available,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Initial_Amount_Available ,';',1,N.Number-1)-1) end as Initial_Amount_Available,
Case when DelimCount = 0 then Beginning_Amount_Available_Liquidity else SubString(  Beginning_Amount_Available_Liquidity , dbo.FN_CERTUS_INSTR(  Beginning_Amount_Available_Liquidity ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Beginning_Amount_Available_Liquidity,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Beginning_Amount_Available_Liquidity ,';',1,N.Number-1)-1) end as Beginning_Amount_Available_Liquidity,
Case when DelimCount = 0 then Ending_Amount_Available_Liquidity else SubString(  Ending_Amount_Available_Liquidity , dbo.FN_CERTUS_INSTR(  Ending_Amount_Available_Liquidity ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Ending_Amount_Available_Liquidity,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Ending_Amount_Available_Liquidity ,';',1,N.Number-1)-1) end as Ending_Amount_Available_Liquidity,
Case when DelimCount = 0 then Liquidity_Provider_Jurisdiction else SubString(  Liquidity_Provider_Jurisdiction , dbo.FN_CERTUS_INSTR(  Liquidity_Provider_Jurisdiction ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Liquidity_Provider_Jurisdiction,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Liquidity_Provider_Jurisdiction ,';',1,N.Number-1)-1) end as Liquidity_Provider_Jurisdiction,
Case when DelimCount = 0 then Liquidity_Provider_ID else SubString(  Liquidity_Provider_ID , dbo.FN_CERTUS_INSTR(  Liquidity_Provider_ID ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Liquidity_Provider_ID,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Liquidity_Provider_ID ,';',1,N.Number-1)-1) end as Liquidity_Provider_ID,
Case when DelimCount = 0 then Liquidity_Provider_ID_Descriptor else SubString(  Liquidity_Provider_ID_Descriptor , dbo.FN_CERTUS_INSTR(  Liquidity_Provider_ID_Descriptor ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Liquidity_Provider_ID_Descriptor,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Liquidity_Provider_ID_Descriptor ,';',1,N.Number-1)-1) end as Liquidity_Provider_ID_Descriptor,
Case when DelimCount = 0 then Liquidity_Facility_Name else SubString(  Liquidity_Facility_Name , dbo.FN_CERTUS_INSTR(  Liquidity_Facility_Name ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Liquidity_Facility_Name,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Liquidity_Facility_Name ,';',1,N.Number-1)-1) end as Liquidity_Facility_Name,
Case when DelimCount = 0 then Liquidity_Facility_Type else SubString(  Liquidity_Facility_Type , dbo.FN_CERTUS_INSTR(  Liquidity_Facility_Type ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Liquidity_Facility_Type,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Liquidity_Facility_Type ,';',1,N.Number-1)-1) end as Liquidity_Facility_Type,
Case when DelimCount = 0 then Reimbursements else SubString(  Reimbursements , dbo.FN_CERTUS_INSTR(  Reimbursements ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Reimbursements,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Reimbursements ,';',1,N.Number-1)-1) end as Reimbursements,
Case when DelimCount = 0 then Draws_Liquidity else SubString(  Draws_Liquidity , dbo.FN_CERTUS_INSTR(  Draws_Liquidity ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Draws_Liquidity,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Draws_Liquidity ,';',1,N.Number-1)-1) end as Draws_Liquidity,
Case when DelimCount = 0 then Reduction_Liquidity else SubString(  Reduction_Liquidity , dbo.FN_CERTUS_INSTR(  Reduction_Liquidity ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Reduction_Liquidity,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Reduction_Liquidity ,';',1,N.Number-1)-1) end as Reduction_Liquidity,
Case when DelimCount = 0 then Beginning_Amount_Drawn_Liquidity else SubString(  Beginning_Amount_Drawn_Liquidity , dbo.FN_CERTUS_INSTR(  Beginning_Amount_Drawn_Liquidity ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Beginning_Amount_Drawn_Liquidity,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Beginning_Amount_Drawn_Liquidity ,';',1,N.Number-1)-1) end as Beginning_Amount_Drawn_Liquidity,
Case when DelimCount = 0 then Ending_Amount_Drawn_Liquidity else SubString(  Ending_Amount_Drawn_Liquidity , dbo.FN_CERTUS_INSTR(  Ending_Amount_Drawn_Liquidity ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Ending_Amount_Drawn_Liquidity,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Ending_Amount_Drawn_Liquidity ,';',1,N.Number-1)-1) end as Ending_Amount_Drawn_Liquidity,
Case when DelimCount = 0 then Facility_Currency else SubString(  Facility_Currency , dbo.FN_CERTUS_INSTR(  Facility_Currency ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Facility_Currency,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Facility_Currency ,';',1,N.Number-1)-1) end as Facility_Currency,
Case when DelimCount = 0 then Facility_Fee else SubString(  Facility_Fee , dbo.FN_CERTUS_INSTR(  Facility_Fee ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Facility_Fee,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Facility_Fee ,';',1,N.Number-1)-1) end as Facility_Fee
from 
	DV.S_TRAN_v00 s
	inner join 
	DV.L_TRAN_v00 l on s.hk_l_TRAN_v00=l.hk_l_TRAN_v00
	inner join
	DV.h_client_master h_cm on h_cm.hk_h_client_master=l.hk_h_client_master
	inner join
	DV.h_feed_master h_fm on h_fm.hk_h_feed_master=l.hk_h_feed_master
	inner join
	DV.h_reporting_period h_rp on h_rp.hk_h_reporting_period=l.hk_h_reporting_period
	inner join
	DV.h_transaction h_t on h_t.hk_h_transaction=l.hk_h_transaction
	inner join
	(select TRAN_v00_DelimiterCount.hk_l_TRAN_v00, case 
		when Delim_Liquidity_Provider_Name=Delim_Liquidity_Provider_ABN
		and Delim_Liquidity_Provider_Name=Delim_Initial_Amount_Available
		and Delim_Liquidity_Provider_Name=Delim_Beginning_Amount_Available_Liquidity
		and Delim_Liquidity_Provider_Name=Delim_Ending_Amount_Available_Liquidity
		and Delim_Liquidity_Provider_Name=Delim_Liquidity_Provider_Jurisdiction
		and Delim_Liquidity_Provider_Name=Delim_Liquidity_Provider_ID
		and Delim_Liquidity_Provider_Name=Delim_Liquidity_Provider_ID_Descriptor
		and Delim_Liquidity_Provider_Name=Delim_Liquidity_Facility_Name
		and Delim_Liquidity_Provider_Name=Delim_Liquidity_Facility_Type
		and Delim_Liquidity_Provider_Name=Delim_Reimbursements
		and Delim_Liquidity_Provider_Name=Delim_Draws_Liquidity
		and Delim_Liquidity_Provider_Name=Delim_Reduction_Liquidity
		and Delim_Liquidity_Provider_Name=Delim_Beginning_Amount_Drawn_Liquidity
		and Delim_Liquidity_Provider_Name=Delim_Ending_Amount_Drawn_Liquidity
		and Delim_Liquidity_Provider_Name=Delim_Facility_Currency
		and Delim_Liquidity_Provider_Name=Delim_Facility_Fee
		then Delim_Liquidity_Provider_Name+1 end as DelimCount from INGEST.TRAN_v00_DelimiterCount) delim on delim.hk_l_TRAN_v00=l.hk_l_TRAN_v00
	inner join
	Nbrs N ON (N.Number <= DelimCount) or (DelimCount = 0 and N.Number =1)

GO


