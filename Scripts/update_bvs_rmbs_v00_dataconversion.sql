/****** Object:  StoredProcedure [dbo].[update_bvs_rmbs_v00_dataconversion]    Script Date: 25/06/2018 1:21:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--==============================================================================
-- DBMS Name        :    SQL Server
-- Procedure Name   :    update_bvs_rmbs_v00_dataconversion
-- Template         :    cert_wsl_sqlserver_proc_dv_perm
-- Template Version :    6.9.1.0
-- Description      :    Update the Satellite table bvs_rmbs_v00_dataconversion
-- Generated by     :    WhereScape RED Version 8.0.1.0 (build 171017-084020)
-- Generated for    :    Certus Evaluate until 01 July 2018
-- Generated on     :    Monday, June 25, 2018 at 13:16:49
-- Author           :    Bronwen
--==============================================================================
-- Notes / History
--


--
CREATE PROCEDURE [dbo].[update_bvs_rmbs_v00_dataconversion]
  @p_sequence         INTEGER
, @p_job_name         VARCHAR(256)
, @p_task_name        VARCHAR(256)
, @p_job_id           INTEGER
, @p_task_id          INTEGER
, @p_return_msg       VARCHAR(256) OUTPUT
, @p_status           INTEGER      OUTPUT

AS
  SET XACT_ABORT OFF  -- Turn off auto abort on errors
  SET NOCOUNT ON      -- Turn off row count messages

  --=====================================================
  -- Control variables used in most programs
  --=====================================================
  DECLARE
    @v_msgtext               VARCHAR(255)  -- Text for audit_trail
  , @v_sql                   NVARCHAR(255) -- Text for SQL statements
  , @v_step                  INTEGER       -- return code
  , @v_insert_count          INTEGER       -- no of records inserted
  , @v_return_status         INTEGER       -- Update result status
  , @v_current_datetime      DATETIME      -- Used for date insert

  --=====================================================
  -- General Variables
  --=====================================================


  --=====================================================
  -- MAIN
  --=====================================================
  DECLARE @v_joinColumn VARCHAR(MAX); 
  DECLARE @v_hubKey VARCHAR(MAX); 
  DECLARE @v_srcTable VARCHAR(MAX); 
  DECLARE @SQL VARCHAR(max); 
  DECLARE @v_insert_msg VARCHAR(max); 
  DECLARE 
  @v_ParentTable   nvarchar(256)      --  Target Table Name
, @v_ParentColumn   nvarchar(256)  
, @v_sql2    nvarchar(4000) 
, @v_sql5    nvarchar(4000) 
, @v_sql6   nvarchar(4000) 
, @v_sql7   nvarchar(4000) 
, @v_tabtype   nvarchar(4000) 

  SET @v_insert_msg = ''
  SET @v_step = 100

  SET @v_insert_count = 0
  SET @v_current_datetime = GETDATE()


  BEGIN TRY

    --=====================================================
    -- Insert new records
    --=====================================================

    SET @v_step = 200



    BEGIN TRANSACTION



    

---------------------------------------------------
    
-- 
-- Load All Hubs 



--    Loading all hubs regardless where we load a satellite
--    walk the columns 
--    find the hk_ columns
--    if hk column then find the source table column and the business keys
--    we have to save them somewhere and the 


    

       	set @v_tabtype=0
	   SET @v_step = 300


 INSERT INTO DV.bvs_rmbs_v00_dataconversion
      ( dss_load_datetime
      , dss_change_hash
      , dss_record_source
      , dss_start_date
      , dss_create_time
      , hk_l_rmbs_v00
      , collateral_date
      , most_recent_approval_amount
      , current_balance
      , scheduled_balance
      , loan_guarantee_flag
      , redraw_feature_flag
      , available_redraw
      , offset_account_flag
      , offset_account_balance
      , conforming_mortgage
      , origination_date_settlement_date
      , loan_term
      , remaining_term
      , seasoning
      , maturity_date
      , loan_securitised_date
      , original_ltv
      , current_ltv
      , scheduled_ltv
      , scheduled_minimum_payment
      , lmi_attachment_point
      , variable_loan_amount
      , current_interest_rate
      , reference_index_value
      , interest_margin
      , restructuring_arrangement_flag
      , interest_only_expiry_date
      , current_rate_expiry_date
      , default_balance
      , foreclosure_proceeds
      , date_of_foreclosure
      , loss_on_sale
      , days_in_arrears
      , amount_in_arrears
      , days_since_last_scheduled_payment
      , cumulative_missed_payments
      , claim_submitted_to_lmi
      , claim_paid_by_lmi
      , claim_denied_by_lmi
      , claim_pending_with_lmi
      , original_property_value
      , most_recent_property_value
      , most_recent_property_valuation_date
      , first_home_buyer_flag
      , employment_type_secondary_borrower
      , borrower_type_primary_borrower
      , borrower_type_secondary_borrower
      , legal_entity_type_secondary_borrower
      , bankruptcy_flag
      , last_credit_discharge_date
      , number_of_debtors
      , credit_score_secondary_borrower
      , debt_serviceability_metric_score
      , income
      , secondary_income
      , income_verification_for_secondary_income
      , abs_statistical_area
      , interest_rate
      , origination_date)
    SELECT DISTINCT    
stage_bvrmbs_v00_dataconversion.dss_load_datetime AS dss_load_datetime 
      , stage_bvrmbs_v00_dataconversion.dss_change_hash_stage_bvrmbs_v00_dataconversion AS dss_change_hash 
      , stage_bvrmbs_v00_dataconversion.dss_record_source AS dss_record_source 
      , @v_current_datetime AS dss_start_date 
      , @v_current_datetime AS dss_create_time 
      , stage_bvrmbs_v00_dataconversion.hk_l_rmbs_v00 AS hk_l_rmbs_v00 
      , stage_bvrmbs_v00_dataconversion.collateral_date AS collateral_date 
      , stage_bvrmbs_v00_dataconversion.most_recent_approval_amount AS most_recent_approval_amount 
      , stage_bvrmbs_v00_dataconversion.current_balance AS current_balance 
      , stage_bvrmbs_v00_dataconversion.scheduled_balance AS scheduled_balance 
      , stage_bvrmbs_v00_dataconversion.loan_guarantee_flag AS loan_guarantee_flag 
      , stage_bvrmbs_v00_dataconversion.redraw_feature_flag AS redraw_feature_flag 
      , stage_bvrmbs_v00_dataconversion.available_redraw AS available_redraw 
      , stage_bvrmbs_v00_dataconversion.offset_account_flag AS offset_account_flag 
      , stage_bvrmbs_v00_dataconversion.offset_account_balance AS offset_account_balance 
      , stage_bvrmbs_v00_dataconversion.conforming_mortgage AS conforming_mortgage 
      , stage_bvrmbs_v00_dataconversion.origination_date_settlement_date AS origination_date_settlement_date 
      , stage_bvrmbs_v00_dataconversion.loan_term AS loan_term 
      , stage_bvrmbs_v00_dataconversion.remaining_term AS remaining_term 
      , stage_bvrmbs_v00_dataconversion.seasoning AS seasoning 
      , stage_bvrmbs_v00_dataconversion.maturity_date AS maturity_date 
      , stage_bvrmbs_v00_dataconversion.loan_securitised_date AS loan_securitised_date 
      , stage_bvrmbs_v00_dataconversion.original_ltv AS original_ltv 
      , stage_bvrmbs_v00_dataconversion.current_ltv AS current_ltv 
      , stage_bvrmbs_v00_dataconversion.scheduled_ltv AS scheduled_ltv 
      , stage_bvrmbs_v00_dataconversion.scheduled_minimum_payment AS scheduled_minimum_payment 
      , stage_bvrmbs_v00_dataconversion.lmi_attachment_point AS lmi_attachment_point 
      , stage_bvrmbs_v00_dataconversion.variable_loan_amount AS variable_loan_amount 
      , stage_bvrmbs_v00_dataconversion.current_interest_rate AS current_interest_rate 
      , stage_bvrmbs_v00_dataconversion.reference_index_value AS reference_index_value 
      , stage_bvrmbs_v00_dataconversion.interest_margin AS interest_margin 
      , stage_bvrmbs_v00_dataconversion.restructuring_arrangement_flag AS restructuring_arrangement_flag 
      , stage_bvrmbs_v00_dataconversion.interest_only_expiry_date AS interest_only_expiry_date 
      , stage_bvrmbs_v00_dataconversion.current_rate_expiry_date AS current_rate_expiry_date 
      , stage_bvrmbs_v00_dataconversion.default_balance AS default_balance 
      , stage_bvrmbs_v00_dataconversion.foreclosure_proceeds AS foreclosure_proceeds 
      , stage_bvrmbs_v00_dataconversion.date_of_foreclosure AS date_of_foreclosure 
      , stage_bvrmbs_v00_dataconversion.loss_on_sale AS loss_on_sale 
      , stage_bvrmbs_v00_dataconversion.days_in_arrears AS days_in_arrears 
      , stage_bvrmbs_v00_dataconversion.amount_in_arrears AS amount_in_arrears 
      , stage_bvrmbs_v00_dataconversion.days_since_last_scheduled_payment AS days_since_last_scheduled_payment 
      , stage_bvrmbs_v00_dataconversion.cumulative_missed_payments AS cumulative_missed_payments 
      , stage_bvrmbs_v00_dataconversion.claim_submitted_to_lmi AS claim_submitted_to_lmi 
      , stage_bvrmbs_v00_dataconversion.claim_paid_by_lmi AS claim_paid_by_lmi 
      , stage_bvrmbs_v00_dataconversion.claim_denied_by_lmi AS claim_denied_by_lmi 
      , stage_bvrmbs_v00_dataconversion.claim_pending_with_lmi AS claim_pending_with_lmi 
      , stage_bvrmbs_v00_dataconversion.original_property_value AS original_property_value 
      , stage_bvrmbs_v00_dataconversion.most_recent_property_value AS most_recent_property_value 
      , stage_bvrmbs_v00_dataconversion.most_recent_property_valuation_date AS most_recent_property_valuation_date 
      , stage_bvrmbs_v00_dataconversion.first_home_buyer_flag AS first_home_buyer_flag 
      , stage_bvrmbs_v00_dataconversion.employment_type_secondary_borrower AS employment_type_secondary_borrower 
      , stage_bvrmbs_v00_dataconversion.borrower_type_primary_borrower AS borrower_type_primary_borrower 
      , stage_bvrmbs_v00_dataconversion.borrower_type_secondary_borrower AS borrower_type_secondary_borrower 
      , stage_bvrmbs_v00_dataconversion.legal_entity_type_secondary_borrower AS legal_entity_type_secondary_borrower 
      , stage_bvrmbs_v00_dataconversion.bankruptcy_flag AS bankruptcy_flag 
      , stage_bvrmbs_v00_dataconversion.last_credit_discharge_date AS last_credit_discharge_date 
      , stage_bvrmbs_v00_dataconversion.number_of_debtors AS number_of_debtors 
      , stage_bvrmbs_v00_dataconversion.credit_score_secondary_borrower AS credit_score_secondary_borrower 
      , stage_bvrmbs_v00_dataconversion.debt_serviceability_metric_score AS debt_serviceability_metric_score 
      , stage_bvrmbs_v00_dataconversion.income AS income 
      , stage_bvrmbs_v00_dataconversion.secondary_income AS secondary_income 
      , stage_bvrmbs_v00_dataconversion.income_verification_for_secondary_income AS income_verification_for_secondary_income 
      , stage_bvrmbs_v00_dataconversion.abs_statistical_area AS abs_statistical_area 
      , stage_bvrmbs_v00_dataconversion.interest_rate AS interest_rate 
      , stage_bvrmbs_v00_dataconversion.origination_date AS origination_date 
   
    FROM [Stage].[stage_bvrmbs_v00_dataconversion] stage_bvrmbs_v00_dataconversion
WHERE EXISTS( 
SELECT 1 
FROM	   	  	   	  	   	   
	   	  STAGE.stage_bvrmbs_v00_dataconversion SRC
		   WHERE NOT EXISTS( 
	  SELECT 1 
FROM ( 
 SELECT 
   bvs_rmbs_v00_dataconversion.hk_l_rmbs_v00
  ,bvs_rmbs_v00_dataconversion.dss_change_hash
 FROM   DV.bvs_rmbs_v00_dataconversion bvs_rmbs_v00_dataconversion 
 INNER JOIN (
     SELECT 
          bvs_rmbs_v00_dataconversion.hk_l_rmbs_v00
	   ,MAX(bvs_rmbs_v00_dataconversion.dss_start_date) AS dss_start_date
	   ,MAX(bvs_rmbs_v00_dataconversion.dss_version) AS dss_version
     FROM   DV.bvs_rmbs_v00_dataconversion bvs_rmbs_v00_dataconversion 
     WHERE EXISTS( 
	   SELECT 1  
	   FROM	   	  	   	  	   	   
	   	  STAGE.stage_bvrmbs_v00_dataconversion stage_bvrmbs_v00_dataconversion 
	   WHERE 
              stage_bvrmbs_v00_dataconversion.hk_l_rmbs_v00 = bvs_rmbs_v00_dataconversion.hk_l_rmbs_v00
	   ) GROUP BY 
               bvs_rmbs_v00_dataconversion.hk_l_rmbs_v00
     ) MAX_ROWS ON 
	    MAX_ROWS.hk_l_rmbs_v00=bvs_rmbs_v00_dataconversion.hk_l_rmbs_v00  
	    AND  MAX_ROWS.dss_start_date=bvs_rmbs_v00_dataconversion.dss_start_date  
 ) CURRENT_ROWS 
	WHERE  
		 stage_bvrmbs_v00_dataconversion.hk_l_rmbs_v00 = CURRENT_ROWS.hk_l_rmbs_v00
		 AND stage_bvrmbs_v00_dataconversion.dss_change_hash_stage_bvrmbs_v00_dataconversion = CURRENT_ROWS.dss_change_hash
	 )and SRC.hk_l_rmbs_v00=stage_bvrmbs_v00_dataconversion.hk_l_rmbs_v00 )
 
        SELECT @v_insert_count = @@ROWCOUNT

         SELECT @v_insert_count = @@ROWCOUNT

        SET @v_insert_msg = @v_insert_msg  + 'bvs_rmbs_v00_dataconversion updated. ' + CONVERT(VARCHAR,@v_insert_count) + ' new records.'
  

        ;

 INSERT INTO DV.bvs_rmbs_v00_dataconversion
      ( dss_load_datetime
      , dss_change_hash
      , dss_record_source
      , dss_start_date
      , dss_create_time
      , hk_l_rmbs_v00
      , collateral_date
      , most_recent_approval_amount
      , current_balance
      , scheduled_balance
      , loan_guarantee_flag
      , redraw_feature_flag
      , available_redraw
      , offset_account_flag
      , offset_account_balance
      , conforming_mortgage
      , origination_date_settlement_date
      , loan_term
      , remaining_term
      , seasoning
      , maturity_date
      , loan_securitised_date
      , original_ltv
      , current_ltv
      , scheduled_ltv
      , scheduled_minimum_payment
      , lmi_attachment_point
      , variable_loan_amount
      , current_interest_rate
      , reference_index_value
      , interest_margin
      , restructuring_arrangement_flag
      , interest_only_expiry_date
      , current_rate_expiry_date
      , default_balance
      , foreclosure_proceeds
      , date_of_foreclosure
      , loss_on_sale
      , days_in_arrears
      , amount_in_arrears
      , days_since_last_scheduled_payment
      , cumulative_missed_payments
      , claim_submitted_to_lmi
      , claim_paid_by_lmi
      , claim_denied_by_lmi
      , claim_pending_with_lmi
      , original_property_value
      , most_recent_property_value
      , most_recent_property_valuation_date
      , first_home_buyer_flag
      , employment_type_secondary_borrower
      , borrower_type_primary_borrower
      , borrower_type_secondary_borrower
      , legal_entity_type_secondary_borrower
      , bankruptcy_flag
      , last_credit_discharge_date
      , number_of_debtors
      , credit_score_secondary_borrower
      , debt_serviceability_metric_score
      , income
      , secondary_income
      , income_verification_for_secondary_income
      , abs_statistical_area
      , interest_rate
      , origination_date)
SELECT DISTINCT    
stage_bvrmbs_v00_dataconversion.dss_load_datetime AS dss_load_datetime 
      , stage_bvrmbs_v00_dataconversion.dss_change_hash_stage_bvrmbs_v00_dataconversion AS dss_change_hash 
      , stage_bvrmbs_v00_dataconversion.dss_record_source AS dss_record_source 
      , @v_current_datetime AS dss_start_date 
      , @v_current_datetime AS dss_create_time 
      , stage_bvrmbs_v00_dataconversion.hk_l_rmbs_v00 AS hk_l_rmbs_v00 
      , stage_bvrmbs_v00_dataconversion.collateral_date AS collateral_date 
      , stage_bvrmbs_v00_dataconversion.most_recent_approval_amount AS most_recent_approval_amount 
      , stage_bvrmbs_v00_dataconversion.current_balance AS current_balance 
      , stage_bvrmbs_v00_dataconversion.scheduled_balance AS scheduled_balance 
      , stage_bvrmbs_v00_dataconversion.loan_guarantee_flag AS loan_guarantee_flag 
      , stage_bvrmbs_v00_dataconversion.redraw_feature_flag AS redraw_feature_flag 
      , stage_bvrmbs_v00_dataconversion.available_redraw AS available_redraw 
      , stage_bvrmbs_v00_dataconversion.offset_account_flag AS offset_account_flag 
      , stage_bvrmbs_v00_dataconversion.offset_account_balance AS offset_account_balance 
      , stage_bvrmbs_v00_dataconversion.conforming_mortgage AS conforming_mortgage 
      , stage_bvrmbs_v00_dataconversion.origination_date_settlement_date AS origination_date_settlement_date 
      , stage_bvrmbs_v00_dataconversion.loan_term AS loan_term 
      , stage_bvrmbs_v00_dataconversion.remaining_term AS remaining_term 
      , stage_bvrmbs_v00_dataconversion.seasoning AS seasoning 
      , stage_bvrmbs_v00_dataconversion.maturity_date AS maturity_date 
      , stage_bvrmbs_v00_dataconversion.loan_securitised_date AS loan_securitised_date 
      , stage_bvrmbs_v00_dataconversion.original_ltv AS original_ltv 
      , stage_bvrmbs_v00_dataconversion.current_ltv AS current_ltv 
      , stage_bvrmbs_v00_dataconversion.scheduled_ltv AS scheduled_ltv 
      , stage_bvrmbs_v00_dataconversion.scheduled_minimum_payment AS scheduled_minimum_payment 
      , stage_bvrmbs_v00_dataconversion.lmi_attachment_point AS lmi_attachment_point 
      , stage_bvrmbs_v00_dataconversion.variable_loan_amount AS variable_loan_amount 
      , stage_bvrmbs_v00_dataconversion.current_interest_rate AS current_interest_rate 
      , stage_bvrmbs_v00_dataconversion.reference_index_value AS reference_index_value 
      , stage_bvrmbs_v00_dataconversion.interest_margin AS interest_margin 
      , stage_bvrmbs_v00_dataconversion.restructuring_arrangement_flag AS restructuring_arrangement_flag 
      , stage_bvrmbs_v00_dataconversion.interest_only_expiry_date AS interest_only_expiry_date 
      , stage_bvrmbs_v00_dataconversion.current_rate_expiry_date AS current_rate_expiry_date 
      , stage_bvrmbs_v00_dataconversion.default_balance AS default_balance 
      , stage_bvrmbs_v00_dataconversion.foreclosure_proceeds AS foreclosure_proceeds 
      , stage_bvrmbs_v00_dataconversion.date_of_foreclosure AS date_of_foreclosure 
      , stage_bvrmbs_v00_dataconversion.loss_on_sale AS loss_on_sale 
      , stage_bvrmbs_v00_dataconversion.days_in_arrears AS days_in_arrears 
      , stage_bvrmbs_v00_dataconversion.amount_in_arrears AS amount_in_arrears 
      , stage_bvrmbs_v00_dataconversion.days_since_last_scheduled_payment AS days_since_last_scheduled_payment 
      , stage_bvrmbs_v00_dataconversion.cumulative_missed_payments AS cumulative_missed_payments 
      , stage_bvrmbs_v00_dataconversion.claim_submitted_to_lmi AS claim_submitted_to_lmi 
      , stage_bvrmbs_v00_dataconversion.claim_paid_by_lmi AS claim_paid_by_lmi 
      , stage_bvrmbs_v00_dataconversion.claim_denied_by_lmi AS claim_denied_by_lmi 
      , stage_bvrmbs_v00_dataconversion.claim_pending_with_lmi AS claim_pending_with_lmi 
      , stage_bvrmbs_v00_dataconversion.original_property_value AS original_property_value 
      , stage_bvrmbs_v00_dataconversion.most_recent_property_value AS most_recent_property_value 
      , stage_bvrmbs_v00_dataconversion.most_recent_property_valuation_date AS most_recent_property_valuation_date 
      , stage_bvrmbs_v00_dataconversion.first_home_buyer_flag AS first_home_buyer_flag 
      , stage_bvrmbs_v00_dataconversion.employment_type_secondary_borrower AS employment_type_secondary_borrower 
      , stage_bvrmbs_v00_dataconversion.borrower_type_primary_borrower AS borrower_type_primary_borrower 
      , stage_bvrmbs_v00_dataconversion.borrower_type_secondary_borrower AS borrower_type_secondary_borrower 
      , stage_bvrmbs_v00_dataconversion.legal_entity_type_secondary_borrower AS legal_entity_type_secondary_borrower 
      , stage_bvrmbs_v00_dataconversion.bankruptcy_flag AS bankruptcy_flag 
      , stage_bvrmbs_v00_dataconversion.last_credit_discharge_date AS last_credit_discharge_date 
      , stage_bvrmbs_v00_dataconversion.number_of_debtors AS number_of_debtors 
      , stage_bvrmbs_v00_dataconversion.credit_score_secondary_borrower AS credit_score_secondary_borrower 
      , stage_bvrmbs_v00_dataconversion.debt_serviceability_metric_score AS debt_serviceability_metric_score 
      , stage_bvrmbs_v00_dataconversion.income AS income 
      , stage_bvrmbs_v00_dataconversion.secondary_income AS secondary_income 
      , stage_bvrmbs_v00_dataconversion.income_verification_for_secondary_income AS income_verification_for_secondary_income 
      , stage_bvrmbs_v00_dataconversion.abs_statistical_area AS abs_statistical_area 
      , stage_bvrmbs_v00_dataconversion.interest_rate AS interest_rate 
      , stage_bvrmbs_v00_dataconversion.origination_date AS origination_date 
FROM	   	  	   	  	   	   
	   	  STAGE.stage_bvrmbs_v00_dataconversion stage_bvrmbs_v00_dataconversion 
        WHERE EXISTS( 
	  SELECT 1 
FROM ( 
 SELECT 
   bvs_rmbs_v00_dataconversion.hk_l_rmbs_v00
  ,bvs_rmbs_v00_dataconversion.dss_change_hash
 FROM   DV.bvs_rmbs_v00_dataconversion bvs_rmbs_v00_dataconversion 
 INNER JOIN (
     SELECT 
          bvs_rmbs_v00_dataconversion.hk_l_rmbs_v00
	   ,MAX(bvs_rmbs_v00_dataconversion.dss_start_date) AS dss_start_date
	   ,MAX(bvs_rmbs_v00_dataconversion.dss_version) AS dss_version
     FROM   DV.bvs_rmbs_v00_dataconversion bvs_rmbs_v00_dataconversion 
     WHERE EXISTS( 
	   SELECT 1  
	   FROM	   	  	   	  	   	   
	   	  STAGE.stage_bvrmbs_v00_dataconversion stage_bvrmbs_v00_dataconversion 
	   WHERE 
              stage_bvrmbs_v00_dataconversion.hk_l_rmbs_v00 = bvs_rmbs_v00_dataconversion.hk_l_rmbs_v00
	   ) GROUP BY 
               bvs_rmbs_v00_dataconversion.hk_l_rmbs_v00
     ) MAX_ROWS ON 
	    MAX_ROWS.hk_l_rmbs_v00=bvs_rmbs_v00_dataconversion.hk_l_rmbs_v00  
	    AND  MAX_ROWS.dss_start_date=bvs_rmbs_v00_dataconversion.dss_start_date  
 ) CURRENT_ROWS 
	WHERE  NOT EXISTS( SELECT 1 FROM STAGE.stage_bvrmbs_v00_dataconversion  stage_bvrmbs_v00_dataconversion WHERE  
		 stage_bvrmbs_v00_dataconversion.hk_l_rmbs_v00 = CURRENT_ROWS.hk_l_rmbs_v00
		 AND stage_bvrmbs_v00_dataconversion.dss_change_hash_stage_bvrmbs_v00_dataconversion = CURRENT_ROWS.dss_change_hash
	 )and stage_bvrmbs_v00_dataconversion.hk_l_rmbs_v00=CURRENT_ROWS.hk_l_rmbs_v00 )
 
        SELECT @v_insert_count = @@ROWCOUNT





  COMMIT

    --=====================================================
    -- All Done report the results
    --=====================================================
    SET @v_step = 400


    SET @p_status = 1
    SET @p_return_msg = @v_insert_msg + 'bvs_rmbs_v00_dataconversion updated. '
      + CONVERT(VARCHAR,@v_insert_count) + ' new records.'

    RETURN 0


  END TRY
  BEGIN CATCH

    SET @p_status = -2
    SET @p_return_msg = SUBSTRING('update_bvs_rmbs_v00_dataconversion FAILED with error '
      + CONVERT(VARCHAR,ISNULL(ERROR_NUMBER(),0))
      + ' Step ' + CONVERT(VARCHAR,ISNULL(@v_step,0))
      + '. Error Msg: ' + ERROR_MESSAGE(),1,255)

  END CATCH
  IF XACT_STATE() <> 0
  BEGIN
    ROLLBACK TRANSACTION
  END

  RETURN 0

GO


