/****** Object:  View [INGEST].[RMBS_COL_v00_DataConversion]    Script Date: 6/07/2018 2:14:32 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--Drop view INGEST.RMBS_COL_v00_DataConversion
Create view [INGEST].[RMBS_COL_v00_DataConversion] as 
Select 
hk_l_rmbs_col_v00

, Try_Convert([int], Replace(Security_Property_Postcode,',','')) as Security_Property_Postcode 

, Try_Convert([decimal](18,2), Replace(Original_Security_Property_Value,',','')) as Original_Security_Property_Value 
, Try_Convert([decimal](18,2), Replace(Most_Recent_Security_Property_Value,',','')) as Most_Recent_Security_Property_Value 


,coalesce(Try_Convert([date], Most_Recent_Security_Property_Valuation_Date,105), Try_Convert([date], Most_Recent_Security_Property_Valuation_Date, 121)) as Most_Recent_Security_Property_Valuation_Date 



, Try_Convert([int], Replace(Abs_Statistical_Area,',','')) as Abs_Statistical_Area 

, Try_Convert([bit], Replace(Main_Security_Flag,',','')) as Main_Security_Flag 


FROM DV.s_rmbs_col_v00 
	where s_rmbs_col_v00.dss_load_datetime = (Select max(dss_load_datetime) from dv.s_rmbs_col_v00 z where z.hk_l_rmbs_col_v00 = s_rmbs_col_v00.hk_l_rmbs_col_v00)
GO


