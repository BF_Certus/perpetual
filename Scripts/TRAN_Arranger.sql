/****** Object:  View [INGEST].[TRAN_Arranger]    Script Date: 4/07/2018 11:01:25 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--Drop view [INGEST].[TRAN_Arranger]
--go
Create View [INGEST].[TRAN_Arranger] as 
 
  WITH Nbrs ( Number ) AS (
    SELECT 1 UNION ALL
    SELECT 1 + Number FROM Nbrs WHERE Number < 99
)
SELECT h_cm.ClientID, h_fm.FeedID, h_rp.Reporting_Period, h_t.Master_Trust_Name_or_SPV, h_t.Series_Trust_Name_or_Series, h_t.Transaction_ID,
DelimCount as Arranger_Count, N.Number as Arranger_Position,
Case when DelimCount = 0 then Arranger_Name else SubString(  Arranger_Name , dbo.FN_CERTUS_INSTR(  Arranger_Name ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Arranger_Name,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Arranger_Name ,';',1,N.Number-1)-1) end as Arranger_Name,
Case when DelimCount = 0 then Arranger_ABN else SubString(  Arranger_ABN , dbo.FN_CERTUS_INSTR(  Arranger_ABN ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Arranger_ABN,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Arranger_ABN ,';',1,N.Number-1)-1) end as Arranger_ABN,
Case when DelimCount = 0 then Arranger_Jurisdiction else SubString(  Arranger_Jurisdiction , dbo.FN_CERTUS_INSTR(  Arranger_Jurisdiction ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Arranger_Jurisdiction,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Arranger_Jurisdiction ,';',1,N.Number-1)-1) end as Arranger_Jurisdiction,
Case when DelimCount = 0 then Arranger_ID else SubString(  Arranger_ID , dbo.FN_CERTUS_INSTR(  Arranger_ID ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Arranger_ID,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Arranger_ID ,';',1,N.Number-1)-1) end as Arranger_ID,
Case when DelimCount = 0 then Arranger_ID_Descriptor else SubString(  Arranger_ID_Descriptor , dbo.FN_CERTUS_INSTR(  Arranger_ID_Descriptor ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Arranger_ID_Descriptor,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Arranger_ID_Descriptor ,';',1,N.Number-1)-1) end as Arranger_ID_Descriptor,
Case when DelimCount = 0 then Arranger_Fee_Currency else SubString(  Arranger_Fee_Currency , dbo.FN_CERTUS_INSTR(  Arranger_Fee_Currency ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Arranger_Fee_Currency,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Arranger_Fee_Currency ,';',1,N.Number-1)-1) end as Arranger_Fee_Currency,
Case when DelimCount = 0 then Arranger_Fee else SubString(  Arranger_Fee , dbo.FN_CERTUS_INSTR(  Arranger_Fee ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Arranger_Fee,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Arranger_Fee ,';',1,N.Number-1)-1) end as Arranger_Fee
from 
	DV.S_TRAN_v00 s
	inner join 
	DV.L_TRAN_v00 l on s.hk_l_TRAN_v00=l.hk_l_TRAN_v00
	inner join
	DV.h_client_master h_cm on h_cm.hk_h_client_master=l.hk_h_client_master
	inner join
	DV.h_feed_master h_fm on h_fm.hk_h_feed_master=l.hk_h_feed_master
	inner join
	DV.h_reporting_period h_rp on h_rp.hk_h_reporting_period=l.hk_h_reporting_period
	inner join
	DV.h_transaction h_t on h_t.hk_h_transaction=l.hk_h_transaction
	inner join
	(select TRAN_v00_DelimiterCount.hk_l_TRAN_v00, case
		when Delim_Arranger_Name=Delim_Arranger_ABN
		and Delim_Arranger_Name=Delim_Arranger_Jurisdiction
		and Delim_Arranger_Name=Delim_Arranger_ID
		and Delim_Arranger_Name=Delim_Arranger_ID_Descriptor
		and Delim_Arranger_Name=Delim_Arranger_Fee_Currency
		and Delim_Arranger_Name=Delim_Arranger_Fee
		then Delim_Arranger_Name+1 end as DelimCount from INGEST.TRAN_v00_DelimiterCount) delim on delim.hk_l_TRAN_v00=l.hk_l_TRAN_v00
	inner join
	Nbrs N ON (N.Number <= DelimCount) or (DelimCount = 0 and N.Number =1)
GO


