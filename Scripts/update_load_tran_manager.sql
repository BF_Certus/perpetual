truncate table LOAD.load_TRAN_manager;
INSERT INTO LOAD.load_TRAN_manager
(clientid, feedid, reporting_period, master_trust_name_or_spv, series_trust_name_or_series,
transaction_id, manager_name, manager_count, manager_position, manager_abn,
manager_jurisdiction, manager_id, manager_id_descriptor, manager_fee_currency,
manager_fee, dss_record_source, dss_load_datetime)
SELECT
TRAN_Manager.ClientID,TRAN_Manager.FeedID,TRAN_Manager.Reporting_Period,
TRAN_Manager.Master_Trust_Name_or_SPV,TRAN_Manager.Series_Trust_Name_or_Series,
TRAN_Manager.Transaction_ID,TRAN_Manager.Manager_Name,TRAN_Manager.Manager_Count,
TRAN_Manager.Manager_Position,TRAN_Manager.Manager_ABN,TRAN_Manager.Manager_Jurisdiction,
TRAN_Manager.Manager_ID,TRAN_Manager.Manager_ID_Descriptor,TRAN_Manager.Manager_Fee_Currency,
TRAN_Manager.Manager_Fee,NULL,NULL
From INGEST.TRAN_Manager TRAN_Manager
;

;
UPDATE LOAD.load_TRAN_manager SET dss_record_source = 'Perpetual.INGEST.TRAN_Manager',dss_load_datetime = GetDate() ;

