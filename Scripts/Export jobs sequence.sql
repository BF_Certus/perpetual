declare 
	@wjc_name varchar(100)

-- ASSIGN JOB NAME:
select @wjc_name = 'perpetual_metadata';

set nocount on;

declare 
	@whattocreate as char(1)
,	@objname as sysname
,	@wtc_order varchar(20)
,	@rnum	int;

declare @objects table (
	rnum		int not null
,	objname		varchar(128)
,	wtc_order	varchar(20)
)

set @whattocreate = 'P';

print 'DELETE FROM [DV].[LOAD_PROCEDURES] WHERE LOAD_NAME = ''' + @wjc_name + ''';';

with job as (select wjc_job_key from ws_wrk_job_ctrl where wjc_name =  @wjc_name)
	,task as (select wtc_name, wtc_order_a, wtc_order_b, wtc_order_c from ws_wrk_task_ctrl inner join job on wtc_job_key = job.wjc_job_key)
	,normal as (select nt_update_key, nt_schema as schema_name, nt_table_name as objname, wtc_order_a, wtc_order_b, wtc_order_c from ws_normal_tab inner join task on nt_table_name = task.wtc_name)
	,stage as (select st_update_key, st_schema schema_name, st_table_name as objname, wtc_order_a, wtc_order_b, wtc_order_c  from ws_stage_tab inner join task on st_table_name = task.wtc_name)
	,[load] as (select lt_obj_key, lt_schema schema_name, lt_table_name as objname, cast(lt_source_schema as nvarchar(255)) as source_schema, cast(lt_source_table as nvarchar(255)) as source_table, wtc_order_a, wtc_order_b, wtc_order_c from ws_load_tab inner join task on lt_table_name = task.wtc_name)
	,header as (select ph_obj_key, ph_name, wtc_order_a, wtc_order_b, wtc_order_c  from ws_pro_header inner join normal on ph_obj_key = normal.nt_update_key
				union select ph_obj_key, ph_name, wtc_order_a, wtc_order_b, wtc_order_c  from ws_pro_header inner join stage on ph_obj_key = stage.st_update_key
				union select lt_obj_key, 'update_'+ objname, wtc_order_a, wtc_order_b, wtc_order_c from [load]  
	)
	,objnames as (
		select 'DV' schema_name, ph_name objname, wtc_order_a, wtc_order_b, wtc_order_c  from header	
	)
insert into @objects (rnum, objname, wtc_order)
select row_number() over(partition by null order by wtc_order_a, wtc_order_b, wtc_order_c) as rnum, schema_name + '.' + objname as [fullname], 
'' + cast(wtc_order_a as varchar(10)) +'.'+ cast(wtc_order_b as varchar(10)) + '.' + cast(wtc_order_c as varchar(10)) wtc_order from objnames t
order by wtc_order_a, wtc_order_b, wtc_order_c;

set @rnum = 1;

while exists(select * from @objects where rnum = @rnum) 
begin

	select @rnum = @rnum + 1, @objname = objname, @wtc_order = wtc_order from @objects where rnum = @rnum;
	print 'INSERT INTO [DV].[LOAD_PROCEDURES] (Load_Name,Proc_Name,Group_Id,Order_Id, ActionId) VALUES (''' 
		+ @wjc_name + ''',''' + @objname + ''',' + '1' + ',' + cast(@rnum - 1 as varchar(10)) + ',0);';
end

