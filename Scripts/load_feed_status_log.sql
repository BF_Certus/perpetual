CREATE TABLE LOAD.load_feed_status_log (
  feedid int, feed_name varchar(100), feed_status varchar(50) 
, feedprocess_starttime datetime, feedprocess_endtime datetime, isrunning bit 
, active bit, dss_record_source varchar(255), dss_load_datetime datetime2);