DECLARE @p_return_msg VARCHAR(256)
, @p_status           INTEGER;

EXEC dbo.update_stage_bvrmbs_v00_statustracking 0 , '' , '' , 0 , 0 , @p_return_msg OUTPUT, @p_status OUTPUT;
select @p_status, @p_return_msg;
EXEC dbo.update_stage_bvrmbs_v00_validations 0 , '' , '' , 0 , 0 , @p_return_msg OUTPUT, @p_status OUTPUT;
select @p_status, @p_return_msg;
EXEC dbo.update_stage_bvrmbs_v00_dataconversion 0 , '' , '' , 0 , 0 , @p_return_msg OUTPUT, @p_status OUTPUT;
select @p_status, @p_return_msg;
EXEC dbo.update_stage_bvrmbs_v00_referencecodes 0 , '' , '' , 0 , 0 , @p_return_msg OUTPUT, @p_status OUTPUT;
select @p_status, @p_return_msg;
EXEC dbo.update_bvs_rmbs_v00_statustracking 0 , '' , '' , 0 , 0 , @p_return_msg OUTPUT, @p_status OUTPUT;
select @p_status, @p_return_msg;
EXEC dbo.update_bvs_rmbs_v00_validations 0 , '' , '' , 0 , 0 , @p_return_msg OUTPUT, @p_status OUTPUT;
select @p_status, @p_return_msg;
EXEC dbo.update_bvs_rmbs_v00_dataconversion 0 , '' , '' , 0 , 0 , @p_return_msg OUTPUT, @p_status OUTPUT;
select @p_status, @p_return_msg;
EXEC dbo.update_bvs_rmbs_v00_referencecodes 0 , '' , '' , 0 , 0 , @p_return_msg OUTPUT, @p_status OUTPUT;
select @p_status, @p_return_msg;
EXEC dbo.update_stage_bvrmbs_col_v00_dataconversion 0 , '' , '' , 0 , 0 , @p_return_msg OUTPUT, @p_status OUTPUT;
select @p_status, @p_return_msg;
EXEC dbo.update_stage_bvSECU_v00_dataconversion 0 , '' , '' , 0 , 0 , @p_return_msg OUTPUT, @p_status OUTPUT;
select @p_status, @p_return_msg;
EXEC dbo.update_bvs_rmbs_col_v00_dataconversion 0 , '' , '' , 0 , 0 , @p_return_msg OUTPUT, @p_status OUTPUT;
select @p_status, @p_return_msg;
EXEC dbo.update_bvs_SECU_v00_dataconversion 0 , '' , '' , 0 , 0 , @p_return_msg OUTPUT, @p_status OUTPUT;
select @p_status, @p_return_msg;