CREATE TABLE STAGE.stage_TRAN_arranger (
  hk_l_TRAN_arranger binary(20) NOT NULL 
, hk_h_reporting_period binary(20) NOT NULL 
, hk_h_transaction binary(20) NOT NULL, hk_h_client_master binary(20) NOT NULL 
, hk_h_party binary(20) NOT NULL, hk_h_feed_master binary(20) NOT NULL 
, clientid varchar(100), feedid varchar(100), reporting_period varchar(100) 
, master_trust_name_or_spv varchar(100) 
, series_trust_name_or_series varchar(100), transaction_id varchar(100) 
, arranger_name varchar(5000), arranger_count int, arranger_position int 
, arranger_abn varchar(5000), arranger_jurisdiction varchar(5000) 
, arranger_id varchar(5000), arranger_id_descriptor varchar(5000) 
, arranger_fee_currency varchar(5000), arranger_fee varchar(5000) 
, dss_change_hash_stage_bvTRAN_arranger binary(20) NOT NULL 
, dss_record_source varchar(255), dss_load_datetime datetime2 
, dss_create_time datetime2);