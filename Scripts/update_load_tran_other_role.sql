truncate table LOAD.load_TRAN_other_role;
INSERT INTO LOAD.load_TRAN_other_role
(clientid, reporting_period, master_trust_name_or_spv, series_trust_name_or_series,
transaction_id, other_role_position, other_role_name, feedid, other_role_count,
other_role_abn, other_role_jurisdiction, other_role_id, other_role_id_descriptor,
other_role_fee_currency, other_role_fee, dss_record_source, dss_load_datetime)
SELECT
TRAN_Other_Role.ClientID,TRAN_Other_Role.Reporting_Period,TRAN_Other_Role.Master_Trust_Name_or_SPV,
TRAN_Other_Role.Series_Trust_Name_or_Series,TRAN_Other_Role.Transaction_ID,
TRAN_Other_Role.Other_Role_Position,TRAN_Other_Role.Other_Role_Name,
TRAN_Other_Role.FeedID,TRAN_Other_Role.Other_Role_Count,TRAN_Other_Role.Other_Role_ABN,
TRAN_Other_Role.Other_Role_Jurisdiction,TRAN_Other_Role.Other_Role_ID,
TRAN_Other_Role.Other_Role_ID_Descriptor,TRAN_Other_Role.Other_Role_Fee_Currency,
TRAN_Other_Role.Other_Role_Fee,NULL,NULL
From INGEST.TRAN_Other_Role TRAN_Other_Role
;

;
UPDATE LOAD.load_TRAN_other_role SET dss_record_source = 'Perpetual.INGEST.TRAN_Other_Role',dss_load_datetime = GetDate() ;

