CREATE TABLE DV.h_exchange (
  hk_h_exchange binary(20) NOT NULL, dss_record_source varchar(255) 
, dss_load_datetime datetime2, dss_create_time datetime2 
, clientid varchar(100) NOT NULL, exchange_name varchar(100));
CREATE UNIQUE NONCLUSTERED INDEX h_exchange_idx_0 ON DV.h_exchange (hk_h_exchange) WITH (SORT_IN_TEMPDB = OFF);
CREATE UNIQUE NONCLUSTERED INDEX h_exchange_idx_A ON DV.h_exchange (exchange_name,clientid) WITH (SORT_IN_TEMPDB = OFF);
ALTER TABLE DV.h_exchange ADD CONSTRAINT h_exchange_b9d4b41a_idx_PK PRIMARY KEY NONCLUSTERED (hk_h_exchange) WITH (SORT_IN_TEMPDB = OFF);
CREATE UNIQUE NONCLUSTERED INDEX h_exchange_b9d4b41a_idx_A ON DV.h_exchange (clientid,exchange_name) WITH (SORT_IN_TEMPDB = OFF);
