/****** Object:  UserDefinedFunction [dbo].[FN_CERTUS_DELIM_COUNT]    Script Date: 2/07/2018 1:59:19 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 
  -- User-defined function to return number of instances of a delimiter + 1 
  CREATE FUNCTION [dbo].[FN_CERTUS_DELIM_COUNT] (@str VARCHAR(8000), @substr VARCHAR(255))
  RETURNS INT
  AS
  BEGIN
	DECLARE @found INT = 0,
			@pos INT = 1;
 
	WHILE 1=1 
	BEGIN
		-- Find the next occurrence
		SET @pos = CHARINDEX(@substr, @str, @pos);
 
		-- Nothing found
		IF @pos IS NULL OR @pos = 0
		 Begin
		    if @found = 0  			 RETURN @found;
			else  			  return @found+1;
		end
 
		-- The required occurrence found
		--IF @found = 1
		--	BREAK;
 
		-- Prepare to find another one occurrence
		SET @found = @found + 1;
		SET @pos = @pos + 1;
	END
 
	RETURN @found + 1;
  END
GO


