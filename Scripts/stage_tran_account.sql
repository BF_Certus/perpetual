CREATE TABLE STAGE.stage_TRAN_account (
  hk_l_TRAN_account binary(20) NOT NULL 
, hk_h_reporting_period binary(20) NOT NULL 
, hk_h_transaction binary(20) NOT NULL, hk_h_client_master binary(20) NOT NULL 
, hk_h_party binary(20) NOT NULL, hk_h_feed_master binary(20) NOT NULL 
, clientid varchar(100), reporting_period varchar(100) 
, master_trust_name_or_spv varchar(100) 
, series_trust_name_or_series varchar(100), transaction_id varchar(100) 
, account_provider_name varchar(5000) 
, account_provider_jurisdiction varchar(5000), feedid varchar(100) 
, account_count int, account_position int, ending_balance varchar(5000) 
, target_balance varchar(5000), account_provider_abn varchar(5000) 
, account_provider_id varchar(5000) 
, account_provider_id_descriptor varchar(5000), account_currency varchar(5000) 
, account_fee varchar(5000), beginning_balance varchar(5000) 
, account_type varchar(5000), account_name varchar(5000) 
, dss_change_hash_stage_bvTRAN_account binary(20) NOT NULL 
, dss_record_source varchar(255), dss_load_datetime datetime2 
, dss_create_time datetime2);