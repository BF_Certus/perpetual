truncate table LOAD.load_TRAN_swap_or_hedge;
INSERT INTO LOAD.load_TRAN_swap_or_hedge
(clientid, feedid, reporting_period, master_trust_name_or_spv, series_trust_name_or_series,
transaction_id, swap_or_hedge_provider_name, swap_or_hedge_count, swap_or_hedge_position,
swap_or_hedge_provider_abn, swap_or_hedge_type, notional_principal_amount,
swap_or_hedge_provider_jurisdiction, swap_or_hedge_provider_id, swap_or_hedge_provider_id_descriptor,
swap_or_hedge_name, pay_leg_currency, pay_leg_reference_index, pay_leg_reference_index_value,
pay_leg_margin_or_rate, pay_leg_amount, receive_leg_currency, receive_leg_reference_index,
receive_leg_reference_index_value, receive_leg_margin_or_rate, receive_leg_amount,
exchange_rate, swap_or_hedge_fee_currency, swap_or_hedge_fee, dss_record_source,
dss_load_datetime)
SELECT
TRAN_Swap_Or_Hedge.ClientID,TRAN_Swap_Or_Hedge.FeedID,TRAN_Swap_Or_Hedge.Reporting_Period,
TRAN_Swap_Or_Hedge.Master_Trust_Name_or_SPV,TRAN_Swap_Or_Hedge.Series_Trust_Name_or_Series,
TRAN_Swap_Or_Hedge.Transaction_ID,TRAN_Swap_Or_Hedge.Swap_or_Hedge_Provider_Name,
TRAN_Swap_Or_Hedge.Swap_Or_Hedge_Count,TRAN_Swap_Or_Hedge.Swap_Or_Hedge_Position,
TRAN_Swap_Or_Hedge.Swap_or_Hedge_Provider_ABN,TRAN_Swap_Or_Hedge.Swap_or_Hedge_Type,
TRAN_Swap_Or_Hedge.Notional_Principal_Amount,TRAN_Swap_Or_Hedge.Swap_or_Hedge_Provider_Jurisdiction,
TRAN_Swap_Or_Hedge.Swap_or_Hedge_Provider_ID,TRAN_Swap_Or_Hedge.Swap_or_Hedge_Provider_ID_Descriptor,
TRAN_Swap_Or_Hedge.Swap_or_Hedge_Name,TRAN_Swap_Or_Hedge.Pay_Leg_Currency,
TRAN_Swap_Or_Hedge.Pay_Leg_Reference_Index,TRAN_Swap_Or_Hedge.Pay_Leg_Reference_Index_Value,
TRAN_Swap_Or_Hedge.Pay_Leg_Margin_or_Rate,TRAN_Swap_Or_Hedge.Pay_Leg_Amount,
TRAN_Swap_Or_Hedge.Receive_Leg_Currency,TRAN_Swap_Or_Hedge.Receive_Leg_Reference_Index,
TRAN_Swap_Or_Hedge.Receive_Leg_Reference_Index_Value,TRAN_Swap_Or_Hedge.Receive_Leg_Margin_or_Rate,
TRAN_Swap_Or_Hedge.Receive_Leg_Amount,TRAN_Swap_Or_Hedge.Exchange_Rate,
TRAN_Swap_Or_Hedge.Swap_or_Hedge_Fee_Currency,TRAN_Swap_Or_Hedge.Swap_or_Hedge_Fee,
NULL,NULL
From INGEST.TRAN_Swap_Or_Hedge TRAN_Swap_Or_Hedge
;

;
UPDATE LOAD.load_TRAN_swap_or_hedge SET dss_record_source = 'Perpetual.INGEST.TRAN_Swap_Or_Hedge',dss_load_datetime = GetDate() ;

