CREATE TABLE DV.h_rating_agency (
  hk_h_rating_agency binary(20) NOT NULL, dss_record_source varchar(255) 
, dss_load_datetime datetime2, dss_create_time datetime2 
, clientid varchar(100) NOT NULL, ratings_agency_name varchar(100));
CREATE UNIQUE NONCLUSTERED INDEX h_rating_agency_idx_0 ON DV.h_rating_agency (hk_h_rating_agency) WITH (SORT_IN_TEMPDB = OFF);
CREATE UNIQUE NONCLUSTERED INDEX h_rating_agency_idx_A ON DV.h_rating_agency (clientid,ratings_agency_name) WITH (SORT_IN_TEMPDB = OFF);
ALTER TABLE DV.h_rating_agency ADD CONSTRAINT h_rating_agen_eaabc950_idx_PK PRIMARY KEY NONCLUSTERED (hk_h_rating_agency) WITH (SORT_IN_TEMPDB = OFF);
CREATE UNIQUE NONCLUSTERED INDEX h_rating_agen_eaabc950_idx_A ON DV.h_rating_agency (clientid,ratings_agency_name) WITH (SORT_IN_TEMPDB = OFF);
