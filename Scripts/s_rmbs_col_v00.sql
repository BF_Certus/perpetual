CREATE TABLE DV.s_rmbs_col_v00 (
  hk_l_rmbs_col_v00 binary(20) NOT NULL, dss_load_datetime datetime2 
, dss_change_hash binary(20) NOT NULL, dss_record_source varchar(255) 
, dss_start_date datetime2, dss_version integer, dss_create_time datetime2 
, collateral_date nvarchar(255), group_loan_id nvarchar(255) 
, security_property_postcode nvarchar(255) 
, security_property_country nvarchar(255) 
, original_security_property_value nvarchar(255) 
, most_recent_security_property_value nvarchar(255) 
, original_property_valuation_type nvarchar(255) 
, most_recent_property_valuation_type nvarchar(255) 
, most_recent_security_property_valuation_date nvarchar(255) 
, security_property_purpose nvarchar(255), security_property_type nvarchar(255) 
, lien nvarchar(255), abs_statistical_area nvarchar(255) 
, main_security_methodology nvarchar(255), main_security_flag nvarchar(255) 
, property_valuation_currency nvarchar(255));