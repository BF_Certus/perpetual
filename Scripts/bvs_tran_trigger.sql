CREATE TABLE DV.bvs_TRAN_trigger (
  hk_l_TRAN_trigger binary(20) NOT NULL, dss_load_datetime datetime 
, dss_change_hash binary(20) NOT NULL, dss_record_source varchar(255) 
, dss_start_date datetime, dss_version int, dss_create_time datetime 
, feedid varchar(100), trigger_count int, trigger_description varchar(5000) 
, trigger_state varchar(5000));