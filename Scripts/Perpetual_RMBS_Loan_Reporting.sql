/****** Object:  View [IDM].[Perpetual_RMBS_Loan_Reporting]    Script Date: 6/07/2018 2:51:12 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


	create View [IDM].[Perpetual_RMBS_Loan_Reporting] as 
	WITH MonthBalancePRI
	AS 	( 
			SELECT [EOMonth_Collateral_Date], CAST(SUM([Current_Balance]) as DECIMAL(38,9)) AS SUM_Current_Balance_Per_Period      
		FROM  IDM.Perpetual_RMBS_Loan as LP 
			--WHERE  Account_Status NOT IN ('Redeemed', 'Repurchased by Seller (Mandatory)', 'Repurchased by Seller (Discretionary)')
			GROUP BY EOMonth_Collateral_Date
		)


			SELECT
				--	 LP.[Data_Issuer_ID]
					LP.[Data_Issuer_Code] as Master_Data_Issuer_Code
					,LP.[Data_Issuer_Name] as Master_Data_Issuer_Name
				--	,Count_Loan_Key = CAST(COUNT(LP.[Loan_Key]) AS INT)
					,Collateral_Date = LP.EOMonth_Collateral_Date
					,LP.[Property_Postcode]
					,LP.[Property_State]
					,LP.[Property_Region]
					,LP.[Property_Country_Code]
					,LP.[Property_Country_Name]
					,SUM_Current_Balance = CAST(SUM(LP.[Current_Balance]) AS DECIMAL(38,9))
					,SUM_Scheduled_Balance = CAST(SUM(LP.[Scheduled_Balance]) AS DECIMAL(38,9))
					,SUM_Available_Redraw = CAST(SUM(LP.Available_Redraw) AS DECIMAL(38,9))
					,SUM_Offset_Account_Balance = CAST(SUM(LP.Offset_Account_Balance) AS DECIMAL(38,9))
	
					----WA cannot be calculated where the SUM of current balance is null or 0. The error is handled by setting the field to NULL where this occurs.
					,V_WA_Loan_Term = (case	WHEN SUM(LP.Current_Balance) > 0 AND SUM(LP.Current_Balance) is not null THEN CAST(SUM(LP.Loan_Term * LP.Current_Balance) / SUM(LP.Current_Balance) AS DECIMAL(38,6))
											ELSE NULL END)
					,V_WA_Remaining_Term =(case	WHEN SUM(LP.Current_Balance) > 0 AND SUM(LP.Current_Balance) is not null THEN CAST(SUM(LP.Remaining_Term * LP.Current_Balance) / SUM(LP.Current_Balance) AS DECIMAL(38,6))
											ELSE NULL END)
					,V_WA_Seasoning = (case	WHEN SUM(LP.Current_Balance) > 0 AND SUM(LP.Current_Balance) is not null THEN CAST(SUM(LP.Seasoning * LP.Current_Balance) / SUM(LP.Current_Balance) AS DECIMAL(38,6))
											ELSE NULL END)
					,V_WA_Original_LTV = (case	WHEN SUM(LP.Current_Balance) > 0 AND SUM(LP.Current_Balance) is not null THEN CAST(SUM(LP.Original_LTV * LP.Current_Balance) / SUM(LP.Current_Balance) AS DECIMAL(38,6))
											ELSE NULL END)

					,V_WA_Current_LTV = (case	WHEN SUM(LP.Current_Balance) > 0 AND SUM(LP.Current_Balance) is not null THEN CAST(SUM(LP.Current_LTV * LP.Current_Balance) / SUM(LP.Current_Balance) AS DECIMAL(38,6))
											ELSE NULL END)
					,LP.Interest_Rate_Type
					,V_WA_Current_Interest_Rate = (case	WHEN SUM(LP.Current_Balance) > 0 AND SUM(LP.Current_Balance) is not null THEN CAST(SUM(LP.Current_Interest_Rate * LP.Current_Balance) / SUM(LP.Current_Balance) AS DECIMAL(38,6))
														ELSE NULL END)
					,SUM_Default_Balance = CAST(SUM(LP.Default_Balance) AS DECIMAL(38,9))
					,SUM_Foreclosure_Proceeds = CAST(SUM(LP.Foreclosure_Proceeds) AS DECIMAL(38,9))
					,SUM_Loss_on_Sale = CAST(SUM(LP.Loss_on_Sale) AS DECIMAL(38,9))
					,V_WA_Days_In_Arrears = (case	WHEN SUM(LP.Current_Balance) > 0 AND SUM(LP.Current_Balance) is not null THEN CAST(SUM(LP.Days_In_Arrears * LP.Current_Balance) / SUM(LP.Current_Balance)  AS DECIMAL(38,6))
										ELSE NULL END)
					,SUM_Amount_in_Arrears = CAST(SUM(LP.Amount_in_Arrears) AS DECIMAL(38,9))
					,SUM_Claim_Submitted_to_LMI = CAST(SUM(LP.Claim_Submitted_to_LMI) AS DECIMAL(38,9)) 
					,SUM_Claim_Paid_by_LMI = CAST(SUM(LP.Claim_Paid_by_LMI) AS DECIMAL(38,9))
					,SUM_Claim_Denied_by_LMI = CAST(SUM(LP.Claim_Denied_by_LMI) AS DECIMAL(38,9))
					,SUM_Claim_Pending_with_LMI = CAST(SUM(LP.Claim_Pending_with_LMI) AS DECIMAL(38,9))
					,[Master_Trust_Name_or_SPV] as Master_Trust_Name
					,[Series_Trust_Name_or_Series] as Series_Trust_Name
					,[Origination_Year] = LP.OriginationYear
					,LP.Loan_Purpose
					,LP.Loan_Origination_Channel_Flag
					,LP.Loan_Documentation_Type
					,LP.Loan_Type
					,LP.Property_Purpose
					,LP.Property_Type
					,LP.First_Home_Buyer_Flag
					,LP.Country_of_Residence
					,LP.Employment_Type
					,LP.Bankruptcy_Flag
					,V_WA_Interest_Rate = (case	WHEN SUM(LP.Current_Balance) > 0 AND SUM(LP.Current_Balance) is not null THEN CAST(SUM(LP.Interest_Rate * LP.Current_Balance) / SUM(LP.Current_Balance)  AS DECIMAL(38,6))
							ELSE NULL END)
					,SUM_Total_Properties_Value = CAST(SUM(LP.Total_Properties_Value) AS DECIMAL(38,9)) 
					,LP.Arrears_Category
					,BAL.SUM_Current_Balance_Per_Period
					----The group's amount in arrears in calculated as a percentage of the total current balance of the period.
					, PERCENT_Arrears = CAST(SUM(LP.Amount_in_Arrears)/BAL.SUM_Current_Balance_Per_Period AS DECIMAL(38,6))
					,ViewKey = CAST(CONCAT(LP.Transaction_ID, LP.EOMonth_Collateral_Date) AS VARCHAR(102)) 
				--	,SUM_Indexed_Property_Value = CAST(SUM(LP.Indexed_Property_Value) AS DECIMAL(38,9)) 
		--			,V_WA_Indexed_LTV = (CASE	WHEN SUM(LP.Current_Balance) > 0 AND SUM(LP.Current_Balance) IS NOT NULL THEN CAST(SUM(LP.Indexed_LVR * LP.Current_Balance) / SUM(LP.Current_Balance)  AS DECIMAL(38,6))
		--									ELSE NULL END)
				   ,LP.[Origination_Date_Settlement_Date]
			--	   ,LP.[Securitisation_Type]
			--	   ,@EventLogKey
				,CAST(NULL AS varchar(50)) as [Securitisation_Type]
     ,CAST(NULL AS DECIMAL(38,9)) as [SUM_Indexed_Property_Value]
      ,CAST(NULL AS DECIMAL(38,6)) as [V_WA_Indexed_LTV]
	  ,1 as Count_Loan_Key
				FROM  IDM.Perpetual_RMBS_Loan as LP 
				LEFT JOIN MonthBalancePRI BAL ON BAL.EOMonth_Collateral_Date = LP.EOMonth_Collateral_Date
					WHERE LP.Account_Status NOT IN ('Redeemed', 'Repurchased by Seller (Mandatory)', 'Repurchased by Seller (Discretionary)')
				GROUP BY
				--	 LP.[Data_Issuer_ID]
					LP.[Data_Issuer_Code]
					,LP.[Data_Issuer_Name]
					,LP.EOMonth_Collateral_Date
					,LP.Property_Postcode
					,LP.Property_State
					,LP.Property_Region
					,LP.Property_Country_Code
					,LP.Property_Country_Name
					,LP.Interest_Rate_Type
				--	,LP.Master_Trust_Name
				--	,LP.Series_Trust_Name
					,LP.OriginationYear
					,LP.Loan_Purpose
					,LP.Loan_Origination_Channel_Flag
					,LP.Loan_Documentation_Type
					,LP.Loan_Type
					,LP.Property_Purpose
					,LP.Property_Type
					,LP.First_Home_Buyer_Flag
					,LP.Country_of_Residence
					,LP.Employment_Type
					,LP.Bankruptcy_Flag
					,LP.Arrears_Category
					,BAL.SUM_Current_Balance_Per_Period
					,CONCAT(LP.Transaction_ID, LP.EOMonth_Collateral_Date)
					,LP.[Origination_Date_Settlement_Date]
				--    ,LP.[Securitisation_Type]
GO

