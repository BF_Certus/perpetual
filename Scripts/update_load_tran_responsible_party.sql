truncate table LOAD.load_TRAN_responsible_party;
INSERT INTO LOAD.load_TRAN_responsible_party
(clientid, feedid, reporting_period, master_trust_name_or_spv, series_trust_name_or_series,
transaction_id, responsible_party_name, responsible_party_count, responsible_party_position,
responsible_party_abn, responsible_party_jurisdiction, responsible_party_id,
responsible_party_id_descriptor, responsible_party_fee_currency, responsible_party_fee,
dss_record_source, dss_load_datetime)
SELECT
TRAN_Responsible_Party.ClientID,TRAN_Responsible_Party.FeedID,TRAN_Responsible_Party.Reporting_Period,
TRAN_Responsible_Party.Master_Trust_Name_or_SPV,TRAN_Responsible_Party.Series_Trust_Name_or_Series,
TRAN_Responsible_Party.Transaction_ID,TRAN_Responsible_Party.Responsible_Party_Name,
TRAN_Responsible_Party.Responsible_Party_Count,TRAN_Responsible_Party.Responsible_Party_Position,
TRAN_Responsible_Party.Responsible_Party_ABN,TRAN_Responsible_Party.Responsible_Party_Jurisdiction,
TRAN_Responsible_Party.Responsible_Party_ID,TRAN_Responsible_Party.Responsible_Party_ID_Descriptor,
TRAN_Responsible_Party.Responsible_Party_Fee_Currency,TRAN_Responsible_Party.Responsible_Party_Fee,
NULL,NULL
From INGEST.TRAN_Responsible_Party TRAN_Responsible_Party
;

;
UPDATE LOAD.load_TRAN_responsible_party SET dss_record_source = 'Perpetual.INGEST.TRAN_Responsible_Party',dss_load_datetime = GetDate() ;

