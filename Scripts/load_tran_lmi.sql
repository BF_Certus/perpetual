CREATE TABLE LOAD.load_TRAN_lmi (
  clientid varchar(100), feedid varchar(100), reporting_period varchar(100) 
, master_trust_name_or_spv varchar(100) 
, series_trust_name_or_series varchar(100), lmi_provider_name varchar(5000) 
, transaction_id varchar(100), lmi_count int, lmi_position int 
, lmi_provider_abn varchar(5000), loans_insured varchar(5000) 
, timely_payment_claims_outstanding varchar(5000) 
, lmi_provider_jurisdiction varchar(5000), lmi_provider_id varchar(5000) 
, lmi_provider_id_descriptor varchar(5000) 
, loan_currency_insurance varchar(5000), dss_record_source varchar(255) 
, dss_load_datetime datetime2);