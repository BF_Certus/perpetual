truncate table LOAD.load_TRAN_servicer;
INSERT INTO LOAD.load_TRAN_servicer
(clientid, reporting_period, master_trust_name_or_spv, series_trust_name_or_series,
transaction_id, servicer_name, feedid, servicer_count, servicer_position,
servicer_abn, servicer_jurisdiction, servicing_fee_currency, servicing_fee,
servicing_role, servicer_id, servicer_id_descriptor, dss_record_source,
dss_load_datetime)
SELECT
TRAN_Servicer.ClientID,TRAN_Servicer.Reporting_Period,TRAN_Servicer.Master_Trust_Name_or_SPV,
TRAN_Servicer.Series_Trust_Name_or_Series,TRAN_Servicer.Transaction_ID,
TRAN_Servicer.Servicer_Name,TRAN_Servicer.FeedID,TRAN_Servicer.Servicer_Count,
TRAN_Servicer.Servicer_Position,TRAN_Servicer.Servicer_ABN,TRAN_Servicer.Servicer_Jurisdiction,
TRAN_Servicer.Servicing_Fee_Currency,TRAN_Servicer.Servicing_Fee,
TRAN_Servicer.Servicing_Role,TRAN_Servicer.Servicer_ID,TRAN_Servicer.Servicer_ID_Descriptor,
NULL,NULL
From INGEST.TRAN_Servicer TRAN_Servicer
;

;
UPDATE LOAD.load_TRAN_servicer SET dss_record_source = 'Perpetual.INGEST.TRAN_Servicer',dss_load_datetime = GetDate() ;

