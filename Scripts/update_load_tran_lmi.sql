truncate table LOAD.load_TRAN_lmi;
INSERT INTO LOAD.load_TRAN_lmi
(clientid, feedid, reporting_period, master_trust_name_or_spv, series_trust_name_or_series,
lmi_provider_name, transaction_id, lmi_count, lmi_position, lmi_provider_abn,
loans_insured, timely_payment_claims_outstanding, lmi_provider_jurisdiction,
lmi_provider_id, lmi_provider_id_descriptor, loan_currency_insurance, dss_record_source,
dss_load_datetime)
SELECT
TRAN_LMI.ClientID,TRAN_LMI.FeedID,TRAN_LMI.Reporting_Period,
TRAN_LMI.Master_Trust_Name_or_SPV,TRAN_LMI.Series_Trust_Name_or_Series,
TRAN_LMI.LMI_Provider_Name,TRAN_LMI.Transaction_ID,TRAN_LMI.LMI_Count,
TRAN_LMI.LMI_Position,TRAN_LMI.LMI_Provider_ABN,TRAN_LMI.Loans_Insured,
TRAN_LMI.Timely_Payment_Claims_Outstanding,TRAN_LMI.LMI_Provider_Jurisdiction,
TRAN_LMI.LMI_Provider_ID,TRAN_LMI.LMI_Provider_ID_Descriptor,TRAN_LMI.Loan_Currency_Insurance,
NULL,NULL
From INGEST.TRAN_LMI TRAN_LMI
;

;
UPDATE LOAD.load_TRAN_lmi SET dss_record_source = 'Perpetual.INGEST.TRAN_LMI',dss_load_datetime = GetDate() ;

