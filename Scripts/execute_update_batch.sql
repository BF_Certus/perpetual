
--	exec INGEST.execute_update_batch_DV 'PerpetualTest'

IF OBJECT_ID(N'INGEST.execute_update_batch', N'p')IS NOT NULL  
   DROP PROCEDURE INGEST.execute_update_batch;  
GO

create procedure INGEST.execute_update_batch @batchname varchar(100)

as 

set nocount on;
set xact_abort off;

declare 
	@procedures varchar(max)
,	@groupId int
,	@batchId int;

set @groupId =1;


begin try
	insert into DV.load_audit_batch(Batch_Name,Started) select @batchname, getutcdate();
	set @batchId = scope_identity();

	while exists(select 1 from DV.[load_procedures] where Group_Id = @groupId) begin

		select @procedures = COALESCE(@procedures+',' ,'') + Proc_Name from DV.[load_procedures] 
			where Load_Name = @batchname and Group_Id = @groupId order by Order_Id;
		exec dbo.execute_job_procedures  @procedures, @batchId;

		-- set next group
		select  @groupId = @groupId +1,	@procedures = null;
	end

	update DV.load_audit_batch set finished = getutcdate(), [message] = 'Completed successfully.',[status] = 1 where Batch_id = @batchId;
end try
begin catch
	update DV.load_audit_batch set finished = getutcdate(), [message] = ERROR_MESSAGE(),[status] = -2 where Batch_id = @batchId;
	throw;
end catch

go
