CREATE TABLE LOAD.load_TRAN_account (
  clientid varchar(100), reporting_period varchar(100) 
, master_trust_name_or_spv varchar(100) 
, series_trust_name_or_series varchar(100), transaction_id varchar(100) 
, account_provider_name varchar(5000) 
, account_provider_jurisdiction varchar(5000), feedid varchar(100) 
, account_count int, account_position int, ending_balance varchar(5000) 
, target_balance varchar(5000), account_provider_abn varchar(5000) 
, account_provider_id varchar(5000) 
, account_provider_id_descriptor varchar(5000), account_currency varchar(5000) 
, account_fee varchar(5000), beginning_balance varchar(5000) 
, account_type varchar(5000), account_name varchar(5000) 
, dss_record_source varchar(255), dss_load_datetime datetime2);