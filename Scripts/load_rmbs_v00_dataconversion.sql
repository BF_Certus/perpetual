CREATE TABLE LOAD.load_rmbs_v00_dataconversion (
  hk_l_rmbs_v00 binary(20), collateral_date date 
, most_recent_approval_amount decimal(24,9), current_balance decimal(24,9) 
, scheduled_balance decimal(24,9), loan_guarantee_flag bit 
, redraw_feature_flag bit, available_redraw decimal(24,9) 
, offset_account_flag bit, offset_account_balance decimal(24,9) 
, conforming_mortgage bit, origination_date_settlement_date date 
, loan_term decimal(24,9), remaining_term decimal(24,9), seasoning decimal(24,9) 
, maturity_date date, loan_securitised_date date, original_ltv decimal(24,9) 
, current_ltv decimal(24,9), scheduled_ltv decimal(24,9) 
, scheduled_minimum_payment decimal(24,9), lmi_attachment_point decimal(24,9) 
, variable_loan_amount decimal(24,9), current_interest_rate decimal(24,9) 
, reference_index_value decimal(24,9), interest_margin decimal(24,9) 
, restructuring_arrangement_flag bit, interest_only_expiry_date date 
, current_rate_expiry_date date, default_balance decimal(24,9) 
, foreclosure_proceeds decimal(24,9), date_of_foreclosure date 
, loss_on_sale decimal(24,9), days_in_arrears int 
, amount_in_arrears decimal(24,9), days_since_last_scheduled_payment int 
, cumulative_missed_payments decimal(24,9), claim_submitted_to_lmi decimal(24,9) 
, claim_paid_by_lmi decimal(24,9), claim_denied_by_lmi decimal(24,9) 
, claim_pending_with_lmi decimal(24,9), original_property_value decimal(24,9) 
, most_recent_property_value decimal(24,9) 
, most_recent_property_valuation_date date, first_home_buyer_flag bit 
, employment_type_secondary_borrower int, borrower_type_primary_borrower int 
, borrower_type_secondary_borrower int, legal_entity_type_secondary_borrower int 
, bankruptcy_flag bit, last_credit_discharge_date date, number_of_debtors int 
, credit_score_secondary_borrower int 
, debt_serviceability_metric_score decimal(24,9), income decimal(24,9) 
, secondary_income decimal(24,9), income_verification_for_secondary_income int 
, abs_statistical_area int, interest_rate decimal(24,9), origination_date date 
, dss_record_source varchar(255), dss_load_datetime datetime2);