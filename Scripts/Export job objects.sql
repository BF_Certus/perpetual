
/*
Prints DDL scripts of objects required by WhereScape RED job
Input params:
	@wjc_name	- job name
	@types - types of objects to scripts P for procedur), V for view, U for table. Can be any combination
	@drop_existing - generate DROP statement if drop_existing = 1
	@dbname	- (optional)database name to remove, default current database 
Example:
exec REF.Export_Job_Objects 'perpetual_calculations', 'UVP', 0;	-- prints create only scripts for tables, views and procedures
exec REF.Export_Job_Objects 'perpetual_calculations', 'VP', 1;	-- prints drop/create scripts for view and procedures

*/
if object_id('REF.Export_Job_Objects') is not null 
	drop procedure REF.Export_Job_Objects;
go

create procedure REF.Export_Job_Objects 
	@wjc_name varchar(100)
,	@types varchar(10)
,	@drop_existing bit
,	@dbname varchar(100) = null

as 

set nocount on;

-- INPUT PARAMS HERE:
--select 
--	@wjc_name	= 'perpetual_calculations'
--,	@types		= 'P'
--,	@dbname		= 'EDW_customer1'
--,	@drop_existing = 0;


declare 
	@whattocreate as char(1)
,	@schema_name	as sysname
,	@object_name as sysname
,	@short_name as sysname
,	@type as char(1)
,	@prefix varchar(128)
,	@createsql as varchar(max);

--set @whattocreate = 'P' -- ex: p = procedures, v = views
select @dbname = ISNULL(@dbname, DB_NAME());
select @prefix = 'DV.'; 


declare job_cursor cursor for
with job as (select wjc_job_key from ws_wrk_job_ctrl where wjc_name =  @wjc_name)
	,task as (select wtc_name, wtc_order_a, wtc_order_b, wtc_order_c from ws_wrk_task_ctrl inner join job on wtc_job_key = job.wjc_job_key)
	,normal as (select nt_update_key, nt_schema as schema_name, nt_table_name as objname, wtc_order_a, wtc_order_b, wtc_order_c from ws_normal_tab inner join task on nt_table_name = task.wtc_name)
	,stage as (select st_update_key, st_schema schema_name, st_table_name as objname, wtc_order_a, wtc_order_b, wtc_order_c  from ws_stage_tab inner join task on st_table_name = task.wtc_name)
	,[load] as (select lt_obj_key, lt_schema schema_name, lt_table_name as objname, cast(lt_source_schema as nvarchar(255)) as source_schema, cast(lt_source_table as nvarchar(255)) as source_table, wtc_order_a, wtc_order_b, wtc_order_c from ws_load_tab inner join task on lt_table_name = task.wtc_name)
	,header as (select ph_obj_key, ph_name, wtc_order_a, wtc_order_b, wtc_order_c  from ws_pro_header inner join normal on ph_obj_key = normal.nt_update_key
				union select ph_obj_key, ph_name, wtc_order_a, wtc_order_b, wtc_order_c  from ws_pro_header inner join stage on ph_obj_key = stage.st_update_key
--				union select lt_obj_key, 'load_'+ objname, wtc_order_a, wtc_order_b, wtc_order_c from [load]  
	)
	,objnames as (
		select schema_name, objname, wtc_order_a, wtc_order_b, wtc_order_c from [load] t union
		select schema_name, objname, wtc_order_a, wtc_order_b, wtc_order_c  from [stage] union 
		select schema_name, objname, wtc_order_a, wtc_order_b, wtc_order_c  from [normal] union 
--		select source_schema, source_table objname, wtc_order_a, wtc_order_b, wtc_order_c  from [load] union
		select 'dbo' schema_name, ph_name objname, wtc_order_a, wtc_order_b, wtc_order_c  from header	
	)
select schema_name + '.' + objname as [fullname], objname as [shortname], o.[type] from objnames t left join sys.objects o 
	on t.objname = o.name and SCHEMA_ID(t.schema_name) = o.schema_id
order by case type when 'U' then 1 when 'V' then 2 when 'P' then 3 end, wtc_order_a, wtc_order_b, wtc_order_c;

open job_cursor fetch next from job_cursor into @object_name, @short_name, @type;

while @@fetch_status = 0
begin
--	print @object_name + ' : ' + @type;
	if(@type = 'P') set @object_name = @prefix + @short_name;	-- add schema to stored procs

	if(charindex(@type, @types)>0 and @drop_existing = 1) begin	-- generate DROP statement
		select @createsql = CHAR(10) + 'if object_id(''' + @object_name  + ''') is not null ' + CHAR(10) + 'begin' + CHAR(10) +
		char(9) + 'drop '+ case @type when 'P' then 'procedure ' when 'V' then 'view ' when 'U' then 'table ' end +  @object_name + ';' + char(10) + 'end '+ char(10) +
		'GO' + char(10) + CHAR(13) + char(10);
--		if not (@type = 'U' and @object_name like 'INGEST%') begin
			print @createsql;
--		end
		set @createsql = null;
	end

	if(charindex('U', @types)>0 and @type = 'U') begin -- and @object_name not like 'INGEST%' ) begin	-- script user table
		exec dbo.ScriptFor @object_name, @createsql output;
		exec dbo.LongPrint @createsql;
		set @createsql = null;
	end 

	if(charindex(@type, @types)>0 and @type <> 'U') begin								-- script view or procs
		select @createsql = OBJECT_DEFINITION (OBJECT_ID(case @type when 'P' then @short_name else @object_name end)) + CHAR(13) + 'GO' + CHAR(13)+CHAR(10)+CHAR(10);
		select @createsql = REPLACE(@createsql, @dbname + '.', '');					-- remove database reference
		if(@type = 'P') select @createsql = REPLACE(@createsql, @short_name, @prefix + @short_name); -- add schema name to stored proc
		exec dbo.LongPrint @createsql;
		set @createsql = null;
	end

	if exists(select  1  from [REF].[Create_Load_Procs] where TargetTable = @object_name and charindex('P', @types)>0) begin	-- load table procedures
		select @createsql = COALESCE(@createsql, CHAR(13)+char(10)) + CreatProc + char(10)  from [REF].[Create_Load_Procs] where TargetTable = @object_name order by ord, ColOrd;
		if(@drop_existing = 1) begin
			select @createsql= 'IF OBJECT_ID(''' + @prefix + 'update_'+ @short_name + ''') IS NOT NULL  '
				+ CHAR(10) + 'begin' + CHAR(10) +  CHAR(9) + 'DROP PROCEDURE ' + @prefix + 'update_' + @short_name + ';'  + char(10) + 'end '+ char(10)
				+ 'GO' + CHAR(10)+CHAR(13)+REPLACE(REPLACE(@createsql, 'update_'+ @short_name, @prefix + 'update_'+ @short_name),'INGEST_PERPETUAL','INGEST');
		end
		exec dbo.LongPrint @createsql;
		set @createsql = null;
	end 

	fetch next from job_cursor into @object_name, @short_name, @type;
end

close job_cursor
deallocate job_cursor
