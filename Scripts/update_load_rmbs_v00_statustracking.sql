truncate table LOAD.load_rmbs_v00_statustracking;
INSERT INTO LOAD.load_rmbs_v00_statustracking
(hk_l_rmbs_v00, record_status, dss_record_source, dss_load_datetime)
SELECT
RMBS_v00_StatusTracking.hk_l_rmbs_v00,RMBS_v00_StatusTracking.Record_Status,NULL,GETDATE()
From INGEST.RMBS_v00_StatusTracking RMBS_v00_StatusTracking
;
UPDATE LOAD.load_rmbs_v00_statustracking SET dss_record_source = 'Customer1_DW.INGEST.RMBS_v00_StatusTracking' ;