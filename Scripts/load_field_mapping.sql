CREATE TABLE LOAD.load_field_mapping (
  feedid int, target_attribute_name varchar(500), field_mapping_id int
, feed_field_sequence int, feed_header_attribute_name varchar(100) 
, stage_attribute_name varchar(100), business_attribute_name varchar(100) 
, target_attribute_datatype varchar(100), analytics_qualifier varchar(100) 
, active bit, update_date datetime, dss_record_source varchar(255) 
, dss_load_datetime datetime2);