--==============================================================================
-- DBMS Name        :    SQL Server
-- Procedure Name   :    update_s_rmbs_col_v0_afb94090
-- Template         :    cert_wsl_sqlserver_proc_dv_perm
-- Template Version :    6.9.1.0
-- Description      :    Update the Satellite table s_rmbs_col_v00
-- Generated by     :    WhereScape RED Version 8.0.1.0 (build 171017-084020)
-- Generated for    :    Certus Evaluate until 01 August 2018
-- Generated on     :    Monday, July 02, 2018 at 16:34:44
-- Author           :    Bronwen
--==============================================================================
-- Notes / History
--


--
CREATE PROCEDURE update_s_rmbs_col_v00
  @p_sequence         INTEGER
, @p_job_name         VARCHAR(256)
, @p_task_name        VARCHAR(256)
, @p_job_id           INTEGER
, @p_task_id          INTEGER
, @p_return_msg       VARCHAR(256) OUTPUT
, @p_status           INTEGER      OUTPUT

AS
  SET XACT_ABORT OFF  -- Turn off auto abort on errors
  SET NOCOUNT ON      -- Turn off row count messages

  --=====================================================
  -- Control variables used in most programs
  --=====================================================
  DECLARE
    @v_msgtext               VARCHAR(255)  -- Text for audit_trail
  , @v_sql                   NVARCHAR(255) -- Text for SQL statements
  , @v_step                  INTEGER       -- return code
  , @v_insert_count          INTEGER       -- no of records inserted
  , @v_return_status         INTEGER       -- Update result status
  , @v_current_datetime      DATETIME      -- Used for date insert

  --=====================================================
  -- General Variables
  --=====================================================


  --=====================================================
  -- MAIN
  --=====================================================
  DECLARE @v_joinColumn VARCHAR(MAX); 
  DECLARE @v_hubKey VARCHAR(MAX); 
  DECLARE @v_srcTable VARCHAR(MAX); 
  DECLARE @SQL VARCHAR(max); 
  DECLARE @v_insert_msg VARCHAR(max); 
  DECLARE 
  @v_ParentTable   nvarchar(256)      --  Target Table Name
, @v_ParentColumn   nvarchar(256)  
, @v_sql2    nvarchar(4000) 
, @v_sql5    nvarchar(4000) 
, @v_sql6   nvarchar(4000) 
, @v_sql7   nvarchar(4000) 
, @v_tabtype   nvarchar(4000) 

  SET @v_insert_msg = ''
  SET @v_step = 100

  SET @v_insert_count = 0
  SET @v_current_datetime = GETDATE()


  BEGIN TRY

    --=====================================================
    -- Insert new records
    --=====================================================

    SET @v_step = 200



    BEGIN TRANSACTION



    

---------------------------------------------------
    
-- 
-- Load All Hubs 



--    Loading all hubs regardless where we load a satellite
--    walk the columns 
--    find the hk_ columns
--    if hk column then find the source table column and the business keys
--    we have to save them somewhere and the 


    

      

	   SET @v_step = 300


 INSERT INTO [DV].[s_rmbs_col_v00]
      ( hk_l_rmbs_col_v00
      , dss_load_datetime
      , dss_change_hash
      , dss_record_source
      , dss_start_date
      , dss_version
      , dss_create_time
      , collateral_date
      , group_loan_id
      , security_property_postcode
      , security_property_country
      , original_security_property_value
      , most_recent_security_property_value
      , original_property_valuation_type
      , most_recent_property_valuation_type
      , most_recent_security_property_valuation_date
      , security_property_purpose
      , security_property_type
      , lien
      , abs_statistical_area
      , main_security_methodology
      , main_security_flag
      , property_valuation_currency)
    SELECT DISTINCT    
stage_rmbs_col_v00.hk_l_rmbs_col_v00 AS hk_l_rmbs_col_v00 
      , stage_rmbs_col_v00.dss_load_datetime AS dss_load_datetime 
      , stage_rmbs_col_v00.dss_change_hash_stage_rmbs_col_v00 AS dss_change_hash 
      , stage_rmbs_col_v00.dss_record_source AS dss_record_source 
      , @v_current_datetime AS dss_start_date 
      , ISNULL(current_rows.dss_version,0) + 1 AS dss_version 
      , @v_current_datetime AS dss_create_time 
      , stage_rmbs_col_v00.collateral_date AS collateral_date 
      , stage_rmbs_col_v00.group_loan_id AS group_loan_id 
      , stage_rmbs_col_v00.security_property_postcode AS security_property_postcode 
      , stage_rmbs_col_v00.security_property_country AS security_property_country 
      , stage_rmbs_col_v00.original_security_property_value AS original_security_property_value 
      , stage_rmbs_col_v00.most_recent_security_property_value AS most_recent_security_property_value 
      , stage_rmbs_col_v00.original_property_valuation_type AS original_property_valuation_type 
      , stage_rmbs_col_v00.most_recent_property_valuation_type AS most_recent_property_valuation_type 
      , stage_rmbs_col_v00.most_recent_security_property_valuation_date AS most_recent_security_property_valuation_date 
      , stage_rmbs_col_v00.security_property_purpose AS security_property_purpose 
      , stage_rmbs_col_v00.security_property_type AS security_property_type 
      , stage_rmbs_col_v00.lien AS lien 
      , stage_rmbs_col_v00.abs_statistical_area AS abs_statistical_area 
      , stage_rmbs_col_v00.main_security_methodology AS main_security_methodology 
      , stage_rmbs_col_v00.main_security_flag AS main_security_flag 
      , stage_rmbs_col_v00.property_valuation_currency AS property_valuation_currency 
    FROM [STAGE].[stage_rmbs_col_v00] stage_rmbs_col_v00
    LEFT OUTER JOIN (
        SELECT s_rmbs_col_v00.hk_l_rmbs_col_v00
             , MAX(s_rmbs_col_v00.dss_start_date) AS dss_start_date
             , MAX(s_rmbs_col_v00.dss_version) AS dss_version
        FROM   [DV].[s_rmbs_col_v00] s_rmbs_col_v00
        GROUP BY s_rmbs_col_v00.hk_l_rmbs_col_v00
        ) AS current_rows
      ON  stage_rmbs_col_v00.hk_l_rmbs_col_v00 = current_rows.hk_l_rmbs_col_v00
    WHERE NOT EXISTS (
        SELECT 1
        FROM   [DV].[s_rmbs_col_v00] s_rmbs_col_v00
        WHERE  stage_rmbs_col_v00.hk_l_rmbs_col_v00 = s_rmbs_col_v00.hk_l_rmbs_col_v00
        AND    stage_rmbs_col_v00.dss_change_hash_stage_rmbs_col_v00 = s_rmbs_col_v00.dss_change_hash
        AND    current_rows.dss_start_date = s_rmbs_col_v00.dss_start_date
        )

        SELECT @v_insert_count = @@ROWCOUNT



        ;


        SELECT @v_insert_count = @@ROWCOUNT





  COMMIT

    --=====================================================
    -- All Done report the results
    --=====================================================
    SET @v_step = 400


    SET @p_status = 1
    SET @p_return_msg = @v_insert_msg + 's_rmbs_col_v00 updated. '
      + CONVERT(VARCHAR,@v_insert_count) + ' new records.'

    RETURN 0


  END TRY
  BEGIN CATCH

    SET @p_status = -2
    SET @p_return_msg = SUBSTRING('update_s_rmbs_col_v0_afb94090 FAILED with error '
      + CONVERT(VARCHAR,ISNULL(ERROR_NUMBER(),0))
      + ' Step ' + CONVERT(VARCHAR,ISNULL(@v_step,0))
      + '. Error Msg: ' + ERROR_MESSAGE(),1,255)

  END CATCH
  IF XACT_STATE() <> 0
  BEGIN
    ROLLBACK TRANSACTION
  END

  RETURN 0

