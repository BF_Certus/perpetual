CREATE TABLE DV.bvs_TRAN_seller (
  hk_l_TRAN_seller binary(20) NOT NULL, dss_load_datetime datetime 
, dss_change_hash binary(20) NOT NULL, dss_record_source varchar(255) 
, dss_start_date datetime, dss_version int, dss_create_time datetime 
, seller_count int, seller_position int, seller_abn varchar(5000) 
, seller_jurisdiction varchar(5000), seller_id varchar(5000) 
, seller_id_descriptor varchar(5000));