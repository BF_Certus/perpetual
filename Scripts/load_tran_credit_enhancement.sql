CREATE TABLE LOAD.load_TRAN_credit_enhancement (
  clientid varchar(100), reporting_period varchar(100) 
, master_trust_name_or_spv varchar(100) 
, series_trust_name_or_series varchar(100), transaction_id varchar(100) 
, credit_enhancement_name varchar(5000) 
, credit_enhancement_provider_abn varchar(5000), feedid varchar(100) 
, credit_enhancement_count int, credit_enhancement_position int 
, credit_enhancement_provider_name varchar(5000) 
, credit_enhancement_provider_jurisdiction varchar(5000) 
, credit_enhancement_provider_id varchar(5000) 
, credit_enhancement_provider_id_descriptor varchar(5000) 
, credit_enhancement_type varchar(5000) 
, credit_enhancement_currency varchar(5000) 
, credit_enhancement_fee varchar(5000) 
, initial_credit_enhancement_amount varchar(5000) 
, beginning_amount_available_credit varchar(5000) 
, restorations_credit varchar(5000), draws_credit varchar(5000) 
, reduction_credit varchar(5000), ending_amount_available_credit varchar(5000) 
, beginning_amount_drawn_credit varchar(5000) 
, ending_amount_drawn_credit varchar(5000), dss_record_source varchar(255) 
, dss_load_datetime datetime2);