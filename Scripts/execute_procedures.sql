IF OBJECT_ID(N'dbo.execute_job_procedures', N'p')IS NOT NULL  
   DROP PROCEDURE dbo.execute_job_procedures;  
GO

-- exec  dbo.execute_job_procedures  '[INGEST].[update_proc_test],[INGEST].[update_proc_test]';

CREATE PROCEDURE dbo.execute_job_procedures  
	@procedures	varchar(max)
,	@batchId	int = null	

AS 

SET NOCOUNT ON;
SET XACT_ABORT OFF;

DECLARE 
	@proc_name VARCHAR(128)
,	@return_msg	VARCHAR(256)
,	@status		INTEGER
,	@audit_id	bigint;

WHILE LEN(@procedures) > 0 BEGIN
	SET @proc_name = left(@procedures, charindex(',', @procedures+',')-1);

	BEGIN TRY
		INSERT INTO DV.[load_audit_sp] ([Procedue_Name],[BatchId],[Started]) SELECT @proc_name, @batchId, getutcdate();
		SELECT @audit_id = SCOPE_IDENTITY();
	
		EXEC @proc_name 0 , '' , '' , 0 , 0 , @return_msg OUTPUT, @status OUTPUT WITH RESULT SETS NONE;

		UPDATE DV.[load_audit_sp] SET [Finished] = getutcdate(), [Status] =@status, [Message] =@return_msg 
		WHERE [Audit_id] =@audit_id;

		IF @status <> 1 THROW 60000, @return_msg, 1;
	END TRY
	BEGIN CATCH
		UPDATE DV.[load_audit_sp] SET [Finished] = getutcdate(), [Status] =-2, [Message] =ERROR_MESSAGE() 
		WHERE [Audit_id] =@audit_id;
		THROW;
	END CATCH

	SET @procedures = stuff(@procedures, 1, charindex(',', @procedures+','), '')
END


RETURN 0

GO

