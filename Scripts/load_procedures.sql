
IF OBJECT_ID ('DV.load_procedures', 'U') IS NOT NULL  
   DROP TABLE DV.load_procedures;  
go

create table DV.load_procedures (
	Load_Name varchar(128) not null,
	Proc_Name varchar(128) not null,
	Group_Id int not null,
	Order_Id int not null,
	ActionId int not null,
	constraint load_procedures_PK primary key (Load_Name,Proc_Name)
)
go
