/****** Object:  View [INGEST].[TRAN_Originator]    Script Date: 4/07/2018 11:01:25 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--Drop view [INGEST].[TRAN_Originator]
--go
Create View [INGEST].[TRAN_Originator] as 
 
  WITH Nbrs ( Number ) AS (
    SELECT 1 UNION ALL
    SELECT 1 + Number FROM Nbrs WHERE Number < 99
)
SELECT h_cm.ClientID, h_fm.FeedID, h_rp.Reporting_Period, h_t.Master_Trust_Name_or_SPV, h_t.Series_Trust_Name_or_Series, h_t.Transaction_ID,
DelimCount as Originator_Count, N.Number as Originator_Position,
Case when DelimCount = 0 then Originator_Name else SubString(  Originator_Name , dbo.FN_CERTUS_INSTR(  Originator_Name ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Originator_Name,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Originator_Name ,';',1,N.Number-1)-1) end as Originator_Name,
Case when DelimCount = 0 then Originator_ABN else SubString(  Originator_ABN , dbo.FN_CERTUS_INSTR(  Originator_ABN ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Originator_ABN,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Originator_ABN ,';',1,N.Number-1)-1) end as Originator_ABN,
Case when DelimCount = 0 then Originator_Jurisdiction else SubString(  Originator_Jurisdiction , dbo.FN_CERTUS_INSTR(  Originator_Jurisdiction ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Originator_Jurisdiction,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Originator_Jurisdiction ,';',1,N.Number-1)-1) end as Originator_Jurisdiction,
Case when DelimCount = 0 then Originator_ID else SubString(  Originator_ID , dbo.FN_CERTUS_INSTR(  Originator_ID ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Originator_ID,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Originator_ID ,';',1,N.Number-1)-1) end as Originator_ID,
Case when DelimCount = 0 then Originator_ID_Descriptor else SubString(  Originator_ID_Descriptor , dbo.FN_CERTUS_INSTR(  Originator_ID_Descriptor ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Originator_ID_Descriptor,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Originator_ID_Descriptor ,';',1,N.Number-1)-1) end as Originator_ID_Descriptor,
Case when DelimCount = 0 then Loans_Originated_Settlement_Date else SubString(  Loans_Originated_Settlement_Date , dbo.FN_CERTUS_INSTR(  Loans_Originated_Settlement_Date ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Loans_Originated_Settlement_Date,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Loans_Originated_Settlement_Date ,';',1,N.Number-1)-1) end as Loans_Originated_Settlement_Date,
Case when DelimCount = 0 then Loans_Originated else SubString(  Loans_Originated , dbo.FN_CERTUS_INSTR(  Loans_Originated ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Loans_Originated,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Loans_Originated ,';',1,N.Number-1)-1) end as Loans_Originated
from 
	DV.S_TRAN_v00 s
	inner join 
	DV.L_TRAN_v00 l on s.hk_l_TRAN_v00=l.hk_l_TRAN_v00
	inner join
	DV.h_client_master h_cm on h_cm.hk_h_client_master=l.hk_h_client_master
	inner join
	DV.h_feed_master h_fm on h_fm.hk_h_feed_master=l.hk_h_feed_master
	inner join
	DV.h_reporting_period h_rp on h_rp.hk_h_reporting_period=l.hk_h_reporting_period
	inner join
	DV.h_transaction h_t on h_t.hk_h_transaction=l.hk_h_transaction
	inner join
	(select TRAN_v00_DelimiterCount.hk_l_TRAN_v00, case 
		when Delim_Originator_Name=Delim_Originator_ABN
		and Delim_Originator_Name=Delim_Originator_Jurisdiction
		and Delim_Originator_Name=Delim_Originator_ID
		and Delim_Originator_Name=Delim_Originator_ID_Descriptor
		and Delim_Originator_Name=Delim_Loans_Originated_Settlement_Date
		and Delim_Originator_Name=Delim_Loans_Originated
		then Delim_Originator_Name+1 end as DelimCount from INGEST.TRAN_v00_DelimiterCount) delim on delim.hk_l_TRAN_v00=l.hk_l_TRAN_v00
	inner join
	Nbrs N ON (N.Number <= DelimCount) or (DelimCount = 0 and N.Number =1)
GO


