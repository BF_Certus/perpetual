/****** Object:  View [INGEST].[SECU_Rename]    Script Date: 2/07/2018 3:23:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--Drop view INGEST.SECU
Create view [INGEST].[SECU_Rename] as 
Select ClientID, 
S.Feed_Name,
Convert([nvarchar](100),PS000) as Reporting_Period
,PS001 as Report_Date
,PS002 as Transaction_Id
,PS003 as Master_Trust_Name_Or_Spv
,PS004 as Series_Trust_Name_Or_Series
,PS005 as Tranche_Name
,PS006 as Isin
,PS007 as Austraclear_Series_Id
,PS008 as Clearing_System_Name
,PS009 as Issue_Date
,PS010 as Accrual_Begin_Date
,PS011 as Accrual_End_Date
,PS012 as Accrual_Period
,PS013 as Tranche_Currency
,PS014 as Exchange_Name
,PS015 as Name
,PS016 as Original_Rating
,PS017 as Current_Rating
,PS018 as Original_Subordination
,PS019 as Current_Subordination
,PS020 as Current_Principal_Face_Amount
,PS021 as Offer_Type
,PS022 as Coupon_Payment_Reference
,PS023 as Distribution_Date
,PS024 as Record_Date
,PS025 as Determination_Date
,PS026 as Beginning_Stated_Factor
,PS027 as Ending_Stated_Factor
,PS028 as Original_Principal_Face_Amount
,PS029 as Beginning_Invested_Amount
,PS030 as Ending_Invested_Amount
,PS031 as Beginning_Stated_Amount
,PS032 as Ending_Stated_Amount
,PS033 as Beginning_Invested_Factor
,PS034 as Ending_Invested_Factor
,PS035 as Coupon_Reference_Index
,PS036 as Coupon_Reference_Index_Value
,PS037 as Coupon_Margin
,PS038 as Step_Up_Margin
,PS039 as Step_Up_Date
,PS040 as Scheduled_Coupon
,PS041 as Scheduled_Coupon_Factor
,PS042 as Current_Coupon_Rate
,PS043 as Coupon_Distribution
,PS044 as Coupon_Distribution_Factor
,PS045 as Scheduled_Principal
,PS046 as Scheduled_Principal_Factor
,PS047 as Principal_Distribution
,PS048 as Principal_Distribution_Factor
,PS049 as Coupon_Shortfall_Factor
,PS050 as Ending_Cumulative_Coupon_Shortfall_Factor
,PS051 as Coupon_Shortfall
,PS052 as Ending_Cumulative_Coupon_Shortfall
,PS053 as Principal_Shortfall
,PS054 as Ending_Cumulative_Principal_Shortfall
,PS055 as Legal_Maturity_Date
,PS056 as Original_Estimated_Weighted_Average_Life
,PS057 as Current_Weighted_Average_Life
,PS058 as Expected_Final_Maturity_Date
,PS059 as Redemption_Price
,PS060 as ABN
,PS061 as Jurisdiction
,PS062 as Id
,PS063 as Id_Descriptor
,PS064 as Governing_Law
,PS065 as Soft_Bullet_Date
,PS066 as Coupon_Basis
,PS067 as Step_Up_Rate
,PS068 as Beginning_Cumulative_Chargeoffs
,PS069 as Beginning_Cumulative_Coupon_Shortfall
,PS070 as Beginning_Cumulative_Principal_Shortfall
,PS071 as Coupon_Shortfall_Makeup
,PS072 as Principal_Shortfall_Makeup
,PS073 as Allocated_Chargeoffs
,PS074 as Chargeoff_Restorations
,PS075 as Ending_Cumulative_Chargeoffs
,PS076 as Beginning_Cumulative_Chargeoff_Factor
,PS077 as Beginning_Cumulative_Coupon_Shortfall_Factor
,PS078 as Beginning_Cumulative_Principal_Shortfall_Factor
,PS079 as Coupon_Shortfall_Makeup_Factor
,PS080 as Principal_Shortfall_Makeup_Factor
,PS081 as Principal_Shortfall_Factor
,PS082 as Allocated_Chargeoff_Factor
,PS083 as Chargeoff_Restoration_Factor
,PS084 as Ending_Cumulative_Chargeoff_Factor
,PS085 as Ending_Cumulative_Principal_Shortfall_Factor
,PS086 as Coupon_Rate 

FROM INGEST.Ext_Stage_SECU  S inner join  
  Ingest.Feed_Master FM on S.FeedID = FM.FeedID;
GO


