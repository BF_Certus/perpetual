CREATE TABLE DV.h_loan (
  hk_h_loan binary(20) NOT NULL, dss_record_source varchar(255) 
, dss_load_datetime datetime2, dss_create_time datetime2 
, clientid varchar(100) NOT NULL, master_trust_name_or_spv varchar(100) 
, series_trust_name_or_series varchar(100), loan_id varchar(100));
CREATE UNIQUE NONCLUSTERED INDEX h_loan_idx_0 ON DV.h_loan (hk_h_loan) WITH (SORT_IN_TEMPDB = OFF);
CREATE UNIQUE NONCLUSTERED INDEX h_loan_idx_A ON DV.h_loan (series_trust_name_or_series,clientid,master_trust_name_or_spv,loan_id) WITH (SORT_IN_TEMPDB = OFF);
