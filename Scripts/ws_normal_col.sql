Create or alter view [dbo].[ws_normal_col] as 
Select table_num as nc_obj_key,
	KEY_NUM as nc_col_key,
	COL as nc_col_name,
	KEY_TYPE as nc_key_type
FROM [dv].[ws_METADATA]
GO
