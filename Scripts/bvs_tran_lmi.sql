CREATE TABLE DV.bvs_TRAN_lmi (
  hk_l_TRAN_lmi binary(20) NOT NULL, dss_load_datetime datetime 
, dss_change_hash binary(20) NOT NULL, dss_record_source varchar(255) 
, dss_start_date datetime, dss_version int, dss_create_time datetime 
, transaction_id varchar(100), lmi_count int, lmi_position int 
, lmi_provider_abn varchar(5000), loans_insured varchar(5000) 
, timely_payment_claims_outstanding varchar(5000) 
, lmi_provider_jurisdiction varchar(5000), lmi_provider_id varchar(5000) 
, lmi_provider_id_descriptor varchar(5000) 
, loan_currency_insurance varchar(5000));