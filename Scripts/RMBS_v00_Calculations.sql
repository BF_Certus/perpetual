/****** Object:  View [INGEST].[RMBS_v00_Calculations]    Script Date: 6/07/2018 10:55:40 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--Drop view [INGEST].[RMBS_v00_Calculations]
Create view [INGEST].[RMBS_v00_Calculations]  as 

Select
l_rmbs_v00.hk_l_rmbs_v00
,CASE WHEN bvs_rmbs_v00_dataconversion.Offset_Account_Flag = 1 then 'Yes' when bvs_rmbs_v00_dataconversion.Offset_Account_Flag = 0 then 'No' else Null end as Offset_Account_Flag
--,COALESCE(PL019.Value, [Scheduled_Payment_Policy]) as Scheduled_Payment_Policy
,CASE WHEN bvs_rmbs_v00_dataconversion.Conforming_Mortgage = 1 then 'Yes' when bvs_rmbs_v00_dataconversion.Conforming_Mortgage = 0 then 'No' else Null end as Conforming_Mortgage_Flag
--,COALESCE(PL021.Value, [LMI_Underwriting_Type]) as LMI_Underwriting_Type
--,COALESCE(PL023.Value, [Loan_Purpose]) as Loan_Purpose
--,CASE WHEN LD.Loan_Origination_Channel_Flag = 1 then 'Yes' when LD.Loan_Origination_Channel_Flag = 0 then 'No' else Null end as Loan_Origination_Channel_Flag
,CASE WHEN bvs_rmbs_v00_dataconversion.Restructuring_Arrangement_Flag = 1 then 'Yes' when bvs_rmbs_v00_dataconversion.Restructuring_Arrangement_Flag = 0 then 'No' else Null end as Restructuring_Arrangement_Flag
,CASE WHEN bvs_rmbs_v00_dataconversion.First_Home_Buyer_Flag = 1 then 'Yes' when bvs_rmbs_v00_dataconversion.First_Home_Buyer_Flag = 0 then 'No' else Null end as First_Home_Buyer_Flag
,CASE WHEN bvs_rmbs_v00_dataconversion.Bankruptcy_Flag = 1 then 'Yes' when bvs_rmbs_v00_dataconversion.Bankruptcy_Flag = 0 then 'No' else Null end as Bankruptcy_Flag

,                     [Interest_Only_Expiry_Term] =
                                 DATEDIFF(MONTH, bvs_rmbs_v00_dataconversion.[Collateral_Date], bvs_rmbs_v00_dataconversion.[Interest_Only_Expiry_Date])
                                 +
                                 (DATEDIFF( DAY,
                                                                  DATEADD(MONTH, DATEDIFF(MONTH, bvs_rmbs_v00_dataconversion.[Collateral_Date], 
                                                                  bvs_rmbs_v00_dataconversion.[Interest_Only_Expiry_Date]),
                                                                  bvs_rmbs_v00_dataconversion.[Collateral_Date]),
                                                                  bvs_rmbs_v00_dataconversion.[Interest_Only_Expiry_Date])
                                 /100.00), 

                                 [Fixed_Rate_Expiry_Term] =
                                 --CASE WHEN ISDATE([Collateral_Date]) = 0 OR ISDATE([Interest_Only_Expiry_Date]) = 0 OR ISDATE([Current_Rate_Expiry_Date]) = 0 THEN NULL ELSE
                                 DATEDIFF(   MONTH, bvs_rmbs_v00_dataconversion.[Collateral_Date], 
                                 bvs_rmbs_v00_dataconversion.[Interest_Only_Expiry_Date])
                                 +
                                 (DATEDIFF( DAY,
                                                                  DATEADD(MONTH, DATEDIFF(MONTH, bvs_rmbs_v00_dataconversion.[Collateral_Date], bvs_rmbs_v00_dataconversion.[Interest_Only_Expiry_Date])
                                                                                                             ,
                                                                                                             TRY_CONVERT(date, bvs_rmbs_v00_dataconversion.Collateral_Date, 105)
                                                                                        )
                                 , TRY_CONVERT(date, bvs_rmbs_v00_dataconversion.Current_Rate_Expiry_Date, 105)
                                 )
                                 /100.00) --END


                                 ,[Indexed_LVR]                =        
                                                                             
                                 CASE WHEN DateDiff(DAY,'20050131',Convert(Date, '01-' + h_reporting_period.Reporting_Period, 105)) >=0 AND 
                                 bvs_rmbs_v00_dataconversion.Most_Recent_Property_Valuation_Date IS NOT NULL AND 
                                 s_rmbs_v00.Property_Postcode IS NOT NULL 
                                                                                                                                                                                                     
                                 THEN bvs_rmbs_v00_dataconversion.[Origination_Date_Settlement_Date] --                          bvs_rmbs_v00_dataconversion.Current_Balance / NULLIF((bvs_rmbs_v00_dataconversion.Most_Recent_Property_Value * (0 / NULLIF(0, 0))), 0)
                                 ELSE bvs_rmbs_v00_dataconversion.[Origination_Date_Settlement_Date] END,
                                 h_client_master.clientid as Data_Issuer_ID,
                                 [Data_Issuer_Code]                                                                                     =  s_client_master.client_name,
                                 [Data_Issuer_Name]                                                                                   =  s_client_master.client_name,
                                 [Data_Issuer_ABN]                                                                                      = 99999999,
                                 [Arrears_Category]                                                                                      = CASE WHEN bvs_rmbs_v00_dataconversion.Days_In_Arrears <= 30 OR bvs_rmbs_v00_dataconversion.Days_In_Arrears is null THEN '0-30'
                                                                                                                                                                                  WHEN bvs_rmbs_v00_dataconversion.Days_In_Arrears >30 AND bvs_rmbs_v00_dataconversion.Days_In_Arrears <= 60 THEN '30-60'
                                                                                                                                                                                  WHEN bvs_rmbs_v00_dataconversion.Days_In_Arrears >60 AND bvs_rmbs_v00_dataconversion.Days_In_Arrears <= 90 THEN '60-90'
                                                                                                                                                                                  WHEN bvs_rmbs_v00_dataconversion.Days_In_Arrears >90 THEN '90+'
                                                                                                                                                                                          ELSE NULL END,
                                 [Date_Key]                                                                                                    = Convert(Date, '01-' + h_reporting_period.Reporting_Period, 105),
                                 [EOMonth_Collateral_Date]                                                           = EOMONTH(bvs_rmbs_v00_dataconversion.[Collateral_Date]),
                                 [OriginationYear]                                                                              = YEAR(bvs_rmbs_v00_dataconversion.[Origination_Date_Settlement_Date]),
                                 [Property_Region]                                                                                       =  '!Unknown',
                                 [Property_State]                                                                               =  '!Unknown'
--                             From dv.bvs_rmbs_v00_dataconversion src 
--                   where src.dss_load_datetime = (Select max(dss_load_datetime) from DV.bvs_rmbs_v00_dataconversion z where z.hk_l_rmbs_v00 = src.hk_l_rmbs_v00)
--G
                                                   
FROM DV.l_rmbs_v00 l_rmbs_v00 full outer join DV.REF_PITs REF_PITs on 1=1
full outer join DV.bvs_rmbs_v00_dataconversion bvs_rmbs_v00_dataconversion
           on l_rmbs_v00.hk_l_rmbs_v00 = bvs_rmbs_v00_dataconversion.hk_l_rmbs_v00 
           and bvs_rmbs_v00_dataconversion.dss_load_datetime = (
                      Select Max(z.dss_load_datetime)
                      from DV.bvs_rmbs_v00_dataconversion z
                      where z.hk_l_rmbs_v00 = l_rmbs_v00.hk_l_rmbs_v00
                      and z.dss_load_datetime <= Ref_PITs.PIT_Date)
full outer join DV.h_client_master h_client_master
           on l_rmbs_v00.hk_h_client_master = h_client_master.hk_h_client_master
full outer join DV.h_reporting_period h_reporting_period
           on l_rmbs_v00.hk_h_reporting_period = h_reporting_period.hk_h_reporting_period
full outer join DV.s_rmbs_v00 s_rmbs_v00
           on l_rmbs_v00.hk_l_rmbs_v00 = s_rmbs_v00.hk_l_rmbs_v00
           and s_rmbs_v00.dss_load_datetime = (
                      Select Max(z.dss_load_datetime)
                      from DV.s_rmbs_v00 z
                      where z.hk_l_rmbs_v00 = l_rmbs_v00.hk_l_rmbs_v00
                      and z.dss_load_datetime <= Ref_PITs.PIT_Date)
full outer join DV.s_client_master s_client_master
           on l_rmbs_v00.hk_h_client_master = s_client_master.hk_h_client_master
           and s_client_master.dss_load_datetime = (
                      Select Max(z.dss_load_datetime)
                      from DV.s_client_master z
                      where z.hk_h_client_master = l_rmbs_v00.hk_h_client_master
                      and z.dss_load_datetime <= Ref_PITs.PIT_Date)
WHERE REF_PITs.PIT_NAME = 'Current Date';

