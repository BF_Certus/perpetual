/****** Object:  View [INGEST].[TRAN_Trigger]    Script Date: 4/07/2018 11:01:25 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--Drop view [INGEST].[TRAN_Trigger]
--go
Create View [INGEST].[TRAN_Trigger] as 
 
  WITH Nbrs ( Number ) AS (
    SELECT 1 UNION ALL
    SELECT 1 + Number FROM Nbrs WHERE Number < 99
)
SELECT h_cm.ClientID, h_fm.FeedID, h_rp.Reporting_Period, h_t.Master_Trust_Name_or_SPV, h_t.Series_Trust_Name_or_Series, h_t.Transaction_ID,
DelimCount as Trigger_Count, N.Number as Trigger_Position,
Case when DelimCount = 0 then Trigger_Name else SubString(  Trigger_Name , dbo.FN_CERTUS_INSTR(  Trigger_Name ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Trigger_Name,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Trigger_Name ,';',1,N.Number-1)-1) end as Trigger_Name,
Case when DelimCount = 0 then Trigger_Description else SubString(  Trigger_Description , dbo.FN_CERTUS_INSTR(  Trigger_Description ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Trigger_Description,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Trigger_Description ,';',1,N.Number-1)-1) end as Trigger_Description,
Case when DelimCount = 0 then Trigger_State else SubString(  Trigger_State , dbo.FN_CERTUS_INSTR(  Trigger_State ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Trigger_State,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Trigger_State ,';',1,N.Number-1)-1) end as Trigger_State
from 
	DV.S_TRAN_v00 s
	inner join 
	DV.L_TRAN_v00 l on s.hk_l_TRAN_v00=l.hk_l_TRAN_v00
	inner join
	DV.h_client_master h_cm on h_cm.hk_h_client_master=l.hk_h_client_master
	inner join
	DV.h_feed_master h_fm on h_fm.hk_h_feed_master=l.hk_h_feed_master
	inner join
	DV.h_reporting_period h_rp on h_rp.hk_h_reporting_period=l.hk_h_reporting_period
	inner join
	DV.h_transaction h_t on h_t.hk_h_transaction=l.hk_h_transaction
	inner join
	(select TRAN_v00_DelimiterCount.hk_l_TRAN_v00, case 
		when Delim_Trigger_Name=Delim_Trigger_Description
		and Delim_Trigger_Name=Delim_Trigger_State
		then Delim_Trigger_Name+1 end as DelimCount from INGEST.TRAN_v00_DelimiterCount) delim on delim.hk_l_TRAN_v00=l.hk_l_TRAN_v00
	inner join
	Nbrs N ON (N.Number <= DelimCount) or (DelimCount = 0 and N.Number =1)

GO


