/****** Object:  View [INGEST].[TRAN_Signature_Authority]    Script Date: 4/07/2018 11:01:25 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--Drop view [INGEST].[TRAN_Signature_Authority]
--go
Create View [INGEST].[TRAN_Signature_Authority] as 
 
  WITH Nbrs ( Number ) AS (
    SELECT 1 UNION ALL
    SELECT 1 + Number FROM Nbrs WHERE Number < 99
)
SELECT h_cm.ClientID, h_fm.FeedID, h_rp.Reporting_Period, h_t.Master_Trust_Name_or_SPV, h_t.Series_Trust_Name_or_Series, h_t.Transaction_ID,
DelimCount as Signature_Authority_Count, N.Number as Signature_Authority_Position,
Case when DelimCount = 0 then Signature_Authority_Name else SubString(  Signature_Authority_Name , dbo.FN_CERTUS_INSTR(  Signature_Authority_Name ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Signature_Authority_Name,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Signature_Authority_Name ,';',1,N.Number-1)-1) end as Signature_Authority_Name,
Case when DelimCount = 0 then Signature_Authority_ABN else SubString(  Signature_Authority_ABN , dbo.FN_CERTUS_INSTR(  Signature_Authority_ABN ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Signature_Authority_ABN,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Signature_Authority_ABN ,';',1,N.Number-1)-1) end as Signature_Authority_ABN,
Case when DelimCount = 0 then Signature_Authority_Jurisdiction else SubString(  Signature_Authority_Jurisdiction , dbo.FN_CERTUS_INSTR(  Signature_Authority_Jurisdiction ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Signature_Authority_Jurisdiction,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Signature_Authority_Jurisdiction ,';',1,N.Number-1)-1) end as Signature_Authority_Jurisdiction,
Case when DelimCount = 0 then Signature_Authority_ID else SubString(  Signature_Authority_ID , dbo.FN_CERTUS_INSTR(  Signature_Authority_ID ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Signature_Authority_ID,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Signature_Authority_ID ,';',1,N.Number-1)-1) end as Signature_Authority_ID,
Case when DelimCount = 0 then Signature_Authority_ID_Descriptor else SubString(  Signature_Authority_ID_Descriptor , dbo.FN_CERTUS_INSTR(  Signature_Authority_ID_Descriptor ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Signature_Authority_ID_Descriptor,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Signature_Authority_ID_Descriptor ,';',1,N.Number-1)-1) end as Signature_Authority_ID_Descriptor,
Case when DelimCount = 0 then Account_Signature_Authority_Name else SubString(  Account_Signature_Authority_Name , dbo.FN_CERTUS_INSTR(  Account_Signature_Authority_Name ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Account_Signature_Authority_Name,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Account_Signature_Authority_Name ,';',1,N.Number-1)-1) end as Account_Signature_Authority_Name
from 
	DV.S_TRAN_v00 s
	inner join 
	DV.L_TRAN_v00 l on s.hk_l_TRAN_v00=l.hk_l_TRAN_v00
	inner join
	DV.h_client_master h_cm on h_cm.hk_h_client_master=l.hk_h_client_master
	inner join
	DV.h_feed_master h_fm on h_fm.hk_h_feed_master=l.hk_h_feed_master
	inner join
	DV.h_reporting_period h_rp on h_rp.hk_h_reporting_period=l.hk_h_reporting_period
	inner join
	DV.h_transaction h_t on h_t.hk_h_transaction=l.hk_h_transaction
	inner join
	(select TRAN_v00_DelimiterCount.hk_l_TRAN_v00, case
		when Delim_Signature_Authority_Name=Delim_Signature_Authority_ABN
		and Delim_Signature_Authority_Name=Delim_Signature_Authority_Jurisdiction
		and Delim_Signature_Authority_Name=Delim_Signature_Authority_ID
		and Delim_Signature_Authority_Name=Delim_Signature_Authority_ID_Descriptor
		and Delim_Signature_Authority_Name=Delim_Account_Signature_Authority_Name
		then Delim_Signature_Authority_Name+1 end as DelimCount from INGEST.TRAN_v00_DelimiterCount) delim on delim.hk_l_TRAN_v00=l.hk_l_TRAN_v00
	inner join
	Nbrs N ON (N.Number <= DelimCount) or (DelimCount = 0 and N.Number =1)

GO


