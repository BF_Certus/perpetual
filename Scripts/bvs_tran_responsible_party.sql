CREATE TABLE DV.bvs_TRAN_responsible_party (
  hk_l_TRAN_responsible_party binary(20) NOT NULL 
, dss_load_datetime datetime, dss_change_hash binary(20) NOT NULL 
, dss_record_source varchar(255), dss_start_date datetime, dss_version int 
, dss_create_time datetime, responsible_party_count int 
, responsible_party_position int, responsible_party_abn varchar(5000) 
, responsible_party_jurisdiction varchar(5000) 
, responsible_party_id varchar(5000) 
, responsible_party_id_descriptor varchar(5000) 
, responsible_party_fee_currency varchar(5000) 
, responsible_party_fee varchar(5000));