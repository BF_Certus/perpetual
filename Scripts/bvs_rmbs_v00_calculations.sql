CREATE TABLE DV.bvs_rmbs_v00_calculations (
  dss_load_datetime datetime2, dss_change_hash binary(20) NOT NULL 
, dss_record_source varchar(255), dss_start_date datetime2, dss_version integer 
, dss_create_time datetime2, hk_l_rmbs_v00 binary(20) 
, offset_account_flag varchar(3), conforming_mortgage_flag varchar(3) 
, restructuring_arrangement_flag varchar(3), first_home_buyer_flag varchar(3) 
, bankruptcy_flag varchar(3), interest_only_expiry_term numeric(19,6) 
, fixed_rate_expiry_term numeric(19,6), indexed_lvr date 
, data_issuer_code varchar(100), data_issuer_name varchar(100) 
, data_issuer_abn int NOT NULL, arrears_category varchar(5) 
, date_key varchar(100), eomonth_collateral_date date, originationyear int 
, property_region varchar(8) NOT NULL, property_state varchar(8) NOT NULL);