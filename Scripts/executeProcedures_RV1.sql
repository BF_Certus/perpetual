DECLARE @p_return_msg VARCHAR(256)
, @p_status           INTEGER;

EXEC dbo.update_stage_client_master 0 , '' , '' , 0 , 0 , @p_return_msg OUTPUT, @p_status OUTPUT;
select @p_status, @p_return_msg;
EXEC dbo.update_stage_party 0 , '' , '' , 0 , 0 , @p_return_msg OUTPUT, @p_status OUTPUT;
select @p_status, @p_return_msg;
EXEC dbo.update_stage_rmbs_v00 0 , '' , '' , 0 , 0 , @p_return_msg OUTPUT, @p_status OUTPUT;
select @p_status, @p_return_msg;
EXEC dbo.update_stage_TRAN_v00 0 , '' , '' , 0 , 0 , @p_return_msg OUTPUT, @p_status OUTPUT;
select @p_status, @p_return_msg;
EXEC dbo.update_stage_SECU_v00 0 , '' , '' , 0 , 0 , @p_return_msg OUTPUT, @p_status OUTPUT;
select @p_status, @p_return_msg;
EXEC dbo.update_stage_SECU_exchange_name 0 , '' , '' , 0 , 0 , @p_return_msg OUTPUT, @p_status OUTPUT;
select @p_status, @p_return_msg;
EXEC dbo.update_stage_SECU_clearing_system 0 , '' , '' , 0 , 0 , @p_return_msg OUTPUT, @p_status OUTPUT;
select @p_status, @p_return_msg;
EXEC dbo.update_stage_SECU_ratings 0 , '' , '' , 0 , 0 , @p_return_msg OUTPUT, @p_status OUTPUT;
select @p_status, @p_return_msg;
EXEC dbo.update_stage_rmbs_col_v00 0 , '' , '' , 0 , 0 , @p_return_msg OUTPUT, @p_status OUTPUT;
select @p_status, @p_return_msg;
EXEC dbo.update_l_TRAN_v00 0 , '' , '' , 0 , 0 , @p_return_msg OUTPUT, @p_status OUTPUT;
select @p_status, @p_return_msg;
EXEC dbo.update_l_rmbs_v00 0 , '' , '' , 0 , 0 , @p_return_msg OUTPUT, @p_status OUTPUT;
select @p_status, @p_return_msg;
EXEC dbo.update_l_SECU_v00 0 , '' , '' , 0 , 0 , @p_return_msg OUTPUT, @p_status OUTPUT;
select @p_status, @p_return_msg;
EXEC dbo.update_l_SECU_exchange_name 0 , '' , '' , 0 , 0 , @p_return_msg OUTPUT, @p_status OUTPUT;
select @p_status, @p_return_msg;
EXEC dbo.update_l_SECU_clearing_system 0 , '' , '' , 0 , 0 , @p_return_msg OUTPUT, @p_status OUTPUT;
select @p_status, @p_return_msg;
EXEC dbo.update_l_SECU_ratings 0 , '' , '' , 0 , 0 , @p_return_msg OUTPUT, @p_status OUTPUT;
select @p_status, @p_return_msg;
EXEC dbo.update_l_rmbs_col_v00 0 , '' , '' , 0 , 0 , @p_return_msg OUTPUT, @p_status OUTPUT;
select @p_status, @p_return_msg;
EXEC dbo.update_s_feed_master 0 , '' , '' , 0 , 0 , @p_return_msg OUTPUT, @p_status OUTPUT;
select @p_status, @p_return_msg;
EXEC dbo.update_s_client_master 0 , '' , '' , 0 , 0 , @p_return_msg OUTPUT, @p_status OUTPUT;
select @p_status, @p_return_msg;
EXEC dbo.update_s_rmbs_rts 0 , '' , '' , 0 , 0 , @p_return_msg OUTPUT, @p_status OUTPUT;
select @p_status, @p_return_msg;
EXEC dbo.update_s_rmbs_v00 0 , '' , '' , 0 , 0 , @p_return_msg OUTPUT, @p_status OUTPUT;
select @p_status, @p_return_msg;
EXEC dbo.update_s_TRAN_rts 0 , '' , '' , 0 , 0 , @p_return_msg OUTPUT, @p_status OUTPUT;
select @p_status, @p_return_msg;
EXEC dbo.update_s_TRAN_v00 0 , '' , '' , 0 , 0 , @p_return_msg OUTPUT, @p_status OUTPUT;
select @p_status, @p_return_msg;
EXEC dbo.update_s_SECU_v00 0 , '' , '' , 0 , 0 , @p_return_msg OUTPUT, @p_status OUTPUT;
select @p_status, @p_return_msg;
EXEC dbo.update_s_SECU_exchange_name 0 , '' , '' , 0 , 0 , @p_return_msg OUTPUT, @p_status OUTPUT;
select @p_status, @p_return_msg;
EXEC dbo.update_s_SECU_clearing_system 0 , '' , '' , 0 , 0 , @p_return_msg OUTPUT, @p_status OUTPUT;
select @p_status, @p_return_msg;
EXEC dbo.update_s_SECU_ratings 0 , '' , '' , 0 , 0 , @p_return_msg OUTPUT, @p_status OUTPUT;
select @p_status, @p_return_msg;
EXEC dbo.update_s_rmbs_col_v00 0 , '' , '' , 0 , 0 , @p_return_msg OUTPUT, @p_status OUTPUT;
select @p_status, @p_return_msg;