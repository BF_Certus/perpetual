/****** Object:  View [INGEST].[TRAN_Account]    Script Date: 4/07/2018 11:01:25 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--Drop view [INGEST].[TRAN_Account]
--go
Create View [INGEST].[TRAN_Account] as 
 
  WITH Nbrs ( Number ) AS (
    SELECT 1 UNION ALL
    SELECT 1 + Number FROM Nbrs WHERE Number < 99
)
SELECT h_cm.ClientID, h_fm.FeedID, h_rp.Reporting_Period, h_t.Master_Trust_Name_or_SPV, h_t.Series_Trust_Name_or_Series, h_t.Transaction_ID,
DelimCount as Account_Count, N.Number as Account_Position,
Case when DelimCount = 0 then Account_Provider_Name else SubString(  Account_Provider_Name , dbo.FN_CERTUS_INSTR(  Account_Provider_Name ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Account_Provider_Name,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Account_Provider_Name ,';',1,N.Number-1)-1) end as Account_Provider_Name,
Case when DelimCount = 0 then Ending_Balance else SubString(  Ending_Balance , dbo.FN_CERTUS_INSTR(  Ending_Balance ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Ending_Balance,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Ending_Balance ,';',1,N.Number-1)-1) end as Ending_Balance,
Case when DelimCount = 0 then Target_Balance else SubString(  Target_Balance , dbo.FN_CERTUS_INSTR(  Target_Balance ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Target_Balance,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Target_Balance ,';',1,N.Number-1)-1) end as Target_Balance,
Case when DelimCount = 0 then Account_Provider_ABN else SubString(  Account_Provider_ABN , dbo.FN_CERTUS_INSTR(  Account_Provider_ABN ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Account_Provider_ABN,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Account_Provider_ABN ,';',1,N.Number-1)-1) end as Account_Provider_ABN,
Case when DelimCount = 0 then Account_Provider_Jurisdiction else SubString(  Account_Provider_Jurisdiction , dbo.FN_CERTUS_INSTR(  Account_Provider_Jurisdiction ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Account_Provider_Jurisdiction,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Account_Provider_Jurisdiction ,';',1,N.Number-1)-1) end as Account_Provider_Jurisdiction,
Case when DelimCount = 0 then Account_Provider_ID else SubString(  Account_Provider_ID , dbo.FN_CERTUS_INSTR(  Account_Provider_ID ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Account_Provider_ID,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Account_Provider_ID ,';',1,N.Number-1)-1) end as Account_Provider_ID,
Case when DelimCount = 0 then Account_Provider_ID_Descriptor else SubString(  Account_Provider_ID_Descriptor , dbo.FN_CERTUS_INSTR(  Account_Provider_ID_Descriptor ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Account_Provider_ID_Descriptor,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Account_Provider_ID_Descriptor ,';',1,N.Number-1)-1) end as Account_Provider_ID_Descriptor,
Case when DelimCount = 0 then Account_Currency else SubString(  Account_Currency , dbo.FN_CERTUS_INSTR(  Account_Currency ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Account_Currency,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Account_Currency ,';',1,N.Number-1)-1) end as Account_Currency,
Case when DelimCount = 0 then Account_Fee else SubString(  Account_Fee , dbo.FN_CERTUS_INSTR(  Account_Fee ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Account_Fee,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Account_Fee ,';',1,N.Number-1)-1) end as Account_Fee,
Case when DelimCount = 0 then Beginning_Balance else SubString(  Beginning_Balance , dbo.FN_CERTUS_INSTR(  Beginning_Balance ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Beginning_Balance,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Beginning_Balance ,';',1,N.Number-1)-1) end as Beginning_Balance,
Case when DelimCount = 0 then Account_Type else SubString(  Account_Type , dbo.FN_CERTUS_INSTR(  Account_Type ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Account_Type,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Account_Type ,';',1,N.Number-1)-1) end as Account_Type,
Case when DelimCount = 0 then Account_Name else SubString(  Account_Name , dbo.FN_CERTUS_INSTR(  Account_Name ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Account_Name,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Account_Name ,';',1,N.Number-1)-1) end as Account_Name
from 
	DV.S_TRAN_v00 s
	inner join 
	DV.L_TRAN_v00 l on s.hk_l_TRAN_v00=l.hk_l_TRAN_v00
	inner join
	DV.h_client_master h_cm on h_cm.hk_h_client_master=l.hk_h_client_master
	inner join
	DV.h_feed_master h_fm on h_fm.hk_h_feed_master=l.hk_h_feed_master
	inner join
	DV.h_reporting_period h_rp on h_rp.hk_h_reporting_period=l.hk_h_reporting_period
	inner join
	DV.h_transaction h_t on h_t.hk_h_transaction=l.hk_h_transaction
	inner join
	(select TRAN_v00_DelimiterCount.hk_l_TRAN_v00, case
		when Delim_Account_Provider_Name=Delim_Ending_Balance
		and Delim_Account_Provider_Name=Delim_Target_Balance
		and Delim_Account_Provider_Name=Delim_Account_Provider_ABN
		and Delim_Account_Provider_Name=Delim_Account_Provider_Jurisdiction
		and Delim_Account_Provider_Name=Delim_Account_Provider_ID
		and Delim_Account_Provider_Name=Delim_Account_Provider_ID_Descriptor
		and Delim_Account_Provider_Name=Delim_Account_Currency
		and Delim_Account_Provider_Name=Delim_Account_Fee
		and Delim_Account_Provider_Name=Delim_Beginning_Balance
		and Delim_Account_Provider_Name=Delim_Account_Type
		and Delim_Account_Provider_Name=Delim_Account_Name
		then Delim_Account_Provider_Name+1 end as DelimCount from INGEST.TRAN_v00_DelimiterCount) delim on delim.hk_l_TRAN_v00=l.hk_l_TRAN_v00
	inner join
	Nbrs N ON (N.Number <= DelimCount) or (DelimCount = 0 and N.Number =1)

GO


