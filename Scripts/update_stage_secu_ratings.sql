--==============================================================================
-- DBMS Name        :    SQL Server
-- Procedure Name   :    update_stage_rmbs_se_b146c3b6
-- Template         :    cert_wsl_sqlserver_proc_dv_stage
-- Template Version :    6.9.1.0
-- Description      :    Update the Stage Table table stage_SECU_ratings
-- Generated by     :    WhereScape RED Version 8.0.1.0 (build 171017-084020)
-- Generated for    :    Certus Evaluate until 01 August 2018
-- Generated on     :    Monday, July 02, 2018 at 16:24:40
-- Author           :    Bronwen
--==============================================================================
-- Notes / History
--
CREATE PROCEDURE update_stage_SECU_ratings
  @p_sequence         INTEGER
, @p_job_name         VARCHAR(256)
, @p_task_name        VARCHAR(256)
, @p_job_id           INTEGER
, @p_task_id          INTEGER
, @p_return_msg       VARCHAR(256) OUTPUT
, @p_status           INTEGER      OUTPUT

AS
  SET XACT_ABORT OFF  -- Turn off auto abort on errors
  SET NOCOUNT ON      -- Turn off row count messages

  --=====================================================
  -- Control variables used in most programs
  --=====================================================
  DECLARE
    @v_msgtext               VARCHAR(255)  -- Text for audit_trail
  , @v_sql                   NVARCHAR(255) -- Text for SQL statements
  , @v_step                  INTEGER       -- return code
  , @v_insert_count          INTEGER       -- no of records inserted
  , @v_return_status         INTEGER       -- Update result status
  , @v_current_datetime      DATETIME      -- Used for date insert

  --=====================================================
  -- General Variables
  --=====================================================


  --=====================================================
  -- MAIN
  --=====================================================
  SET @v_step = 100

  SET @v_insert_count = 0
  SET @v_current_datetime = GETDATE()

  BEGIN TRY

    --=====================================================
    -- Delete existing records
    --=====================================================
    SET @v_step = 200

    SET @v_sql = N'TRUNCATE TABLE [STAGE].[stage_SECU_ratings]';
    EXEC @v_return_status = sp_executesql @v_sql

    --=====================================================
    -- Insert new records
    --=====================================================
    SET @v_step = 300

    BEGIN TRANSACTION

      INSERT INTO [STAGE].[stage_SECU_ratings]
      ( hk_l_SECU_ratings
      , hk_h_rating_agency
      , hk_h_security
      , hk_h_transaction
      , hk_h_reporting_period
      , hk_h_client_master
      , clientid
      , name
      , reporting_period
      , transaction_id
      , master_trust_name_or_spv
      , series_trust_name_or_series
      , tranche_name
      , report_date
      , original_rating
      , current_rating
      , abn
      , jurisdiction
      , id
      , id_descriptor
      , dss_change_hash_stage_SECU_ratings
      , dss_record_source
      , dss_load_datetime
      , dss_create_time)
      SELECT  CONVERT(BINARY(20),HASHBYTES('sha1',
               COALESCE(CAST(load_SECU_ratings.clientid AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_SECU_ratings.clientid AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_SECU_ratings.name AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_SECU_ratings.clientid AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_SECU_ratings.reporting_period AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_SECU_ratings.clientid AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_SECU_ratings.transaction_id AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_SECU_ratings.master_trust_name_or_spv AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_SECU_ratings.series_trust_name_or_series AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_SECU_ratings.tranche_name AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_SECU_ratings.clientid AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_SECU_ratings.transaction_id AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_SECU_ratings.master_trust_name_or_spv AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_SECU_ratings.series_trust_name_or_series AS VARCHAR(MAX)),'null')
               ) , 2) AS hk_l_SECU_ratings 
           , CONVERT(BINARY(20),HASHBYTES('sha1',
               COALESCE(CAST(load_SECU_ratings.clientid AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_SECU_ratings.name AS VARCHAR(MAX)),'null')
               ) , 2) AS hk_h_rating_agency 
           , CONVERT(BINARY(20),HASHBYTES('sha1',
               COALESCE(CAST(load_SECU_ratings.clientid AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_SECU_ratings.transaction_id AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_SECU_ratings.master_trust_name_or_spv AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_SECU_ratings.series_trust_name_or_series AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_SECU_ratings.tranche_name AS VARCHAR(MAX)),'null')
               ) , 2) AS hk_h_security 
           , CONVERT(BINARY(20),HASHBYTES('sha1',
               COALESCE(CAST(load_SECU_ratings.clientid AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_SECU_ratings.transaction_id AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_SECU_ratings.master_trust_name_or_spv AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_SECU_ratings.series_trust_name_or_series AS VARCHAR(MAX)),'null')
               ) , 2) AS hk_h_transaction 
           , CONVERT(BINARY(20),HASHBYTES('sha1',
               COALESCE(CAST(load_SECU_ratings.clientid AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_SECU_ratings.reporting_period AS VARCHAR(MAX)),'null')
               ) , 2) AS hk_h_reporting_period 
           , CONVERT(BINARY(20),HASHBYTES('sha1',
               COALESCE(CAST(load_SECU_ratings.clientid AS VARCHAR(MAX)),'null')
               ) , 2) AS hk_h_client_master 
           , load_SECU_ratings.clientid AS clientid 
           , load_SECU_ratings.name AS name 
           , load_SECU_ratings.reporting_period AS reporting_period 
           , load_SECU_ratings.transaction_id AS transaction_id 
           , load_SECU_ratings.master_trust_name_or_spv AS master_trust_name_or_spv 
           , load_SECU_ratings.series_trust_name_or_series AS series_trust_name_or_series 
           , load_SECU_ratings.tranche_name AS tranche_name 
           , load_SECU_ratings.report_date AS report_date 
           , load_SECU_ratings.original_rating AS original_rating 
           , load_SECU_ratings.current_rating AS current_rating 
           , load_SECU_ratings.abn AS abn 
           , load_SECU_ratings.jurisdiction AS jurisdiction 
           , load_SECU_ratings.id AS id 
           , load_SECU_ratings.id_descriptor AS id_descriptor 
           , CONVERT(BINARY(20),HASHBYTES('sha1',
               COALESCE(CAST(load_SECU_ratings.report_date AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_SECU_ratings.original_rating AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_SECU_ratings.current_rating AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_SECU_ratings.abn AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_SECU_ratings.jurisdiction AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_SECU_ratings.id AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_SECU_ratings.id_descriptor AS VARCHAR(MAX)),'null')
               ) , 2) AS dss_change_hash_stage_SECU_ratings 
           , load_SECU_ratings.dss_record_source AS dss_record_source 
           , load_SECU_ratings.dss_load_datetime AS dss_load_datetime 
           , @v_current_datetime AS dss_create_time 
      FROM [LOAD].[load_SECU_ratings] load_SECU_ratings
      ;

      SELECT @v_insert_count = @@ROWCOUNT

    COMMIT

    --=====================================================
    -- All Done report the results
    --=====================================================
    SET @v_step = 400

    SET @p_status = 1
    SET @p_return_msg = 'stage_SECU_ratings updated. '
      + CONVERT(VARCHAR,@v_insert_count) + ' new records.'

    RETURN 0

  END TRY
  BEGIN CATCH

    SET @p_status = -2
    SET @p_return_msg = SUBSTRING('update_stage_rmbs_se_b146c3b6 FAILED with error '
      + CONVERT(VARCHAR,ISNULL(ERROR_NUMBER(),0))
      + ' Step ' + CONVERT(VARCHAR,ISNULL(@v_step,0))
      + '. Error Msg: ' + ERROR_MESSAGE(),1,255)

  END CATCH
  IF XACT_STATE() <> 0
  BEGIN
    ROLLBACK TRANSACTION
  END

  RETURN 0
