CREATE TABLE DV.h_borrower (
  hk_h_borrower binary(20) NOT NULL, dss_record_source varchar(255) 
, dss_load_datetime datetime2, dss_create_time datetime2 
, clientid varchar(100) NOT NULL, borrower_id varchar(100));
CREATE UNIQUE NONCLUSTERED INDEX h_borrower_idx_0 ON DV.h_borrower (hk_h_borrower) WITH (SORT_IN_TEMPDB = OFF);
CREATE UNIQUE NONCLUSTERED INDEX h_borrower_idx_A ON DV.h_borrower (clientid,borrower_id) WITH (SORT_IN_TEMPDB = OFF);
