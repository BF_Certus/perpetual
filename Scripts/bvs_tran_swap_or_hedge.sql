CREATE TABLE DV.bvs_TRAN_swap_or_hedge (
  hk_l_TRAN_swap_or_hedge binary(20) NOT NULL, dss_load_datetime datetime 
, dss_change_hash binary(20) NOT NULL, dss_record_source varchar(255) 
, dss_start_date datetime, dss_version int, dss_create_time datetime 
, swap_or_hedge_count int, swap_or_hedge_position int 
, swap_or_hedge_provider_abn varchar(5000), swap_or_hedge_type varchar(5000) 
, notional_principal_amount varchar(5000) 
, swap_or_hedge_provider_jurisdiction varchar(5000) 
, swap_or_hedge_provider_id varchar(5000) 
, swap_or_hedge_provider_id_descriptor varchar(5000) 
, swap_or_hedge_name varchar(5000), pay_leg_currency varchar(5000) 
, pay_leg_reference_index varchar(5000) 
, pay_leg_reference_index_value varchar(5000) 
, pay_leg_margin_or_rate varchar(5000), pay_leg_amount varchar(5000) 
, receive_leg_currency varchar(5000), receive_leg_reference_index varchar(5000) 
, receive_leg_reference_index_value varchar(5000) 
, receive_leg_margin_or_rate varchar(5000), receive_leg_amount varchar(5000) 
, exchange_rate varchar(5000), swap_or_hedge_fee_currency varchar(5000) 
, swap_or_hedge_fee varchar(5000))
;