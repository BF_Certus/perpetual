CREATE TABLE DV.h_security (
  hk_h_security binary(20) NOT NULL, dss_record_source varchar(255) 
, dss_load_datetime datetime2, dss_create_time datetime2 
, clientid varchar(100) NOT NULL, transaction_id varchar(100) 
, master_trust_name_or_spv varchar(100) 
, series_trust_name_or_series varchar(100), tranche_name varchar(100))
CREATE UNIQUE NONCLUSTERED INDEX h_security_idx_0 ON DV.h_security (hk_h_security) WITH (SORT_IN_TEMPDB = OFF);
CREATE UNIQUE NONCLUSTERED INDEX h_security_idx_A ON DV.h_security (transaction_id,series_trust_name_or_series,clientid,tranche_name,master_trust_name_or_spv) WITH (SORT_IN_TEMPDB = OFF);
ALTER TABLE DV.h_security ADD CONSTRAINT h_security_7bcd9d37_idx_PK PRIMARY KEY NONCLUSTERED (hk_h_security) WITH (SORT_IN_TEMPDB = OFF);
CREATE UNIQUE NONCLUSTERED INDEX h_security_7bcd9d37_idx_A ON DV.h_security (clientid,transaction_id,master_trust_name_or_spv,series_trust_name_or_series,tranche_name) WITH (SORT_IN_TEMPDB = OFF);
