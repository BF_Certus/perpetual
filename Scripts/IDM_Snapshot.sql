drop table IDM.Perpetual_RMBS_Loan_Snapshot;
drop table IDM.Perpetual_RMBS_Loan_Reporting_Snapshot;
drop table IDM.Perpetual_RMBS_v00_Base_Snapshot;

select * into IDM.Perpetual_RMBS_Loan_Snapshot from IDM.Perpetual_RMBS_Loan;
select * into IDM.Perpetual_RMBS_Loan_Reporting_Snapshot from IDM.Perpetual_RMBS_Loan_Reporting;
select * into IDM.Perpetual_RMBS_v00_Base_Snapshot from IDM.Perpetual_RMBS_v00_Base;