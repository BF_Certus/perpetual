CREATE TABLE DV.l_TRAN_manager (
  hk_l_TRAN_manager binary(20) NOT NULL 
, hk_h_feed_master binary(20) NOT NULL, hk_h_party binary(20) NOT NULL 
, hk_h_reporting_period binary(20) NOT NULL 
, hk_h_transaction binary(20) NOT NULL, dss_record_source varchar(255) 
, dss_load_datetime datetime2, dss_create_time datetime2);
CREATE UNIQUE NONCLUSTERED INDEX l_bv_TRAN_manager_idx_0 ON DV.l_TRAN_manager (hk_l_TRAN_manager) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_bv_TRAN_manager_idx_1 ON DV.l_TRAN_manager (hk_h_feed_master) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_bv_TRAN_manager_idx_2 ON DV.l_TRAN_manager (hk_h_party) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_bv_TRAN_manager_idx_3 ON DV.l_TRAN_manager (hk_h_reporting_period) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_bv_TRAN_manager_idx_4 ON DV.l_TRAN_manager (hk_h_transaction) WITH (SORT_IN_TEMPDB = OFF);
CREATE UNIQUE NONCLUSTERED INDEX l_bv_TRAN_manager_idx_A ON DV.l_TRAN_manager (hk_h_feed_master,hk_h_party,hk_h_reporting_period,hk_h_transaction) WITH (SORT_IN_TEMPDB = OFF);
