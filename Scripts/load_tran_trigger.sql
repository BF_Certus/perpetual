CREATE TABLE LOAD.load_TRAN_trigger (
  clientid varchar(100), reporting_period varchar(100) 
, master_trust_name_or_spv varchar(100) 
, series_trust_name_or_series varchar(100), transaction_id varchar(100) 
, trigger_position int, trigger_name varchar(5000), feedid varchar(100) 
, trigger_count int, trigger_description varchar(5000) 
, trigger_state varchar(5000), dss_record_source varchar(255) 
, dss_load_datetime datetime2);