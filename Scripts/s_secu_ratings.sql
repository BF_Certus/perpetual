CREATE TABLE DV.s_SECU_ratings (
  hk_l_SECU_ratings binary(20) NOT NULL, dss_load_datetime datetime2 
, dss_change_hash binary(20) NOT NULL, dss_record_source varchar(255) 
, dss_start_date datetime2, dss_version integer, dss_create_time datetime2 
, report_date nvarchar(255), original_rating nvarchar(255) 
, current_rating nvarchar(255), abn nvarchar(255), jurisdiction nvarchar(255) 
, id nvarchar(255), id_descriptor nvarchar(255));