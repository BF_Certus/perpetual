CREATE TABLE DV.bvs_TRAN_issuing_trustee (
  hk_l_TRAN_issuing_trustee binary(20) NOT NULL, dss_load_datetime datetime 
, dss_change_hash binary(20) NOT NULL, dss_record_source varchar(255) 
, dss_start_date datetime, dss_version int, dss_create_time datetime 
, feedid varchar(100), issuing_trustee_count int, issuing_trustee_position int 
, issuing_trustee_jurisdiction varchar(5000), issuing_trustee_id varchar(5000) 
, issuing_trustee_id_descriptor varchar(5000) 
, issuing_trustee_fee_currency varchar(5000), issuing_trustee_fee varchar(5000));