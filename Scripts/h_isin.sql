CREATE TABLE DV.h_isin (
  hk_h_isin binary(20) NOT NULL, dss_record_source varchar(255) 
, dss_load_datetime datetime2, dss_create_time datetime2 
, clientid varchar(100) NOT NULL, isin varchar(100));
CREATE UNIQUE NONCLUSTERED INDEX h_isin_idx_0 ON DV.h_isin (hk_h_isin) WITH (SORT_IN_TEMPDB = OFF);
CREATE UNIQUE NONCLUSTERED INDEX h_isin_idx_A ON DV.h_isin (clientid,isin) WITH (SORT_IN_TEMPDB = OFF);
ALTER TABLE DV.h_isin ADD CONSTRAINT h_isin_b6e43146_idx_PK PRIMARY KEY NONCLUSTERED (hk_h_isin) WITH (SORT_IN_TEMPDB = OFF);
CREATE UNIQUE NONCLUSTERED INDEX h_isin_b6e43146_idx_A ON DV.h_isin (clientid,isin) WITH (SORT_IN_TEMPDB = OFF);
