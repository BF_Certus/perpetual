--==============================================================================
-- DBMS Name        :    SQL Server
-- Procedure Name   :    update_stage_rmbs_v0
-- Template         :    cert_wsl_sqlserver_proc_dv_stage
-- Template Version :    6.9.1.0
-- Description      :    Update the Stage Table table stage_rmbs_v00
-- Generated by     :    WhereScape RED Version 8.0.1.0 (build 171017-084020)
-- Generated for    :    Certus Evaluate until 01 July 2018
-- Generated on     :    Sunday, June 17, 2018 at 18:18:12
-- Author           :    Bronwen
--==============================================================================
-- Notes / History
--
CREATE PROCEDURE update_stage_rmbs_v00
  @p_sequence         INTEGER
, @p_job_name         VARCHAR(256)
, @p_task_name        VARCHAR(256)
, @p_job_id           INTEGER
, @p_task_id          INTEGER
, @p_return_msg       VARCHAR(256) OUTPUT
, @p_status           INTEGER      OUTPUT

AS
  SET XACT_ABORT OFF  -- Turn off auto abort on errors
  SET NOCOUNT ON      -- Turn off row count messages

  --=====================================================
  -- Control variables used in most programs
  --=====================================================
  DECLARE
    @v_msgtext               VARCHAR(255)  -- Text for audit_trail
  , @v_sql                   NVARCHAR(255) -- Text for SQL statements
  , @v_step                  INTEGER       -- return code
  , @v_insert_count          INTEGER       -- no of records inserted
  , @v_return_status         INTEGER       -- Update result status
  , @v_current_datetime      DATETIME      -- Used for date insert

  --=====================================================
  -- General Variables
  --=====================================================


  --=====================================================
  -- MAIN
  --=====================================================
  SET @v_step = 100

  SET @v_insert_count = 0
  SET @v_current_datetime = GETDATE()

  BEGIN TRY

    --=====================================================
    -- Delete existing records
    --=====================================================
    SET @v_step = 200

    SET @v_sql = N'TRUNCATE TABLE [STAGE].[stage_rmbs_v00]';
    EXEC @v_return_status = sp_executesql @v_sql

    --=====================================================
    -- Insert new records
    --=====================================================
    SET @v_step = 300

    BEGIN TRANSACTION

      INSERT INTO [STAGE].[stage_rmbs_v00]
      ( hk_l_rmbs_v00
      , hk_h_borrower
      , hk_h_loan
      , hk_h_client_master
      , hk_h_reporting_period
      , hk_h_transaction
      , hk_h_party_servicer_name
      , hk_h_party_originator_name
      , hk_h_party_seller_name
      , clientid
      , servicer_name
      , originator_name
      , seller_name
      , borrower_id
      , reporting_period
      , transaction_id
      , master_trust_name_or_spv
      , series_trust_name_or_series
      , loan_id
      , collateral_date
      , servicer_abn
      , originator_abn
      , group_loan_id
      , most_recent_approval_amount
      , current_balance
      , scheduled_balance
      , loan_guarantee_flag
      , redraw_feature_flag
      , available_redraw
      , offset_account_flag
      , offset_account_balance
      , scheduled_payment_policy
      , conforming_mortgage
      , lmi_underwriting_type
      , origination_date_settlement_date
      , loan_purpose
      , loan_origination_channel_flag
      , loan_term
      , remaining_term
      , seasoning
      , maturity_date
      , loan_securitised_date
      , original_ltv
      , current_ltv
      , scheduled_ltv
      , loan_documentation_type
      , loan_type
      , scheduled_minimum_payment
      , payment_frequency
      , lmi_provider_name
      , lmi_attachment_point
      , loan_currency
      , variable_loan_amount
      , interest_rate_type
      , current_interest_rate
      , reference_index_value
      , interest_margin
      , interest_rate_reset_interval
      , restructuring_arrangement_flag
      , interest_only_expiry_date
      , current_rate_expiry_date
      , account_status
      , default_balance
      , foreclosure_proceeds
      , date_of_foreclosure
      , loss_on_sale
      , days_in_arrears
      , amount_in_arrears
      , days_since_last_scheduled_payment
      , cumulative_missed_payments
      , claim_submitted_to_lmi
      , claim_paid_by_lmi
      , claim_denied_by_lmi
      , claim_pending_with_lmi
      , property_postcode
      , property_country_name
      , original_property_value
      , most_recent_property_value
      , original_property_valuation_type
      , most_recent_property_valuation_type
      , most_recent_property_valuation_date
      , property_purpose
      , property_type
      , lien
      , first_home_buyer_flag
      , country_of_residence
      , borrower_residency_secondary_borrower
      , employment_type
      , employment_type_secondary_borrower
      , borrower_type_primary_borrower
      , borrower_type_secondary_borrower
      , legal_entity_type
      , legal_entity_type_secondary_borrower
      , bankruptcy_flag
      , last_credit_discharge_date
      , number_of_debtors
      , external_credit_score_provider_name
      , credit_score
      , credit_score_secondary_borrower
      , debt_serviceability_metric
      , debt_serviceability_metric_score
      , income
      , income_verification
      , secondary_income
      , income_verification_for_secondary_income
      , abs_statistical_area
      , arrears_methodology
      , main_security_methodology
      , primary_borrower_methodology
      , originator_id
      , seller_abn
      , seller_id
      , lmi_provider_abn
      , lmi_provider_id
      , servicer_id
      , savings_verification
      , interest_rate
      , origination_date
      , feed_name
      , dss_change_hash_stage_rmbs_v00_1
      , dss_change_hash_stage_rmbs_v00_2
      , dss_record_source
      , dss_load_datetime
      , dss_create_time)
      SELECT  CONVERT(BINARY(20),HASHBYTES('sha1',
               COALESCE(CAST(load_rmbs_v00.clientid AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.borrower_id AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.master_trust_name_or_spv AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.series_trust_name_or_series AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.loan_id AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.servicer_name AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.originator_name AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.reporting_period AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.transaction_id AS VARCHAR(MAX)),'null')
               ) , 2) AS hk_l_rmbs_v00 
           , CONVERT(BINARY(20),HASHBYTES('sha1',
               COALESCE(CAST(load_rmbs_v00.clientid AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.borrower_id AS VARCHAR(MAX)),'null')
               ) , 2) AS hk_h_borrower 
           , CONVERT(BINARY(20),HASHBYTES('sha1',
               COALESCE(CAST(load_rmbs_v00.clientid AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.master_trust_name_or_spv AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.series_trust_name_or_series AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.loan_id AS VARCHAR(MAX)),'null')
               ) , 2) AS hk_h_loan 
           , CONVERT(BINARY(20),HASHBYTES('sha1',
               COALESCE(CAST(load_rmbs_v00.clientid AS VARCHAR(MAX)),'null')
               ) , 2) AS hk_h_client_master 
           , CONVERT(BINARY(20),HASHBYTES('sha1',
               COALESCE(CAST(load_rmbs_v00.clientid AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.reporting_period AS VARCHAR(MAX)),'null')
               ) , 2) AS hk_h_reporting_period 
           , CONVERT(BINARY(20),HASHBYTES('sha1',
               COALESCE(CAST(load_rmbs_v00.clientid AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.transaction_id AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.master_trust_name_or_spv AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.series_trust_name_or_series AS VARCHAR(MAX)),'null')
               ) , 2) AS hk_h_transaction 
           , CONVERT(BINARY(20),HASHBYTES('sha1',
               COALESCE(CAST(load_rmbs_v00.clientid AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.servicer_name AS VARCHAR(MAX)),'null')
               ) , 2) AS hk_h_party_servicer_name 
           , CONVERT(BINARY(20),HASHBYTES('sha1',
               COALESCE(CAST(load_rmbs_v00.clientid AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.originator_name AS VARCHAR(MAX)),'null')
               ) , 2) AS hk_h_party_originator_name 
           , CONVERT(BINARY(20),HASHBYTES('sha1',
               COALESCE(CAST(load_rmbs_v00.clientid AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.seller_name AS VARCHAR(MAX)),'null')
               ) , 2) AS hk_h_party_seller_name 
           , load_rmbs_v00.clientid AS clientid 
           , load_rmbs_v00.servicer_name AS servicer_name 
           , load_rmbs_v00.originator_name AS originator_name 
           , load_rmbs_v00.seller_name AS seller_name 
           , load_rmbs_v00.borrower_id AS borrower_id 
           , load_rmbs_v00.reporting_period AS reporting_period 
           , load_rmbs_v00.transaction_id AS transaction_id 
           , load_rmbs_v00.master_trust_name_or_spv AS master_trust_name_or_spv 
           , load_rmbs_v00.series_trust_name_or_series AS series_trust_name_or_series 
           , load_rmbs_v00.loan_id AS loan_id 
           , load_rmbs_v00.collateral_date AS collateral_date 
           , load_rmbs_v00.servicer_abn AS servicer_abn 
           , load_rmbs_v00.originator_abn AS originator_abn 
           , load_rmbs_v00.group_loan_id AS group_loan_id 
           , load_rmbs_v00.most_recent_approval_amount AS most_recent_approval_amount 
           , load_rmbs_v00.current_balance AS current_balance 
           , load_rmbs_v00.scheduled_balance AS scheduled_balance 
           , load_rmbs_v00.loan_guarantee_flag AS loan_guarantee_flag 
           , load_rmbs_v00.redraw_feature_flag AS redraw_feature_flag 
           , load_rmbs_v00.available_redraw AS available_redraw 
           , load_rmbs_v00.offset_account_flag AS offset_account_flag 
           , load_rmbs_v00.offset_account_balance AS offset_account_balance 
           , load_rmbs_v00.scheduled_payment_policy AS scheduled_payment_policy 
           , load_rmbs_v00.conforming_mortgage AS conforming_mortgage 
           , load_rmbs_v00.lmi_underwriting_type AS lmi_underwriting_type 
           , load_rmbs_v00.origination_date_settlement_date AS origination_date_settlement_date 
           , load_rmbs_v00.loan_purpose AS loan_purpose 
           , load_rmbs_v00.loan_origination_channel_flag AS loan_origination_channel_flag 
           , load_rmbs_v00.loan_term AS loan_term 
           , load_rmbs_v00.remaining_term AS remaining_term 
           , load_rmbs_v00.seasoning AS seasoning 
           , load_rmbs_v00.maturity_date AS maturity_date 
           , load_rmbs_v00.loan_securitised_date AS loan_securitised_date 
           , load_rmbs_v00.original_ltv AS original_ltv 
           , load_rmbs_v00.current_ltv AS current_ltv 
           , load_rmbs_v00.scheduled_ltv AS scheduled_ltv 
           , load_rmbs_v00.loan_documentation_type AS loan_documentation_type 
           , load_rmbs_v00.loan_type AS loan_type 
           , load_rmbs_v00.scheduled_minimum_payment AS scheduled_minimum_payment 
           , load_rmbs_v00.payment_frequency AS payment_frequency 
           , load_rmbs_v00.lmi_provider_name AS lmi_provider_name 
           , load_rmbs_v00.lmi_attachment_point AS lmi_attachment_point 
           , load_rmbs_v00.loan_currency AS loan_currency 
           , load_rmbs_v00.variable_loan_amount AS variable_loan_amount 
           , load_rmbs_v00.interest_rate_type AS interest_rate_type 
           , load_rmbs_v00.current_interest_rate AS current_interest_rate 
           , load_rmbs_v00.reference_index_value AS reference_index_value 
           , load_rmbs_v00.interest_margin AS interest_margin 
           , load_rmbs_v00.interest_rate_reset_interval AS interest_rate_reset_interval 
           , load_rmbs_v00.restructuring_arrangement_flag AS restructuring_arrangement_flag 
           , load_rmbs_v00.interest_only_expiry_date AS interest_only_expiry_date 
           , load_rmbs_v00.current_rate_expiry_date AS current_rate_expiry_date 
           , load_rmbs_v00.account_status AS account_status 
           , load_rmbs_v00.default_balance AS default_balance 
           , load_rmbs_v00.foreclosure_proceeds AS foreclosure_proceeds 
           , load_rmbs_v00.date_of_foreclosure AS date_of_foreclosure 
           , load_rmbs_v00.loss_on_sale AS loss_on_sale 
           , load_rmbs_v00.days_in_arrears AS days_in_arrears 
           , load_rmbs_v00.amount_in_arrears AS amount_in_arrears 
           , load_rmbs_v00.days_since_last_scheduled_payment AS days_since_last_scheduled_payment 
           , load_rmbs_v00.cumulative_missed_payments AS cumulative_missed_payments 
           , load_rmbs_v00.claim_submitted_to_lmi AS claim_submitted_to_lmi 
           , load_rmbs_v00.claim_paid_by_lmi AS claim_paid_by_lmi 
           , load_rmbs_v00.claim_denied_by_lmi AS claim_denied_by_lmi 
           , load_rmbs_v00.claim_pending_with_lmi AS claim_pending_with_lmi 
           , load_rmbs_v00.property_postcode AS property_postcode 
           , load_rmbs_v00.property_country_name AS property_country_name 
           , load_rmbs_v00.original_property_value AS original_property_value 
           , load_rmbs_v00.most_recent_property_value AS most_recent_property_value 
           , load_rmbs_v00.original_property_valuation_type AS original_property_valuation_type 
           , load_rmbs_v00.most_recent_property_valuation_type AS most_recent_property_valuation_type 
           , load_rmbs_v00.most_recent_property_valuation_date AS most_recent_property_valuation_date 
           , load_rmbs_v00.property_purpose AS property_purpose 
           , load_rmbs_v00.property_type AS property_type 
           , load_rmbs_v00.lien AS lien 
           , load_rmbs_v00.first_home_buyer_flag AS first_home_buyer_flag 
           , load_rmbs_v00.country_of_residence AS country_of_residence 
           , load_rmbs_v00.borrower_residency_secondary_borrower AS borrower_residency_secondary_borrower 
           , load_rmbs_v00.employment_type AS employment_type 
           , load_rmbs_v00.employment_type_secondary_borrower AS employment_type_secondary_borrower 
           , load_rmbs_v00.borrower_type_primary_borrower AS borrower_type_primary_borrower 
           , load_rmbs_v00.borrower_type_secondary_borrower AS borrower_type_secondary_borrower 
           , load_rmbs_v00.legal_entity_type AS legal_entity_type 
           , load_rmbs_v00.legal_entity_type_secondary_borrower AS legal_entity_type_secondary_borrower 
           , load_rmbs_v00.bankruptcy_flag AS bankruptcy_flag 
           , load_rmbs_v00.last_credit_discharge_date AS last_credit_discharge_date 
           , load_rmbs_v00.number_of_debtors AS number_of_debtors 
           , load_rmbs_v00.external_credit_score_provider_name AS external_credit_score_provider_name 
           , load_rmbs_v00.credit_score AS credit_score 
           , load_rmbs_v00.credit_score_secondary_borrower AS credit_score_secondary_borrower 
           , load_rmbs_v00.debt_serviceability_metric AS debt_serviceability_metric 
           , load_rmbs_v00.debt_serviceability_metric_score AS debt_serviceability_metric_score 
           , load_rmbs_v00.income AS income 
           , load_rmbs_v00.income_verification AS income_verification 
           , load_rmbs_v00.secondary_income AS secondary_income 
           , load_rmbs_v00.income_verification_for_secondary_income AS income_verification_for_secondary_income 
           , load_rmbs_v00.abs_statistical_area AS abs_statistical_area 
           , load_rmbs_v00.arrears_methodology AS arrears_methodology 
           , load_rmbs_v00.main_security_methodology AS main_security_methodology 
           , load_rmbs_v00.primary_borrower_methodology AS primary_borrower_methodology 
           , load_rmbs_v00.originator_id AS originator_id 
           , load_rmbs_v00.seller_abn AS seller_abn 
           , load_rmbs_v00.seller_id AS seller_id 
           , load_rmbs_v00.lmi_provider_abn AS lmi_provider_abn 
           , load_rmbs_v00.lmi_provider_id AS lmi_provider_id 
           , load_rmbs_v00.servicer_id AS servicer_id 
           , load_rmbs_v00.savings_verification AS savings_verification 
           , load_rmbs_v00.interest_rate AS interest_rate 
           , load_rmbs_v00.origination_date AS origination_date 
           , load_rmbs_v00.feed_name AS feed_name 
           , CONVERT(BINARY(20),HASHBYTES('sha1',
               COALESCE(CAST(load_rmbs_v00.collateral_date AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.servicer_abn AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.originator_abn AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.group_loan_id AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.most_recent_approval_amount AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.current_balance AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.scheduled_balance AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.loan_guarantee_flag AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.redraw_feature_flag AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.available_redraw AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.offset_account_flag AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.offset_account_balance AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.scheduled_payment_policy AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.conforming_mortgage AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.lmi_underwriting_type AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.origination_date_settlement_date AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.loan_purpose AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.loan_origination_channel_flag AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.loan_term AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.remaining_term AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.seasoning AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.maturity_date AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.loan_securitised_date AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.original_ltv AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.current_ltv AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.scheduled_ltv AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.loan_documentation_type AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.loan_type AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.scheduled_minimum_payment AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.payment_frequency AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.lmi_provider_name AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.lmi_attachment_point AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.loan_currency AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.variable_loan_amount AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.interest_rate_type AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.current_interest_rate AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.reference_index_value AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.interest_margin AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.interest_rate_reset_interval AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.restructuring_arrangement_flag AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.interest_only_expiry_date AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.current_rate_expiry_date AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.account_status AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.default_balance AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.foreclosure_proceeds AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.date_of_foreclosure AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.loss_on_sale AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.days_in_arrears AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.amount_in_arrears AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.days_since_last_scheduled_payment AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.cumulative_missed_payments AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.claim_submitted_to_lmi AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.claim_paid_by_lmi AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.claim_denied_by_lmi AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.claim_pending_with_lmi AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.property_postcode AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.property_country_name AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.original_property_value AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.most_recent_property_value AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.original_property_valuation_type AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.most_recent_property_valuation_type AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.most_recent_property_valuation_date AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.property_purpose AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.property_type AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.lien AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.first_home_buyer_flag AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.country_of_residence AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.borrower_residency_secondary_borrower AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.employment_type AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.employment_type_secondary_borrower AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.borrower_type_primary_borrower AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.borrower_type_secondary_borrower AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.legal_entity_type AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.legal_entity_type_secondary_borrower AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.bankruptcy_flag AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.last_credit_discharge_date AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.number_of_debtors AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.external_credit_score_provider_name AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.credit_score AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.credit_score_secondary_borrower AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.debt_serviceability_metric AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.debt_serviceability_metric_score AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.income AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.income_verification AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.secondary_income AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.income_verification_for_secondary_income AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.abs_statistical_area AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.arrears_methodology AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.main_security_methodology AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.primary_borrower_methodology AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.originator_id AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.seller_abn AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.seller_id AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.lmi_provider_abn AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.lmi_provider_id AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.servicer_id AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.savings_verification AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.interest_rate AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00.origination_date AS VARCHAR(MAX)),'null')
               ) , 2) AS dss_change_hash_stage_rmbs_v00_1 
           , CONVERT(BINARY(20),HASHBYTES('sha1',
               COALESCE(CAST(load_rmbs_v00.feed_name AS VARCHAR(MAX)),'null')
               ) , 2) AS dss_change_hash_stage_rmbs_v00_2 
           , load_rmbs_v00.dss_record_source AS dss_record_source 
           , load_rmbs_v00.dss_load_datetime AS dss_load_datetime 
           , @v_current_datetime AS dss_create_time 
      FROM LOAD.load_rmbs_v00 load_rmbs_v00
      ;

      SELECT @v_insert_count = @@ROWCOUNT

    COMMIT

    --=====================================================
    -- All Done report the results
    --=====================================================
    SET @v_step = 400

    SET @p_status = 1
    SET @p_return_msg = 'stage_rmbs_v00 updated. '
      + CONVERT(VARCHAR,@v_insert_count) + ' new records.'

    RETURN 0

  END TRY
  BEGIN CATCH

    SET @p_status = -2
    SET @p_return_msg = SUBSTRING('update_stage_rmbs_v0 FAILED with error '
      + CONVERT(VARCHAR,ISNULL(ERROR_NUMBER(),0))
      + ' Step ' + CONVERT(VARCHAR,ISNULL(@v_step,0))
      + '. Error Msg: ' + ERROR_MESSAGE(),1,255)

  END CATCH
  IF XACT_STATE() <> 0
  BEGIN
    ROLLBACK TRANSACTION
  END

  RETURN 0
