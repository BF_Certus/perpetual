CREATE TABLE LOAD.load_TRAN_responsible_party (
  clientid varchar(100), feedid varchar(100), reporting_period varchar(100) 
, master_trust_name_or_spv varchar(100) 
, series_trust_name_or_series varchar(100), transaction_id varchar(100) 
, responsible_party_name varchar(5000), responsible_party_count int 
, responsible_party_position int, responsible_party_abn varchar(5000) 
, responsible_party_jurisdiction varchar(5000) 
, responsible_party_id varchar(5000) 
, responsible_party_id_descriptor varchar(5000) 
, responsible_party_fee_currency varchar(5000) 
, responsible_party_fee varchar(5000), dss_record_source varchar(255) 
, dss_load_datetime datetime2);