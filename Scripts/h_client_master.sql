CREATE TABLE DV.h_client_master (
  hk_h_client_master binary(20) NOT NULL, dss_record_source varchar(255) 
, dss_load_datetime datetime2, dss_create_time datetime2 
, clientid varchar(100) NOT NULL);
CREATE UNIQUE NONCLUSTERED INDEX h_client_master_idx_0 ON DV.h_client_master (hk_h_client_master) WITH (SORT_IN_TEMPDB = OFF);
CREATE UNIQUE NONCLUSTERED INDEX h_client_master_idx_A ON DV.h_client_master (clientid) WITH (SORT_IN_TEMPDB = OFF);
ALTER TABLE DV.h_client_master ADD CONSTRAINT h_client_mast_694265bf_idx_PK PRIMARY KEY NONCLUSTERED (hk_h_client_master) WITH (SORT_IN_TEMPDB = OFF);
CREATE UNIQUE NONCLUSTERED INDEX h_client_mast_694265bf_idx_A ON DV.h_client_master (clientid) WITH (SORT_IN_TEMPDB = OFF);
