truncate table LOAD.load_client_master;
INSERT INTO LOAD.load_client_master
(clientid, client_name, service_level, analytics_subscription, active, update_date,
dss_record_source, dss_load_datetime)
SELECT
client_master.clientid,client_master.client_name,client_master.service_level,
client_master.analytics_subscription,client_master.active,client_master.update_date,
NULL,NULL
From INGEST.client_master client_master
;
UPDATE LOAD.load_client_master SET dss_record_source = 'Customer1_DW.INGEST.Client_Master',dss_load_datetime = GetDate() ;