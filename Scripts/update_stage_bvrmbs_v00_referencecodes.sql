/****** Object:  StoredProcedure [dbo].[update_stage_bvrmbs_v00_referencecodes]    Script Date: 25/06/2018 1:07:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--==============================================================================
-- DBMS Name        :    SQL Server
-- Procedure Name   :    update_stage_bvrmbs_v00_referencecodes
-- Template         :    cert_wsl_sqlserver_proc_dv_stage
-- Template Version :    6.9.1.0
-- Description      :    Update the Stage Table table stage_bvrmbs_v00_referencecodes
-- Generated by     :    WhereScape RED Version 8.0.1.0 (build 171017-084020)
-- Generated for    :    Certus Evaluate until 01 July 2018
-- Generated on     :    Monday, June 25, 2018 at 12:49:29
-- Author           :    Bronwen
--==============================================================================
-- Notes / History
--
CREATE PROCEDURE [dbo].[update_stage_bvrmbs_v00_referencecodes]
  @p_sequence         INTEGER
, @p_job_name         VARCHAR(256)
, @p_task_name        VARCHAR(256)
, @p_job_id           INTEGER
, @p_task_id          INTEGER
, @p_return_msg       VARCHAR(256) OUTPUT
, @p_status           INTEGER      OUTPUT

AS
  SET XACT_ABORT OFF  -- Turn off auto abort on errors
  SET NOCOUNT ON      -- Turn off row count messages

  --=====================================================
  -- Control variables used in most programs
  --=====================================================
  DECLARE
    @v_msgtext               VARCHAR(255)  -- Text for audit_trail
  , @v_sql                   NVARCHAR(255) -- Text for SQL statements
  , @v_step                  INTEGER       -- return code
  , @v_insert_count          INTEGER       -- no of records inserted
  , @v_return_status         INTEGER       -- Update result status
  , @v_current_datetime      DATETIME      -- Used for date insert

  --=====================================================
  -- General Variables
  --=====================================================


  --=====================================================
  -- MAIN
  --=====================================================
  SET @v_step = 100

  SET @v_insert_count = 0
  SET @v_current_datetime = GETDATE()

  BEGIN TRY

    --=====================================================
    -- Delete existing records
    --=====================================================
    SET @v_step = 200

    SET @v_sql = N'TRUNCATE TABLE STAGE.stage_bvrmbs_v00_referencecodes';
    EXEC @v_return_status = sp_executesql @v_sql

    --=====================================================
    -- Insert new records
    --=====================================================
    SET @v_step = 300

    BEGIN TRANSACTION

      INSERT INTO STAGE.stage_bvrmbs_v00_referencecodes
      ( hk_l_rmbs_v00
      , scheduled_payment_policy
      , lmi_underwriting_type
      , loan_purpose
      , loan_documentation_type
      , loan_type
      , payment_frequency
      , lmi_provider_name
      , interest_rate_type
      , interest_rate_reset_interval
      , account_status
      , property_country_code
      , property_country_name
      , original_property_valuation_type
      , most_recent_property_valuation_type
      , property_purpose
      , property_type
      , lien
      , employment_type
      , legal_entity_type
      , income_verification
      , arrears_methodology
      , main_security_methodology
      , primary_borrower_methodology
      , savings_verification
      , dss_change_hash_stage_bvrmbs_v00_referencecodes
      , dss_load_datetime
      , dss_record_source
      , dss_create_time)
      SELECT  load_rmbs_v00_referencecodes.hk_l_rmbs_v00 AS hk_l_rmbs_v00 
           , load_rmbs_v00_referencecodes.scheduled_payment_policy AS scheduled_payment_policy 
           , load_rmbs_v00_referencecodes.lmi_underwriting_type AS lmi_underwriting_type 
           , load_rmbs_v00_referencecodes.loan_purpose AS loan_purpose 
           , load_rmbs_v00_referencecodes.loan_documentation_type AS loan_documentation_type 
           , load_rmbs_v00_referencecodes.loan_type AS loan_type 
           , load_rmbs_v00_referencecodes.payment_frequency AS payment_frequency 
           , load_rmbs_v00_referencecodes.lmi_provider_name AS lmi_provider_name 
           , load_rmbs_v00_referencecodes.interest_rate_type AS interest_rate_type 
           , load_rmbs_v00_referencecodes.interest_rate_reset_interval AS interest_rate_reset_interval 
           , load_rmbs_v00_referencecodes.account_status AS account_status 
           , load_rmbs_v00_referencecodes.property_country_code AS property_country_code 
           , load_rmbs_v00_referencecodes.property_country_name AS property_country_name 
           , load_rmbs_v00_referencecodes.original_property_valuation_type AS original_property_valuation_type 
           , load_rmbs_v00_referencecodes.most_recent_property_valuation_type AS most_recent_property_valuation_type 
           , load_rmbs_v00_referencecodes.property_purpose AS property_purpose 
           , load_rmbs_v00_referencecodes.property_type AS property_type 
           , load_rmbs_v00_referencecodes.lien AS lien 
           , load_rmbs_v00_referencecodes.employment_type AS employment_type 
           , load_rmbs_v00_referencecodes.legal_entity_type AS legal_entity_type 
           , load_rmbs_v00_referencecodes.income_verification AS income_verification 
           , load_rmbs_v00_referencecodes.arrears_methodology AS arrears_methodology 
           , load_rmbs_v00_referencecodes.main_security_methodology AS main_security_methodology 
           , load_rmbs_v00_referencecodes.primary_borrower_methodology AS primary_borrower_methodology 
           , load_rmbs_v00_referencecodes.savings_verification AS savings_verification 
           , CONVERT(BINARY(20),HASHBYTES('sha1',
               COALESCE(CAST(load_rmbs_v00_referencecodes.scheduled_payment_policy AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00_referencecodes.lmi_underwriting_type AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00_referencecodes.loan_purpose AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00_referencecodes.loan_documentation_type AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00_referencecodes.loan_type AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00_referencecodes.payment_frequency AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00_referencecodes.lmi_provider_name AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00_referencecodes.interest_rate_type AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00_referencecodes.interest_rate_reset_interval AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00_referencecodes.account_status AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00_referencecodes.property_country_code AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00_referencecodes.property_country_name AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00_referencecodes.original_property_valuation_type AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00_referencecodes.most_recent_property_valuation_type AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00_referencecodes.property_purpose AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00_referencecodes.property_type AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00_referencecodes.lien AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00_referencecodes.employment_type AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00_referencecodes.legal_entity_type AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00_referencecodes.income_verification AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00_referencecodes.arrears_methodology AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00_referencecodes.main_security_methodology AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00_referencecodes.primary_borrower_methodology AS VARCHAR(MAX)),'null') +'||'+
               COALESCE(CAST(load_rmbs_v00_referencecodes.savings_verification AS VARCHAR(MAX)),'null')
               ) , 2) AS dss_change_hash_stage_bvrmbs_v00_referencecodes 
           , load_rmbs_v00_referencecodes.dss_load_datetime AS dss_load_datetime 
           , load_rmbs_v00_referencecodes.dss_record_source AS dss_record_source 
           , @v_current_datetime AS dss_create_time 
      FROM LOAD.[load_rmbs_v00_referencecodes] load_rmbs_v00_referencecodes
      ;

      SELECT @v_insert_count = @@ROWCOUNT

    COMMIT

    --=====================================================
    -- All Done report the results
    --=====================================================
    SET @v_step = 400

    SET @p_status = 1
    SET @p_return_msg = 'stage_bvrmbs_v00_referencecodes updated. '
      + CONVERT(VARCHAR,@v_insert_count) + ' new records.'

    RETURN 0

  END TRY
  BEGIN CATCH

    SET @p_status = -2
    SET @p_return_msg = SUBSTRING('update_stage_bvrmbs_v00_referencecodes FAILED with error '
      + CONVERT(VARCHAR,ISNULL(ERROR_NUMBER(),0))
      + ' Step ' + CONVERT(VARCHAR,ISNULL(@v_step,0))
      + '. Error Msg: ' + ERROR_MESSAGE(),1,255)

  END CATCH
  IF XACT_STATE() <> 0
  BEGIN
    ROLLBACK TRANSACTION
  END

  RETURN 0
GO


