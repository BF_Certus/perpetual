CREATE TABLE LOAD.load_TRAN_manager (
  clientid varchar(100), feedid varchar(100), reporting_period varchar(100) 
, master_trust_name_or_spv varchar(100) 
, series_trust_name_or_series varchar(100), transaction_id varchar(100) 
, manager_name varchar(5000), manager_count int, manager_position int 
, manager_abn varchar(5000), manager_jurisdiction varchar(5000) 
, manager_id varchar(5000), manager_id_descriptor varchar(5000) 
, manager_fee_currency varchar(5000), manager_fee varchar(5000) 
, dss_record_source varchar(255), dss_load_datetime datetime2);