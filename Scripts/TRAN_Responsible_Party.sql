/****** Object:  View [INGEST].[TRAN_Responsible_Party]    Script Date: 4/07/2018 11:01:25 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--Drop view [INGEST].[TRAN_Responsible_Party]
--go
Create View [INGEST].[TRAN_Responsible_Party] as 
 
  WITH Nbrs ( Number ) AS (
    SELECT 1 UNION ALL
    SELECT 1 + Number FROM Nbrs WHERE Number < 99
)
SELECT h_cm.ClientID, h_fm.FeedID, h_rp.Reporting_Period, h_t.Master_Trust_Name_or_SPV, h_t.Series_Trust_Name_or_Series, h_t.Transaction_ID,
DelimCount as Responsible_Party_Count, N.Number as Responsible_Party_Position,
Case when DelimCount = 0 then Responsible_Party_Name else SubString(  Responsible_Party_Name , dbo.FN_CERTUS_INSTR(  Responsible_Party_Name ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Responsible_Party_Name,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Responsible_Party_Name ,';',1,N.Number-1)-1) end as Responsible_Party_Name,
Case when DelimCount = 0 then Responsible_Party_ABN else SubString(  Responsible_Party_ABN , dbo.FN_CERTUS_INSTR(  Responsible_Party_ABN ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Responsible_Party_ABN,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Responsible_Party_ABN ,';',1,N.Number-1)-1) end as Responsible_Party_ABN,
Case when DelimCount = 0 then Responsible_Party_Jurisdiction else SubString(  Responsible_Party_Jurisdiction , dbo.FN_CERTUS_INSTR(  Responsible_Party_Jurisdiction ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Responsible_Party_Jurisdiction,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Responsible_Party_Jurisdiction ,';',1,N.Number-1)-1) end as Responsible_Party_Jurisdiction,
Case when DelimCount = 0 then Responsible_Party_ID else SubString(  Responsible_Party_ID , dbo.FN_CERTUS_INSTR(  Responsible_Party_ID ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Responsible_Party_ID,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Responsible_Party_ID ,';',1,N.Number-1)-1) end as Responsible_Party_ID,
Case when DelimCount = 0 then Responsible_Party_ID_Descriptor else SubString(  Responsible_Party_ID_Descriptor , dbo.FN_CERTUS_INSTR(  Responsible_Party_ID_Descriptor ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Responsible_Party_ID_Descriptor,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Responsible_Party_ID_Descriptor ,';',1,N.Number-1)-1) end as Responsible_Party_ID_Descriptor,
Case when DelimCount = 0 then Responsible_Party_Fee_Currency else SubString(  Responsible_Party_Fee_Currency , dbo.FN_CERTUS_INSTR(  Responsible_Party_Fee_Currency ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Responsible_Party_Fee_Currency,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Responsible_Party_Fee_Currency ,';',1,N.Number-1)-1) end as Responsible_Party_Fee_Currency,
Case when DelimCount = 0 then Responsible_Party_Fee else SubString(  Responsible_Party_Fee , dbo.FN_CERTUS_INSTR(  Responsible_Party_Fee ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Responsible_Party_Fee,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Responsible_Party_Fee ,';',1,N.Number-1)-1) end as Responsible_Party_Fee
from 
	DV.S_TRAN_v00 s
	inner join 
	DV.L_TRAN_v00 l on s.hk_l_TRAN_v00=l.hk_l_TRAN_v00
	inner join
	DV.h_client_master h_cm on h_cm.hk_h_client_master=l.hk_h_client_master
	inner join
	DV.h_feed_master h_fm on h_fm.hk_h_feed_master=l.hk_h_feed_master
	inner join
	DV.h_reporting_period h_rp on h_rp.hk_h_reporting_period=l.hk_h_reporting_period
	inner join
	DV.h_transaction h_t on h_t.hk_h_transaction=l.hk_h_transaction
	inner join
	(select TRAN_v00_DelimiterCount.hk_l_TRAN_v00, case 
		when Delim_Responsible_Party_Name=Delim_Responsible_Party_ABN
		and Delim_Responsible_Party_Name=Delim_Responsible_Party_Jurisdiction
		and Delim_Responsible_Party_Name=Delim_Responsible_Party_ID
		and Delim_Responsible_Party_Name=Delim_Responsible_Party_ID_Descriptor
		and Delim_Responsible_Party_Name=Delim_Responsible_Party_Fee_Currency
		and Delim_Responsible_Party_Name=Delim_Responsible_Party_Fee
		then Delim_Responsible_Party_Name+1 end as DelimCount from INGEST.TRAN_v00_DelimiterCount) delim on delim.hk_l_TRAN_v00=l.hk_l_TRAN_v00
	inner join
	Nbrs N ON (N.Number <= DelimCount) or (DelimCount = 0 and N.Number =1)

GO


