CREATE TABLE LOAD.load_security (
  clientid int, transaction_id varchar(5000) 
, master_trust_name_or_spv varchar(5000) 
, series_trust_name_or_series varchar(5000), tranche_name varchar(5000) 
, feedname varchar(100), dss_record_source varchar(255) 
, dss_load_datetime datetime2);