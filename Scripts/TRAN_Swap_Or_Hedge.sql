/****** Object:  View [INGEST].[TRAN_Swap_Or_Hedge]    Script Date: 4/07/2018 11:01:25 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--Drop view [INGEST].[TRAN_Swap_Or_Hedge]
--go
Create View [INGEST].[TRAN_Swap_Or_Hedge] as 
 
  WITH Nbrs ( Number ) AS (
    SELECT 1 UNION ALL
    SELECT 1 + Number FROM Nbrs WHERE Number < 99
)
SELECT h_cm.ClientID, h_fm.FeedID, h_rp.Reporting_Period, h_t.Master_Trust_Name_or_SPV, h_t.Series_Trust_Name_or_Series, h_t.Transaction_ID,
DelimCount as Swap_Or_Hedge_Count, N.Number as Swap_Or_Hedge_Position,
Case when DelimCount = 0 then Swap_or_Hedge_Provider_Name else SubString(  Swap_or_Hedge_Provider_Name , dbo.FN_CERTUS_INSTR(  Swap_or_Hedge_Provider_Name ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Swap_or_Hedge_Provider_Name,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Swap_or_Hedge_Provider_Name ,';',1,N.Number-1)-1) end as Swap_or_Hedge_Provider_Name,
Case when DelimCount = 0 then Swap_or_Hedge_Provider_ABN else SubString(  Swap_or_Hedge_Provider_ABN , dbo.FN_CERTUS_INSTR(  Swap_or_Hedge_Provider_ABN ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Swap_or_Hedge_Provider_ABN,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Swap_or_Hedge_Provider_ABN ,';',1,N.Number-1)-1) end as Swap_or_Hedge_Provider_ABN,
Case when DelimCount = 0 then Swap_or_Hedge_Type else SubString(  Swap_or_Hedge_Type , dbo.FN_CERTUS_INSTR(  Swap_or_Hedge_Type ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Swap_or_Hedge_Type,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Swap_or_Hedge_Type ,';',1,N.Number-1)-1) end as Swap_or_Hedge_Type,
Case when DelimCount = 0 then Notional_Principal_Amount else SubString(  Notional_Principal_Amount , dbo.FN_CERTUS_INSTR(  Notional_Principal_Amount ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Notional_Principal_Amount,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Notional_Principal_Amount ,';',1,N.Number-1)-1) end as Notional_Principal_Amount,
Case when DelimCount = 0 then Swap_or_Hedge_Provider_Jurisdiction else SubString(  Swap_or_Hedge_Provider_Jurisdiction , dbo.FN_CERTUS_INSTR(  Swap_or_Hedge_Provider_Jurisdiction ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Swap_or_Hedge_Provider_Jurisdiction,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Swap_or_Hedge_Provider_Jurisdiction ,';',1,N.Number-1)-1) end as Swap_or_Hedge_Provider_Jurisdiction,
Case when DelimCount = 0 then Swap_or_Hedge_Provider_ID else SubString(  Swap_or_Hedge_Provider_ID , dbo.FN_CERTUS_INSTR(  Swap_or_Hedge_Provider_ID ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Swap_or_Hedge_Provider_ID,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Swap_or_Hedge_Provider_ID ,';',1,N.Number-1)-1) end as Swap_or_Hedge_Provider_ID,
Case when DelimCount = 0 then Swap_or_Hedge_Provider_ID_Descriptor else SubString(  Swap_or_Hedge_Provider_ID_Descriptor , dbo.FN_CERTUS_INSTR(  Swap_or_Hedge_Provider_ID_Descriptor ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Swap_or_Hedge_Provider_ID_Descriptor,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Swap_or_Hedge_Provider_ID_Descriptor ,';',1,N.Number-1)-1) end as Swap_or_Hedge_Provider_ID_Descriptor,
Case when DelimCount = 0 then Swap_or_Hedge_Name else SubString(  Swap_or_Hedge_Name , dbo.FN_CERTUS_INSTR(  Swap_or_Hedge_Name ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Swap_or_Hedge_Name,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Swap_or_Hedge_Name ,';',1,N.Number-1)-1) end as Swap_or_Hedge_Name,
Case when DelimCount = 0 then Pay_Leg_Currency else SubString(  Pay_Leg_Currency , dbo.FN_CERTUS_INSTR(  Pay_Leg_Currency ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Pay_Leg_Currency,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Pay_Leg_Currency ,';',1,N.Number-1)-1) end as Pay_Leg_Currency,
Case when DelimCount = 0 then Pay_Leg_Reference_Index else SubString(  Pay_Leg_Reference_Index , dbo.FN_CERTUS_INSTR(  Pay_Leg_Reference_Index ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Pay_Leg_Reference_Index,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Pay_Leg_Reference_Index ,';',1,N.Number-1)-1) end as Pay_Leg_Reference_Index,
Case when DelimCount = 0 then Pay_Leg_Reference_Index_Value else SubString(  Pay_Leg_Reference_Index_Value , dbo.FN_CERTUS_INSTR(  Pay_Leg_Reference_Index_Value ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Pay_Leg_Reference_Index_Value,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Pay_Leg_Reference_Index_Value ,';',1,N.Number-1)-1) end as Pay_Leg_Reference_Index_Value,
Case when DelimCount = 0 then Pay_Leg_Margin_or_Rate else SubString(  Pay_Leg_Margin_or_Rate , dbo.FN_CERTUS_INSTR(  Pay_Leg_Margin_or_Rate ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Pay_Leg_Margin_or_Rate,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Pay_Leg_Margin_or_Rate ,';',1,N.Number-1)-1) end as Pay_Leg_Margin_or_Rate,
Case when DelimCount = 0 then Pay_Leg_Amount else SubString(  Pay_Leg_Amount , dbo.FN_CERTUS_INSTR(  Pay_Leg_Amount ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Pay_Leg_Amount,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Pay_Leg_Amount ,';',1,N.Number-1)-1) end as Pay_Leg_Amount,
Case when DelimCount = 0 then Receive_Leg_Currency else SubString(  Receive_Leg_Currency , dbo.FN_CERTUS_INSTR(  Receive_Leg_Currency ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Receive_Leg_Currency,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Receive_Leg_Currency ,';',1,N.Number-1)-1) end as Receive_Leg_Currency,
Case when DelimCount = 0 then Receive_Leg_Reference_Index else SubString(  Receive_Leg_Reference_Index , dbo.FN_CERTUS_INSTR(  Receive_Leg_Reference_Index ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Receive_Leg_Reference_Index,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Receive_Leg_Reference_Index ,';',1,N.Number-1)-1) end as Receive_Leg_Reference_Index,
Case when DelimCount = 0 then Receive_Leg_Reference_Index_Value else SubString(  Receive_Leg_Reference_Index_Value , dbo.FN_CERTUS_INSTR(  Receive_Leg_Reference_Index_Value ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Receive_Leg_Reference_Index_Value,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Receive_Leg_Reference_Index_Value ,';',1,N.Number-1)-1) end as Receive_Leg_Reference_Index_Value,
Case when DelimCount = 0 then Receive_Leg_Margin_or_Rate else SubString(  Receive_Leg_Margin_or_Rate , dbo.FN_CERTUS_INSTR(  Receive_Leg_Margin_or_Rate ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Receive_Leg_Margin_or_Rate,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Receive_Leg_Margin_or_Rate ,';',1,N.Number-1)-1) end as Receive_Leg_Margin_or_Rate,
Case when DelimCount = 0 then Receive_Leg_Amount else SubString(  Receive_Leg_Amount , dbo.FN_CERTUS_INSTR(  Receive_Leg_Amount ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Receive_Leg_Amount,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Receive_Leg_Amount ,';',1,N.Number-1)-1) end as Receive_Leg_Amount,
Case when DelimCount = 0 then Exchange_Rate else SubString(  Exchange_Rate , dbo.FN_CERTUS_INSTR(  Exchange_Rate ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Exchange_Rate,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Exchange_Rate ,';',1,N.Number-1)-1) end as Exchange_Rate,
Case when DelimCount = 0 then Swap_or_Hedge_Fee_Currency else SubString(  Swap_or_Hedge_Fee_Currency , dbo.FN_CERTUS_INSTR(  Swap_or_Hedge_Fee_Currency ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Swap_or_Hedge_Fee_Currency,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Swap_or_Hedge_Fee_Currency ,';',1,N.Number-1)-1) end as Swap_or_Hedge_Fee_Currency,
Case when DelimCount = 0 then Swap_or_Hedge_Fee else SubString(  Swap_or_Hedge_Fee , dbo.FN_CERTUS_INSTR(  Swap_or_Hedge_Fee ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR( concat(Swap_or_Hedge_Fee,';') ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Swap_or_Hedge_Fee ,';',1,N.Number-1)-1) end as Swap_or_Hedge_Fee
from 
	DV.S_TRAN_v00 s
	inner join 
	DV.L_TRAN_v00 l on s.hk_l_TRAN_v00=l.hk_l_TRAN_v00
	inner join
	DV.h_client_master h_cm on h_cm.hk_h_client_master=l.hk_h_client_master
	inner join
	DV.h_feed_master h_fm on h_fm.hk_h_feed_master=l.hk_h_feed_master
	inner join
	DV.h_reporting_period h_rp on h_rp.hk_h_reporting_period=l.hk_h_reporting_period
	inner join
	DV.h_transaction h_t on h_t.hk_h_transaction=l.hk_h_transaction
	inner join
	(select TRAN_v00_DelimiterCount.hk_l_TRAN_v00, case
		when Delim_Swap_or_Hedge_Provider_Name=Delim_Swap_or_Hedge_Provider_ABN
		and Delim_Swap_or_Hedge_Provider_Name=Delim_Swap_or_Hedge_Type
		and Delim_Swap_or_Hedge_Provider_Name=Delim_Notional_Principal_Amount
		and Delim_Swap_or_Hedge_Provider_Name=Delim_Swap_or_Hedge_Provider_Jurisdiction
		and Delim_Swap_or_Hedge_Provider_Name=Delim_Swap_or_Hedge_Provider_ID
		and Delim_Swap_or_Hedge_Provider_Name=Delim_Swap_or_Hedge_Provider_ID_Descriptor
		and Delim_Swap_or_Hedge_Provider_Name=Delim_Swap_or_Hedge_Name
		and Delim_Swap_or_Hedge_Provider_Name=Delim_Pay_Leg_Currency
		and Delim_Swap_or_Hedge_Provider_Name=Delim_Pay_Leg_Reference_Index
		and Delim_Swap_or_Hedge_Provider_Name=Delim_Pay_Leg_Reference_Index_Value
		and Delim_Swap_or_Hedge_Provider_Name=Delim_Pay_Leg_Margin_or_Rate
		and Delim_Swap_or_Hedge_Provider_Name=Delim_Pay_Leg_Amount
		and Delim_Swap_or_Hedge_Provider_Name=Delim_Receive_Leg_Currency
		and Delim_Swap_or_Hedge_Provider_Name=Delim_Receive_Leg_Reference_Index
		and Delim_Swap_or_Hedge_Provider_Name=Delim_Receive_Leg_Reference_Index_Value
		and Delim_Swap_or_Hedge_Provider_Name=Delim_Receive_Leg_Margin_or_Rate
		and Delim_Swap_or_Hedge_Provider_Name=Delim_Receive_Leg_Amount
		and Delim_Swap_or_Hedge_Provider_Name=Delim_Exchange_Rate
		and Delim_Swap_or_Hedge_Provider_Name=Delim_Swap_or_Hedge_Fee_Currency
		and Delim_Swap_or_Hedge_Provider_Name=Delim_Swap_or_Hedge_Fee
		then Delim_Swap_or_Hedge_Provider_Name+1 end as DelimCount from INGEST.TRAN_v00_DelimiterCount) delim on delim.hk_l_TRAN_v00=l.hk_l_TRAN_v00
	inner join
	Nbrs N ON (N.Number <= DelimCount) or (DelimCount = 0 and N.Number =1)

GO


