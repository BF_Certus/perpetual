CREATE TABLE DV.bvs_TRAN_credit_enhancement (
  hk_l_TRAN_credit_enhancement binary(20) NOT NULL 
, dss_load_datetime datetime, dss_change_hash binary(20) NOT NULL 
, dss_record_source varchar(255), dss_start_date datetime, dss_version int 
, dss_create_time datetime, feedid varchar(100), credit_enhancement_count int 
, credit_enhancement_position int 
, credit_enhancement_provider_name varchar(5000) 
, credit_enhancement_provider_jurisdiction varchar(5000) 
, credit_enhancement_provider_id varchar(5000) 
, credit_enhancement_provider_id_descriptor varchar(5000) 
, credit_enhancement_type varchar(5000) 
, credit_enhancement_currency varchar(5000) 
, credit_enhancement_fee varchar(5000) 
, initial_credit_enhancement_amount varchar(5000) 
, beginning_amount_available_credit varchar(5000) 
, restorations_credit varchar(5000), draws_credit varchar(5000) 
, reduction_credit varchar(5000), ending_amount_available_credit varchar(5000) 
, beginning_amount_drawn_credit varchar(5000) 
, ending_amount_drawn_credit varchar(5000));