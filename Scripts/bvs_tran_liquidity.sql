CREATE TABLE DV.bvs_TRAN_liquidity (
  hk_l_TRAN_liquidity binary(20) NOT NULL, dss_load_datetime datetime 
, dss_change_hash binary(20) NOT NULL, dss_record_source varchar(255) 
, dss_start_date datetime, dss_version int, dss_create_time datetime 
, liquidity_count int, liquidity_position int 
, liquidity_provider_abn varchar(5000), initial_amount_available varchar(5000) 
, beginning_amount_available_liquidity varchar(5000) 
, ending_amount_available_liquidity varchar(5000) 
, liquidity_provider_jurisdiction varchar(5000) 
, liquidity_provider_id varchar(5000) 
, liquidity_provider_id_descriptor varchar(5000) 
, liquidity_facility_name varchar(5000), liquidity_facility_type varchar(5000) 
, reimbursements varchar(5000), draws_liquidity varchar(5000) 
, reduction_liquidity varchar(5000) 
, beginning_amount_drawn_liquidity varchar(5000) 
, ending_amount_drawn_liquidity varchar(5000), facility_currency varchar(5000) 
, facility_fee varchar(5000));