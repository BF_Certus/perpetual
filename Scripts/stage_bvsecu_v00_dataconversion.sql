CREATE TABLE [STAGE].stage_bvSECU_v00_dataconversion (
  hk_l_SECU_v00 binary(20), report_date date, issue_date date 
, accrual_begin_date date, accrual_end_date date, accrual_period int 
, original_subordination decimal(24,9), current_subordination decimal(24,9) 
, current_principal_face_amount decimal(18,2), coupon_payment_reference bit 
, distribution_date date, record_date date, determination_date date 
, beginning_stated_factor decimal(24,9), ending_stated_factor decimal(24,9) 
, original_principal_face_amount decimal(18,2) 
, beginning_invested_amount decimal(18,2), ending_invested_amount decimal(18,2) 
, beginning_stated_amount decimal(18,2), ending_stated_amount decimal(18,2) 
, beginning_invested_factor decimal(24,9), ending_invested_factor decimal(24,9) 
, coupon_reference_index_value decimal(24,9), coupon_margin decimal(24,9) 
, step_up_margin decimal(24,9), step_up_date date 
, scheduled_coupon decimal(18,2), scheduled_coupon_factor decimal(24,9) 
, current_coupon_rate decimal(24,9), coupon_distribution decimal(18,2) 
, coupon_distribution_factor decimal(24,9), scheduled_principal decimal(18,2) 
, scheduled_principal_factor decimal(24,9), principal_distribution decimal(18,2) 
, principal_distribution_factor decimal(24,9) 
, coupon_shortfall_factor decimal(24,9) 
, ending_cumulative_coupon_shortfall_factor decimal(24,9) 
, coupon_shortfall decimal(18,2) 
, ending_cumulative_coupon_shortfall decimal(18,2) 
, principal_shortfall decimal(18,2) 
, ending_cumulative_principal_shortfall decimal(18,2), legal_maturity_date date 
, original_estimated_weighted_average_life decimal(24,9) 
, current_weighted_average_life decimal(24,9), expected_final_maturity_date date 
, redemption_price decimal(24,9), abn bigint, soft_bullet_date date 
, step_up_rate decimal(24,9), beginning_cumulative_chargeoffs decimal(18,2) 
, beginning_cumulative_coupon_shortfall decimal(18,2) 
, beginning_cumulative_principal_shortfall decimal(18,2) 
, coupon_shortfall_makeup decimal(18,2) 
, principal_shortfall_makeup decimal(18,2), allocated_chargeoffs decimal(18,2) 
, chargeoff_restorations decimal(18,2) 
, ending_cumulative_chargeoffs decimal(18,2) 
, beginning_cumulative_chargeoff_factor decimal(24,9) 
, beginning_cumulative_coupon_shortfall_factor decimal(24,9) 
, beginning_cumulative_principal_shortfall_factor decimal(24,9) 
, coupon_shortfall_makeup_factor decimal(24,9) 
, principal_shortfall_makeup_factor decimal(24,9) 
, principal_shortfall_factor decimal(24,9) 
, allocated_chargeoff_factor decimal(24,9) 
, chargeoff_restoration_factor decimal(24,9) 
, ending_cumulative_chargeoff_factor decimal(24,9) 
, ending_cumulative_principal_shortfall_factor decimal(24,9) 
, coupon_rate decimal(24,9) 
, dss_change_hash_stage_bvSECU_v00_dataconversion binary(20) NOT NULL 
, dss_load_datetime datetime2, dss_record_source varchar(255) 
, dss_create_time datetime2);