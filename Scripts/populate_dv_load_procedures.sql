set nocount on;


declare 
	@procedures varchar(max),
	@procname varchar(128),
	@loadname varchar(100),
	@groupid int,
	@orderid int;

set @loadname = 'Perpetual';

delete from [INGEST].[dv_load_procedures] where Load_Name = @loadname;


set @groupid = 1;

while @groupid > 0
begin
	if(@groupid = 1)
		set @procedures = 
			 'dbo.update_stage_client_master'+
			',dbo.update_stage_party'+
			',dbo.update_stage_rmbs_v00'+
			',dbo.update_stage_TRAN_v00'+
			',dbo.update_stage_SECU_v00'+
			',dbo.update_stage_SECU_exchange_name'+
			',dbo.update_stage_SECU_clearing_system'+
			',dbo.update_stage_SECU_ratings'+
			',dbo.update_stage_rmbs_col_v00'+
			',dbo.update_l_TRAN_v00'+
			',dbo.update_l_rmbs_v00'+
			',dbo.update_l_SECU_v00'+
			',dbo.update_l_SECU_exchange_name'+
			',dbo.update_l_SECU_clearing_system'+
			',dbo.update_l_SECU_ratings'+
			',dbo.update_l_rmbs_col_v00'+
			',dbo.update_s_feed_master'+
			',dbo.update_s_client_master'+
			',dbo.update_s_rmbs_rts'+
			',dbo.update_s_rmbs_v00'+
			',dbo.update_s_TRAN_rts'+
			',dbo.update_s_TRAN_v00';
	else if (@groupid = 2)
		set @procedures = 
			'dbo.update_stage_TRAN_account'+
			',dbo.update_stage_TRAN_arranger'+
			',dbo.update_stage_TRAN_authority'+
			',dbo.update_stage_TRAN_credit_enhancement'+
			',dbo.update_stage_TRAN_identure_or_security_trustee'+
			',dbo.update_stage_TRAN_issuing_trustee'+
			',dbo.update_stage_TRAN_liquidity'+
			',dbo.update_stage_TRAN_lmi'+
			',dbo.update_stage_TRAN_manager'+
			',dbo.update_stage_TRAN_originator'+
			',dbo.update_stage_TRAN_other_role'+
			',dbo.update_stage_TRAN_paying_agent'+
			',dbo.update_stage_TRAN_responsible_party'+
			',dbo.update_stage_TRAN_seller'+
			',dbo.update_stage_TRAN_servicer'+
			',dbo.update_stage_TRAN_swap_or_hedge'+
			',dbo.update_stage_TRAN_trigger'+
			',dbo.update_l_TRAN_account'+
			',dbo.update_l_TRAN_arranger'+
			',dbo.update_l_TRAN_authority'+
			',dbo.update_l_TRAN_credit_enhancement'+
			',dbo.update_l_TRAN_identure_or_security_trustee'+
			',dbo.update_l_TRAN_issuing_trustee'+
			',dbo.update_l_TRAN_liquidity'+
			',dbo.update_l_TRAN_lmi'+
			',dbo.update_l_TRAN_manager'+
			',dbo.update_l_TRAN_originator'+
			',dbo.update_l_TRAN_other_role'+
			',dbo.update_l_TRAN_paying_agent'+
			',dbo.update_l_TRAN_responsible_party'+
			',dbo.update_l_TRAN_seller'+
			',dbo.update_l_TRAN_servicer'+
			',dbo.update_l_TRAN_swap_or_hedge'+
			',dbo.update_l_TRAN_trigger';
	else if (@groupid = 3)
		set @procedures = 'dbo.update_stage_bvrmbs_v00_statustracking'+
			',dbo.update_stage_bvrmbs_v00_validations'+
			',dbo.update_stage_bvrmbs_v00_dataconversion'+
			',dbo.update_stage_bvrmbs_v00_referencecodes'+
			',dbo.update_bvs_rmbs_v00_statustracking'+
			',dbo.update_bvs_rmbs_v00_validations'+
			',dbo.update_bvs_rmbs_v00_dataconversion'+
			',dbo.update_bvs_rmbs_v00_referencecodes'+
			',dbo.update_stage_bvrmbs_col_v00_dataconversion'+
			',dbo.update_stage_bvSECU_v00_dataconversion';
	else if (@groupid = 4)
		set @procedures = 
			'dbo.update_stage_bvrmbs_v00_calculations'+
			',dbo.update_bvs_rmbs_v00_calculations';
	else
		break;

	set @orderid = 1;

	while len(@procedures) > 0
	begin
		set @procname = left(@procedures, charindex(',', @procedures+',')-1);

		insert into [dbo].[dv_load_procedures]([Load_Name],[Proc_Name],[Group_Id],[Order_Id],[ActionId])
		values (@loadname,@procname,@groupid,@orderid,1);
		set @orderid = @orderid +1;
		--print @procname;
		set @procedures = stuff(@procedures, 1, charindex(',', @procedures+','), '')
	end

	set @groupid = @groupid + 1;

end

select * from [dbo].[dv_load_procedures] order by group_id, order_id;

