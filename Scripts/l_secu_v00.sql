CREATE TABLE DV.l_SECU_v00 (
  hk_l_SECU_v00 binary(20) NOT NULL, hk_h_client_master binary(20) NOT NULL 
, hk_h_isin binary(20) NOT NULL, hk_h_reporting_period binary(20) NOT NULL 
, hk_h_security binary(20) NOT NULL, hk_h_transaction binary(20) NOT NULL 
, dss_record_source varchar(255), dss_load_datetime datetime2 
, dss_create_time datetime2);
CREATE NONCLUSTERED INDEX l_SECU_v00_idx_1 ON DV.l_SECU_v00 (hk_h_client_master) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_SECU_v00_idx_2 ON DV.l_SECU_v00 (hk_h_isin) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_SECU_v00_idx_3 ON DV.l_SECU_v00 (hk_h_reporting_period) WITH (SORT_IN_TEMPDB = OFF);
ALTER TABLE DV.l_SECU_v00 ADD CONSTRAINT l_SECU_v0_bf7d2788_idx_PK PRIMARY KEY NONCLUSTERED (hk_l_SECU_v00) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_SECU_v00_idx_4 ON DV.l_SECU_v00 (hk_h_security) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_SECU_v00_idx_5 ON DV.l_SECU_v00 (hk_h_transaction) WITH (SORT_IN_TEMPDB = OFF);
CREATE UNIQUE NONCLUSTERED INDEX l_SECU_v00_idx_A ON DV.l_SECU_v00 (hk_h_client_master,hk_h_isin,hk_h_reporting_period,hk_h_security,hk_h_transaction) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_SECU_v0_bf7d2788_idx_1 ON DV.l_SECU_v00 (hk_h_client_master) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_SECU_v0_bf7d2788_idx_2 ON DV.l_SECU_v00 (hk_h_isin) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_SECU_v0_bf7d2788_idx_3 ON DV.l_SECU_v00 (hk_h_reporting_period) WITH (SORT_IN_TEMPDB = OFF);
CREATE UNIQUE NONCLUSTERED INDEX l_SECU_v00_idx_0 ON DV.l_SECU_v00 (hk_l_SECU_v00) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_SECU_v0_bf7d2788_idx_4 ON DV.l_SECU_v00 (hk_h_security) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_SECU_v0_bf7d2788_idx_5 ON DV.l_SECU_v00 (hk_h_transaction) WITH (SORT_IN_TEMPDB = OFF);
