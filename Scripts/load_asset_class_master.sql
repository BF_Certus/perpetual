CREATE TABLE LOAD.load_asset_class_master (
  asset_class_id int, asset_class_code varchar(10) 
, asset_class_description varchar(100), active bit, update_date datetime 
, dss_record_source varchar(255), dss_load_datetime datetime2);