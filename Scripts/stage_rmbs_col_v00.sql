CREATE TABLE STAGE.stage_rmbs_col_v00 (
  hk_l_rmbs_col_v00 binary(20) NOT NULL, hk_h_client_master binary(20) NOT NULL 
, hk_h_collateral binary(20) NOT NULL, hk_h_loan binary(20) NOT NULL 
, hk_h_reporting_period binary(20) NOT NULL 
, hk_h_transaction binary(20) NOT NULL, clientid int 
, reporting_period nvarchar(100), transaction_id nvarchar(255) 
, master_trust_name_or_spv nvarchar(255) 
, series_trust_name_or_series nvarchar(255), loan_id nvarchar(255) 
, collateral_id nvarchar(255), collateral_date nvarchar(255) 
, group_loan_id nvarchar(255), security_property_postcode nvarchar(255) 
, security_property_country nvarchar(255) 
, original_security_property_value nvarchar(255) 
, most_recent_security_property_value nvarchar(255) 
, original_property_valuation_type nvarchar(255) 
, most_recent_property_valuation_type nvarchar(255) 
, most_recent_security_property_valuation_date nvarchar(255) 
, security_property_purpose nvarchar(255), security_property_type nvarchar(255) 
, lien nvarchar(255), abs_statistical_area nvarchar(255) 
, main_security_methodology nvarchar(255), main_security_flag nvarchar(255) 
, property_valuation_currency nvarchar(255) 
, dss_change_hash_stage_rmbs_col_v00 binary(20) NOT NULL 
, dss_load_datetime datetime2, dss_record_source varchar(255) 
, dss_create_time datetime2);