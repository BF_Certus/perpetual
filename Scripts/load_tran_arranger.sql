CREATE TABLE LOAD.load_TRAN_arranger (
  clientid varchar(100), feedid varchar(100), reporting_period varchar(100) 
, master_trust_name_or_spv varchar(100) 
, series_trust_name_or_series varchar(100), transaction_id varchar(100) 
, arranger_name varchar(5000), arranger_count int, arranger_position int 
, arranger_abn varchar(5000), arranger_jurisdiction varchar(5000) 
, arranger_id varchar(5000), arranger_id_descriptor varchar(5000) 
, arranger_fee_currency varchar(5000), arranger_fee varchar(5000) 
, dss_record_source varchar(255), dss_load_datetime datetime2);