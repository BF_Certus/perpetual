CREATE TABLE STAGE.stage_client_master (
  hk_h_client_master binary(20) NOT NULL, clientid int, client_name varchar(100) 
, service_level int, analytics_subscription varchar(100), active bit 
, update_date datetime, dss_change_hash_stage_client_master binary(20) NOT NULL 
, dss_record_source varchar(255), dss_load_datetime datetime2 
, dss_create_time datetime2)