CREATE TABLE STAGE.stage_bvrmbs_col_v00_dataconversion (
  hk_l_rmbs_col_v00 binary(20), security_property_postcode int 
, original_security_property_value decimal(18,2) 
, most_recent_security_property_value decimal(18,2) 
, most_recent_security_property_valuation_date date, abs_statistical_area int 
, main_security_flag bit 
, dss_change_hash_stage_bvrmbs_col_v00_dataconversion binary(20) NOT NULL 
, dss_load_datetime datetime2, dss_record_source varchar(255) 
, dss_create_time datetime2);