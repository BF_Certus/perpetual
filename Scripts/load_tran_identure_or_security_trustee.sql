CREATE TABLE LOAD.load_TRAN_identure_or_security_trustee (
  clientid varchar(100), feedid varchar(100), reporting_period varchar(100) 
, master_trust_name_or_spv varchar(100) 
, series_trust_name_or_series varchar(100), transaction_id varchar(100) 
, indenture_or_security_trustee_name varchar(5000) 
, identure_or_security_trustee_count int 
, identure_or_security_trustee_position int 
, indenture_or_security_trustee_abn varchar(5000) 
, indenture_or_security_trustee_jurisdiction varchar(5000) 
, indenture_or_security_trustee_id varchar(5000) 
, indenture_or_security_trustee_id_descriptor varchar(5000) 
, indenture_or_security_trustee_fee_currency varchar(5000) 
, indenture_or_security_trustee_fee varchar(5000) 
, dss_record_source varchar(255), dss_load_datetime datetime2);