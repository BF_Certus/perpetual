IF OBJECT_ID(N'dbo.update_load_tables_RV', N'p')IS NOT NULL  
   DROP PROCEDURE dbo.update_load_tables_RV;  
GO


CREATE PROCEDURE [dbo].[update_load_tables_RV] @groupid INT

AS

SET NOCOUNT ON;
SET XACT_ABORT ON;

IF @groupid=1 BEGIN

	--	load_feed_master
	TRUNCATE TABLE LOAD.load_feed_master;
	INSERT INTO LOAD.load_feed_master 
	(feedid, feed_name, clientid, asset_class_id, feed_level, feed_details, stage_table, target_table, control_file_name, checksum_format, feed_location, feed_issuer_contact, data_validation_required, onboarding_required, active, update_date, dss_record_source, dss_load_datetime)
	SELECT Feed_Master.FeedID,Feed_Master.Feed_Name,Feed_Master.ClientID,Feed_Master.Asset_Class_ID,Feed_Master.Feed_Level,Feed_Master.Feed_Details,Feed_Master.Stage_Table,Feed_Master.Target_Table,Feed_Master.Control_File_Name,Feed_Master.Checksum_Format,Feed_Master.Feed_Location,Feed_Master.Feed_Issuer_Contact,Feed_Master.Data_Validation_Required,Feed_Master.Onboarding_Required,Feed_Master.Active,Feed_Master.Update_Date,'',''
	FROM INGEST.Feed_Master Feed_Master;
	UPDATE LOAD.load_feed_master SET dss_record_source = '.INGEST.Feed_Master',dss_load_datetime = GetDate() ;


	--	load_client_master
	TRUNCATE TABLE LOAD.load_client_master;
	INSERT INTO LOAD.load_client_master
	(clientid, client_name, service_level, analytics_subscription, active, update_date,
	dss_record_source, dss_load_datetime)
	SELECT client_master.clientid,client_master.client_name,client_master.service_level,
	client_master.analytics_subscription,client_master.active,client_master.update_date,
	NULL,NULL
	From INGEST.client_master client_master;
	UPDATE LOAD.load_client_master SET dss_record_source = 'Customer1_DW.INGEST.Client_Master',dss_load_datetime = GetDate() ;

	--	load_party ------------------------------------------------------------
	TRUNCATE TABLE LOAD.load_party;
	INSERT INTO LOAD.load_party
	(clientid, party_name, feed_name, dss_record_source, dss_load_datetime)
	SELECT party.clientid,party.party_name,party.feed_name,NULL,NULL
	From INGEST.party party;
	UPDATE LOAD.load_party SET dss_record_source = Feed_Name,dss_load_datetime = GetDate() ;


	--	load_rmbs_v00 ------------------------------------------------------------
	truncate table LOAD.load_rmbs_v00;
	INSERT INTO LOAD.load_rmbs_v00
	(clientid, borrower_id, reporting_period, transaction_id, master_trust_name_or_spv,
	series_trust_name_or_series, servicer_name, originator_name, loan_id, seller_name,
	collateral_date, servicer_abn, originator_abn, group_loan_id, most_recent_approval_amount,
	current_balance, scheduled_balance, loan_guarantee_flag, redraw_feature_flag,
	available_redraw, offset_account_flag, offset_account_balance, scheduled_payment_policy,
	conforming_mortgage, lmi_underwriting_type, origination_date_settlement_date,
	loan_purpose, loan_origination_channel_flag, loan_term, remaining_term,
	seasoning, maturity_date, loan_securitised_date, original_ltv, current_ltv,
	scheduled_ltv, loan_documentation_type, loan_type, scheduled_minimum_payment,
	payment_frequency, lmi_provider_name, lmi_attachment_point, loan_currency,
	variable_loan_amount, interest_rate_type, current_interest_rate, reference_index_value,
	interest_margin, interest_rate_reset_interval, restructuring_arrangement_flag,
	interest_only_expiry_date, current_rate_expiry_date, account_status, default_balance,
	foreclosure_proceeds, date_of_foreclosure, loss_on_sale, days_in_arrears,
	amount_in_arrears, days_since_last_scheduled_payment, cumulative_missed_payments,
	claim_submitted_to_lmi, claim_paid_by_lmi, claim_denied_by_lmi, claim_pending_with_lmi,
	property_postcode, property_country_name, original_property_value, most_recent_property_value,
	original_property_valuation_type, most_recent_property_valuation_type,
	most_recent_property_valuation_date, property_purpose, property_type, lien,
	first_home_buyer_flag, country_of_residence, borrower_residency_secondary_borrower,
	employment_type, employment_type_secondary_borrower, borrower_type_primary_borrower,
	borrower_type_secondary_borrower, legal_entity_type, legal_entity_type_secondary_borrower,
	bankruptcy_flag, last_credit_discharge_date, number_of_debtors, external_credit_score_provider_name,
	credit_score, credit_score_secondary_borrower, debt_serviceability_metric,
	debt_serviceability_metric_score, income, income_verification, secondary_income,
	income_verification_for_secondary_income, abs_statistical_area, arrears_methodology,
	main_security_methodology, primary_borrower_methodology, originator_id,
	seller_abn, seller_id, lmi_provider_abn, lmi_provider_id, servicer_id,
	savings_verification, interest_rate, origination_date, feed_name, dss_record_source,
	dss_load_datetime)
	SELECT
	rmbs_v00.clientid,rmbs_v00.borrower_id,rmbs_v00.reporting_period,rmbs_v00.transaction_id,
	rmbs_v00.master_trust_name_or_spv,rmbs_v00.series_trust_name_or_series,
	rmbs_v00.servicer_name,rmbs_v00.originator_name,rmbs_v00.loan_id,rmbs_v00.seller_name,
	rmbs_v00.collateral_date,rmbs_v00.servicer_abn,rmbs_v00.originator_abn,
	rmbs_v00.group_loan_id,rmbs_v00.most_recent_approval_amount,rmbs_v00.current_balance,
	rmbs_v00.scheduled_balance,rmbs_v00.loan_guarantee_flag,rmbs_v00.redraw_feature_flag,
	rmbs_v00.available_redraw,rmbs_v00.offset_account_flag,rmbs_v00.offset_account_balance,
	rmbs_v00.scheduled_payment_policy,rmbs_v00.conforming_mortgage,rmbs_v00.lmi_underwriting_type,
	rmbs_v00.origination_date_settlement_date,rmbs_v00.loan_purpose,rmbs_v00.loan_origination_channel_flag,
	rmbs_v00.loan_term,rmbs_v00.remaining_term,rmbs_v00.seasoning,rmbs_v00.maturity_date,
	rmbs_v00.loan_securitised_date,rmbs_v00.original_ltv,rmbs_v00.current_ltv,
	rmbs_v00.scheduled_ltv,rmbs_v00.loan_documentation_type,rmbs_v00.loan_type,
	rmbs_v00.scheduled_minimum_payment,rmbs_v00.payment_frequency,rmbs_v00.lmi_provider_name,
	rmbs_v00.lmi_attachment_point,rmbs_v00.loan_currency,rmbs_v00.variable_loan_amount,
	rmbs_v00.interest_rate_type,rmbs_v00.current_interest_rate,rmbs_v00.reference_index_value,
	rmbs_v00.interest_margin,rmbs_v00.interest_rate_reset_interval,rmbs_v00.restructuring_arrangement_flag,
	rmbs_v00.interest_only_expiry_date,rmbs_v00.current_rate_expiry_date,rmbs_v00.account_status,
	rmbs_v00.default_balance,rmbs_v00.foreclosure_proceeds,rmbs_v00.date_of_foreclosure,
	rmbs_v00.loss_on_sale,rmbs_v00.days_in_arrears,rmbs_v00.amount_in_arrears,
	rmbs_v00.days_since_last_scheduled_payment,rmbs_v00.cumulative_missed_payments,
	rmbs_v00.claim_submitted_to_lmi,rmbs_v00.claim_paid_by_lmi,rmbs_v00.claim_denied_by_lmi,
	rmbs_v00.claim_pending_with_lmi,rmbs_v00.property_postcode,rmbs_v00.property_country_name,
	rmbs_v00.original_property_value,rmbs_v00.most_recent_property_value,rmbs_v00.original_property_valuation_type,
	rmbs_v00.most_recent_property_valuation_type,rmbs_v00.most_recent_property_valuation_date,
	rmbs_v00.property_purpose,rmbs_v00.property_type,rmbs_v00.lien,rmbs_v00.first_home_buyer_flag,
	rmbs_v00.country_of_residence,rmbs_v00.borrower_residency_secondary_borrower,
	rmbs_v00.employment_type,rmbs_v00.employment_type_secondary_borrower,rmbs_v00.borrower_type_primary_borrower,
	rmbs_v00.borrower_type_secondary_borrower,rmbs_v00.legal_entity_type,rmbs_v00.legal_entity_type_secondary_borrower,
	rmbs_v00.bankruptcy_flag,rmbs_v00.last_credit_discharge_date,rmbs_v00.number_of_debtors,
	rmbs_v00.external_credit_score_provider_name,rmbs_v00.credit_score,rmbs_v00.credit_score_secondary_borrower,
	rmbs_v00.debt_serviceability_metric,rmbs_v00.debt_serviceability_metric_score,
	rmbs_v00.income,rmbs_v00.income_verification,rmbs_v00.secondary_income,
	rmbs_v00.income_verification_for_secondary_income,rmbs_v00.abs_statistical_area,
	rmbs_v00.arrears_methodology,rmbs_v00.main_security_methodology,rmbs_v00.primary_borrower_methodology,
	rmbs_v00.originator_id,rmbs_v00.seller_abn,rmbs_v00.seller_id,rmbs_v00.lmi_provider_abn,
	rmbs_v00.lmi_provider_id,rmbs_v00.servicer_id,rmbs_v00.savings_verification,
	rmbs_v00.interest_rate,rmbs_v00.origination_date,rmbs_v00.feed_name,NULL,NULL
	From INGEST.rmbs_v00 rmbs_v00
	;
	UPDATE LOAD.load_rmbs_v00 SET dss_record_source = Feed_Name,dss_load_datetime = GetDate() ;

	-- load_TRAN_v00
	truncate table LOAD.load_TRAN_v00;
	INSERT INTO LOAD.load_TRAN_v00
	(clientid, feedid, reporting_period, transaction_id, master_trust_name_or_spv,
	series_trust_name_or_series, report_date, data_location, responsible_party_name,
	responsible_party_abn, issuing_trustee_name, issuing_trustee_abn, indenture_or_security_trustee_name,
	indenture_or_security_trustee_abn, sponsor_name, sponsor_abn, arranger_name,
	arranger_abn, underwriter_name, underwriter_abn, manager_name, manager_abn,
	servicer_name, servicer_abn, liquidity_provider_name, liquidity_provider_abn,
	lmi_provider_name, lmi_provider_abn, paying_agent_name, paying_agent_abn,
	swap_or_hedge_provider_name, swap_or_hedge_provider_abn, swap_or_hedge_type,
	governing_law, trigger_name, revolving_period_close_date, trigger_description,
	pre_funding_close_date, asset_substitution_close_date, collection_period_frequency,
	remittance_frequency, senior_fees_and_expenses, credit_enhancement_name,
	account_provider_name, ending_balance, target_balance, initial_amount_available,
	beginning_amount_available_liquidity, ending_amount_available_liquidity,
	notional_principal_amount, collection_period_begin_date, collection_period_end_date,
	loans_insured, timely_payment_claims_outstanding, responsible_party_jurisdiction,
	responsible_party_id, responsible_party_id_descriptor, issuing_trustee_jurisdiction,
	issuing_trustee_id, issuing_trustee_id_descriptor, issuing_trustee_fee_currency,
	issuing_trustee_fee, indenture_or_security_trustee_jurisdiction, indenture_or_security_trustee_id,
	indenture_or_security_trustee_id_descriptor, indenture_or_security_trustee_fee_currency,
	indenture_or_security_trustee_fee, sponsor_jurisdiction, sponsor_id, sponsor_id_descriptor,
	arranger_jurisdiction, arranger_id, arranger_id_descriptor, underwriter_jurisdiction,
	underwriter_id, underwriter_id_descriptor, manager_jurisdiction, manager_id,
	manager_id_descriptor, manager_fee_currency, manager_fee, paying_agent_jurisdiction,
	paying_agent_id, paying_agent_id_descriptor, paying_agent_fee_currency,
	paying_agent_fee, originator_name, originator_abn, originator_jurisdiction,
	originator_id, originator_id_descriptor, loans_originated_settlement_date,
	loans_originated, seller_name, seller_abn, seller_jurisdiction, seller_id,
	seller_id_descriptor, loans_sold_settlement_date, loans_sold, servicer_jurisdiction,
	servicing_fee_currency, servicing_fee, liquidity_provider_jurisdiction,
	liquidity_provider_id, liquidity_provider_id_descriptor, liquidity_facility_name,
	liquidity_facility_type, reimbursements, draws_liquidity, reduction_liquidity,
	beginning_amount_drawn_liquidity, ending_amount_drawn_liquidity, facility_currency,
	facility_fee, credit_enhancement_provider_name, credit_enhancement_provider_abn,
	credit_enhancement_provider_jurisdiction, credit_enhancement_provider_id,
	credit_enhancement_provider_id_descriptor, credit_enhancement_type, credit_enhancement_currency,
	credit_enhancement_fee, initial_credit_enhancement_amount, beginning_amount_available_credit,
	restorations_credit, draws_credit, reduction_credit, ending_amount_available_credit,
	beginning_amount_drawn_credit, ending_amount_drawn_credit, lmi_provider_jurisdiction,
	lmi_provider_id, lmi_provider_id_descriptor, loan_currency_insurance, loans_insured_settlement_date,
	loans_insured_tpc_settlement_date, loans_insured_tpc, lmi_claims_outstanding,
	swap_or_hedge_provider_jurisdiction, swap_or_hedge_provider_id, swap_or_hedge_provider_id_descriptor,
	swap_or_hedge_name, pay_leg_currency, pay_leg_reference_index, pay_leg_reference_index_value,
	pay_leg_margin_or_rate, pay_leg_amount, receive_leg_currency, receive_leg_reference_index,
	receive_leg_reference_index_value, receive_leg_margin_or_rate, receive_leg_amount,
	exchange_rate, swap_or_hedge_fee_currency, swap_or_hedge_fee, trigger_state,
	account_provider_abn, account_provider_jurisdiction, account_provider_id,
	account_provider_id_descriptor, account_currency, account_fee, beginning_balance,
	signature_authority_name, signature_authority_abn, signature_authority_jurisdiction,
	signature_authority_id, signature_authority_id_descriptor, authority_type,
	authority_holder_name, authority_holder_abn, authority_holder_jurisdiction,
	authority_holder_id, authority_holder_id_descriptor, eligible_loan_criteria,
	next_report_date, trust_settlement_date, servicing_role, account_type,
	role_name, account_name, account_signature_authority_name, servicer_id,
	servicer_id_descriptor, other_role_name, other_role_abn, other_role_jurisdiction,
	other_role_id, other_role_id_descriptor, other_role_fee_currency, other_role_fee,
	arranger_fee_currency, arranger_fee, underwriter_fee_currency, underwriter_fee,
	responsible_party_fee_currency, responsible_party_fee, feed_name, dss_record_source,
	dss_load_datetime)
	SELECT
	TRAN_v00.ClientID,TRAN_v00.FeedID,TRAN_v00.Reporting_Period,
	TRAN_v00.Transaction_ID,TRAN_v00.Master_Trust_Name_or_SPV,TRAN_v00.Series_Trust_Name_or_Series,
	TRAN_v00.Report_Date,TRAN_v00.Data_Location,TRAN_v00.Responsible_Party_Name,
	TRAN_v00.Responsible_Party_ABN,TRAN_v00.Issuing_Trustee_Name,TRAN_v00.Issuing_Trustee_ABN,
	TRAN_v00.Indenture_or_Security_Trustee_Name,TRAN_v00.Indenture_or_Security_Trustee_ABN,
	TRAN_v00.Sponsor_Name,TRAN_v00.Sponsor_ABN,TRAN_v00.Arranger_Name,
	TRAN_v00.Arranger_ABN,TRAN_v00.Underwriter_Name,TRAN_v00.Underwriter_ABN,
	TRAN_v00.Manager_Name,TRAN_v00.Manager_ABN,TRAN_v00.Servicer_Name,
	TRAN_v00.Servicer_ABN,TRAN_v00.Liquidity_Provider_Name,TRAN_v00.Liquidity_Provider_ABN,
	TRAN_v00.LMI_Provider_Name,TRAN_v00.LMI_Provider_ABN,TRAN_v00.Paying_Agent_Name,
	TRAN_v00.Paying_Agent_ABN,TRAN_v00.Swap_or_Hedge_Provider_Name,
	TRAN_v00.Swap_or_Hedge_Provider_ABN,TRAN_v00.Swap_or_Hedge_Type,
	TRAN_v00.Governing_Law,TRAN_v00.Trigger_Name,TRAN_v00.Revolving_Period_Close_Date,
	TRAN_v00.Trigger_Description,TRAN_v00.Pre_Funding_Close_Date,TRAN_v00.Asset_Substitution_Close_Date,
	TRAN_v00.Collection_Period_Frequency,TRAN_v00.Remittance_Frequency,
	TRAN_v00.Senior_Fees_and_Expenses,TRAN_v00.Credit_Enhancement_Name,
	TRAN_v00.Account_Provider_Name,TRAN_v00.Ending_Balance,TRAN_v00.Target_Balance,
	TRAN_v00.Initial_Amount_Available,TRAN_v00.Beginning_Amount_Available_Liquidity,
	TRAN_v00.Ending_Amount_Available_Liquidity,TRAN_v00.Notional_Principal_Amount,
	TRAN_v00.Collection_Period_Begin_Date,TRAN_v00.Collection_Period_End_Date,
	TRAN_v00.Loans_Insured,TRAN_v00.Timely_Payment_Claims_Outstanding,
	TRAN_v00.Responsible_Party_Jurisdiction,TRAN_v00.Responsible_Party_ID,
	TRAN_v00.Responsible_Party_ID_Descriptor,TRAN_v00.Issuing_Trustee_Jurisdiction,
	TRAN_v00.Issuing_Trustee_ID,TRAN_v00.Issuing_Trustee_ID_Descriptor,
	TRAN_v00.Issuing_Trustee_Fee_Currency,TRAN_v00.Issuing_Trustee_Fee,
	TRAN_v00.Indenture_or_Security_Trustee_Jurisdiction,TRAN_v00.Indenture_or_Security_Trustee_ID,
	TRAN_v00.Indenture_or_Security_Trustee_ID_Descriptor,TRAN_v00.Indenture_or_Security_Trustee_Fee_Currency,
	TRAN_v00.Indenture_or_Security_Trustee_Fee,TRAN_v00.Sponsor_Jurisdiction,
	TRAN_v00.Sponsor_ID,TRAN_v00.Sponsor_ID_Descriptor,TRAN_v00.Arranger_Jurisdiction,
	TRAN_v00.Arranger_ID,TRAN_v00.Arranger_ID_Descriptor,TRAN_v00.Underwriter_Jurisdiction,
	TRAN_v00.Underwriter_ID,TRAN_v00.Underwriter_ID_Descriptor,TRAN_v00.Manager_Jurisdiction,
	TRAN_v00.Manager_ID,TRAN_v00.Manager_ID_Descriptor,TRAN_v00.Manager_Fee_Currency,
	TRAN_v00.Manager_Fee,TRAN_v00.Paying_Agent_Jurisdiction,TRAN_v00.Paying_Agent_ID,
	TRAN_v00.Paying_Agent_ID_Descriptor,TRAN_v00.Paying_Agent_Fee_Currency,
	TRAN_v00.Paying_Agent_Fee,TRAN_v00.Originator_Name,TRAN_v00.Originator_ABN,
	TRAN_v00.Originator_Jurisdiction,TRAN_v00.Originator_ID,TRAN_v00.Originator_ID_Descriptor,
	TRAN_v00.Loans_Originated_Settlement_Date,TRAN_v00.Loans_Originated,
	TRAN_v00.Seller_Name,TRAN_v00.Seller_ABN,TRAN_v00.Seller_Jurisdiction,
	TRAN_v00.Seller_ID,TRAN_v00.Seller_ID_Descriptor,TRAN_v00.Loans_Sold_Settlement_Date,
	TRAN_v00.Loans_Sold,TRAN_v00.Servicer_Jurisdiction,TRAN_v00.Servicing_Fee_Currency,
	TRAN_v00.Servicing_Fee,TRAN_v00.Liquidity_Provider_Jurisdiction,
	TRAN_v00.Liquidity_Provider_ID,TRAN_v00.Liquidity_Provider_ID_Descriptor,
	TRAN_v00.Liquidity_Facility_Name,TRAN_v00.Liquidity_Facility_Type,
	TRAN_v00.Reimbursements,TRAN_v00.Draws_Liquidity,TRAN_v00.Reduction_Liquidity,
	TRAN_v00.Beginning_Amount_Drawn_Liquidity,TRAN_v00.Ending_Amount_Drawn_Liquidity,
	TRAN_v00.Facility_Currency,TRAN_v00.Facility_Fee,TRAN_v00.Credit_Enhancement_Provider_Name,
	TRAN_v00.Credit_Enhancement_Provider_ABN,TRAN_v00.Credit_Enhancement_Provider_Jurisdiction,
	TRAN_v00.Credit_Enhancement_Provider_ID,TRAN_v00.Credit_Enhancement_Provider_ID_Descriptor,
	TRAN_v00.Credit_Enhancement_Type,TRAN_v00.Credit_Enhancement_Currency,
	TRAN_v00.Credit_Enhancement_Fee,TRAN_v00.Initial_Credit_Enhancement_Amount,
	TRAN_v00.Beginning_Amount_Available_Credit,TRAN_v00.Restorations_Credit,
	TRAN_v00.Draws_Credit,TRAN_v00.Reduction_Credit,TRAN_v00.Ending_Amount_Available_Credit,
	TRAN_v00.Beginning_Amount_Drawn_Credit,TRAN_v00.Ending_Amount_Drawn_Credit,
	TRAN_v00.LMI_Provider_Jurisdiction,TRAN_v00.LMI_Provider_ID,TRAN_v00.LMI_Provider_ID_Descriptor,
	TRAN_v00.Loan_Currency_Insurance,TRAN_v00.Loans_Insured_Settlement_Date,
	TRAN_v00.Loans_Insured_TPC_Settlement_Date,TRAN_v00.Loans_Insured_TPC,
	TRAN_v00.LMI_Claims_Outstanding,TRAN_v00.Swap_or_Hedge_Provider_Jurisdiction,
	TRAN_v00.Swap_or_Hedge_Provider_ID,TRAN_v00.Swap_or_Hedge_Provider_ID_Descriptor,
	TRAN_v00.Swap_or_Hedge_Name,TRAN_v00.Pay_Leg_Currency,TRAN_v00.Pay_Leg_Reference_Index,
	TRAN_v00.Pay_Leg_Reference_Index_Value,TRAN_v00.Pay_Leg_Margin_or_Rate,
	TRAN_v00.Pay_Leg_Amount,TRAN_v00.Receive_Leg_Currency,TRAN_v00.Receive_Leg_Reference_Index,
	TRAN_v00.Receive_Leg_Reference_Index_Value,TRAN_v00.Receive_Leg_Margin_or_Rate,
	TRAN_v00.Receive_Leg_Amount,TRAN_v00.Exchange_Rate,TRAN_v00.Swap_or_Hedge_Fee_Currency,
	TRAN_v00.Swap_or_Hedge_Fee,TRAN_v00.Trigger_State,TRAN_v00.Account_Provider_ABN,
	TRAN_v00.Account_Provider_Jurisdiction,TRAN_v00.Account_Provider_ID,
	TRAN_v00.Account_Provider_ID_Descriptor,TRAN_v00.Account_Currency,
	TRAN_v00.Account_Fee,TRAN_v00.Beginning_Balance,TRAN_v00.Signature_Authority_Name,
	TRAN_v00.Signature_Authority_ABN,TRAN_v00.Signature_Authority_Jurisdiction,
	TRAN_v00.Signature_Authority_ID,TRAN_v00.Signature_Authority_ID_Descriptor,
	TRAN_v00.Authority_Type,TRAN_v00.Authority_Holder_Name,TRAN_v00.Authority_Holder_ABN,
	TRAN_v00.Authority_Holder_Jurisdiction,TRAN_v00.Authority_Holder_ID,
	TRAN_v00.Authority_Holder_ID_Descriptor,TRAN_v00.Eligible_Loan_Criteria,
	TRAN_v00.Next_Report_Date,TRAN_v00.Trust_Settlement_Date,TRAN_v00.Servicing_Role,
	TRAN_v00.Account_Type,TRAN_v00.Role_Name,TRAN_v00.Account_Name,
	TRAN_v00.Account_Signature_Authority_Name,TRAN_v00.Servicer_ID,
	TRAN_v00.Servicer_ID_Descriptor,TRAN_v00.Other_Role_Name,TRAN_v00.Other_Role_ABN,
	TRAN_v00.Other_Role_Jurisdiction,TRAN_v00.Other_Role_ID,TRAN_v00.Other_Role_ID_Descriptor,
	TRAN_v00.Other_Role_Fee_Currency,TRAN_v00.Other_Role_Fee,TRAN_v00.Arranger_Fee_Currency,
	TRAN_v00.Arranger_Fee,TRAN_v00.Underwriter_Fee_Currency,TRAN_v00.Underwriter_Fee,
	TRAN_v00.Responsible_Party_Fee_Currency,TRAN_v00.Responsible_Party_Fee,
	TRAN_v00.Feed_Name,NULL,NULL
	From INGEST.TRAN_v00 TRAN_v00
	;
	UPDATE LOAD.load_TRAN_v00 SET dss_record_source = Feed_Name,dss_load_datetime = GetDate() ;


	-- load_SECU_v00 -----------------------------------------------------------------------------
	truncate table LOAD.load_SECU_v00;
	INSERT INTO LOAD.load_SECU_v00
	(clientid, reporting_period, transaction_id, master_trust_name_or_spv, series_trust_name_or_series,
	tranche_name, isin, report_date, austraclear_series_id, clearing_system_name,
	issue_date, accrual_begin_date, accrual_end_date, accrual_period, tranche_currency,
	exchange_name, name, original_rating, current_rating, original_subordination,
	current_subordination, current_principal_face_amount, offer_type, coupon_payment_reference,
	distribution_date, record_date, determination_date, beginning_stated_factor,
	ending_stated_factor, original_principal_face_amount, beginning_invested_amount,
	ending_invested_amount, beginning_stated_amount, ending_stated_amount,
	beginning_invested_factor, ending_invested_factor, coupon_reference_index,
	coupon_reference_index_value, coupon_margin, step_up_margin, step_up_date,
	scheduled_coupon, scheduled_coupon_factor, current_coupon_rate, coupon_distribution,
	coupon_distribution_factor, scheduled_principal, scheduled_principal_factor,
	principal_distribution, principal_distribution_factor, coupon_shortfall_factor,
	ending_cumulative_coupon_shortfall_factor, coupon_shortfall, ending_cumulative_coupon_shortfall,
	principal_shortfall, ending_cumulative_principal_shortfall, legal_maturity_date,
	original_estimated_weighted_average_life, current_weighted_average_life,
	expected_final_maturity_date, redemption_price, abn, jurisdiction, id,
	id_descriptor, governing_law, soft_bullet_date, coupon_basis, step_up_rate,
	beginning_cumulative_chargeoffs, beginning_cumulative_coupon_shortfall,
	beginning_cumulative_principal_shortfall, coupon_shortfall_makeup, principal_shortfall_makeup,
	allocated_chargeoffs, chargeoff_restorations, ending_cumulative_chargeoffs,
	beginning_cumulative_chargeoff_factor, beginning_cumulative_coupon_shortfall_factor,
	beginning_cumulative_principal_shortfall_factor, coupon_shortfall_makeup_factor,
	principal_shortfall_makeup_factor, principal_shortfall_factor, allocated_chargeoff_factor,
	chargeoff_restoration_factor, ending_cumulative_chargeoff_factor, ending_cumulative_principal_shortfall_factor,
	coupon_rate, dss_record_source, dss_load_datetime)
	SELECT
	SECU_v00.ClientID,SECU_v00.Reporting_Period,SECU_v00.Transaction_Id,
	SECU_v00.Master_Trust_Name_Or_Spv,SECU_v00.Series_Trust_Name_Or_Series,
	SECU_v00.Tranche_Name,SECU_v00.Isin,SECU_v00.Report_Date,SECU_v00.Austraclear_Series_Id,
	SECU_v00.Clearing_System_Name,SECU_v00.Issue_Date,SECU_v00.Accrual_Begin_Date,
	SECU_v00.Accrual_End_Date,SECU_v00.Accrual_Period,SECU_v00.Tranche_Currency,
	SECU_v00.Exchange_Name,SECU_v00.Name,SECU_v00.Original_Rating,
	SECU_v00.Current_Rating,SECU_v00.Original_Subordination,SECU_v00.Current_Subordination,
	SECU_v00.Current_Principal_Face_Amount,SECU_v00.Offer_Type,SECU_v00.Coupon_Payment_Reference,
	SECU_v00.Distribution_Date,SECU_v00.Record_Date,SECU_v00.Determination_Date,
	SECU_v00.Beginning_Stated_Factor,SECU_v00.Ending_Stated_Factor,
	SECU_v00.Original_Principal_Face_Amount,SECU_v00.Beginning_Invested_Amount,
	SECU_v00.Ending_Invested_Amount,SECU_v00.Beginning_Stated_Amount,
	SECU_v00.Ending_Stated_Amount,SECU_v00.Beginning_Invested_Factor,
	SECU_v00.Ending_Invested_Factor,SECU_v00.Coupon_Reference_Index,
	SECU_v00.Coupon_Reference_Index_Value,SECU_v00.Coupon_Margin,SECU_v00.Step_Up_Margin,
	SECU_v00.Step_Up_Date,SECU_v00.Scheduled_Coupon,SECU_v00.Scheduled_Coupon_Factor,
	SECU_v00.Current_Coupon_Rate,SECU_v00.Coupon_Distribution,SECU_v00.Coupon_Distribution_Factor,
	SECU_v00.Scheduled_Principal,SECU_v00.Scheduled_Principal_Factor,
	SECU_v00.Principal_Distribution,SECU_v00.Principal_Distribution_Factor,
	SECU_v00.Coupon_Shortfall_Factor,SECU_v00.Ending_Cumulative_Coupon_Shortfall_Factor,
	SECU_v00.Coupon_Shortfall,SECU_v00.Ending_Cumulative_Coupon_Shortfall,
	SECU_v00.Principal_Shortfall,SECU_v00.Ending_Cumulative_Principal_Shortfall,
	SECU_v00.Legal_Maturity_Date,SECU_v00.Original_Estimated_Weighted_Average_Life,
	SECU_v00.Current_Weighted_Average_Life,SECU_v00.Expected_Final_Maturity_Date,
	SECU_v00.Redemption_Price,SECU_v00.ABN,SECU_v00.Jurisdiction,
	SECU_v00.Id,SECU_v00.Id_Descriptor,SECU_v00.Governing_Law,SECU_v00.Soft_Bullet_Date,
	SECU_v00.Coupon_Basis,SECU_v00.Step_Up_Rate,SECU_v00.Beginning_Cumulative_Chargeoffs,
	SECU_v00.Beginning_Cumulative_Coupon_Shortfall,SECU_v00.Beginning_Cumulative_Principal_Shortfall,
	SECU_v00.Coupon_Shortfall_Makeup,SECU_v00.Principal_Shortfall_Makeup,
	SECU_v00.Allocated_Chargeoffs,SECU_v00.Chargeoff_Restorations,SECU_v00.Ending_Cumulative_Chargeoffs,
	SECU_v00.Beginning_Cumulative_Chargeoff_Factor,SECU_v00.Beginning_Cumulative_Coupon_Shortfall_Factor,
	SECU_v00.Beginning_Cumulative_Principal_Shortfall_Factor,SECU_v00.Coupon_Shortfall_Makeup_Factor,
	SECU_v00.Principal_Shortfall_Makeup_Factor,SECU_v00.Principal_Shortfall_Factor,
	SECU_v00.Allocated_Chargeoff_Factor,SECU_v00.Chargeoff_Restoration_Factor,
	SECU_v00.Ending_Cumulative_Chargeoff_Factor,SECU_v00.Ending_Cumulative_Principal_Shortfall_Factor,
	SECU_v00.Coupon_Rate,"Feed_Name",GETDATE()
	From INGEST.SECU_v00 SECU_v00;

	-- load_SECU_exchange_name ----------------------------------------------------------------
	truncate table LOAD.load_SECU_exchange_name;
	INSERT INTO LOAD.load_SECU_exchange_name
	(clientid, reporting_period, transaction_id, master_trust_name_or_spv, series_trust_name_or_series,
	tranche_name, exchange_name, report_date, dss_record_source, dss_load_datetime)
	SELECT
	SECU_Exchange_Name.ClientID,SECU_Exchange_Name.Reporting_Period,
	SECU_Exchange_Name.Transaction_Id,SECU_Exchange_Name.Master_Trust_Name_Or_Spv,
	SECU_Exchange_Name.Series_Trust_Name_Or_Series,SECU_Exchange_Name.Tranche_Name,
	SECU_Exchange_Name.Exchange_Name,SECU_Exchange_Name.Report_Date,
	"Feed_Name",GETDATE()
	From INGEST.SECU_Exchange_Name SECU_Exchange_Name;


	-- load_SECU_clearing_system ---------------------------------------------------------------
	truncate table LOAD.load_SECU_clearing_system;
	INSERT INTO LOAD.load_SECU_clearing_system
	(clientid, reporting_period, transaction_id, master_trust_name_or_spv, series_trust_name_or_series,
	tranche_name, clearing_system_name, report_date, dss_record_source, dss_load_datetime)
	SELECT
	SECU_Clearing_System.ClientID,SECU_Clearing_System.Reporting_Period,
	SECU_Clearing_System.Transaction_Id,SECU_Clearing_System.Master_Trust_Name_Or_Spv,
	SECU_Clearing_System.Series_Trust_Name_Or_Series,SECU_Clearing_System.Tranche_Name,
	SECU_Clearing_System.Clearing_System_Name,SECU_Clearing_System.Report_Date,
	"Feed_Name",GETDATE()
	From INGEST.SECU_Clearing_System SECU_Clearing_System;


	-- load_SECU_ratings	----------------------------------------------------------------------
	truncate table LOAD.load_SECU_ratings;
	INSERT INTO LOAD.load_SECU_ratings
	(clientid, reporting_period, transaction_id, master_trust_name_or_spv, series_trust_name_or_series,
	tranche_name, name, report_date, original_rating, current_rating, abn,
	jurisdiction, id, id_descriptor, dss_record_source, dss_load_datetime)
	SELECT
	SECU_Ratings.ClientID,SECU_Ratings.Reporting_Period,SECU_Ratings.Transaction_Id,
	SECU_Ratings.Master_Trust_Name_Or_Spv,SECU_Ratings.Series_Trust_Name_Or_Series,
	SECU_Ratings.Tranche_Name,SECU_Ratings.Name,SECU_Ratings.Report_Date,
	SECU_Ratings.Original_Rating,SECU_Ratings.Current_Rating,SECU_Ratings.ABN,
	SECU_Ratings.Jurisdiction,SECU_Ratings.Id,SECU_Ratings.Id_Descriptor,
	"Feed_Name",GETDATE()
	From INGEST.SECU_Ratings SECU_Ratings;


	-- load_rmbs_col_v00 ----------------------------------------------------------------------
	truncate table LOAD.load_rmbs_col_v00;
	INSERT INTO LOAD.load_rmbs_col_v00
	(clientid, reporting_period, transaction_id, master_trust_name_or_spv, series_trust_name_or_series,
	loan_id, collateral_id, collateral_date, group_loan_id, security_property_postcode,
	security_property_country, original_security_property_value, most_recent_security_property_value,
	original_property_valuation_type, most_recent_property_valuation_type,
	most_recent_security_property_valuation_date, security_property_purpose,
	security_property_type, lien, abs_statistical_area, main_security_methodology,
	main_security_flag, property_valuation_currency, dss_record_source, dss_load_datetime)
	SELECT
	RMBS_COL_v00.ClientID,RMBS_COL_v00.Reporting_Period,RMBS_COL_v00.Transaction_Id,
	RMBS_COL_v00.Master_Trust_Name_Or_Spv,RMBS_COL_v00.Series_Trust_Name_Or_Series,
	RMBS_COL_v00.Loan_Id,RMBS_COL_v00.Collateral_Id,RMBS_COL_v00.Collateral_Date,
	RMBS_COL_v00.Group_Loan_Id,RMBS_COL_v00.Security_Property_Postcode,RMBS_COL_v00.Security_Property_Country,
	RMBS_COL_v00.Original_Security_Property_Value,RMBS_COL_v00.Most_Recent_Security_Property_Value,
	RMBS_COL_v00.Original_Property_Valuation_Type,RMBS_COL_v00.Most_Recent_Property_Valuation_Type,
	RMBS_COL_v00.Most_Recent_Security_Property_Valuation_Date,RMBS_COL_v00.Security_Property_Purpose,
	RMBS_COL_v00.Security_Property_Type,RMBS_COL_v00.Lien,RMBS_COL_v00.Abs_Statistical_Area,
	RMBS_COL_v00.Main_Security_Methodology,RMBS_COL_v00.Main_Security_Flag,
	RMBS_COL_v00.Property_Valuation_Currency,Feed_Name,GETDATE()
	From INGEST.RMBS_COL_v00 RMBS_COL_v00
	;
END;

IF @groupid=2 BEGIN

	--	load_TRAN_account
	truncate table LOAD.load_TRAN_account;
	INSERT INTO LOAD.load_TRAN_account
	(clientid, reporting_period, master_trust_name_or_spv, series_trust_name_or_series,
	transaction_id, account_provider_name, account_provider_jurisdiction, feedid,
	account_count, account_position, ending_balance, target_balance, account_provider_abn,
	account_provider_id, account_provider_id_descriptor, account_currency,
	account_fee, beginning_balance, account_type, account_name, dss_record_source,
	dss_load_datetime)
	SELECT
	TRAN_Account.ClientID,TRAN_Account.Reporting_Period,TRAN_Account.Master_Trust_Name_or_SPV,
	TRAN_Account.Series_Trust_Name_or_Series,TRAN_Account.Transaction_ID,
	TRAN_Account.Account_Provider_Name,TRAN_Account.Account_Provider_Jurisdiction,
	TRAN_Account.FeedID,TRAN_Account.Account_Count,TRAN_Account.Account_Position,
	TRAN_Account.Ending_Balance,TRAN_Account.Target_Balance,TRAN_Account.Account_Provider_ABN,
	TRAN_Account.Account_Provider_ID,TRAN_Account.Account_Provider_ID_Descriptor,
	TRAN_Account.Account_Currency,TRAN_Account.Account_Fee,TRAN_Account.Beginning_Balance,
	TRAN_Account.Account_Type,TRAN_Account.Account_Name,NULL,NULL
	From INGEST.TRAN_Account TRAN_Account;
	UPDATE LOAD.load_TRAN_account SET dss_record_source = 'Perpetual.INGEST.TRAN_Account',dss_load_datetime = GetDate() ;


	-- load_TRAN_arranger
	truncate table LOAD.load_TRAN_arranger;
	INSERT INTO LOAD.load_TRAN_arranger
	(clientid, feedid, reporting_period, master_trust_name_or_spv, series_trust_name_or_series,
	transaction_id, arranger_name, arranger_count, arranger_position, arranger_abn,
	arranger_jurisdiction, arranger_id, arranger_id_descriptor, arranger_fee_currency,
	arranger_fee, dss_record_source, dss_load_datetime)
	SELECT
	TRAN_Arranger.ClientID,TRAN_Arranger.FeedID,TRAN_Arranger.Reporting_Period,
	TRAN_Arranger.Master_Trust_Name_or_SPV,TRAN_Arranger.Series_Trust_Name_or_Series,
	TRAN_Arranger.Transaction_ID,TRAN_Arranger.Arranger_Name,TRAN_Arranger.Arranger_Count,
	TRAN_Arranger.Arranger_Position,TRAN_Arranger.Arranger_ABN,TRAN_Arranger.Arranger_Jurisdiction,
	TRAN_Arranger.Arranger_ID,TRAN_Arranger.Arranger_ID_Descriptor,
	TRAN_Arranger.Arranger_Fee_Currency,TRAN_Arranger.Arranger_Fee,NULL,NULL
	From INGEST.TRAN_Arranger TRAN_Arranger;
		
	UPDATE LOAD.load_TRAN_arranger SET dss_record_source = 'Perpetual.INGEST.TRAN_Arranger',dss_load_datetime = GetDate() ;


	-- load_TRAN_authority
	truncate table LOAD.load_TRAN_authority;
	INSERT INTO LOAD.load_TRAN_authority
	(clientid, feedid, reporting_period, master_trust_name_or_spv, series_trust_name_or_series,
	transaction_id, authority_holder_name, authority_count, authority_position,
	authority_type, authority_holder_abn, authority_holder_jurisdiction, authority_holder_id,
	authority_holder_id_descriptor, dss_record_source, dss_load_datetime)
	SELECT
	TRAN_Authority.ClientID,TRAN_Authority.FeedID,TRAN_Authority.Reporting_Period,
	TRAN_Authority.Master_Trust_Name_or_SPV,TRAN_Authority.Series_Trust_Name_or_Series,
	TRAN_Authority.Transaction_ID,TRAN_Authority.Authority_Holder_Name,
	TRAN_Authority.Authority_Count,TRAN_Authority.Authority_Position,
	TRAN_Authority.Authority_Type,TRAN_Authority.Authority_Holder_ABN,
	TRAN_Authority.Authority_Holder_Jurisdiction,TRAN_Authority.Authority_Holder_ID,
	TRAN_Authority.Authority_Holder_ID_Descriptor,NULL,NULL
	From INGEST.TRAN_Authority TRAN_Authority	;
		
	UPDATE LOAD.load_TRAN_authority SET dss_record_source = 'Perpetual.INGEST.TRAN_Authority',dss_load_datetime = GetDate() ;

	
	-- load_TRAN_credit_enhancement
	truncate table LOAD.load_TRAN_credit_enhancement;
	INSERT INTO LOAD.load_TRAN_credit_enhancement
	(clientid, reporting_period, master_trust_name_or_spv, series_trust_name_or_series,
	transaction_id, credit_enhancement_name, credit_enhancement_provider_abn,
	feedid, credit_enhancement_count, credit_enhancement_position, credit_enhancement_provider_name,
	credit_enhancement_provider_jurisdiction, credit_enhancement_provider_id,
	credit_enhancement_provider_id_descriptor, credit_enhancement_type, credit_enhancement_currency,
	credit_enhancement_fee, initial_credit_enhancement_amount, beginning_amount_available_credit,
	restorations_credit, draws_credit, reduction_credit, ending_amount_available_credit,
	beginning_amount_drawn_credit, ending_amount_drawn_credit, dss_record_source,
	dss_load_datetime)
	SELECT
	TRAN_Credit_Enhancement.ClientID,TRAN_Credit_Enhancement.Reporting_Period,
	TRAN_Credit_Enhancement.Master_Trust_Name_or_SPV,TRAN_Credit_Enhancement.Series_Trust_Name_or_Series,
	TRAN_Credit_Enhancement.Transaction_ID,TRAN_Credit_Enhancement.Credit_Enhancement_Name,
	TRAN_Credit_Enhancement.Credit_Enhancement_Provider_ABN,TRAN_Credit_Enhancement.FeedID,
	TRAN_Credit_Enhancement.Credit_Enhancement_Count,TRAN_Credit_Enhancement.Credit_Enhancement_Position,
	TRAN_Credit_Enhancement.Credit_Enhancement_Provider_Name,TRAN_Credit_Enhancement.Credit_Enhancement_Provider_Jurisdiction,
	TRAN_Credit_Enhancement.Credit_Enhancement_Provider_ID,TRAN_Credit_Enhancement.Credit_Enhancement_Provider_ID_Descriptor,
	TRAN_Credit_Enhancement.Credit_Enhancement_Type,TRAN_Credit_Enhancement.Credit_Enhancement_Currency,
	TRAN_Credit_Enhancement.Credit_Enhancement_Fee,TRAN_Credit_Enhancement.Initial_Credit_Enhancement_Amount,
	TRAN_Credit_Enhancement.Beginning_Amount_Available_Credit,TRAN_Credit_Enhancement.Restorations_Credit,
	TRAN_Credit_Enhancement.Draws_Credit,TRAN_Credit_Enhancement.Reduction_Credit,
	TRAN_Credit_Enhancement.Ending_Amount_Available_Credit,TRAN_Credit_Enhancement.Beginning_Amount_Drawn_Credit,
	TRAN_Credit_Enhancement.Ending_Amount_Drawn_Credit,NULL,NULL
	From INGEST.TRAN_Credit_Enhancement TRAN_Credit_Enhancement	;
		
	UPDATE LOAD.load_TRAN_credit_enhancement SET dss_record_source = 'Perpetual.INGEST.TRAN_Credit_Enhancement',dss_load_datetime = GetDate() ;



	-- load_TRAN_identure_or_security_trustee
	truncate table LOAD.load_TRAN_identure_or_security_trustee;
	INSERT INTO LOAD.load_TRAN_identure_or_security_trustee
	(clientid, feedid, reporting_period, master_trust_name_or_spv, series_trust_name_or_series,
	transaction_id, indenture_or_security_trustee_name, identure_or_security_trustee_count,
	identure_or_security_trustee_position, indenture_or_security_trustee_abn,
	indenture_or_security_trustee_jurisdiction, indenture_or_security_trustee_id,
	indenture_or_security_trustee_id_descriptor, indenture_or_security_trustee_fee_currency,
	indenture_or_security_trustee_fee, dss_record_source, dss_load_datetime)
	SELECT
	TRAN_Identure_or_Security_Trustee.ClientID,TRAN_Identure_or_Security_Trustee.FeedID,
	TRAN_Identure_or_Security_Trustee.Reporting_Period,TRAN_Identure_or_Security_Trustee.Master_Trust_Name_or_SPV,
	TRAN_Identure_or_Security_Trustee.Series_Trust_Name_or_Series,TRAN_Identure_or_Security_Trustee.Transaction_ID,
	TRAN_Identure_or_Security_Trustee.Indenture_or_Security_Trustee_Name,
	TRAN_Identure_or_Security_Trustee.Identure_or_Security_Trustee_Count,
	TRAN_Identure_or_Security_Trustee.Identure_or_Security_Trustee_Position,
	TRAN_Identure_or_Security_Trustee.Indenture_or_Security_Trustee_ABN,
	TRAN_Identure_or_Security_Trustee.Indenture_or_Security_Trustee_Jurisdiction,
	TRAN_Identure_or_Security_Trustee.Indenture_or_Security_Trustee_ID,
	TRAN_Identure_or_Security_Trustee.Indenture_or_Security_Trustee_ID_Descriptor,
	TRAN_Identure_or_Security_Trustee.Indenture_or_Security_Trustee_Fee_Currency,
	TRAN_Identure_or_Security_Trustee.Indenture_or_Security_Trustee_Fee,NULL,NULL
	From INGEST.TRAN_Identure_or_Security_Trustee TRAN_Identure_or_Security_Trustee;

	UPDATE LOAD.load_TRAN_identure_or_security_trustee SET dss_record_source = 'Perpetual.INGEST.TRAN_Identure_or_Security_Trustee',dss_load_datetime = GetDate() ;
	
	-- load_TRAN_issuing_trustee
	truncate table LOAD.load_TRAN_issuing_trustee;
	INSERT INTO LOAD.load_TRAN_issuing_trustee
	(clientid, reporting_period, master_trust_name_or_spv, series_trust_name_or_series,
	transaction_id, issuing_trustee_name, issuing_trustee_abn, feedid, issuing_trustee_count,
	issuing_trustee_position, issuing_trustee_jurisdiction, issuing_trustee_id,
	issuing_trustee_id_descriptor, issuing_trustee_fee_currency, issuing_trustee_fee,
	dss_record_source, dss_load_datetime)
	SELECT
	TRAN_Issuing_Trustee.ClientID,TRAN_Issuing_Trustee.Reporting_Period,
	TRAN_Issuing_Trustee.Master_Trust_Name_or_SPV,TRAN_Issuing_Trustee.Series_Trust_Name_or_Series,
	TRAN_Issuing_Trustee.Transaction_ID,TRAN_Issuing_Trustee.Issuing_Trustee_Name,
	TRAN_Issuing_Trustee.Issuing_Trustee_ABN,TRAN_Issuing_Trustee.FeedID,
	TRAN_Issuing_Trustee.Issuing_Trustee_Count,TRAN_Issuing_Trustee.Issuing_Trustee_Position,
	TRAN_Issuing_Trustee.Issuing_Trustee_Jurisdiction,TRAN_Issuing_Trustee.Issuing_Trustee_ID,
	TRAN_Issuing_Trustee.Issuing_Trustee_ID_Descriptor,TRAN_Issuing_Trustee.Issuing_Trustee_Fee_Currency,
	TRAN_Issuing_Trustee.Issuing_Trustee_Fee,NULL,NULL
	From INGEST.TRAN_Issuing_Trustee TRAN_Issuing_Trustee;
		
	UPDATE LOAD.load_TRAN_issuing_trustee SET dss_record_source = 'Perpetual.INGEST.TRAN_Issuing_Trustee',dss_load_datetime = GetDate() ;

	
	-- load_TRAN_liquidity
	truncate table LOAD.load_TRAN_liquidity;
	INSERT INTO LOAD.load_TRAN_liquidity
	(clientid, feedid, reporting_period, master_trust_name_or_spv, series_trust_name_or_series,
	transaction_id, liquidity_provider_name, liquidity_count, liquidity_position,
	liquidity_provider_abn, initial_amount_available, beginning_amount_available_liquidity,
	ending_amount_available_liquidity, liquidity_provider_jurisdiction, liquidity_provider_id,
	liquidity_provider_id_descriptor, liquidity_facility_name, liquidity_facility_type,
	reimbursements, draws_liquidity, reduction_liquidity, beginning_amount_drawn_liquidity,
	ending_amount_drawn_liquidity, facility_currency, facility_fee, dss_record_source,
	dss_load_datetime)
	SELECT
	TRAN_Liquidity.ClientID,TRAN_Liquidity.FeedID,TRAN_Liquidity.Reporting_Period,
	TRAN_Liquidity.Master_Trust_Name_or_SPV,TRAN_Liquidity.Series_Trust_Name_or_Series,
	TRAN_Liquidity.Transaction_ID,TRAN_Liquidity.Liquidity_Provider_Name,
	TRAN_Liquidity.Liquidity_Count,TRAN_Liquidity.Liquidity_Position,
	TRAN_Liquidity.Liquidity_Provider_ABN,TRAN_Liquidity.Initial_Amount_Available,
	TRAN_Liquidity.Beginning_Amount_Available_Liquidity,TRAN_Liquidity.Ending_Amount_Available_Liquidity,
	TRAN_Liquidity.Liquidity_Provider_Jurisdiction,TRAN_Liquidity.Liquidity_Provider_ID,
	TRAN_Liquidity.Liquidity_Provider_ID_Descriptor,TRAN_Liquidity.Liquidity_Facility_Name,
	TRAN_Liquidity.Liquidity_Facility_Type,TRAN_Liquidity.Reimbursements,
	TRAN_Liquidity.Draws_Liquidity,TRAN_Liquidity.Reduction_Liquidity,
	TRAN_Liquidity.Beginning_Amount_Drawn_Liquidity,TRAN_Liquidity.Ending_Amount_Drawn_Liquidity,
	TRAN_Liquidity.Facility_Currency,TRAN_Liquidity.Facility_Fee,NULL,NULL
	From INGEST.TRAN_Liquidity TRAN_Liquidity;
	
	UPDATE LOAD.load_TRAN_liquidity SET dss_record_source = 'Perpetual.INGEST.TRAN_Liquidity',dss_load_datetime = GetDate() ;
	

	-- load_TRAN_lmi
	truncate table LOAD.load_TRAN_lmi;
	INSERT INTO LOAD.load_TRAN_lmi
	(clientid, feedid, reporting_period, master_trust_name_or_spv, series_trust_name_or_series,
	lmi_provider_name, transaction_id, lmi_count, lmi_position, lmi_provider_abn,
	loans_insured, timely_payment_claims_outstanding, lmi_provider_jurisdiction,
	lmi_provider_id, lmi_provider_id_descriptor, loan_currency_insurance, dss_record_source,
	dss_load_datetime)
	SELECT
	TRAN_LMI.ClientID,TRAN_LMI.FeedID,TRAN_LMI.Reporting_Period,
	TRAN_LMI.Master_Trust_Name_or_SPV,TRAN_LMI.Series_Trust_Name_or_Series,
	TRAN_LMI.LMI_Provider_Name,TRAN_LMI.Transaction_ID,TRAN_LMI.LMI_Count,
	TRAN_LMI.LMI_Position,TRAN_LMI.LMI_Provider_ABN,TRAN_LMI.Loans_Insured,
	TRAN_LMI.Timely_Payment_Claims_Outstanding,TRAN_LMI.LMI_Provider_Jurisdiction,
	TRAN_LMI.LMI_Provider_ID,TRAN_LMI.LMI_Provider_ID_Descriptor,TRAN_LMI.Loan_Currency_Insurance,
	NULL,NULL
	From INGEST.TRAN_LMI TRAN_LMI;
		
	UPDATE LOAD.load_TRAN_lmi SET dss_record_source = 'Perpetual.INGEST.TRAN_LMI',dss_load_datetime = GetDate() ;



	-- load_TRAN_manager
	truncate table LOAD.load_TRAN_manager;
	INSERT INTO LOAD.load_TRAN_manager
	(clientid, feedid, reporting_period, master_trust_name_or_spv, series_trust_name_or_series,
	transaction_id, manager_name, manager_count, manager_position, manager_abn,
	manager_jurisdiction, manager_id, manager_id_descriptor, manager_fee_currency,
	manager_fee, dss_record_source, dss_load_datetime)
	SELECT
	TRAN_Manager.ClientID,TRAN_Manager.FeedID,TRAN_Manager.Reporting_Period,
	TRAN_Manager.Master_Trust_Name_or_SPV,TRAN_Manager.Series_Trust_Name_or_Series,
	TRAN_Manager.Transaction_ID,TRAN_Manager.Manager_Name,TRAN_Manager.Manager_Count,
	TRAN_Manager.Manager_Position,TRAN_Manager.Manager_ABN,TRAN_Manager.Manager_Jurisdiction,
	TRAN_Manager.Manager_ID,TRAN_Manager.Manager_ID_Descriptor,TRAN_Manager.Manager_Fee_Currency,
	TRAN_Manager.Manager_Fee,NULL,NULL
	From INGEST.TRAN_Manager TRAN_Manager;
		
	UPDATE LOAD.load_TRAN_manager SET dss_record_source = 'Perpetual.INGEST.TRAN_Manager',dss_load_datetime = GetDate() ;


	-- load_TRAN_originator
	truncate table LOAD.load_TRAN_originator;
	INSERT INTO LOAD.load_TRAN_originator
	(clientid, feedid, reporting_period, master_trust_name_or_spv, series_trust_name_or_series,
	transaction_id, originator_name, originator_count, originator_position,
	originator_abn, originator_jurisdiction, originator_id, originator_id_descriptor,
	loans_originated_settlement_date, loans_originated, dss_record_source, dss_load_datetime)
	SELECT
	TRAN_Originator.ClientID,TRAN_Originator.FeedID,TRAN_Originator.Reporting_Period,
	TRAN_Originator.Master_Trust_Name_or_SPV,TRAN_Originator.Series_Trust_Name_or_Series,
	TRAN_Originator.Transaction_ID,TRAN_Originator.Originator_Name,
	TRAN_Originator.Originator_Count,TRAN_Originator.Originator_Position,
	TRAN_Originator.Originator_ABN,TRAN_Originator.Originator_Jurisdiction,
	TRAN_Originator.Originator_ID,TRAN_Originator.Originator_ID_Descriptor,
	TRAN_Originator.Loans_Originated_Settlement_Date,TRAN_Originator.Loans_Originated,
	NULL,NULL
	From INGEST.TRAN_Originator TRAN_Originator;

	UPDATE LOAD.load_TRAN_originator SET dss_record_source = 'Perpetual.INGEST.TRAN_Originator',dss_load_datetime = GetDate() ;

	
	-- load_TRAN_other_role
	truncate table LOAD.load_TRAN_other_role;
	INSERT INTO LOAD.load_TRAN_other_role
	(clientid, reporting_period, master_trust_name_or_spv, series_trust_name_or_series,
	transaction_id, other_role_position, other_role_name, feedid, other_role_count,
	other_role_abn, other_role_jurisdiction, other_role_id, other_role_id_descriptor,
	other_role_fee_currency, other_role_fee, dss_record_source, dss_load_datetime)
	SELECT
	TRAN_Other_Role.ClientID,TRAN_Other_Role.Reporting_Period,TRAN_Other_Role.Master_Trust_Name_or_SPV,
	TRAN_Other_Role.Series_Trust_Name_or_Series,TRAN_Other_Role.Transaction_ID,
	TRAN_Other_Role.Other_Role_Position,TRAN_Other_Role.Other_Role_Name,
	TRAN_Other_Role.FeedID,TRAN_Other_Role.Other_Role_Count,TRAN_Other_Role.Other_Role_ABN,
	TRAN_Other_Role.Other_Role_Jurisdiction,TRAN_Other_Role.Other_Role_ID,
	TRAN_Other_Role.Other_Role_ID_Descriptor,TRAN_Other_Role.Other_Role_Fee_Currency,
	TRAN_Other_Role.Other_Role_Fee,NULL,NULL
	From INGEST.TRAN_Other_Role TRAN_Other_Role;

	UPDATE LOAD.load_TRAN_other_role SET dss_record_source = 'Perpetual.INGEST.TRAN_Other_Role',dss_load_datetime = GetDate() ;

	
	-- load_TRAN_paying_agent
	truncate table LOAD.load_TRAN_paying_agent;
	INSERT INTO LOAD.load_TRAN_paying_agent
	(clientid, feedid, reporting_period, master_trust_name_or_spv, series_trust_name_or_series,
	transaction_id, paying_agent_name, paying_agent_count, paying_agent_position,
	paying_agent_abn, paying_agent_jurisdiction, paying_agent_id, paying_agent_id_descriptor,
	paying_agent_fee_currency, paying_agent_fee, dss_record_source, dss_load_datetime)
	SELECT
	TRAN_Paying_Agent.ClientID,TRAN_Paying_Agent.FeedID,TRAN_Paying_Agent.Reporting_Period,
	TRAN_Paying_Agent.Master_Trust_Name_or_SPV,TRAN_Paying_Agent.Series_Trust_Name_or_Series,
	TRAN_Paying_Agent.Transaction_ID,TRAN_Paying_Agent.Paying_Agent_Name,
	TRAN_Paying_Agent.Paying_Agent_Count,TRAN_Paying_Agent.Paying_Agent_Position,
	TRAN_Paying_Agent.Paying_Agent_ABN,TRAN_Paying_Agent.Paying_Agent_Jurisdiction,
	TRAN_Paying_Agent.Paying_Agent_ID,TRAN_Paying_Agent.Paying_Agent_ID_Descriptor,
	TRAN_Paying_Agent.Paying_Agent_Fee_Currency,TRAN_Paying_Agent.Paying_Agent_Fee,
	NULL,NULL
	From INGEST.TRAN_Paying_Agent TRAN_Paying_Agent;
		
	UPDATE LOAD.load_TRAN_paying_agent SET dss_record_source = 'Perpetual.INGEST.TRAN_Paying_Agent',dss_load_datetime = GetDate() ;

	
	-- load_TRAN_responsible_party
	truncate table LOAD.load_TRAN_responsible_party;
	INSERT INTO LOAD.load_TRAN_responsible_party
	(clientid, feedid, reporting_period, master_trust_name_or_spv, series_trust_name_or_series,
	transaction_id, responsible_party_name, responsible_party_count, responsible_party_position,
	responsible_party_abn, responsible_party_jurisdiction, responsible_party_id,
	responsible_party_id_descriptor, responsible_party_fee_currency, responsible_party_fee,
	dss_record_source, dss_load_datetime)
	SELECT
	TRAN_Responsible_Party.ClientID,TRAN_Responsible_Party.FeedID,TRAN_Responsible_Party.Reporting_Period,
	TRAN_Responsible_Party.Master_Trust_Name_or_SPV,TRAN_Responsible_Party.Series_Trust_Name_or_Series,
	TRAN_Responsible_Party.Transaction_ID,TRAN_Responsible_Party.Responsible_Party_Name,
	TRAN_Responsible_Party.Responsible_Party_Count,TRAN_Responsible_Party.Responsible_Party_Position,
	TRAN_Responsible_Party.Responsible_Party_ABN,TRAN_Responsible_Party.Responsible_Party_Jurisdiction,
	TRAN_Responsible_Party.Responsible_Party_ID,TRAN_Responsible_Party.Responsible_Party_ID_Descriptor,
	TRAN_Responsible_Party.Responsible_Party_Fee_Currency,TRAN_Responsible_Party.Responsible_Party_Fee,
	NULL,NULL
	From INGEST.TRAN_Responsible_Party TRAN_Responsible_Party;

	UPDATE LOAD.load_TRAN_responsible_party SET dss_record_source = 'Perpetual.INGEST.TRAN_Responsible_Party',dss_load_datetime = GetDate() ;

	
	-- load_TRAN_seller
	truncate table LOAD.load_TRAN_seller;
	INSERT INTO LOAD.load_TRAN_seller
	(clientid, feedid, reporting_period, master_trust_name_or_spv, series_trust_name_or_series,
	transaction_id, seller_name, seller_count, seller_position, seller_abn,
	seller_jurisdiction, seller_id, seller_id_descriptor, dss_record_source,
	dss_load_datetime)
	SELECT
	TRAN_Seller.ClientID,TRAN_Seller.FeedID,TRAN_Seller.Reporting_Period,
	TRAN_Seller.Master_Trust_Name_or_SPV,TRAN_Seller.Series_Trust_Name_or_Series,
	TRAN_Seller.Transaction_ID,TRAN_Seller.Seller_Name,TRAN_Seller.Seller_Count,
	TRAN_Seller.Seller_Position,TRAN_Seller.Seller_ABN,TRAN_Seller.Seller_Jurisdiction,
	TRAN_Seller.Seller_ID,TRAN_Seller.Seller_ID_Descriptor,NULL,NULL
	From INGEST.TRAN_Seller TRAN_Seller;

	UPDATE LOAD.load_TRAN_seller SET dss_record_source = 'Perpetual.INGEST.TRAN_Seller',dss_load_datetime = GetDate() ;

	
	-- load_TRAN_servicer
	truncate table LOAD.load_TRAN_servicer;
	INSERT INTO LOAD.load_TRAN_servicer
	(clientid, reporting_period, master_trust_name_or_spv, series_trust_name_or_series,
	transaction_id, servicer_name, feedid, servicer_count, servicer_position,
	servicer_abn, servicer_jurisdiction, servicing_fee_currency, servicing_fee,
	servicing_role, servicer_id, servicer_id_descriptor, dss_record_source,
	dss_load_datetime)
	SELECT
	TRAN_Servicer.ClientID,TRAN_Servicer.Reporting_Period,TRAN_Servicer.Master_Trust_Name_or_SPV,
	TRAN_Servicer.Series_Trust_Name_or_Series,TRAN_Servicer.Transaction_ID,
	TRAN_Servicer.Servicer_Name,TRAN_Servicer.FeedID,TRAN_Servicer.Servicer_Count,
	TRAN_Servicer.Servicer_Position,TRAN_Servicer.Servicer_ABN,TRAN_Servicer.Servicer_Jurisdiction,
	TRAN_Servicer.Servicing_Fee_Currency,TRAN_Servicer.Servicing_Fee,
	TRAN_Servicer.Servicing_Role,TRAN_Servicer.Servicer_ID,TRAN_Servicer.Servicer_ID_Descriptor,
	NULL,NULL
	From INGEST.TRAN_Servicer TRAN_Servicer;
		
	UPDATE LOAD.load_TRAN_servicer SET dss_record_source = 'Perpetual.INGEST.TRAN_Servicer',dss_load_datetime = GetDate() ;


	-- load_TRAN_swap_or_hedge
	truncate table LOAD.load_TRAN_swap_or_hedge;
	INSERT INTO LOAD.load_TRAN_swap_or_hedge
	(clientid, feedid, reporting_period, master_trust_name_or_spv, series_trust_name_or_series,
	transaction_id, swap_or_hedge_provider_name, swap_or_hedge_count, swap_or_hedge_position,
	swap_or_hedge_provider_abn, swap_or_hedge_type, notional_principal_amount,
	swap_or_hedge_provider_jurisdiction, swap_or_hedge_provider_id, swap_or_hedge_provider_id_descriptor,
	swap_or_hedge_name, pay_leg_currency, pay_leg_reference_index, pay_leg_reference_index_value,
	pay_leg_margin_or_rate, pay_leg_amount, receive_leg_currency, receive_leg_reference_index,
	receive_leg_reference_index_value, receive_leg_margin_or_rate, receive_leg_amount,
	exchange_rate, swap_or_hedge_fee_currency, swap_or_hedge_fee, dss_record_source,
	dss_load_datetime)
	SELECT
	TRAN_Swap_Or_Hedge.ClientID,TRAN_Swap_Or_Hedge.FeedID,TRAN_Swap_Or_Hedge.Reporting_Period,
	TRAN_Swap_Or_Hedge.Master_Trust_Name_or_SPV,TRAN_Swap_Or_Hedge.Series_Trust_Name_or_Series,
	TRAN_Swap_Or_Hedge.Transaction_ID,TRAN_Swap_Or_Hedge.Swap_or_Hedge_Provider_Name,
	TRAN_Swap_Or_Hedge.Swap_Or_Hedge_Count,TRAN_Swap_Or_Hedge.Swap_Or_Hedge_Position,
	TRAN_Swap_Or_Hedge.Swap_or_Hedge_Provider_ABN,TRAN_Swap_Or_Hedge.Swap_or_Hedge_Type,
	TRAN_Swap_Or_Hedge.Notional_Principal_Amount,TRAN_Swap_Or_Hedge.Swap_or_Hedge_Provider_Jurisdiction,
	TRAN_Swap_Or_Hedge.Swap_or_Hedge_Provider_ID,TRAN_Swap_Or_Hedge.Swap_or_Hedge_Provider_ID_Descriptor,
	TRAN_Swap_Or_Hedge.Swap_or_Hedge_Name,TRAN_Swap_Or_Hedge.Pay_Leg_Currency,
	TRAN_Swap_Or_Hedge.Pay_Leg_Reference_Index,TRAN_Swap_Or_Hedge.Pay_Leg_Reference_Index_Value,
	TRAN_Swap_Or_Hedge.Pay_Leg_Margin_or_Rate,TRAN_Swap_Or_Hedge.Pay_Leg_Amount,
	TRAN_Swap_Or_Hedge.Receive_Leg_Currency,TRAN_Swap_Or_Hedge.Receive_Leg_Reference_Index,
	TRAN_Swap_Or_Hedge.Receive_Leg_Reference_Index_Value,TRAN_Swap_Or_Hedge.Receive_Leg_Margin_or_Rate,
	TRAN_Swap_Or_Hedge.Receive_Leg_Amount,TRAN_Swap_Or_Hedge.Exchange_Rate,
	TRAN_Swap_Or_Hedge.Swap_or_Hedge_Fee_Currency,TRAN_Swap_Or_Hedge.Swap_or_Hedge_Fee,
	NULL,NULL
	From INGEST.TRAN_Swap_Or_Hedge TRAN_Swap_Or_Hedge;

	UPDATE LOAD.load_TRAN_swap_or_hedge SET dss_record_source = 'Perpetual.INGEST.TRAN_Swap_Or_Hedge',dss_load_datetime = GetDate() ;
	

	-- load_TRAN_trigger
	truncate table LOAD.load_TRAN_trigger;
	INSERT INTO LOAD.load_TRAN_trigger
	(clientid, reporting_period, master_trust_name_or_spv, series_trust_name_or_series,
	transaction_id, trigger_position, trigger_name, feedid, trigger_count,
	trigger_description, trigger_state, dss_record_source, dss_load_datetime)
	SELECT
	TRAN_Trigger.ClientID,TRAN_Trigger.Reporting_Period,TRAN_Trigger.Master_Trust_Name_or_SPV,
	TRAN_Trigger.Series_Trust_Name_or_Series,TRAN_Trigger.Transaction_ID,
	TRAN_Trigger.Trigger_Position,TRAN_Trigger.Trigger_Name,TRAN_Trigger.FeedID,
	TRAN_Trigger.Trigger_Count,TRAN_Trigger.Trigger_Description,TRAN_Trigger.Trigger_State,
	NULL,NULL
	From INGEST.TRAN_Trigger TRAN_Trigger;
	UPDATE LOAD.load_TRAN_trigger SET dss_record_source = 'Perpetual.INGEST.TRAN_Trigger',dss_load_datetime = GetDate() ;

END;

IF @groupid=3 BEGIN

	-- load_rmbs_col_v00_dataconversion
	truncate table LOAD.load_rmbs_col_v00_dataconversion;
	INSERT INTO LOAD.load_rmbs_col_v00_dataconversion
	(hk_l_rmbs_col_v00, security_property_postcode, original_security_property_value,
	most_recent_security_property_value, most_recent_security_property_valuation_date,
	abs_statistical_area, main_security_flag, dss_record_source, dss_load_datetime)
	SELECT
	RMBS_COL_v00_DataConversion.hk_l_rmbs_col_v00,RMBS_COL_v00_DataConversion.Security_Property_Postcode,
	RMBS_COL_v00_DataConversion.Original_Security_Property_Value,RMBS_COL_v00_DataConversion.Most_Recent_Security_Property_Value,
	RMBS_COL_v00_DataConversion.Most_Recent_Security_Property_Valuation_Date,
	RMBS_COL_v00_DataConversion.Abs_Statistical_Area,RMBS_COL_v00_DataConversion.Main_Security_Flag,
	'S_RMBS_COL_v00',GetDate()
	From INGEST.RMBS_COL_v00_DataConversion RMBS_COL_v00_DataConversion
	;

	-- load_rmbs_v00_calculations
	truncate table LOAD.load_rmbs_v00_calculations;
	INSERT INTO LOAD.load_rmbs_v00_calculations
	(hk_l_rmbs_v00, offset_account_flag, conforming_mortgage_flag, restructuring_arrangement_flag,
	first_home_buyer_flag, bankruptcy_flag, interest_only_expiry_term, fixed_rate_expiry_term,
	indexed_lvr, data_issuer_code, data_issuer_name, data_issuer_abn, arrears_category,
	date_key, eomonth_collateral_date, originationyear, property_region, property_state,
	dss_record_source, dss_load_datetime)
	SELECT
	RMBS_v00_Calculations.hk_l_rmbs_v00,RMBS_v00_Calculations.Offset_Account_Flag,
	RMBS_v00_Calculations.Conforming_Mortgage_Flag,RMBS_v00_Calculations.Restructuring_Arrangement_Flag,
	RMBS_v00_Calculations.First_Home_Buyer_Flag,RMBS_v00_Calculations.Bankruptcy_Flag,
	RMBS_v00_Calculations.Interest_Only_Expiry_Term,RMBS_v00_Calculations.Fixed_Rate_Expiry_Term,
	RMBS_v00_Calculations.Indexed_LVR,RMBS_v00_Calculations.Data_Issuer_Code,
	RMBS_v00_Calculations.Data_Issuer_Name,RMBS_v00_Calculations.Data_Issuer_ABN,
	RMBS_v00_Calculations.Arrears_Category,RMBS_v00_Calculations.Date_Key,RMBS_v00_Calculations.EOMonth_Collateral_Date,
	RMBS_v00_Calculations.OriginationYear,RMBS_v00_Calculations.Property_Region,
	RMBS_v00_Calculations.Property_State,NULL,GETDATE()
	From INGEST.RMBS_v00_Calculations RMBS_v00_Calculations;
	UPDATE LOAD.load_rmbs_v00_calculations SET dss_record_source = 'Customer1_DW.INGEST.RMBS_v00_Calculations' ;


	-- load_rmbs_v00_dataconversion
	truncate table LOAD.load_rmbs_v00_dataconversion;
	INSERT INTO LOAD.load_rmbs_v00_dataconversion
	(hk_l_rmbs_v00, collateral_date, most_recent_approval_amount, current_balance,
	scheduled_balance, loan_guarantee_flag, redraw_feature_flag, available_redraw,
	offset_account_flag, offset_account_balance, conforming_mortgage, origination_date_settlement_date,
	loan_term, remaining_term, seasoning, maturity_date, loan_securitised_date,
	original_ltv, current_ltv, scheduled_ltv, scheduled_minimum_payment, lmi_attachment_point,
	variable_loan_amount, current_interest_rate, reference_index_value, interest_margin,
	restructuring_arrangement_flag, interest_only_expiry_date, current_rate_expiry_date,
	default_balance, foreclosure_proceeds, date_of_foreclosure, loss_on_sale,
	days_in_arrears, amount_in_arrears, days_since_last_scheduled_payment,
	cumulative_missed_payments, claim_submitted_to_lmi, claim_paid_by_lmi,
	claim_denied_by_lmi, claim_pending_with_lmi, original_property_value, most_recent_property_value,
	most_recent_property_valuation_date, first_home_buyer_flag, employment_type_secondary_borrower,
	borrower_type_primary_borrower, borrower_type_secondary_borrower, legal_entity_type_secondary_borrower,
	bankruptcy_flag, last_credit_discharge_date, number_of_debtors, credit_score_secondary_borrower,
	debt_serviceability_metric_score, income, secondary_income, income_verification_for_secondary_income,
	abs_statistical_area, interest_rate, origination_date, dss_record_source,
	dss_load_datetime)
	SELECT
	RMBS_v00_DataConversion.hk_l_RMBS_V00,RMBS_v00_DataConversion.Collateral_Date,
	RMBS_v00_DataConversion.Most_Recent_Approval_Amount,RMBS_v00_DataConversion.Current_Balance,
	RMBS_v00_DataConversion.Scheduled_Balance,RMBS_v00_DataConversion.Loan_Guarantee_Flag,
	RMBS_v00_DataConversion.Redraw_Feature_Flag,RMBS_v00_DataConversion.Available_Redraw,
	RMBS_v00_DataConversion.Offset_Account_Flag,RMBS_v00_DataConversion.Offset_Account_Balance,
	RMBS_v00_DataConversion.Conforming_Mortgage,RMBS_v00_DataConversion.Origination_Date_Settlement_Date,
	RMBS_v00_DataConversion.Loan_Term,RMBS_v00_DataConversion.Remaining_Term,
	RMBS_v00_DataConversion.Seasoning,RMBS_v00_DataConversion.Maturity_Date,
	RMBS_v00_DataConversion.Loan_Securitised_Date,RMBS_v00_DataConversion.Original_LTV,
	RMBS_v00_DataConversion.Current_LTV,RMBS_v00_DataConversion.Scheduled_LTV,
	RMBS_v00_DataConversion.Scheduled_Minimum_Payment,RMBS_v00_DataConversion.LMI_Attachment_Point,
	RMBS_v00_DataConversion.Variable_Loan_Amount,RMBS_v00_DataConversion.Current_Interest_Rate,
	RMBS_v00_DataConversion.Reference_Index_Value,RMBS_v00_DataConversion.Interest_Margin,
	RMBS_v00_DataConversion.Restructuring_Arrangement_Flag,RMBS_v00_DataConversion.Interest_Only_Expiry_Date,
	RMBS_v00_DataConversion.Current_Rate_Expiry_Date,RMBS_v00_DataConversion.Default_Balance,
	RMBS_v00_DataConversion.Foreclosure_Proceeds,RMBS_v00_DataConversion.Date_of_Foreclosure,
	RMBS_v00_DataConversion.Loss_on_Sale,RMBS_v00_DataConversion.Days_In_Arrears,
	RMBS_v00_DataConversion.Amount_in_Arrears,RMBS_v00_DataConversion.Days_Since_Last_Scheduled_Payment,
	RMBS_v00_DataConversion.Cumulative_Missed_Payments,RMBS_v00_DataConversion.Claim_Submitted_to_LMI,
	RMBS_v00_DataConversion.Claim_Paid_by_LMI,RMBS_v00_DataConversion.Claim_Denied_by_LMI,
	RMBS_v00_DataConversion.Claim_Pending_with_LMI,RMBS_v00_DataConversion.Original_Property_Value,
	RMBS_v00_DataConversion.Most_Recent_Property_Value,RMBS_v00_DataConversion.Most_Recent_Property_Valuation_Date,
	RMBS_v00_DataConversion.First_Home_Buyer_Flag,RMBS_v00_DataConversion.Employment_Type_Secondary_Borrower,
	RMBS_v00_DataConversion.Borrower_type_Primary_Borrower,RMBS_v00_DataConversion.Borrower_type_Secondary_Borrower,
	RMBS_v00_DataConversion.Legal_Entity_Type_Secondary_Borrower,RMBS_v00_DataConversion.Bankruptcy_Flag,
	RMBS_v00_DataConversion.Last_Credit_Discharge_Date,RMBS_v00_DataConversion.Number_of_Debtors,
	RMBS_v00_DataConversion.Credit_Score_Secondary_Borrower,RMBS_v00_DataConversion.Debt_Serviceability_Metric_Score,
	RMBS_v00_DataConversion.Income,RMBS_v00_DataConversion.Secondary_Income,
	RMBS_v00_DataConversion.Income_Verification_for_Secondary_Income,RMBS_v00_DataConversion.ABS_Statistical_Area,
	RMBS_v00_DataConversion.Interest_Rate,RMBS_v00_DataConversion.Origination_Date,
	'INGEST.RMBS_v00_DataConversion',GETDATE()
	From INGEST.RMBS_v00_DataConversion RMBS_v00_DataConversion
	;


	-- load_rmbs_v00_referencecodes
	truncate table LOAD.load_rmbs_v00_referencecodes;
	INSERT INTO LOAD.load_rmbs_v00_referencecodes
	(hk_l_rmbs_v00, scheduled_payment_policy, lmi_underwriting_type, loan_purpose,
	loan_documentation_type, loan_type, payment_frequency, lmi_provider_name,
	interest_rate_type, interest_rate_reset_interval, account_status, property_country_code,
	property_country_name, original_property_valuation_type, most_recent_property_valuation_type,
	property_purpose, property_type, lien, employment_type, legal_entity_type,
	income_verification, arrears_methodology, main_security_methodology, primary_borrower_methodology,
	savings_verification, dss_record_source, dss_load_datetime)
	SELECT
	RMBS_v00_ReferenceCodes.hk_l_rmbs_v00,RMBS_v00_ReferenceCodes.Scheduled_Payment_Policy,
	RMBS_v00_ReferenceCodes.LMI_Underwriting_Type,RMBS_v00_ReferenceCodes.Loan_Purpose,
	RMBS_v00_ReferenceCodes.Loan_Documentation_Type,RMBS_v00_ReferenceCodes.Loan_Type,
	RMBS_v00_ReferenceCodes.Payment_Frequency,RMBS_v00_ReferenceCodes.LMI_Provider_Name,
	RMBS_v00_ReferenceCodes.Interest_Rate_Type,RMBS_v00_ReferenceCodes.Interest_Rate_Reset_Interval,
	RMBS_v00_ReferenceCodes.Account_Status,RMBS_v00_ReferenceCodes.Property_Country_Code,
	RMBS_v00_ReferenceCodes.Property_Country_Name,RMBS_v00_ReferenceCodes.Original_Property_Valuation_Type,
	RMBS_v00_ReferenceCodes.Most_Recent_Property_Valuation_Type,RMBS_v00_ReferenceCodes.Property_Purpose,
	RMBS_v00_ReferenceCodes.Property_Type,RMBS_v00_ReferenceCodes.Lien,RMBS_v00_ReferenceCodes.Employment_Type,
	RMBS_v00_ReferenceCodes.Legal_Entity_Type,RMBS_v00_ReferenceCodes.Income_Verification,
	RMBS_v00_ReferenceCodes.Arrears_Methodology,RMBS_v00_ReferenceCodes.Main_Security_Methodology,
	RMBS_v00_ReferenceCodes.Primary_Borrower_Methodology,RMBS_v00_ReferenceCodes.Savings_Verification,
	NULL,GETDATE()
	From INGEST.RMBS_v00_ReferenceCodes RMBS_v00_ReferenceCodes
	;
	UPDATE LOAD.load_rmbs_v00_referencecodes SET dss_record_source = 'Customer1_DW.INGEST.RMBS_v00_ReferenceCodes' ;



	-- load_rmbs_v00_statustracking
	truncate table LOAD.load_rmbs_v00_statustracking;
	INSERT INTO LOAD.load_rmbs_v00_statustracking
	(hk_l_rmbs_v00, record_status, dss_record_source, dss_load_datetime)
	SELECT
	RMBS_v00_StatusTracking.hk_l_rmbs_v00,RMBS_v00_StatusTracking.Record_Status,NULL,GETDATE()
	From INGEST.RMBS_v00_StatusTracking RMBS_v00_StatusTracking
	;
	UPDATE LOAD.load_rmbs_v00_statustracking SET dss_record_source = 'Customer1_DW.INGEST.RMBS_v00_StatusTracking' ;


	-- load_rmbs_v00_validations
	truncate table LOAD.load_rmbs_v00_validations;
	INSERT INTO LOAD.load_rmbs_v00_validations
	(hk_l_rmbs_v00, collateral_date_vld, most_recent_approval_amount_vld, current_balance_vld,
	scheduled_balance_vld, available_redraw_vld, offset_account_balancet_vld,
	loan_term_vld, remaining_term_vld, seasoning_vld, maturity_date_vld, loan_securitised_date_vld,
	original_ltv_vld, current_ltv_vld, scheduled_ltv_vld, scheduled_minimum_payment_vld,
	lmi_attachment_point_vld, variable_loan_amount_vld, current_interest_rate_vld,
	interest_only_expiry_date_vld, current_rate_expiry_date_vld, default_balance_vld,
	foreclosure_proceeds_vld, loss_on_sale_vld, days_in_arrears_vld, amount_in_arrears_vld,
	claim_submitted_to_lmi_vld, claim_paid_by_lmi_vld, claim_denied_by_lmi_vld,
	claim_pending_with_lmi_vld, original_property_value_vld, most_recent_property_value_vld,
	loan_guarantee_flag_vld, redraw_feature_flag_vld, offset_account_flag_vld,
	conforming_mortgage_vld, origination_date_settlement_date_vld, loan_origination_channel_flag_vld,
	reference_index_value_vld, interest_margin_vld, restructuring_arrangement_flag_vld,
	date_of_foreclosure_vld, days_since_last_scheduled_payment_vld, cumulative_missed_payments_vld,
	most_recent_property_valuation_date_vld, first_home_buyer_flag_vld, employment_type_secondary_borrower_vld,
	borrower_type_primary_borrower_vld, borrower_type_secondary_borrower_vld,
	legal_entity_type_secondary_borrower_vld, bankruptcy_flag_vld, last_credit_discharge_date_vld,
	number_of_debtors_vld, credit_score_secondary_borrower_vld, debt_serviceability_metric_score_vld,
	income_vld, secondary_income_vld, income_verification_for_secondary_income_vld,
	abs_statistical_area_vld, interest_rate_vld, property_postcode_vld, dss_record_source,
	dss_load_datetime)
	SELECT
	RMBS_v00_Validations.hk_l_rmbs_v00,RMBS_v00_Validations.Collateral_Date_VLD,
	RMBS_v00_Validations.Most_Recent_Approval_Amount_VLD,RMBS_v00_Validations.Current_Balance_VLD,
	RMBS_v00_Validations.Scheduled_Balance_VLD,RMBS_v00_Validations.Available_Redraw_VLD,
	RMBS_v00_Validations.Offset_Account_Balancet_VLD,RMBS_v00_Validations.Loan_Term_VLD,
	RMBS_v00_Validations.Remaining_Term_VLD,RMBS_v00_Validations.Seasoning_VLD,
	RMBS_v00_Validations.Maturity_Date_VLD,RMBS_v00_Validations.Loan_Securitised_Date_VLD,
	RMBS_v00_Validations.Original_LTV_VLD,RMBS_v00_Validations.Current_LTV_VLD,
	RMBS_v00_Validations.Scheduled_LTV_VLD,RMBS_v00_Validations.Scheduled_Minimum_Payment_VLD,
	RMBS_v00_Validations.LMI_Attachment_Point_VLD,RMBS_v00_Validations.Variable_Loan_Amount_VLD,
	RMBS_v00_Validations.Current_Interest_Rate_VLD,RMBS_v00_Validations.Interest_Only_Expiry_Date_VLD,
	RMBS_v00_Validations.Current_Rate_Expiry_Date_VLD,RMBS_v00_Validations.Default_Balance_VLD,
	RMBS_v00_Validations.Foreclosure_Proceeds_VLD,RMBS_v00_Validations.Loss_on_Sale_VLD,
	RMBS_v00_Validations.Days_In_Arrears_VLD,RMBS_v00_Validations.Amount_in_Arrears_VLD,
	RMBS_v00_Validations.Claim_Submitted_to_LMI_VLD,RMBS_v00_Validations.Claim_Paid_by_LMI_VLD,
	RMBS_v00_Validations.Claim_Denied_by_LMI_VLD,RMBS_v00_Validations.Claim_Pending_with_LMI_VLD,
	RMBS_v00_Validations.Original_Property_Value_VLD,RMBS_v00_Validations.Most_Recent_Property_Value_VLD,
	RMBS_v00_Validations.Loan_Guarantee_Flag_VLD,RMBS_v00_Validations.Redraw_Feature_Flag_VLD,
	RMBS_v00_Validations.Offset_Account_Flag_VLD,RMBS_v00_Validations.Conforming_Mortgage_VLD,
	RMBS_v00_Validations.Origination_Date_Settlement_Date_VLD,RMBS_v00_Validations.Loan_Origination_Channel_Flag_VLD,
	RMBS_v00_Validations.Reference_Index_Value_VLD,RMBS_v00_Validations.Interest_Margin_VLD,
	RMBS_v00_Validations.Restructuring_Arrangement_Flag_VLD,RMBS_v00_Validations.Date_of_Foreclosure_VLD,
	RMBS_v00_Validations.Days_Since_Last_Scheduled_Payment_VLD,RMBS_v00_Validations.Cumulative_Missed_Payments_VLD,
	RMBS_v00_Validations.Most_Recent_Property_Valuation_Date_VLD,RMBS_v00_Validations.First_Home_Buyer_Flag_VLD,
	RMBS_v00_Validations.Employment_Type_Secondary_Borrower_VLD,RMBS_v00_Validations.Borrower_type_Primary_Borrower_VLD,
	RMBS_v00_Validations.Borrower_type_Secondary_Borrower_VLD,RMBS_v00_Validations.Legal_Entity_Type_Secondary_Borrower_VLD,
	RMBS_v00_Validations.Bankruptcy_Flag_VLD,RMBS_v00_Validations.Last_Credit_Discharge_Date_VLD,
	RMBS_v00_Validations.Number_of_Debtors_VLD,RMBS_v00_Validations.Credit_Score_Secondary_Borrower_VLD,
	RMBS_v00_Validations.Debt_Serviceability_Metric_Score_VLD,RMBS_v00_Validations.Income_VLD,
	RMBS_v00_Validations.Secondary_Income_VLD,RMBS_v00_Validations.Income_Verification_for_Secondary_Income_VLD,
	RMBS_v00_Validations.ABS_Statistical_Area_VLD,RMBS_v00_Validations.Interest_Rate_VLD,
	RMBS_v00_Validations.Property_Postcode_VLD,NULL,GETDATE()
	From INGEST.RMBS_v00_Validations RMBS_v00_Validations
	;
	UPDATE LOAD.load_rmbs_v00_validations SET dss_record_source = 'Customer1_DW.INGEST.RMBS_v00_Validations' ;


	-- load_SECU_v00_dataconversion
	truncate table LOAD.load_SECU_v00_dataconversion;
	INSERT INTO LOAD.load_SECU_v00_dataconversion
	(hk_l_SECU_v00, report_date, issue_date, accrual_begin_date, accrual_end_date,
	accrual_period, original_subordination, current_subordination, current_principal_face_amount,
	coupon_payment_reference, distribution_date, record_date, determination_date,
	beginning_stated_factor, ending_stated_factor, original_principal_face_amount,
	beginning_invested_amount, ending_invested_amount, beginning_stated_amount,
	ending_stated_amount, beginning_invested_factor, ending_invested_factor,
	coupon_reference_index_value, coupon_margin, step_up_margin, step_up_date,
	scheduled_coupon, scheduled_coupon_factor, current_coupon_rate, coupon_distribution,
	coupon_distribution_factor, scheduled_principal, scheduled_principal_factor,
	principal_distribution, principal_distribution_factor, coupon_shortfall_factor,
	ending_cumulative_coupon_shortfall_factor, coupon_shortfall, ending_cumulative_coupon_shortfall,
	principal_shortfall, ending_cumulative_principal_shortfall, legal_maturity_date,
	original_estimated_weighted_average_life, current_weighted_average_life,
	expected_final_maturity_date, redemption_price, abn, soft_bullet_date,
	step_up_rate, beginning_cumulative_chargeoffs, beginning_cumulative_coupon_shortfall,
	beginning_cumulative_principal_shortfall, coupon_shortfall_makeup, principal_shortfall_makeup,
	allocated_chargeoffs, chargeoff_restorations, ending_cumulative_chargeoffs,
	beginning_cumulative_chargeoff_factor, beginning_cumulative_coupon_shortfall_factor,
	beginning_cumulative_principal_shortfall_factor, coupon_shortfall_makeup_factor,
	principal_shortfall_makeup_factor, principal_shortfall_factor, allocated_chargeoff_factor,
	chargeoff_restoration_factor, ending_cumulative_chargeoff_factor, ending_cumulative_principal_shortfall_factor,
	coupon_rate, dss_record_source, dss_load_datetime)
	SELECT
	SECU_v00_DataConversion.hk_l_SECU_v00,SECU_v00_DataConversion.Report_Date,
	SECU_v00_DataConversion.Issue_Date,SECU_v00_DataConversion.Accrual_Begin_Date,
	SECU_v00_DataConversion.Accrual_End_Date,SECU_v00_DataConversion.Accrual_Period,
	SECU_v00_DataConversion.Original_Subordination,SECU_v00_DataConversion.Current_Subordination,
	SECU_v00_DataConversion.Current_Principal_Face_Amount,SECU_v00_DataConversion.Coupon_Payment_Reference,
	SECU_v00_DataConversion.Distribution_Date,SECU_v00_DataConversion.Record_Date,
	SECU_v00_DataConversion.Determination_Date,SECU_v00_DataConversion.Beginning_Stated_Factor,
	SECU_v00_DataConversion.Ending_Stated_Factor,SECU_v00_DataConversion.Original_Principal_Face_Amount,
	SECU_v00_DataConversion.Beginning_Invested_Amount,SECU_v00_DataConversion.Ending_Invested_Amount,
	SECU_v00_DataConversion.Beginning_Stated_Amount,SECU_v00_DataConversion.Ending_Stated_Amount,
	SECU_v00_DataConversion.Beginning_Invested_Factor,SECU_v00_DataConversion.Ending_Invested_Factor,
	SECU_v00_DataConversion.Coupon_Reference_Index_Value,SECU_v00_DataConversion.Coupon_Margin,
	SECU_v00_DataConversion.Step_Up_Margin,SECU_v00_DataConversion.Step_Up_Date,
	SECU_v00_DataConversion.Scheduled_Coupon,SECU_v00_DataConversion.Scheduled_Coupon_Factor,
	SECU_v00_DataConversion.Current_Coupon_Rate,SECU_v00_DataConversion.Coupon_Distribution,
	SECU_v00_DataConversion.Coupon_Distribution_Factor,SECU_v00_DataConversion.Scheduled_Principal,
	SECU_v00_DataConversion.Scheduled_Principal_Factor,SECU_v00_DataConversion.Principal_Distribution,
	SECU_v00_DataConversion.Principal_Distribution_Factor,SECU_v00_DataConversion.Coupon_Shortfall_Factor,
	SECU_v00_DataConversion.Ending_Cumulative_Coupon_Shortfall_Factor,SECU_v00_DataConversion.Coupon_Shortfall,
	SECU_v00_DataConversion.Ending_Cumulative_Coupon_Shortfall,SECU_v00_DataConversion.Principal_Shortfall,
	SECU_v00_DataConversion.Ending_Cumulative_Principal_Shortfall,SECU_v00_DataConversion.Legal_Maturity_Date,
	SECU_v00_DataConversion.Original_Estimated_Weighted_Average_Life,SECU_v00_DataConversion.Current_Weighted_Average_Life,
	SECU_v00_DataConversion.Expected_Final_Maturity_Date,SECU_v00_DataConversion.Redemption_Price,
	SECU_v00_DataConversion.ABN,SECU_v00_DataConversion.Soft_Bullet_Date,
	SECU_v00_DataConversion.Step_Up_Rate,SECU_v00_DataConversion.Beginning_Cumulative_Chargeoffs,
	SECU_v00_DataConversion.Beginning_Cumulative_Coupon_Shortfall,SECU_v00_DataConversion.Beginning_Cumulative_Principal_Shortfall,
	SECU_v00_DataConversion.Coupon_Shortfall_Makeup,SECU_v00_DataConversion.Principal_Shortfall_Makeup,
	SECU_v00_DataConversion.Allocated_Chargeoffs,SECU_v00_DataConversion.Chargeoff_Restorations,
	SECU_v00_DataConversion.Ending_Cumulative_Chargeoffs,SECU_v00_DataConversion.Beginning_Cumulative_Chargeoff_Factor,
	SECU_v00_DataConversion.Beginning_Cumulative_Coupon_Shortfall_Factor,
	SECU_v00_DataConversion.Beginning_Cumulative_Principal_Shortfall_Factor,
	SECU_v00_DataConversion.Coupon_Shortfall_Makeup_Factor,SECU_v00_DataConversion.Principal_Shortfall_Makeup_Factor,
	SECU_v00_DataConversion.Principal_Shortfall_Factor,SECU_v00_DataConversion.Allocated_Chargeoff_Factor,
	SECU_v00_DataConversion.Chargeoff_Restoration_Factor,SECU_v00_DataConversion.Ending_Cumulative_Chargeoff_Factor,
	SECU_v00_DataConversion.Ending_Cumulative_Principal_Shortfall_Factor,
	SECU_v00_DataConversion.Coupon_Rate,'s_SECU_v00',GetDate()
	From INGEST.SECU_v00_DataConversion SECU_v00_DataConversion
	;

END;


IF @groupid = 4 BEGIN

	truncate table LOAD.load_rmbs_v00_calculations;
	INSERT INTO LOAD.load_rmbs_v00_calculations
	(hk_l_rmbs_v00, offset_account_flag, conforming_mortgage_flag, restructuring_arrangement_flag,
	first_home_buyer_flag, bankruptcy_flag, interest_only_expiry_term, fixed_rate_expiry_term,
	indexed_lvr, data_issuer_code, data_issuer_name, data_issuer_abn, arrears_category,
	date_key, eomonth_collateral_date, originationyear, property_region, property_state,
	dss_record_source, dss_load_datetime)
	SELECT
	RMBS_v00_Calculations.hk_l_rmbs_v00,RMBS_v00_Calculations.Offset_Account_Flag,
	RMBS_v00_Calculations.Conforming_Mortgage_Flag,RMBS_v00_Calculations.Restructuring_Arrangement_Flag,
	RMBS_v00_Calculations.First_Home_Buyer_Flag,RMBS_v00_Calculations.Bankruptcy_Flag,
	RMBS_v00_Calculations.Interest_Only_Expiry_Term,RMBS_v00_Calculations.Fixed_Rate_Expiry_Term,
	RMBS_v00_Calculations.Indexed_LVR,RMBS_v00_Calculations.Data_Issuer_Code,
	RMBS_v00_Calculations.Data_Issuer_Name,RMBS_v00_Calculations.Data_Issuer_ABN,
	RMBS_v00_Calculations.Arrears_Category,RMBS_v00_Calculations.Date_Key,RMBS_v00_Calculations.EOMonth_Collateral_Date,
	RMBS_v00_Calculations.OriginationYear,RMBS_v00_Calculations.Property_Region,
	RMBS_v00_Calculations.Property_State,NULL,GETDATE()
	From INGEST.RMBS_v00_Calculations RMBS_v00_Calculations;
	UPDATE LOAD.load_rmbs_v00_calculations SET dss_record_source = 'Customer1_DW.INGEST.RMBS_v00_Calculations' ;
END;


GO


