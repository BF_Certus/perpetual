CREATE TABLE STAGE.stage_bvrmbs_v00_calculations (
  hk_l_rmbs_v00 binary(20), offset_account_flag varchar(3) 
, conforming_mortgage_flag varchar(3), restructuring_arrangement_flag varchar(3) 
, first_home_buyer_flag varchar(3), bankruptcy_flag varchar(3) 
, interest_only_expiry_term numeric(19,6), fixed_rate_expiry_term numeric(19,6) 
, indexed_lvr date, data_issuer_code varchar(100), data_issuer_name varchar(100) 
, data_issuer_abn int, arrears_category varchar(5), date_key varchar(100) 
, eomonth_collateral_date date, originationyear int, property_region varchar(8) 
, property_state varchar(8) 
, dss_change_hash_stage_bvrmbs_v00_calculations binary(20) NOT NULL 
, dss_load_datetime datetime2, dss_record_source varchar(255) 
, dss_create_time datetime2);