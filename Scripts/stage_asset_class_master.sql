CREATE TABLE STAGE.stage_asset_class_master (
  hk_h_asset_class_master binary(20) NOT NULL, asset_class_id int 
, asset_class_code varchar(10), asset_class_description varchar(100), active bit 
, update_date datetime 
, dss_change_hash_stage_asset_class_master binary(20) NOT NULL 
, dss_record_source varchar(255), dss_load_datetime datetime2 
, dss_create_time datetime2);