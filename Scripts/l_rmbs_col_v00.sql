CREATE TABLE DV.l_rmbs_col_v00 (
  hk_l_rmbs_col_v00 binary(20) NOT NULL, hk_h_client_master binary(20) NOT NULL 
, hk_h_collateral binary(20) NOT NULL, hk_h_loan binary(20) NOT NULL 
, hk_h_reporting_period binary(20) NOT NULL 
, hk_h_transaction binary(20) NOT NULL, dss_record_source varchar(255) 
, dss_load_datetime datetime2, dss_create_time datetime2);
CREATE UNIQUE NONCLUSTERED INDEX l_rmbs_col_v00_idx_0 ON DV.l_rmbs_col_v00 (hk_l_rmbs_col_v00) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_rmbs_col_v00_idx_1 ON DV.l_rmbs_col_v00 (hk_h_client_master) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_rmbs_col_v00_idx_2 ON DV.l_rmbs_col_v00 (hk_h_collateral) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_rmbs_col_v00_idx_3 ON DV.l_rmbs_col_v00 (hk_h_loan) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_rmbs_col_v00_idx_4 ON DV.l_rmbs_col_v00 (hk_h_reporting_period) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_rmbs_col_v00_idx_5 ON DV.l_rmbs_col_v00 (hk_h_transaction) WITH (SORT_IN_TEMPDB = OFF);
CREATE UNIQUE NONCLUSTERED INDEX l_rmbs_col_v00_idx_A ON DV.l_rmbs_col_v00 (hk_h_client_master,hk_h_collateral,hk_h_loan,hk_h_reporting_period,hk_h_transaction) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_rmbs_col_v0_82a8bd37_idx_1 ON DV.l_rmbs_col_v00 (hk_h_client_master) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_rmbs_col_v0_82a8bd37_idx_2 ON DV.l_rmbs_col_v00 (hk_h_collateral) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_rmbs_col_v0_82a8bd37_idx_3 ON DV.l_rmbs_col_v00 (hk_h_loan) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_rmbs_col_v0_82a8bd37_idx_4 ON DV.l_rmbs_col_v00 (hk_h_reporting_period) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_rmbs_col_v0_82a8bd37_idx_5 ON DV.l_rmbs_col_v00 (hk_h_transaction) WITH (SORT_IN_TEMPDB = OFF);
ALTER TABLE DV.l_rmbs_col_v00 ADD CONSTRAINT l_rmbs_col_v0_82a8bd37_idx_PK PRIMARY KEY NONCLUSTERED (hk_l_rmbs_col_v00) WITH (SORT_IN_TEMPDB = OFF);
