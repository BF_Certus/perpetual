truncate table LOAD.load_TRAN_authority;
INSERT INTO LOAD.load_TRAN_authority
(clientid, feedid, reporting_period, master_trust_name_or_spv, series_trust_name_or_series,
transaction_id, authority_holder_name, authority_count, authority_position,
authority_type, authority_holder_abn, authority_holder_jurisdiction, authority_holder_id,
authority_holder_id_descriptor, dss_record_source, dss_load_datetime)
SELECT
TRAN_Authority.ClientID,TRAN_Authority.FeedID,TRAN_Authority.Reporting_Period,
TRAN_Authority.Master_Trust_Name_or_SPV,TRAN_Authority.Series_Trust_Name_or_Series,
TRAN_Authority.Transaction_ID,TRAN_Authority.Authority_Holder_Name,
TRAN_Authority.Authority_Count,TRAN_Authority.Authority_Position,
TRAN_Authority.Authority_Type,TRAN_Authority.Authority_Holder_ABN,
TRAN_Authority.Authority_Holder_Jurisdiction,TRAN_Authority.Authority_Holder_ID,
TRAN_Authority.Authority_Holder_ID_Descriptor,NULL,NULL
From INGEST.TRAN_Authority TRAN_Authority
;

;
UPDATE LOAD.load_TRAN_authority SET dss_record_source = 'Perpetual.INGEST.TRAN_Authority',dss_load_datetime = GetDate() ;

