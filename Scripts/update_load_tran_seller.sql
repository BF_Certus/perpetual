truncate table LOAD.load_TRAN_seller;
INSERT INTO LOAD.load_TRAN_seller
(clientid, feedid, reporting_period, master_trust_name_or_spv, series_trust_name_or_series,
transaction_id, seller_name, seller_count, seller_position, seller_abn,
seller_jurisdiction, seller_id, seller_id_descriptor, dss_record_source,
dss_load_datetime)
SELECT
TRAN_Seller.ClientID,TRAN_Seller.FeedID,TRAN_Seller.Reporting_Period,
TRAN_Seller.Master_Trust_Name_or_SPV,TRAN_Seller.Series_Trust_Name_or_Series,
TRAN_Seller.Transaction_ID,TRAN_Seller.Seller_Name,TRAN_Seller.Seller_Count,
TRAN_Seller.Seller_Position,TRAN_Seller.Seller_ABN,TRAN_Seller.Seller_Jurisdiction,
TRAN_Seller.Seller_ID,TRAN_Seller.Seller_ID_Descriptor,NULL,NULL
From INGEST.TRAN_Seller TRAN_Seller
;

;
UPDATE LOAD.load_TRAN_seller SET dss_record_source = 'Perpetual.INGEST.TRAN_Seller',dss_load_datetime = GetDate() ;

