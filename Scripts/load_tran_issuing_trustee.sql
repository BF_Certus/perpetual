CREATE TABLE LOAD.load_TRAN_issuing_trustee (
  clientid varchar(100), reporting_period varchar(100) 
, master_trust_name_or_spv varchar(100) 
, series_trust_name_or_series varchar(100), transaction_id varchar(100) 
, issuing_trustee_name varchar(5000), issuing_trustee_abn varchar(5000) 
, feedid varchar(100), issuing_trustee_count int, issuing_trustee_position int 
, issuing_trustee_jurisdiction varchar(5000), issuing_trustee_id varchar(5000) 
, issuing_trustee_id_descriptor varchar(5000) 
, issuing_trustee_fee_currency varchar(5000), issuing_trustee_fee varchar(5000) 
, dss_record_source varchar(255), dss_load_datetime datetime2);