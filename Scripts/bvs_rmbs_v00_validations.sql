CREATE TABLE DV.bvs_rmbs_v00_validations (
  dss_load_datetime datetime2, dss_change_hash binary(20) NOT NULL 
, dss_record_source varchar(255), dss_start_date datetime2, dss_version integer 
, dss_create_time datetime2, hk_l_rmbs_v00 binary(20) NOT NULL 
, collateral_date_vld int NOT NULL, most_recent_approval_amount_vld int NOT NULL 
, current_balance_vld int NOT NULL, scheduled_balance_vld int NOT NULL 
, available_redraw_vld int NOT NULL, offset_account_balancet_vld int NOT NULL 
, loan_term_vld int NOT NULL, remaining_term_vld int NOT NULL 
, seasoning_vld int NOT NULL, maturity_date_vld int NOT NULL 
, loan_securitised_date_vld int NOT NULL, original_ltv_vld int NOT NULL 
, current_ltv_vld int NOT NULL, scheduled_ltv_vld int NOT NULL 
, scheduled_minimum_payment_vld int NOT NULL 
, lmi_attachment_point_vld int NOT NULL, variable_loan_amount_vld int NOT NULL 
, current_interest_rate_vld int NOT NULL 
, interest_only_expiry_date_vld int NOT NULL 
, current_rate_expiry_date_vld int NOT NULL, default_balance_vld int NOT NULL 
, foreclosure_proceeds_vld int NOT NULL, loss_on_sale_vld int NOT NULL 
, days_in_arrears_vld int NOT NULL, amount_in_arrears_vld int NOT NULL 
, claim_submitted_to_lmi_vld int NOT NULL, claim_paid_by_lmi_vld int NOT NULL 
, claim_denied_by_lmi_vld int NOT NULL, claim_pending_with_lmi_vld int NOT NULL 
, original_property_value_vld int NOT NULL 
, most_recent_property_value_vld int NOT NULL 
, loan_guarantee_flag_vld int NOT NULL, redraw_feature_flag_vld int NOT NULL 
, offset_account_flag_vld int NOT NULL, conforming_mortgage_vld int NOT NULL 
, origination_date_settlement_date_vld int NOT NULL 
, loan_origination_channel_flag_vld int NOT NULL 
, reference_index_value_vld int NOT NULL, interest_margin_vld int NOT NULL 
, restructuring_arrangement_flag_vld int NOT NULL 
, date_of_foreclosure_vld int NOT NULL 
, days_since_last_scheduled_payment_vld int NOT NULL 
, cumulative_missed_payments_vld int NOT NULL 
, most_recent_property_valuation_date_vld int NOT NULL 
, first_home_buyer_flag_vld int NOT NULL 
, employment_type_secondary_borrower_vld int NOT NULL 
, borrower_type_primary_borrower_vld int NOT NULL 
, borrower_type_secondary_borrower_vld int NOT NULL 
, legal_entity_type_secondary_borrower_vld int NOT NULL 
, bankruptcy_flag_vld int NOT NULL, last_credit_discharge_date_vld int NOT NULL 
, number_of_debtors_vld int NOT NULL 
, credit_score_secondary_borrower_vld int NOT NULL 
, debt_serviceability_metric_score_vld int NOT NULL, income_vld int NOT NULL 
, secondary_income_vld int NOT NULL 
, income_verification_for_secondary_income_vld int NOT NULL 
, abs_statistical_area_vld int NOT NULL, interest_rate_vld int NOT NULL 
, property_postcode_vld int NOT NULL);