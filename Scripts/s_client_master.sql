CREATE TABLE DV.s_client_master (
  hk_h_client_master binary(20) NOT NULL, dss_change_hash binary(20) NOT NULL 
, dss_record_source varchar(255), dss_load_datetime datetime 
, dss_start_date datetime, dss_version integer, dss_create_time datetime 
, client_name varchar(100), service_level int 
, analytics_subscription varchar(100), active bit, update_date datetime);
