/****** Object:  View [INGEST].[RMBS_v00_StatusTracking]    Script Date: 25/06/2018 12:39:39 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create view [INGEST].[RMBS_v00_StatusTracking] as 
Select a.hk_l_rmbs_v00, 
       Case when a.ROW_ID = 1 
			then 'Current' 
			else 'Deleted' 
		end as Record_Status
From (
Select REF_PITs.PIT_Name, 
       REF_PITs.PIT_Date,  
	   l_rmbs_v00.hk_h_loan, 
	   l_rmbs_v00.hk_h_client_master, 
	   l_rmbs_v00.hk_h_reporting_period, 
	   l_rmbs_v00.hk_h_transaction      , 
	   l_rmbs_v00.hk_l_rmbs_v00,
	   ROW_NUMBER ( )   OVER (Partition By  l_rmbs_v00.hk_h_loan,  l_rmbs_v00.hk_h_client_master, 	
											l_rmbs_v00.hk_h_reporting_period, l_rmbs_v00.hk_h_transaction 
							  order by s_rmbs_v00.dss_load_datetime desc,l_rmbs_v00.hk_l_rmbs_v00) As ROW_ID                                                                                           
FROM DV.l_rmbs_v00 l_rmbs_v00 full outer join DV.REF_PITs REF_PITs on  1=1                                                             
 full outer join 
DV.s_rmbs_v00 s_rmbs_v00 on l_rmbs_v00.hk_l_rmbs_v00 = s_rmbs_v00.hk_l_rmbs_v00   and 
s_rmbs_v00.dss_load_datetime = (Select Max(z.dss_load_datetime) from DV.s_rmbs_v00 z 
where z.hk_l_rmbs_v00 = l_rmbs_v00.hk_l_rmbs_v00 and z.dss_load_datetime <= Ref_PITs.PIT_Date)     
WHERE REF_PITs.PIT_NAME = 'Current Date') a
GO


