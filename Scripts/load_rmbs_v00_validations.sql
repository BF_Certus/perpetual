CREATE TABLE LOAD.load_rmbs_v00_validations (
  hk_l_rmbs_v00 binary(20), collateral_date_vld int 
, most_recent_approval_amount_vld int, current_balance_vld int 
, scheduled_balance_vld int, available_redraw_vld int 
, offset_account_balancet_vld int, loan_term_vld int, remaining_term_vld int 
, seasoning_vld int, maturity_date_vld int, loan_securitised_date_vld int 
, original_ltv_vld int, current_ltv_vld int, scheduled_ltv_vld int 
, scheduled_minimum_payment_vld int, lmi_attachment_point_vld int 
, variable_loan_amount_vld int, current_interest_rate_vld int 
, interest_only_expiry_date_vld int, current_rate_expiry_date_vld int 
, default_balance_vld int, foreclosure_proceeds_vld int, loss_on_sale_vld int 
, days_in_arrears_vld int, amount_in_arrears_vld int 
, claim_submitted_to_lmi_vld int, claim_paid_by_lmi_vld int 
, claim_denied_by_lmi_vld int, claim_pending_with_lmi_vld int 
, original_property_value_vld int, most_recent_property_value_vld int 
, loan_guarantee_flag_vld int, redraw_feature_flag_vld int 
, offset_account_flag_vld int, conforming_mortgage_vld int 
, origination_date_settlement_date_vld int 
, loan_origination_channel_flag_vld int, reference_index_value_vld int 
, interest_margin_vld int, restructuring_arrangement_flag_vld int 
, date_of_foreclosure_vld int, days_since_last_scheduled_payment_vld int 
, cumulative_missed_payments_vld int 
, most_recent_property_valuation_date_vld int, first_home_buyer_flag_vld int 
, employment_type_secondary_borrower_vld int 
, borrower_type_primary_borrower_vld int 
, borrower_type_secondary_borrower_vld int 
, legal_entity_type_secondary_borrower_vld int, bankruptcy_flag_vld int 
, last_credit_discharge_date_vld int, number_of_debtors_vld int 
, credit_score_secondary_borrower_vld int 
, debt_serviceability_metric_score_vld int, income_vld int 
, secondary_income_vld int, income_verification_for_secondary_income_vld int 
, abs_statistical_area_vld int, interest_rate_vld int, property_postcode_vld int 
, dss_record_source varchar(255), dss_load_datetime datetime2);