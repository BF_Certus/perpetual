truncate table LOAD.load_SECU_ratings;
INSERT INTO LOAD.load_SECU_ratings
(clientid, reporting_period, transaction_id, master_trust_name_or_spv, series_trust_name_or_series,
tranche_name, name, report_date, original_rating, current_rating, abn,
jurisdiction, id, id_descriptor, dss_record_source, dss_load_datetime)
SELECT
SECU_Ratings.ClientID,SECU_Ratings.Reporting_Period,SECU_Ratings.Transaction_Id,
SECU_Ratings.Master_Trust_Name_Or_Spv,SECU_Ratings.Series_Trust_Name_Or_Series,
SECU_Ratings.Tranche_Name,SECU_Ratings.Name,SECU_Ratings.Report_Date,
SECU_Ratings.Original_Rating,SECU_Ratings.Current_Rating,SECU_Ratings.ABN,
SECU_Ratings.Jurisdiction,SECU_Ratings.Id,SECU_Ratings.Id_Descriptor,
Feed_Name,GETDATE()
From INGEST.SECU_Ratings SECU_Ratings;