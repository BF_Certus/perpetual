CREATE TABLE LOAD.load_rmbs_col_v00_dataconversion (
  hk_l_rmbs_col_v00 binary(20), security_property_postcode int 
, original_security_property_value decimal(18,2) 
, most_recent_security_property_value decimal(18,2) 
, most_recent_security_property_valuation_date date, abs_statistical_area int 
, main_security_flag bit, dss_record_source varchar(255) 
, dss_load_datetime datetime2);