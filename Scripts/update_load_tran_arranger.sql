truncate table LOAD.load_TRAN_arranger;
INSERT INTO LOAD.load_TRAN_arranger
(clientid, feedid, reporting_period, master_trust_name_or_spv, series_trust_name_or_series,
transaction_id, arranger_name, arranger_count, arranger_position, arranger_abn,
arranger_jurisdiction, arranger_id, arranger_id_descriptor, arranger_fee_currency,
arranger_fee, dss_record_source, dss_load_datetime)
SELECT
TRAN_Arranger.ClientID,TRAN_Arranger.FeedID,TRAN_Arranger.Reporting_Period,
TRAN_Arranger.Master_Trust_Name_or_SPV,TRAN_Arranger.Series_Trust_Name_or_Series,
TRAN_Arranger.Transaction_ID,TRAN_Arranger.Arranger_Name,TRAN_Arranger.Arranger_Count,
TRAN_Arranger.Arranger_Position,TRAN_Arranger.Arranger_ABN,TRAN_Arranger.Arranger_Jurisdiction,
TRAN_Arranger.Arranger_ID,TRAN_Arranger.Arranger_ID_Descriptor,
TRAN_Arranger.Arranger_Fee_Currency,TRAN_Arranger.Arranger_Fee,NULL,NULL
From INGEST.TRAN_Arranger TRAN_Arranger
;

;
UPDATE LOAD.load_TRAN_arranger SET dss_record_source = 'Perpetual.INGEST.TRAN_Arranger',dss_load_datetime = GetDate() ;

