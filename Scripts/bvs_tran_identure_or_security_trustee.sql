CREATE TABLE DV.bvs_TRAN_identure_or_security_trustee (
  hk_l_TRAN_identure_or_security_trustee binary(20) NOT NULL 
, dss_load_datetime datetime, dss_change_hash binary(20) NOT NULL 
, dss_record_source varchar(255), dss_start_date datetime, dss_version int 
, dss_create_time datetime, identure_or_security_trustee_count int 
, identure_or_security_trustee_position int 
, indenture_or_security_trustee_abn varchar(5000) 
, indenture_or_security_trustee_jurisdiction varchar(5000) 
, indenture_or_security_trustee_id varchar(5000) 
, indenture_or_security_trustee_id_descriptor varchar(5000) 
, indenture_or_security_trustee_fee_currency varchar(5000) 
, indenture_or_security_trustee_fee varchar(5000));