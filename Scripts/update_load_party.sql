truncate table LOAD.load_party;
INSERT INTO LOAD.load_party
(clientid, party_name, feed_name, dss_record_source, dss_load_datetime)
SELECT
party.clientid,party.party_name,party.feed_name,NULL,NULL
From INGEST.party party
;
UPDATE LOAD.load_party SET dss_record_source = Feed_Name,dss_load_datetime = GetDate() ;
