truncate table LOAD.load_feed_master;
INSERT INTO LOAD.load_feed_master 
(feedid, feed_name, clientid, asset_class_id, feed_level, feed_details, stage_table, target_table, control_file_name, checksum_format, feed_location, feed_issuer_contact, data_validation_required, onboarding_required, active, update_date, dss_record_source, dss_load_datetime)
SELECT
Feed_Master.FeedID,Feed_Master.Feed_Name,Feed_Master.ClientID,Feed_Master.Asset_Class_ID,Feed_Master.Feed_Level,Feed_Master.Feed_Details,Feed_Master.Stage_Table,Feed_Master.Target_Table,Feed_Master.Control_File_Name,Feed_Master.Checksum_Format,Feed_Master.Feed_Location,Feed_Master.Feed_Issuer_Contact,Feed_Master.Data_Validation_Required,Feed_Master.Onboarding_Required,Feed_Master.Active,Feed_Master.Update_Date,'',''
FROM INGEST.Feed_Master Feed_Master;
UPDATE LOAD.load_feed_master SET dss_record_source = '.INGEST.Feed_Master',dss_load_datetime = GetDate() ;