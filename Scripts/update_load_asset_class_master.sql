truncate table LOAD.load_asset_class_master;
INSERT INTO LOAD.load_asset_class_master 
(asset_class_id, asset_class_code, asset_class_description, active, update_date, dss_record_source, dss_load_datetime)
SELECT
Asset_Class_Master.Asset_Class_ID,Asset_Class_Master.Asset_Class_Code,Asset_Class_Master.Asset_Class_Description,Asset_Class_Master.Active,Asset_Class_Master.Update_Date,'',''
FROM INGEST.Asset_Class_Master Asset_Class_Master;
UPDATE LOAD.load_asset_class_master SET dss_record_source = '.INGEST.Asset_Class_Master',dss_load_datetime = GetDate() ;