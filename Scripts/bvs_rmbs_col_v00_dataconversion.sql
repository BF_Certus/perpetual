CREATE TABLE DV.bvs_rmbs_col_v00_dataconversion (
  dss_load_datetime datetime2, dss_change_hash binary(20) NOT NULL 
, dss_record_source varchar(255), dss_start_date datetime2, dss_version integer 
, dss_create_time datetime2, hk_l_rmbs_col_v00 binary(20) NOT NULL 
, security_property_postcode int, original_security_property_value decimal(18,2) 
, most_recent_security_property_value decimal(18,2) 
, most_recent_security_property_valuation_date date, abs_statistical_area int 
, main_security_flag bit);