CREATE TABLE STAGE.stage_bvrmbs_v00_statustracking (
  hk_l_rmbs_v00 binary(20), record_status varchar(7) 
, dss_change_hash_stage_bvrmbs_v00_statustracking binary(20) NOT NULL 
, dss_load_datetime datetime2, dss_record_source varchar(255) 
, dss_create_time datetime2);