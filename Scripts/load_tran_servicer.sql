CREATE TABLE LOAD.load_TRAN_servicer (
  clientid varchar(100), reporting_period varchar(100) 
, master_trust_name_or_spv varchar(100) 
, series_trust_name_or_series varchar(100), transaction_id varchar(100) 
, servicer_name varchar(5000), feedid varchar(100), servicer_count int 
, servicer_position int, servicer_abn varchar(5000) 
, servicer_jurisdiction varchar(5000), servicing_fee_currency varchar(5000) 
, servicing_fee varchar(5000), servicing_role varchar(5000) 
, servicer_id varchar(5000), servicer_id_descriptor varchar(5000) 
, dss_record_source varchar(255), dss_load_datetime datetime2);