CREATE TABLE STAGE.stage_feed_master (
  hk_h_feed_master binary(20) NOT NULL, feedid int, feed_name varchar(100) 
, clientid int, asset_class_id int, feed_level varchar(50) 
, feed_details varchar(50), stage_table varchar(100), target_table varchar(200) 
, control_file_name varchar(100), checksum_format varchar(15) 
, feed_location varchar(500), feed_issuer_contact varchar(500) 
, data_validation_required bit, onboarding_required bit, active bit 
, update_date datetime, dss_change_hash_stage_feed_master binary(20) NOT NULL 
, dss_record_source varchar(255), dss_load_datetime datetime2 
, dss_create_time datetime2);