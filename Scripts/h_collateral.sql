CREATE TABLE DV.h_collateral (
  hk_h_collateral binary(20) NOT NULL, dss_record_source varchar(255) 
, dss_load_datetime datetime2, dss_create_time datetime2 
, clientid varchar(100) NOT NULL, transaction_id varchar(100) 
, master_trust_name_or_spv varchar(100) 
, series_trust_name_or_series varchar(100), loan_id varchar(100) 
, collateral_id varchar(100));
CREATE UNIQUE NONCLUSTERED INDEX h_collateral_idx_0 ON DV.h_collateral (hk_h_collateral) WITH (SORT_IN_TEMPDB = OFF);
CREATE UNIQUE NONCLUSTERED INDEX h_collateral_idx_A ON DV.h_collateral (transaction_id,series_trust_name_or_series,clientid,collateral_id,master_trust_name_or_spv,loan_id) WITH (SORT_IN_TEMPDB = OFF);
ALTER TABLE DV.h_collateral ADD CONSTRAINT h_collateral_f354ae6e_idx_PK PRIMARY KEY NONCLUSTERED (hk_h_collateral) WITH (SORT_IN_TEMPDB = OFF);
CREATE UNIQUE NONCLUSTERED INDEX h_collateral_f354ae6e_idx_A ON DV.h_collateral (clientid,transaction_id,master_trust_name_or_spv,series_trust_name_or_series,loan_id,collateral_id) WITH (SORT_IN_TEMPDB = OFF);
