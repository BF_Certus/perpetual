truncate table LOAD.load_feed_status_log;
INSERT INTO LOAD.load_feed_status_log 
(feedid, feed_name, feed_status, feedprocess_starttime, feedprocess_endtime, isrunning, active, dss_record_source, dss_load_datetime)
SELECT
Feed_Status_Log.FeedID,Feed_Status_Log.Feed_Name,Feed_Status_Log.Feed_Status,Feed_Status_Log.FeedProcess_StartTime,Feed_Status_Log.FeedProcess_EndTime,Feed_Status_Log.IsRunning,Feed_Status_Log.Active,'',''
FROM INGEST.Feed_Status_Log Feed_Status_Log;
UPDATE LOAD.load_feed_status_log SET dss_record_source = '.INGEST.Feed_Status_Log',dss_load_datetime = GetDate() ;