CREATE TABLE DV.bvs_rmbs_v00_statustracking (
  dss_load_datetime datetime2, dss_change_hash binary(20) NOT NULL 
, dss_record_source varchar(255), dss_start_date datetime2, dss_version integer 
, dss_create_time datetime2, hk_l_rmbs_v00 binary(20) 
, record_status varchar(7) NOT NULL);