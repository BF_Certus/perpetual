CREATE TABLE LOAD.load_TRAN_seller (
  clientid varchar(100), feedid varchar(100), reporting_period varchar(100) 
, master_trust_name_or_spv varchar(100) 
, series_trust_name_or_series varchar(100), transaction_id varchar(100) 
, seller_name varchar(5000), seller_count int, seller_position int 
, seller_abn varchar(5000), seller_jurisdiction varchar(5000) 
, seller_id varchar(5000), seller_id_descriptor varchar(5000) 
, dss_record_source varchar(255), dss_load_datetime datetime2);