CREATE TABLE STAGE.stage_TRAN_trigger (
  hk_l_TRAN_trigger binary(20) NOT NULL, hk_h_trigger binary(20) NOT NULL 
, hk_h_client_master binary(20) NOT NULL 
, hk_h_reporting_period binary(20) NOT NULL 
, hk_h_transaction binary(20) NOT NULL, hk_h_feed_master binary(20) NOT NULL 
, clientid varchar(100), reporting_period varchar(100) 
, master_trust_name_or_spv varchar(100) 
, series_trust_name_or_series varchar(100), transaction_id varchar(100) 
, trigger_position int, trigger_name varchar(5000), feedid varchar(100) 
, trigger_count int, trigger_description varchar(5000) 
, trigger_state varchar(5000) 
, dss_change_hash_stage_TRAN_trigger binary(20) NOT NULL 
, dss_load_datetime datetime2, dss_record_source varchar(255) 
, dss_create_time datetime2);