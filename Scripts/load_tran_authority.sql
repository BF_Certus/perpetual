CREATE TABLE LOAD.load_TRAN_authority (
  clientid varchar(100), feedid varchar(100), reporting_period varchar(100) 
, master_trust_name_or_spv varchar(100) 
, series_trust_name_or_series varchar(100), transaction_id varchar(100) 
, authority_holder_name varchar(5000), authority_count int 
, authority_position int, authority_type varchar(5000) 
, authority_holder_abn varchar(5000) 
, authority_holder_jurisdiction varchar(5000), authority_holder_id varchar(5000) 
, authority_holder_id_descriptor varchar(5000), dss_record_source varchar(255) 
, dss_load_datetime datetime2);