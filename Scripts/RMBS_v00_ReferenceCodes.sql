/****** Object:  View [INGEST].[RMBS_v00_ReferenceCodes]    Script Date: 22/06/2018 4:51:23 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create view [INGEST].[RMBS_v00_ReferenceCodes] as 
Select hk_l_rmbs_v00

,COALESCE(PL019.Value, [Scheduled_Payment_Policy]) as Scheduled_Payment_Policy

,COALESCE(PL021.Value, [LMI_Underwriting_Type]) as LMI_Underwriting_Type

,COALESCE(PL023.Value, [Loan_Purpose]) as Loan_Purpose

,COALESCE(PL033.Value, [Loan_Documentation_Type]) as Loan_Documentation_Type
,COALESCE(PL034.Value, [Loan_Type]) as Loan_Type

,COALESCE(PL036.Value, [Payment_Frequency]) as Payment_Frequency
,COALESCE(PL037.Value, [LMI_Provider_Name]) as LMI_Provider_Name


,COALESCE(PL041.Value, [Interest_Rate_Type]) as Interest_Rate_Type



,COALESCE(PL045.Value, [Interest_Rate_Reset_Interval]) as Interest_Rate_Reset_Interval



,COALESCE(PL049.Value, [Account_Status]) as Account_Status


,CASE WHEN [Property_Country_Name] IS NULL THEN NULL WHEN [Property_Country_Name] IS NOT NULL AND ctry.[Country_Code] IS NULL THEN CAST([Property_Country_Name] as varchar(50)) ELSE ctry.[Country_Code] END as Property_Country_Code
,CASE WHEN [Property_Country_Name] IS NULL THEN NULL WHEN [Property_Country_Name] IS NOT NULL AND ctry.[Entity] IS NULL THEN CAST([Property_Country_Name] as varchar(50)) ELSE ctry.[Entity] END as Property_Country_Name


,COALESCE(PL066.Value, [Original_Property_Valuation_Type]) as Original_Property_Valuation_Type
,COALESCE(PL067.Value, [Most_Recent_Property_Valuation_Type]) as Most_Recent_Property_Valuation_Type

,COALESCE(PL069.Value, [Property_Purpose]) as Property_Purpose
,COALESCE(PL070.Value, [Property_Type]) as Property_Type
,COALESCE(PL071.Value, [Lien]) as Lien


,COALESCE(PL076.Value, [Employment_Type]) as Employment_Type

,COALESCE(PL080.Value, [Legal_Entity_Type]) as Legal_Entity_Type

,COALESCE(PL091.Value, [Income_Verification]) as Income_Verification



,COALESCE(PL095.Value, [Arrears_Methodology]) as Arrears_Methodology
,COALESCE(PL096.Value, [Main_Security_Methodology]) as Main_Security_Methodology
,COALESCE(PL097.Value, [Primary_Borrower_Methodology]) as Primary_Borrower_Methodology

,COALESCE(PL105.Value, [Savings_Verification]) as Savings_Verification

from dv.s_rmbs_v00 ld
LEFT JOIN [mbs_source].[CountryCodes] ctry ON ctry.[Country_Code] = LD.Property_Country_Name
-- For testing
--WHERE ld.loan_id = 055600157738655
--and ld.data_issuer_id = 1060

LEFT JOIN [mbs_Source].[PickList] PL019 ON PL019.[Field_Specification_ID] = 1127 AND PL019.[Code] = ld.[Scheduled_Payment_Policy]
LEFT JOIN [mbs_Source].[PickList] PL021 ON PL021.[Field_Specification_ID] = 1129 AND PL021.[Code] = ld.[LMI_Underwriting_Type]
LEFT JOIN [mbs_Source].[PickList] PL023 ON PL023.[Field_Specification_ID] = 1131 AND PL023.[Code] = ld.[Loan_Purpose]
LEFT JOIN [mbs_Source].[PickList] PL033 ON PL033.[Field_Specification_ID] = 1141 AND PL033.[Code] = ld.[Loan_Documentation_Type]
LEFT JOIN [mbs_Source].[PickList] PL034 ON PL034.[Field_Specification_ID] = 1142 AND PL034.[Code] = ld.[Loan_Type]
LEFT JOIN [mbs_Source].[PickList] PL036 ON PL036.[Field_Specification_ID] = 1144 AND PL036.[Code] = ld.[Payment_Frequency]
LEFT JOIN [mbs_Source].[PickList] PL037 ON PL037.[Field_Specification_ID] = 1145 AND PL037.[Code] = ld.[LMI_Provider_Name]
LEFT JOIN [mbs_Source].[PickList] PL041 ON PL041.[Field_Specification_ID] = 1149 AND PL041.[Code] = ld.[Interest_Rate_Type]
LEFT JOIN [mbs_Source].[PickList] PL045 ON PL045.[Field_Specification_ID] = 1153 AND PL045.[Code] = ld.[Interest_Rate_Reset_Interval]
LEFT JOIN [mbs_Source].[PickList] PL049 ON PL049.[Field_Specification_ID] = 1157 AND PL049.[Code] = ld.[Account_Status]
LEFT JOIN [mbs_Source].[PickList] PL066 ON PL066.[Field_Specification_ID] = 1174 AND PL066.[Code] = ld.[Original_Property_Valuation_Type]
LEFT JOIN [mbs_Source].[PickList] PL067 ON PL067.[Field_Specification_ID] = 1175 AND PL067.[Code] = ld.[Most_Recent_Property_Valuation_Type]
LEFT JOIN [mbs_Source].[PickList] PL069 ON PL069.[Field_Specification_ID] = 1177 AND PL069.[Code] = ld.[Property_Purpose]
LEFT JOIN [mbs_Source].[PickList] PL070 ON PL070.[Field_Specification_ID] = 1178 AND PL070.[Code] = ld.[Property_Type]
LEFT JOIN [mbs_Source].[PickList] PL071 ON PL071.[Field_Specification_ID] = 1179 AND PL071.[Code] = ld.[Lien]
LEFT JOIN [mbs_Source].[PickList] PL073 ON PL073.[Field_Specification_ID] = 1181 AND PL073.[Code] = ld.[First_Home_Buyer_Flag]
LEFT JOIN [mbs_Source].[PickList] PL076 ON PL076.[Field_Specification_ID] = 1184 AND PL076.[Code] = ld.[Employment_Type]
LEFT JOIN [mbs_Source].[PickList] PL080 ON PL080.[Field_Specification_ID] = 1188 AND PL080.[Code] = ld.[Legal_Entity_Type]
LEFT JOIN [mbs_Source].[PickList] PL091 ON PL091.[Field_Specification_ID] = 1199 AND PL091.[Code] = ld.[Income_Verification]
LEFT JOIN [mbs_Source].[PickList] PL095 ON PL095.[Field_Specification_ID] = 1203 AND PL095.[Code] = ld.[Arrears_Methodology]
LEFT JOIN [mbs_Source].[PickList] PL096 ON PL096.[Field_Specification_ID] = 1204 AND PL096.[Code] = ld.[Main_Security_Methodology]
LEFT JOIN [mbs_Source].[PickList] PL097 ON PL097.[Field_Specification_ID] = 1205 AND PL097.[Code] = ld.[Primary_Borrower_Methodology]
LEFT JOIN [mbs_Source].[PickList] PL105 ON PL105.[Field_Specification_ID] = 1213 AND PL105.[Code] = ld.[Savings_Verification]
		where ld.dss_load_datetime = (Select max(dss_load_datetime) from dv.s_rmbs_v00 z where z.hk_l_rmbs_v00 = ld.hk_l_rmbs_v00)

GO


