CREATE TABLE LOAD.load_SECU_exchange_name (
  clientid int, reporting_period nvarchar(100), transaction_id nvarchar(255) 
, master_trust_name_or_spv nvarchar(255) 
, series_trust_name_or_series nvarchar(255), tranche_name nvarchar(255) 
, exchange_name nvarchar(255), report_date nvarchar(255) 
, dss_record_source varchar(255), dss_load_datetime datetime2);