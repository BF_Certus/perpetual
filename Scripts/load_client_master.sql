CREATE TABLE LOAD.load_client_master (
  clientid int, client_name varchar(100), service_level int 
, analytics_subscription varchar(100), active bit, update_date datetime 
, dss_record_source varchar(255), dss_load_datetime datetime2);