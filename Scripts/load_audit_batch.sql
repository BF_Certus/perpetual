
IF OBJECT_ID ('DV.load_audit_batch', 'U') IS NOT NULL  
   DROP TABLE DV.load_audit_batch;  
go  

create table DV.load_audit_batch (
	Batch_Id int not null identity(1,1),
	Batch_Name varchar(100) not null,
	[Started] datetime2 not null,
	[Finished] datetime2 null,
	[Message] varchar(4000) null,
	[Status] varchar(256) null,
	constraint audit_update_batch_PK primary key (Batch_Id)
)

go

