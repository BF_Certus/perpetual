CREATE TABLE DV.bvs_TRAN_arranger (
  hk_l_TRAN_arranger binary(20) NOT NULL, dss_load_datetime datetime 
, dss_change_hash binary(20) NOT NULL, dss_record_source varchar(255) 
, dss_start_date datetime, dss_version int, dss_create_time datetime 
, arranger_count int, arranger_position int, arranger_abn varchar(5000) 
, arranger_jurisdiction varchar(5000), arranger_id varchar(5000) 
, arranger_id_descriptor varchar(5000), arranger_fee_currency varchar(5000) 
, arranger_fee varchar(5000));