CREATE TABLE STAGE.stage_bvrmbs_v00_referencecodes (
  hk_l_rmbs_v00 binary(20), scheduled_payment_policy nvarchar(4000) 
, lmi_underwriting_type nvarchar(4000), loan_purpose nvarchar(4000) 
, loan_documentation_type nvarchar(4000), loan_type nvarchar(4000) 
, payment_frequency nvarchar(4000), lmi_provider_name nvarchar(4000) 
, interest_rate_type nvarchar(4000), interest_rate_reset_interval nvarchar(4000) 
, account_status nvarchar(4000), property_country_code nvarchar(100) 
, property_country_name nvarchar(100) 
, original_property_valuation_type nvarchar(4000) 
, most_recent_property_valuation_type nvarchar(4000) 
, property_purpose nvarchar(4000), property_type nvarchar(4000) 
, lien nvarchar(4000), employment_type nvarchar(4000) 
, legal_entity_type nvarchar(4000), income_verification nvarchar(4000) 
, arrears_methodology nvarchar(4000), main_security_methodology nvarchar(4000) 
, primary_borrower_methodology nvarchar(4000) 
, savings_verification nvarchar(4000) 
, dss_change_hash_stage_bvrmbs_v00_referencecodes binary(20) NOT NULL 
, dss_load_datetime datetime2, dss_record_source varchar(255) 
, dss_create_time datetime2);