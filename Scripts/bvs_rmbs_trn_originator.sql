CREATE TABLE DV.bvs_rmbs_trn_originator (
  hk_l_rmbs_trn_originator binary(20) NOT NULL, dss_load_datetime datetime 
, dss_change_hash binary(20) NOT NULL, dss_record_source varchar(255) 
, dss_start_date datetime, dss_version int, dss_create_time datetime 
, originator_count int, originator_position int, originator_abn varchar(5000) 
, originator_jurisdiction varchar(5000), originator_id varchar(5000) 
, originator_id_descriptor varchar(5000) 
, loans_originated_settlement_date varchar(5000), loans_originated varchar(5000));