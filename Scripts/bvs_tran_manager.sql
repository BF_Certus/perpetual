CREATE TABLE DV.bvs_TRAN_manager (
  hk_l_TRAN_manager binary(20) NOT NULL, dss_load_datetime datetime 
, dss_change_hash binary(20) NOT NULL, dss_record_source varchar(255) 
, dss_start_date datetime, dss_version int, dss_create_time datetime 
, manager_count int, manager_position int, manager_abn varchar(5000) 
, manager_jurisdiction varchar(5000), manager_id varchar(5000) 
, manager_id_descriptor varchar(5000), manager_fee_currency varchar(5000) 
, manager_fee varchar(5000));