CREATE TABLE STAGE.stage_TRAN_responsible_party (
  hk_l_TRAN_responsible_party binary(20) NOT NULL 
, hk_h_reporting_period binary(20) NOT NULL 
, hk_h_transaction binary(20) NOT NULL, hk_h_client_master binary(20) NOT NULL 
, hk_h_party binary(20) NOT NULL, hk_h_feed_master binary(20) NOT NULL 
, clientid varchar(100), feedid varchar(100), reporting_period varchar(100) 
, master_trust_name_or_spv varchar(100) 
, series_trust_name_or_series varchar(100), transaction_id varchar(100) 
, responsible_party_name varchar(5000), responsible_party_count int 
, responsible_party_position int, responsible_party_abn varchar(5000) 
, responsible_party_jurisdiction varchar(5000) 
, responsible_party_id varchar(5000) 
, responsible_party_id_descriptor varchar(5000) 
, responsible_party_fee_currency varchar(5000) 
, responsible_party_fee varchar(5000) 
, dss_change_hash_stage_bvTRAN_responsible_party binary(20) NOT NULL 
, dss_record_source varchar(255), dss_load_datetime datetime2 
, dss_create_time datetime2);