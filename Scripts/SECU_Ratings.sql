/****** Object:  View [INGEST].[SECU_Ratings]    Script Date: 2/07/2018 3:23:07 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--Drop view [INGEST].[SECU_Ratings]
Create View [INGEST].[SECU_Ratings] as 
 
  WITH Nbrs ( Number ) AS (
    SELECT 1 UNION ALL
    SELECT 1 + Number FROM Nbrs WHERE Number < 99
)
SELECT 
T.ClientID,
   T.Reporting_Period,

T.Report_Date,

T.Transaction_Id,
T.Master_Trust_Name_Or_Spv,
T.Series_Trust_Name_Or_Series,
T.Tranche_Name,
Case when N.Number = 0 then  Name   when dbo.FN_CERTUS_INSTR(  Name ,';',1,N.Number) = 0  then right(  Name , Len(  Name )-dbo.FN_CERTUS_INSTR(  Name ,';',1,N.Number-1)) else SubString(  Name , dbo.FN_CERTUS_INSTR(  Name ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR(  Name ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Name ,';',1,N.Number-1)-1) end as   Name,  
Case when N.Number = 0 then  Original_Rating   when dbo.FN_CERTUS_INSTR(  Original_Rating ,';',1,N.Number) = 0  then right(  Original_Rating , Len(  Original_Rating )-dbo.FN_CERTUS_INSTR(  Original_Rating ,';',1,N.Number-1)) else SubString(  Original_Rating , dbo.FN_CERTUS_INSTR(  Original_Rating ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR(  Original_Rating ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Original_Rating ,';',1,N.Number-1)-1)end as   Original_Rating, 
Case when N.Number = 0 then  Current_Rating   when dbo.FN_CERTUS_INSTR(  Current_Rating ,';',1,N.Number) = 0  then right(  Current_Rating , Len(  Current_Rating )-dbo.FN_CERTUS_INSTR(  Current_Rating ,';',1,N.Number-1)) else SubString(  Current_Rating , dbo.FN_CERTUS_INSTR(  Current_Rating ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR(  Current_Rating ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Current_Rating ,';',1,N.Number-1)-1)end as   Current_Rating, 
Case when N.Number = 0 then  ABN   when dbo.FN_CERTUS_INSTR(  ABN ,';',1,N.Number) = 0  then right(  ABN , Len(  ABN )-dbo.FN_CERTUS_INSTR(  ABN ,';',1,N.Number-1)) else SubString(  ABN , dbo.FN_CERTUS_INSTR(  ABN ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR(  ABN ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  ABN ,';',1,N.Number-1)-1)end as   ABN, 
Case when N.Number = 0 then  Jurisdiction   when dbo.FN_CERTUS_INSTR(  Jurisdiction ,';',1,N.Number) = 0  then right(  Jurisdiction , Len(  Jurisdiction )-dbo.FN_CERTUS_INSTR(  Jurisdiction ,';',1,N.Number-1)) else SubString(  Jurisdiction , dbo.FN_CERTUS_INSTR(  Jurisdiction ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR(  Jurisdiction ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Jurisdiction ,';',1,N.Number-1)-1)end as   Jurisdiction, 
Case when N.Number = 0 then  Id   when dbo.FN_CERTUS_INSTR(  Id ,';',1,N.Number) = 0  then right(  Id , Len(  Id )-dbo.FN_CERTUS_INSTR(  Id ,';',1,N.Number-1)) else SubString(  Id , dbo.FN_CERTUS_INSTR(  Id ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR(  Id ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Id ,';',1,N.Number-1)-1)end as   Id, 
Case when N.Number = 0 then  Id_Descriptor   when dbo.FN_CERTUS_INSTR(  Id_Descriptor ,';',1,N.Number) = 0  then right(  Id_Descriptor , Len(  Id_Descriptor )-dbo.FN_CERTUS_INSTR(  Id_Descriptor ,';',1,N.Number-1)) else SubString(  Id_Descriptor , dbo.FN_CERTUS_INSTR(  Id_Descriptor ,';',1,N.Number-1)+1, dbo.FN_CERTUS_INSTR(  Id_Descriptor ,';',1,N.Number)- dbo.FN_CERTUS_INSTR(  Id_Descriptor ,';',1,N.Number-1)-1)end as   Id_Descriptor
,Feed_Name
FROM
   (  Select dbo.FN_CERTUS_DELIM_COUNT(
		Name,';') as DelimCount, 
		Name, Reporting_Period,
Report_Date,
ClientID,
Transaction_Id,
Master_Trust_Name_Or_Spv,
Series_Trust_Name_Or_Series,
Tranche_Name,
Original_Rating,
Current_Rating,
ABN,
Jurisdiction,
ID,
ID_Descriptor,
Feed_Name
FROM INGEST.SECU_v00) T
   JOIN
   Nbrs N ON N.Number <= T.DelimCount or (T.DelimCount = 0 and N.Number =1)
GO


