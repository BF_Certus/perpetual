/****** Object:  View [DV].[Ref_PITs]    Script Date: 25/06/2018 12:07:48 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create View [DV].[Ref_PITs] as 
Select 'Current Date' as PIT_Name, GetDate() as PIT_Date
Union Select 'Last Month' as PIT_Name, DateAdd(month, -1, GetDate()) as PIT_Date
Union Select 'Last Day of Month, Last 6 months' as PIT_Name, DateAdd(Day, -1, Convert(date, Cast(DatePart(yyyy,GetDate()) * 10000 + Datepart(mm, GetDate()) * 100 + 1 as varchar),112)) as PIT_Date -- last month
Union Select 'Last Day of Month, Last 6 months' as PIT_Name, DateAdd(day, -1, DateAdd(month, -1, Convert(date, Cast(DatePart(yyyy,GetDate()) * 10000 + Datepart(mm, GetDate()) * 100 + 1 as varchar),112))) as PIT_Date -- 2 months ago
Union Select 'Last Day of Month, Last 6 months' as PIT_Name, DateAdd(day, -1, DateAdd(month, -2, Convert(date, Cast(DatePart(yyyy,GetDate()) * 10000 + Datepart(mm, GetDate()) * 100 + 1 as varchar),112))) as PIT_Date -- 3 months ago
Union Select 'Last Day of Month, Last 6 months' as PIT_Name, DateAdd(day, -1, DateAdd(month, -3, Convert(date, Cast(DatePart(yyyy,GetDate()) * 10000 + Datepart(mm, GetDate()) * 100 + 1 as varchar),112))) as PIT_Date -- 4 months ago
Union Select 'Last Day of Month, Last 6 months' as PIT_Name, DateAdd(day, -1, DateAdd(month, -4, Convert(date, Cast(DatePart(yyyy,GetDate()) * 10000 + Datepart(mm, GetDate()) * 100 + 1 as varchar),112))) as PIT_Date -- 5 months ago
Union Select 'Last Day of Month, Last 6 months' as PIT_Name, DateAdd(day, -1, DateAdd(month, -5, Convert(date, Cast(DatePart(yyyy,GetDate()) * 10000 + Datepart(mm, GetDate()) * 100 + 1 as varchar),112))) as PIT_Date -- 6 months ago
Union Select 'Last 5 days' as PIT_Name, Convert(Date,convert(varchar,GetDate(),112),112) as PIT_Date
Union Select 'Last 5 days' as PIT_Name, Convert(Date,convert(varchar,dateadd(day,-1, GetDate()),112),112) as PIT_Date
Union Select 'Last 5 days' as PIT_Name, Convert(Date,convert(varchar,dateadd(day,-2, GetDate()),112),112) as PIT_Date
Union Select 'Last 5 days' as PIT_Name, Convert(Date,convert(varchar,dateadd(day,-3, GetDate()),112),112) as PIT_Date
Union Select 'Last 5 days' as PIT_Name, Convert(Date,convert(varchar,dateadd(day,-4, GetDate()),112),112) as PIT_Date
GO


