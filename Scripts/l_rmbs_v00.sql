CREATE TABLE DV.l_rmbs_v00 (
  hk_l_rmbs_v00 binary(20) NOT NULL, hk_h_borrower binary(20) NOT NULL 
, hk_h_loan binary(20) NOT NULL, hk_h_client_master binary(20) NOT NULL 
, hk_h_party_originator_name binary(20) NOT NULL 
, hk_h_party_servicer_name binary(20) NOT NULL 
, hk_h_party_seller_name binary(20) NOT NULL 
, hk_h_reporting_period binary(20) NOT NULL 
, hk_h_transaction binary(20) NOT NULL, dss_record_source varchar(255) 
, dss_load_datetime datetime2, dss_create_time datetime2);
CREATE UNIQUE NONCLUSTERED INDEX l_rmbs_v00_idx_0 ON DV.l_rmbs_v00 (hk_l_rmbs_v00) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_rmbs_v00_idx_1 ON DV.l_rmbs_v00 (hk_h_borrower) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_rmbs_v00_idx_2 ON DV.l_rmbs_v00 (hk_h_loan) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_rmbs_v00_idx_3 ON DV.l_rmbs_v00 (hk_h_client_master) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_rmbs_v00_8a6a8c76_idx_1 ON DV.l_rmbs_v00 (hk_h_borrower) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_rmbs_v00_8a6a8c76_idx_2 ON DV.l_rmbs_v00 (hk_h_loan) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_rmbs_v00_8a6a8c76_idx_3 ON DV.l_rmbs_v00 (hk_h_client_master) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_rmbs_v00_8a6a8c76_idx_4 ON DV.l_rmbs_v00 (hk_h_party_originator_name) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_rmbs_v00_8a6a8c76_idx_5 ON DV.l_rmbs_v00 (hk_h_party_servicer_name) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_rmbs_v00_8a6a8c76_idx_6 ON DV.l_rmbs_v00 (hk_h_party_seller_name) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_rmbs_v00_8a6a8c76_idx_7 ON DV.l_rmbs_v00 (hk_h_reporting_period) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_rmbs_v00_8a6a8c76_idx_8 ON DV.l_rmbs_v00 (hk_h_transaction) WITH (SORT_IN_TEMPDB = OFF);
ALTER TABLE DV.l_rmbs_v00 ADD CONSTRAINT l_rmbs_v00_8a6a8c76_idx_PK PRIMARY KEY NONCLUSTERED (hk_l_rmbs_v00) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_rmbs_v00_idx_4 ON DV.l_rmbs_v00 (hk_h_party_servicer_name) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_rmbs_v00_idx_5 ON DV.l_rmbs_v00 (hk_h_party_originator_name) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_rmbs_v00_idx_6 ON DV.l_rmbs_v00 (hk_h_party_seller_name) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_rmbs_v00_idx_7 ON DV.l_rmbs_v00 (hk_h_reporting_period) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_rmbs_v00_idx_8 ON DV.l_rmbs_v00 (hk_h_transaction) WITH (SORT_IN_TEMPDB = OFF);
CREATE UNIQUE NONCLUSTERED INDEX l_rmbs_v00_idx_A ON DV.l_rmbs_v00 (hk_h_borrower,hk_h_loan,hk_h_client_master,hk_h_party_servicer_name,hk_h_party_originator_name,hk_h_party_seller_name,hk_h_reporting_period,hk_h_transaction) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_rmbs_v00_idx_9 ON DV.l_rmbs_v00 (hk_h_transaction) WITH (SORT_IN_TEMPDB = OFF);
