CREATE TABLE DV.l_SECU_exchange_name (
  hk_l_SECU_exchange_name binary(20) NOT NULL 
, hk_h_client_master binary(20) NOT NULL, hk_h_exchange binary(20) NOT NULL 
, hk_h_reporting_period binary(20) NOT NULL, hk_h_security binary(20) NOT NULL 
, hk_h_transaction binary(20) NOT NULL, dss_record_source varchar(255) 
, dss_load_datetime datetime2, dss_create_time datetime2);
CREATE UNIQUE NONCLUSTERED INDEX l_SECU_exchange_name_idx_0 ON DV.l_SECU_exchange_name (hk_l_SECU_exchange_name) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_SECU_exchange_name_idx_1 ON DV.l_SECU_exchange_name (hk_h_client_master) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_SECU_exchange_name_idx_2 ON DV.l_SECU_exchange_name (hk_h_exchange) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_SECU_exchange_name_idx_3 ON DV.l_SECU_exchange_name (hk_h_reporting_period) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_SECU_exchange_name_idx_4 ON DV.l_SECU_exchange_name (hk_h_security) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_SECU_exchange_name_idx_5 ON DV.l_SECU_exchange_name (hk_h_transaction) WITH (SORT_IN_TEMPDB = OFF);
CREATE UNIQUE NONCLUSTERED INDEX l_SECU_exchange_name_idx_A ON DV.l_SECU_exchange_name (hk_h_client_master,hk_h_exchange,hk_h_reporting_period,hk_h_security,hk_h_transaction) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_SECU_ex_3db8db39_idx_1 ON DV.l_SECU_exchange_name (hk_h_client_master) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_SECU_ex_3db8db39_idx_2 ON DV.l_SECU_exchange_name (hk_h_exchange) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_SECU_ex_3db8db39_idx_3 ON DV.l_SECU_exchange_name (hk_h_reporting_period) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_SECU_ex_3db8db39_idx_4 ON DV.l_SECU_exchange_name (hk_h_security) WITH (SORT_IN_TEMPDB = OFF);
CREATE NONCLUSTERED INDEX l_SECU_ex_3db8db39_idx_5 ON DV.l_SECU_exchange_name (hk_h_transaction) WITH (SORT_IN_TEMPDB = OFF);
ALTER TABLE DV.l_SECU_exchange_name ADD CONSTRAINT l_SECU_ex_3db8db39_idx_PK PRIMARY KEY NONCLUSTERED (hk_l_SECU_exchange_name) WITH (SORT_IN_TEMPDB = OFF);
