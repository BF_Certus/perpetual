CREATE TABLE DV.h_transaction (
  hk_h_transaction binary(20) NOT NULL, dss_record_source varchar(255) 
, dss_load_datetime datetime2, dss_create_time datetime2 
, clientid varchar(100) NOT NULL, transaction_id varchar(100) 
, master_trust_name_or_spv varchar(100) 
, series_trust_name_or_series varchar(100));
CREATE UNIQUE NONCLUSTERED INDEX h_transaction_idx_0 ON DV.h_transaction (hk_h_transaction) WITH (SORT_IN_TEMPDB = OFF);
CREATE UNIQUE NONCLUSTERED INDEX h_transaction_idx_A ON DV.h_transaction (transaction_id,series_trust_name_or_series,clientid,master_trust_name_or_spv) WITH (SORT_IN_TEMPDB = OFF);
