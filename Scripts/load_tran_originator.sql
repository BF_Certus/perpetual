CREATE TABLE LOAD.load_TRAN_originator (
  clientid varchar(100), feedid varchar(100), reporting_period varchar(100) 
, master_trust_name_or_spv varchar(100) 
, series_trust_name_or_series varchar(100), transaction_id varchar(100) 
, originator_name varchar(5000), originator_count int, originator_position int 
, originator_abn varchar(5000), originator_jurisdiction varchar(5000) 
, originator_id varchar(5000), originator_id_descriptor varchar(5000) 
, loans_originated_settlement_date varchar(5000), loans_originated varchar(5000) 
, dss_record_source varchar(255), dss_load_datetime datetime2);