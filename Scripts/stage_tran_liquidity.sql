CREATE TABLE STAGE.stage_TRAN_liquidity (
  hk_l_TRAN_liquidity binary(20) NOT NULL 
, hk_h_transaction binary(20) NOT NULL, hk_h_client_master binary(20) NOT NULL 
, hk_h_party binary(20) NOT NULL, hk_h_feed_master binary(20) NOT NULL 
, hk_h_reporting_period binary(20) NOT NULL, clientid varchar(100) 
, feedid varchar(100), reporting_period varchar(100) 
, master_trust_name_or_spv varchar(100) 
, series_trust_name_or_series varchar(100), transaction_id varchar(100) 
, liquidity_provider_name varchar(5000), liquidity_count int 
, liquidity_position int, liquidity_provider_abn varchar(5000) 
, initial_amount_available varchar(5000) 
, beginning_amount_available_liquidity varchar(5000) 
, ending_amount_available_liquidity varchar(5000) 
, liquidity_provider_jurisdiction varchar(5000) 
, liquidity_provider_id varchar(5000) 
, liquidity_provider_id_descriptor varchar(5000) 
, liquidity_facility_name varchar(5000), liquidity_facility_type varchar(5000) 
, reimbursements varchar(5000), draws_liquidity varchar(5000) 
, reduction_liquidity varchar(5000) 
, beginning_amount_drawn_liquidity varchar(5000) 
, ending_amount_drawn_liquidity varchar(5000), facility_currency varchar(5000) 
, facility_fee varchar(5000) 
, dss_change_hash_stage_bvTRAN_liquidity binary(20) NOT NULL 
, dss_record_source varchar(255), dss_load_datetime datetime2 
, dss_create_time datetime2);