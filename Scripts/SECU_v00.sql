/****** Object:  View [INGEST].[SECU_v00]    Script Date: 2/07/2018 2:02:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--Drop view INGEST.SECU_v00
--Drop view [INGEST].[SECU_Convert]
Create view [INGEST].[SECU_v00] as 
Select ClientID, 
S.Feed_Name,

 Convert([nvarchar](100),PS000) as Reporting_Period
,Convert([nvarchar](255),PS001) as Report_Date
,Convert([nvarchar](255),PS002) as Transaction_Id
,Convert([nvarchar](255),PS003) as Master_Trust_Name_Or_Spv
,Convert([nvarchar](255),PS004) as Series_Trust_Name_Or_Series
,Convert([nvarchar](255),PS005) as Tranche_Name
,Convert([nvarchar](255),PS006) as Isin
,Convert([nvarchar](255),PS007) as Austraclear_Series_Id
,Convert([nvarchar](255),PS008) as Clearing_System_Name
,Convert([nvarchar](255),PS009) as Issue_Date
,Convert([nvarchar](255),PS010) as Accrual_Begin_Date
,Convert([nvarchar](255),PS011) as Accrual_End_Date
,Convert([nvarchar](255),PS012) as Accrual_Period
,Convert([nvarchar](255),PS013) as Tranche_Currency
,Convert([nvarchar](255),PS014) as Exchange_Name
,Convert([nvarchar](255),PS015) as Name
,Convert([nvarchar](255),PS016) as Original_Rating
,Convert([nvarchar](255),PS017) as Current_Rating
,Convert([nvarchar](255),PS018) as Original_Subordination
,Convert([nvarchar](255),PS019) as Current_Subordination
,Convert([nvarchar](255),PS020) as Current_Principal_Face_Amount
,Convert([nvarchar](255),PS021) as Offer_Type
,Convert([nvarchar](255),PS022) as Coupon_Payment_Reference
,Convert([nvarchar](255),PS023) as Distribution_Date
,Convert([nvarchar](255),PS024) as Record_Date
,Convert([nvarchar](255),PS025) as Determination_Date
,Convert([nvarchar](255),PS026) as Beginning_Stated_Factor
,Convert([nvarchar](255),PS027) as Ending_Stated_Factor
,Convert([nvarchar](255),PS028) as Original_Principal_Face_Amount
,Convert([nvarchar](255),PS029) as Beginning_Invested_Amount
,Convert([nvarchar](255),PS030) as Ending_Invested_Amount
,Convert([nvarchar](255),PS031) as Beginning_Stated_Amount
,Convert([nvarchar](255),PS032) as Ending_Stated_Amount
,Convert([nvarchar](255),PS033) as Beginning_Invested_Factor
,Convert([nvarchar](255),PS034) as Ending_Invested_Factor
,Convert([nvarchar](255),PS035) as Coupon_Reference_Index
,Convert([nvarchar](255),PS036) as Coupon_Reference_Index_Value
,Convert([nvarchar](255),PS037) as Coupon_Margin
,Convert([nvarchar](255),PS038) as Step_Up_Margin
,Convert([nvarchar](255),PS039) as Step_Up_Date
,Convert([nvarchar](255),PS040) as Scheduled_Coupon
,Convert([nvarchar](255),PS041) as Scheduled_Coupon_Factor
,Convert([nvarchar](255),PS042) as Current_Coupon_Rate
,Convert([nvarchar](255),PS043) as Coupon_Distribution
,Convert([nvarchar](255),PS044) as Coupon_Distribution_Factor
,Convert([nvarchar](255),PS045) as Scheduled_Principal
,Convert([nvarchar](255),PS046) as Scheduled_Principal_Factor
,Convert([nvarchar](255),PS047) as Principal_Distribution
,Convert([nvarchar](255),PS048) as Principal_Distribution_Factor
,Convert([nvarchar](255),PS049) as Coupon_Shortfall_Factor
,Convert([nvarchar](255),PS050) as Ending_Cumulative_Coupon_Shortfall_Factor
,Convert([nvarchar](255),PS051) as Coupon_Shortfall
,Convert([nvarchar](255),PS052) as Ending_Cumulative_Coupon_Shortfall
,Convert([nvarchar](255),PS053) as Principal_Shortfall
,Convert([nvarchar](255),PS054) as Ending_Cumulative_Principal_Shortfall
,Convert([nvarchar](255),PS055) as Legal_Maturity_Date
,Convert([nvarchar](255),PS056) as Original_Estimated_Weighted_Average_Life
,Convert([nvarchar](255),PS057) as Current_Weighted_Average_Life
,Convert([nvarchar](255),PS058) as Expected_Final_Maturity_Date
,Convert([nvarchar](255),PS059) as Redemption_Price
,Convert([nvarchar](255),PS060) as ABN
,Convert([nvarchar](255),PS061) as Jurisdiction
,Convert([nvarchar](255),PS062) as Id
,Convert([nvarchar](255),PS063) as Id_Descriptor
,Convert([nvarchar](255),PS064) as Governing_Law
,Convert([nvarchar](255),PS065) as Soft_Bullet_Date
,Convert([nvarchar](255),PS066) as Coupon_Basis
,Convert([nvarchar](255),PS067) as Step_Up_Rate
,Convert([nvarchar](255),PS068) as Beginning_Cumulative_Chargeoffs
,Convert([nvarchar](255),PS069) as Beginning_Cumulative_Coupon_Shortfall
,Convert([nvarchar](255),PS070) as Beginning_Cumulative_Principal_Shortfall
,Convert([nvarchar](255),PS071) as Coupon_Shortfall_Makeup
,Convert([nvarchar](255),PS072) as Principal_Shortfall_Makeup
,Convert([nvarchar](255),PS073) as Allocated_Chargeoffs
,Convert([nvarchar](255),PS074) as Chargeoff_Restorations
,Convert([nvarchar](255),PS075) as Ending_Cumulative_Chargeoffs
,Convert([nvarchar](255),PS076) as Beginning_Cumulative_Chargeoff_Factor
,Convert([nvarchar](255),PS077) as Beginning_Cumulative_Coupon_Shortfall_Factor
,Convert([nvarchar](255),PS078) as Beginning_Cumulative_Principal_Shortfall_Factor
,Convert([nvarchar](255),PS079) as Coupon_Shortfall_Makeup_Factor
,Convert([nvarchar](255),PS080) as Principal_Shortfall_Makeup_Factor
,Convert([nvarchar](255),PS081) as Principal_Shortfall_Factor
,Convert([nvarchar](255),PS082) as Allocated_Chargeoff_Factor
,Convert([nvarchar](255),PS083) as Chargeoff_Restoration_Factor
,Convert([nvarchar](255),PS084) as Ending_Cumulative_Chargeoff_Factor
,Convert([nvarchar](255),PS085) as Ending_Cumulative_Principal_Shortfall_Factor
,Convert([nvarchar](255),PS086) as Coupon_Rate 

FROM INGEST.Ext_Stage_SECU  S inner join  
  Ingest.Feed_Master FM on S.FeedID = FM.FeedID;
	
GO


